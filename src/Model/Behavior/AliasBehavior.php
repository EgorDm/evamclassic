<?php
namespace App\Model\Behavior;

use App\Utils\DataUtils;
use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Utility\Text;

/**
 * Alias behavior
 */
class AliasBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        $display_value = $data['title'];
        $data['alias'] = Text::slug($display_value);
        $result = $this->_table->find()->where(['alias' => $data['alias']])->first();
        if(!empty($result)) {
            $data['alias'] .= '-'.rand(1000, 9999);
        }
    }


}
