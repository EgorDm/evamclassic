<?php
namespace App\Model\Table;

use App\Model\Entity\VatCode;
use App\Utils\DataUtils;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VatCodes Model
 *
 */
class VatCodesTable extends ExactConnectedTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('vat_codes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('exact_guid');

        $validator
            ->requirePresence('code_vat', 'create')
            ->notEmpty('code_vat');

        $validator
            ->allowEmpty('description');

        $validator
            ->numeric('percentage')
            ->requirePresence('percentage', 'create')
            ->notEmpty('percentage');

        return $validator;
    }

    public static function ConvertFromExact($item)
    {
        $ret = [
            'exact_guid' => $item['ID'],
            'code_vat' => $item['Code'],
            'description' => $item['Description'],
            'percentage' => $item['Percentage'],
            'created' => DataUtils::ExactDateConvertFrom($item['Created']),
            'modified' => DataUtils::ExactDateConvertFrom($item['Modified'])
        ];
        if (isset($item['id'])) {
            $ret['id'] = $item['id'];
        }
        return $ret;
    }

    public function getExactFields()
    {
        return ['ID', 'Code', 'Created', 'Description', 'Modified', 'Percentage'];
    }

    public function getInsertColumns()
    {
        return ['exact_guid', 'code_vat', 'description', 'percentage', 'created', 'modified'];
    }

    public function getUpdateColumns()
    {
        return ['id', 'exact_guid', 'code_vat', 'description', 'percentage', 'created', 'modified'];
    }
}
