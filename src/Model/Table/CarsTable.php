<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cars Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ImageGroups
 */
class CarsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cars');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('ImageGroups', [
            'foreignKey' => 'image_group_id'
        ]);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Alias');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('reference', 'create')
            ->notEmpty('reference');

        $validator
            ->requirePresence('alias', 'create')
            ->notEmpty('alias');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('category');

        $validator
            ->allowEmpty('model');

        $validator
            ->integer('year')
            ->allowEmpty('year');

        $validator
            ->allowEmpty('color');

        $validator
            ->allowEmpty('condition');

        $validator
            ->numeric('price')
            ->allowEmpty('price');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->allowEmpty('image_group_id');

        return $validator;
    }
}
