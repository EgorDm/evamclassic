<?php
namespace App\Model\Table;

use App\Utils\DataUtils;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ItemGroups Model
 *
 * @property \Cake\ORM\Association\HasMany $Items
 */
class ItemGroupsTable extends ExactConnectedTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('item_groups');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Items', [
            'foreignKey' => 'item_group_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('exact_guid');

        $validator
            ->allowEmpty('code');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('description');

        return $validator;
    }

    public static function ConvertFromExact($item)
    {
        $ret = [
            'exact_guid' => $item['ID'],
            'code' => $item['Code'],
            'title' => $item['Description'],
            'description' => $item['Notes'],
            'created' => DataUtils::ExactDateConvertFrom($item['Created']),
            'modified' => DataUtils::ExactDateConvertFrom($item['Modified'])
        ];
        if (isset($item['id'])) {
            $ret['id'] = $item['id'];
        }
        return $ret;
    }

    public function getExactFields()
    {
        return ['ID', 'Code', 'Description', 'Notes', 'Created', 'Modified'];
    }

    public function getInsertColumns()
    {
        return ['exact_guid', 'code', 'title', 'description', 'created', 'modified'];
    }

    public function getUpdateColumns()
    {
        return ['id', 'exact_guid', 'code', 'title', 'description', 'created', 'modified'];
    }
}
