<?php
namespace App\Model\Table;

use App\Utils\DataUtils;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Customers Model
 *
 * @property \Cake\ORM\Association\HasMany $Addresses
 */
class CustomersTable extends ExactConnectedTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('customers');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Orders', [
            'foreignKey' => 'customer_id'
        ]);

        $this->hasMany('Addresses', [
            'foreignKey' => 'customer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('exact_guid');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        return $validator;
    }

    public function validationUpdatePassword(Validator $validator)
    {
        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

    public static function ConvertFromExact($item)
    {
        if ($item['Email'] == null) $item['Email'] = '';
        $ret = [
            'exact_guid' => $item['ID'],
            'name' => $item['Name'],
            'email' => $item['Email'],
            'password' => 'testz',
            'type' => 11,
            'created' => DataUtils::ExactDateConvertFrom($item['Created']),
            'modified' => DataUtils::ExactDateConvertFrom($item['Modified'])
        ];
        if (isset($item['id'])) {
            $ret['id'] = $item['id'];
        }
        return $ret;
    }

    public static function ConvertToExact($item)
    {
        $ret = [
            'ID' => $item['exact_guid'],
            'Name' => $item['name'],
            'Email' => $item['email'],
            'created' => $item['created'], //TODO convert to exact date
            'modified' => $item['modified']
        ];
        return $ret;
    }

    public function getExactFields()
    {
        return ['ID', 'AddressLine1', 'City', 'Country', 'Created', 'Email', 'Modified', 'Name', 'Phone', 'Postcode', 'IsSales'];
    }

    public function getInsertColumns()
    {
        return ['exact_guid', 'name', 'email', 'password', 'type', 'created', 'modified'];
    }

    public function getUpdateColumns()
    {
        return ['id', 'exact_guid', 'name', 'email', 'password', 'type', 'created', 'modified'];
    }


}
