<?php
namespace App\Model\Table;

use App\Utils\DataUtils;
use Cake\Log\Log;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * ItemPrices Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Items
 */
class ItemPricesTable extends ExactConnectedTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('item_prices');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Items', [
            'foreignKey' => 'item_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('exact_guid', 'create')
            ->notEmpty('exact_guid');

        $validator
            ->allowEmpty('item_guid');

        $validator
            ->allowEmpty('default_unit');

        $validator
            ->allowEmpty('items_per_unit');

        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        $validator
            ->numeric('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->dateTime('start_date')
            ->allowEmpty('start_date');

        $validator
            ->dateTime('end_date')
            ->allowEmpty('end_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['item_id'], 'Items'));
        return $rules;
    }

    public static function ConvertFromExact($item)
    {
        $ret = [
            'exact_guid' => $item['ID'],
            'item_guid' => $item['Item'],
            'default_unit' => $item['DefaultItemUnit'],
            'items_per_unit' => $item['NumberOfItemsPerUnit'],
            'quantity' => $item['Quantity'],
            'price' => $item['Price'],
            'start_date' => ($item['StartDate'] == null) ? null : DataUtils::ExactDateConvertFrom($item['StartDate']),
            'end_date' => ($item['EndDate'] == null) ? null : DataUtils::ExactDateConvertFrom($item['EndDate']),
            'created' => DataUtils::ExactDateConvertFrom($item['Created']),
            'modified' => DataUtils::ExactDateConvertFrom($item['Modified'])
        ];
        if (isset($item['id'])) {
            $ret['id'] = $item['id'];
        }
        if (isset($item['item_id'])) {
            $ret['item_id'] = $item['item_id'];
        }
        return $ret;
    }

    public function getExactFields()
    {
        return ['ID', 'Created', 'Currency', 'DefaultItemUnit', 'EndDate', 'Item',
            'Modified', 'NumberOfItemsPerUnit', 'Price', 'Quantity', 'StartDate'];
    }

    public function getInsertColumns()
    {
        return ['exact_guid', 'item_id', 'item_guid', 'default_unit', 'items_per_unit',
            'quantity', 'price', 'start_date', 'end_date', 'created', 'modified'];
    }

    public function getUpdateColumns()
    {
        return ['id', 'exact_guid', 'item_id', 'item_guid', 'default_unit', 'items_per_unit',
            'quantity', 'price', 'start_date', 'end_date', 'created', 'modified'];
    }
}
