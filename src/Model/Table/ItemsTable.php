<?php
namespace App\Model\Table;

use App\Utils\CacheUtils;
use App\Utils\DataUtils;
use Cake\Log\Log;
use Cake\ORM\RulesChecker;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * Items Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ItemGroups
 * @property \Cake\ORM\Association\BelongsTo $ImageGroups
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\BelongsTo $VatCodes
 * @property \Cake\ORM\Association\HasMany $ItemPrices
 */
class ItemsTable extends ExactConnectedTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('items');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ItemGroups', [
            'foreignKey' => 'item_group_id'
        ]);
        $this->belongsToMany('Categories', [
            'through' => 'ItemCategoryAssociations'
        ]);
        $this->belongsTo('ImageGroups', [
            'foreignKey' => 'image_group_id'
        ]);
        $this->belongsTo('VatCodes', [
            'foreignKey' => 'vat_code',
            'bindingKey' => 'code_vat',
            'propertyName' => 'vat_code_model'
        ]);
        $this->hasMany('ItemPrices', [
            'foreignKey' => 'item_id'
        ]);
        $this->belongsToMany('Orders', [
            'through' => 'OrderItemAssociations'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('exact_guid', 'create')
            ->notEmpty('exact_guid');

        $validator
            ->allowEmpty('exact_group_guid');

        $validator
            ->allowEmpty('priority');

        $validator
            ->allowEmpty('code');

        $validator
            ->allowEmpty('warehouse_guid');

        $validator
            ->requirePresence('alias', 'create')
            ->notEmpty('alias');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('vat_code');

        $validator
            ->integer('stock')
            ->allowEmpty('stock');

        $validator
            ->numeric('weight')
            ->allowEmpty('weight');

        $validator
            ->allowEmpty('image');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['item_group_id'], 'ItemGroups'));
        return $rules;
    }

    public static function ConvertFromExact($item)
    {
        $ret = [
            'exact_guid' => $item['ID'],
            'exact_group_guid' => $item['ItemGroup'],
            'code' => $item['Code'],
            'title' => $item['Description'],
            'alias' => Text::slug($item['Description'] . ' ' . $item['Code']),
            'description' => $item['ExtraDescription'],
            'vat_code' => $item['SalesVatCode'],
            'created' => DataUtils::ExactDateConvertFrom($item['Created']),
            'modified' => DataUtils::ExactDateConvertFrom($item['Modified']),
            'sales' => $item['IsWebshopItem']
        ];
        if(CacheUtils::getPreference('excel_for_stock')->int_val != 1) {
            $ret['stock'] = $item['Stock'];
        }
        if (isset($item['item_group_id'])) {
            $ret['item_group_id'] = $item['item_group_id'];
        }
        if (isset($item['id'])) {
            $ret['id'] = $item['id'];
        }
        return $ret;
    }

    public function getExactFields()
    {
        if(CacheUtils::getPreference('excel_for_stock')->int_val == 1) {
            return ['ID', 'ItemGroup', 'Code', 'Description', 'ExtraDescription',
                'SalesVatCode', 'Created', 'Modified', 'Barcode', 'SearchCode', 'PictureUrl', 'IsWebshopItem'];
        }
        return ['ID', 'ItemGroup', 'Code', 'Description', 'ExtraDescription',
            'SalesVatCode', 'Stock', 'Created', 'Modified', 'Barcode', 'SearchCode', 'PictureUrl', 'IsWebshopItem'];
    }

    public function getInsertColumns()
    {
        if(CacheUtils::getPreference('excel_for_stock')->int_val == 1) {
            return ['exact_guid', 'item_group_id', 'exact_group_guid', 'code', 'title', 'alias', 'description',
                'vat_code', 'image', 'created', 'modified', 'sales'];
        }
        return ['exact_guid', 'item_group_id', 'exact_group_guid', 'code', 'title', 'alias', 'description',
            'vat_code', 'stock', 'image', 'created', 'modified', 'sales'];
    }

    public function getUpdateColumns()
    {
        if(CacheUtils::getPreference('excel_for_stock')->int_val == 1) {
            return ['id', 'exact_guid', 'item_group_id', 'exact_group_guid', 'code', 'title', 'alias', 'description',
                'vat_code', 'image', 'created', 'modified', 'sales'];
        }
        return ['id', 'exact_guid', 'item_group_id', 'exact_group_guid', 'code', 'title', 'alias', 'description',
            'vat_code', 'stock', 'image', 'created', 'modified', 'sales'];
    }

    public function getReadonlyFields()
    {
        return array_merge([
            'exact_guid',
            'exact_group_guid',
            'warehouse_guid',
            'sales',
            'image',
        ], parent::getReadonlyFields());
    }


}
