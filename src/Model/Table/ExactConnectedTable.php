<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 6/3/2016
 * Time: 3:42 PM
 */

namespace App\Model\Table;

use Admin\Model\AdminTable;
use Cake\ORM\Table;

class ExactConnectedTable extends AdminTable
{
    /**
     * @return array
     */
    public function getExactFields()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getInsertColumns()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getUpdateColumns()
    {
        return [];
    }

    public static function ConvertFromExact($item) {
        $ret = array();
        return $ret;
    }

    public static function ConvertToExact($item) {
        $ret = array();
        return $ret;
    }

}
