<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Billings
 * @property \Cake\ORM\Association\BelongsTo $Shippings
 * @property \Cake\ORM\Association\HasMany $Items
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('orders');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('BillingAddresses', [
            'className' => 'Addresses',
            'foreignKey' => 'billing_id'
        ]);
        $this->belongsTo('ShippingAddresses', [
            'className' => 'Addresses',
            'foreignKey' => 'shipping_id'
        ]);
        $this->belongsTo('ShippingMethods', [
            'foreignKey' => 'shipping_method_id'
        ]);
        $this->belongsTo('ShippingOptions', [
            'foreignKey' => 'shipping_option_id'
        ]);
        $this->belongsToMany('Items', [
            'through' => 'OrderItemAssociations'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('exact_guid');

        $validator
            ->integer('local_status');

        $validator
            ->integer('status');

        $validator
            ->integer('invoice_status');

        $validator
            ->integer('payment_method')
            ->allowEmpty('payment_method');

        $validator
            ->allowEmpty('paymentid');

        $validator
            ->integer('shipping_method_id')
            ->allowEmpty('shipping_method_id');

        $validator
            ->integer('shipping_option_id')
            ->allowEmpty('shipping_option_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));
        $rules->add($rules->existsIn(['billing_id'], 'BillingAddresses'));
        $rules->add($rules->existsIn(['shipping_id'], 'ShippingAddresses'));
        $rules->add($rules->existsIn(['shipping_method_id'], 'ShippingMethods'));
        $rules->add($rules->existsIn(['shipping_option_id'], 'ShippingOptions'));
        return $rules;
    }
}
