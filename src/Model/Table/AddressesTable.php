<?php
namespace App\Model\Table;

use App\Model\Entity\Address;
use App\Utils\DataUtils;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Addresses Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Customers
 */
class AddressesTable extends ExactConnectedTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('addresses');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Orders', [
            'foreignKey' => 'billing_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'shipping_id'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('exact_guid');

        $validator
            ->integer('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('street', 'create')
            ->notEmpty('street');

        $validator
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->requirePresence('country', 'create')
            ->notEmpty('country');

        $validator
            ->requirePresence('postcode', 'create')
            ->notEmpty('postcode');

        $validator
            ->allowEmpty('phone');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));
        return $rules;
    }

    public static function ConvertFromExact($item)
    {
        $ret = [
            'exact_guid' => $item['ID'],
            'type' => $item['Type'],
            'name' => $item['ContactName'],
            'street' => $item['AddressLine1'],
            'city' => $item['City'],
            'country' => $item['Country'],
            'postcode' => $item['Postcode'],
            'phone' => $item['Phone'],
            'created' => DataUtils::ExactDateConvertFrom($item['Created']),
            'modified' => DataUtils::ExactDateConvertFrom($item['Modified'])
        ];
        if (isset($item['customer_id'])) {
            $ret['customer_id'] = $item['customer_id'];
        } else {
            $ret['customer_id'] = null;
        }
        if (isset($item['id'])) {
            $ret['id'] = $item['id'];
        }
        return $ret;
    }

    public static function ConvertToExact($item)
    {
        $ret = [
            'ID' => $item['exact_guid'],
            'Type' => $item['type'],
            'ContactName' => $item['name'],
            'AddressLine1' => $item['street'],
            'City' => $item['city'],
            'Country' => $item['country'],
            'Postcode' => $item['postcode'],
            'Phone' => $item['phone'],
            'Created' => $item['created'], //TODO convert to exact date
            'Modified' => $item['modified']
        ];
        return $ret;
    }

    public function getExactFields()
    {
        return ['ID', 'Type', 'ContactName', 'AddressLine1', 'City', 'Country', 'Postcode', 'Phone',
            'Created', 'Modified', 'Account'];
    }

    public function getInsertColumns()
    {
        return ['exact_guid', 'type', 'name', 'street', 'city', 'country', 'postcode', 'phone', 'created', 'modified', 'customer_id'];
    }

    public function getUpdateColumns()
    {
        return ['id', 'exact_guid', 'type', 'name', 'street', 'city', 'country', 'postcode', 'phone', 'created', 'modified', 'customer_id'];
    }
}
