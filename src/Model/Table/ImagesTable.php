<?php
namespace App\Model\Table;

use App\Model\Entity\Image;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Images Model
 *
 * @property BelongsToMany ImageGroups
 */
class ImagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('images');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('ImageGroups', [
            'through' => 'ImageGroupAssociations'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'path' => [
                'file_name' => 'file_name',
                'path' => 'webroot{DS}img{DS}uploads{DS}',
                'transformer' => 'App\File\Images\Transformer\ImageTransformer',
                'writer' => 'App\File\Images\Writer\ImageWriter',
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('path', 'create')
            ->notEmpty('path');

        $validator
            ->integer('thumbnail_mask')
            ->requirePresence('thumbnail_mask', 'create')
            ->notEmpty('thumbnail_mask');

        return $validator;
    }
}
