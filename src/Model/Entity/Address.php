<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Address Entity.
 *
 * @property int $id
 * @property string $exact_guid
 * @property int $customer_id
 * @property \App\Model\Entity\Customer $customer
 * @property int $type
 * @property string $name
 * @property string $street
 * @property string $city
 * @property string $country
 * @property string $postcode
 * @property string $phone
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Address extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    public function inEu()
    {
        return (strpos(\Core\Helper\Preference::text('eu_countries'), $this->country) !== false);
    }
}
