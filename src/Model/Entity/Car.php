<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Car Entity.
 *
 * @property int $id
 * @property string $reference
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property string $category
 * @property string $model
 * @property int $year
 * @property string $color
 * @property string $condition
 * @property float $price
 * @property int $status
 * @property string $image_group
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Car extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
