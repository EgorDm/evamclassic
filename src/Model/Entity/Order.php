<?php
namespace App\Model\Entity;

use App\Model\Table\OrdersTable;
use App\Utils\DataUtils;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Payment\Helper\Methods;

/**
 * Order Entity.
 *
 * @property int $id
 * @property string $exact_guid
 * @property int $customer_id
 * @property \App\Model\Entity\User $user
 * @property int $billing_id
 * @property \App\Model\Entity\Billing $billing
 * @property int $shipping_id
 * @property \App\Model\Entity\Shipping $shipping
 * @property int $local_status
 * @property int $status
 * @property int $invoice_status
 * @property int $payment_method
 * @property int $shipping_method
 * @property Customer $customer
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\OrderItemAssociation[] $order_item_associations
 * @property Address $shipping_address
 * @property ShippingOption $shipping_option
 */
class Order extends Entity
{
    /** @var Item[] */
    protected $items;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * @param bool $joinPrices
     * @return \Cake\Datasource\ResultSetInterface|Item[]
     */
    public function getItems($joinPrices = true)
    {
        if(!$this->items) {
            $asscoiations_table = TableRegistry::get('OrderItemAssociations');
            $associations = $asscoiations_table->find()->where(['order_id' => $this->id])->all();
            $associations = DataUtils::indexEntityMap($associations, 'item_id');

            /** @var OrdersTable $table */
            $table = TableRegistry::get('Orders');
            $items = $table->Items->find()->where(['Items.id IN' => array_keys($associations)]);

            if ($joinPrices) {
                $items = $items->matching('ItemPrices', function ($q) {
                    return $q->where([
                        'ItemPrices.start_date <=' => new \DateTime(),
                        'OR' => [['ItemPrices.end_date >' => new \DateTime()], ['ItemPrices.end_date IS' => null]]
                    ])->limit(1);
                })
                    ->contain(['VatCodes' => function ($q) {
                        return $q->select(['code_vat', 'percentage']);
                    }]);
            }
            $this->items = $items->all();

            foreach ($this->items as $item) {
                $item->set('quantity', $associations[$item->id]->quantity);
            }
        }

        return $this->items;
    }

    /**
     * @return \Payment\Model\Methods\PaymentMethodInterface
     */
    public function getPaymentMethod()
    {
        return Methods::getMethod($this->payment_method);
    }

    public function getCustomer()
    {
        if(!$this->customer) {
            $this->customer = TableRegistry::get('Customers')->get($this->customer_id);
        }
        return $this->customer;
    }
}
