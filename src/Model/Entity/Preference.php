<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Preference Entity.
 *
 * @property int $id
 * @property string $alias
 * @property string $title
 * @property string $text_val
 * @property int $int_val
 * @property float $float_val
 */
class Preference extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
