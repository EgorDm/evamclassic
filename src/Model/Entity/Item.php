<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity.
 *
 * @property int $id
 * @property string $exact_guid
 * @property int $item_group_id
 * @property \App\Model\Entity\ItemGroup $item_group
 * @property string $exact_group_guid
 * @property string $code
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property int $vat_code
 * @property int $stock
 * @property string $image
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string urls
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * @param int $qty
     * @return float|int
     */
    public function getPrice($qty = 1)
    {
        return $this->_matchingData['ItemPrices']['price'] * $qty;
    }
}
