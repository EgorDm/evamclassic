<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ItemPrice Entity.
 *
 * @property int $id
 * @property string $exact_guid
 * @property int $item_id
 * @property \App\Model\Entity\Item $item
 * @property string $item_guid
 * @property string $default_unit
 * @property string $items_per_unit
 * @property int $quantity
 * @property float $price
 * @property \Cake\I18n\Time $start_date
 * @property \Cake\I18n\Time $end_date
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class ItemPrice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
