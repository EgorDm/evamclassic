<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 29-Jun-16
 * Time: 22:00
 */

namespace App\File\Images\Writer;


use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Imagine\Exception\RuntimeException;
use Imagine\Image\Box;
use Imagine\Imagick\Imagine;
use Intervention\Image\ImageManager;
use Josegonzalez\Upload\File\Writer\DefaultWriter;
use League\Flysystem\FilesystemInterface;

class ImageWriter extends DefaultWriter
{
    const LARGE_IMAGE_WIDTH = 468;
    const MEDIUM_IMAGE_WIDTH = 266;
    const SMALL_IMAGE_WIDTH = 85;

    protected $root_path = null;

    public function __construct(Table $table, Entity $entity, $data, $field, $settings)
    {
        parent::__construct($table, $entity, $data, $field, $settings);
        $this->root_path = Hash::get($this->settings, 'filesystem.root', ROOT . DS);
    }


    public function writeFile(FilesystemInterface $filesystem, $file, $path)
    {

        if ($this->entity->has('thumbnail_mask')) {
            $mask = $this->entity->get('thumbnail_mask');
            $res = $this->saveResizeImage($file, $path, null);
            if ($mask == 1 || $mask == 3) {
                $res = $this->saveResizeImage($file, $path, ['w' => self::MEDIUM_IMAGE_WIDTH], 'thumbnail_medium');
            }
            if ($mask == 2 || $mask == 3) {
                $res = $this->saveResizeImage($file, $path, ['w' => self::SMALL_IMAGE_WIDTH], 'thumbnail_small');
            }

        }
        return $res;
    }

    /**
     * @param string $file
     * @param string $path
     * @param null|array $size
     * @param null|string $subpath
     * @return bool
     */
    public function saveResizeImage($file, $path, $size = null, $subpath = null)
    {
        ini_set('gd.jpeg_ignore_warning', true);
        try {
            if ($subpath != null) {
                $path = pathinfo($path, PATHINFO_DIRNAME) . DS . $subpath. DS . pathinfo($path, PATHINFO_BASENAME);
            }
            if (!file_exists($this->root_path.pathinfo($path, PATHINFO_DIRNAME))) {
                mkdir($this->root_path.pathinfo($path, PATHINFO_DIRNAME), 0777, true);
            }

            list($width, $height, $mime) = getimagesize($file);
            $img = null;
            switch ($mime) {
                case IMAGETYPE_JPEG:
                    $img = @imagecreatefromjpeg($file);
                    break;
                case IMAGETYPE_PNG:
                    $img = @imagecreatefrompng($file);
                    break;
                case IMAGETYPE_GIF:
                    $img = @imagecreatefromgif($file);
                    break;
            }
            if(empty($img) || $img == false) {
                return false;
            }

            if ($size != null) {
                if (!empty($size['w']) && !empty($size['h'])) {
                } else if (!empty($size['w'])) {
                    $size['h'] = ceil(($size['w'] / $width) * $height);
                } else if (!empty($size['h'])) {
                    $size['w'] = ceil(($size['h'] / $height) * $width);
                }
            } else {
                $size = ['w' => $width, 'h' => $height];
            }
            $dst = imagecreatetruecolor($size['w'],  $size['h']);
            imagecopyresampled($dst, $img, 0, 0, 0, 0, $size['w'], $size['h'], $width, $height);

            imagejpeg($dst, $this->root_path.$path);
            imagedestroy($dst);
            imagedestroy($img);
            return true;
        } catch (RuntimeException $e) {
            Log::debug('ImageWriter'.$e->getMessage());
        }
        return false;
    }

    function scale_image($src_image, $dst_image, $op = 'fit') {
        $src_width = imagesx($src_image);
        $src_height = imagesy($src_image);

        $dst_width = imagesx($dst_image);
        $dst_height = imagesy($dst_image);

        // Try to match destination image by width
        $new_width = $dst_width;
        $new_height = round($new_width*($src_height/$src_width));
        $new_x = 0;
        $new_y = round(($dst_height-$new_height)/2);

        // FILL and FIT mode are mutually exclusive
        if ($op =='fill')
            $next = $new_height < $dst_height;
        else
            $next = $new_height > $dst_height;

        // If match by width failed and destination image does not fit, try by height
        if ($next) {
            $new_height = $dst_height;
            $new_width = round($new_height*($src_width/$src_height));
            $new_x = round(($dst_width - $new_width)/2);
            $new_y = 0;
        }

        // Copy image on right place
        imagecopyresampled($dst_image, $src_image , $new_x, $new_y, 0, 0, $new_width, $new_height, $src_width, $src_height);
    }
}