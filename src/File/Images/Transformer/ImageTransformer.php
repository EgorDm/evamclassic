<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 29-Jun-16
 * Time: 20:31
 */

namespace App\File\Images\Transformer;


use Cake\Log\Log;
use Cake\Utility\Inflector;
use Cake\Utility\Text;
use Josegonzalez\Upload\File\Transformer\DefaultTransformer;

class ImageTransformer extends DefaultTransformer
{
    public function transform()
    {
        if(!$this->entity->get('id') && !isset($this->data[$this->entity->get('alias')][$this->entity->get('primaryKey')])) {
            $filename = generateRandomString(6).date('U');
            $filename = Text::slug($filename, '-').'.jpg';
        } else {
            $filename = $this->entity->get('file_name');
        }
        return [$this->data['tmp_name'] => strtolower($filename), 'field_name' => strtolower($filename)];
    }
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}