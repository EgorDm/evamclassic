<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 16-Jul-16
 * Time: 14:43
 */

namespace App\Utils;


class CheckoutSteps
{
    const
        SHIPPING_ADDRESS = 0,
        BILLING_ADDRESS = 1,
        SHIPPING_METHOD = 2,
        PAYMENT_METHOD = 3,
        OVERVIEW = 4,
        PAYMENT = 5,
        PMNT_OPEN = 10,
        PMNT_PENDING = 11,
        PMNT_CANCELLED = 12,
        PMNT_EXPIRED = 13,
        PMNT_PAID = 14,
        PMNT_PAIDOUT = 15,
        PMNT_REFUNDED = 16,
        PMNT_CHARGED_BACK = 17,
        PMNT_FAILED = 18;

    public static $status_names = [
        self::SHIPPING_ADDRESS => 'Choosing shipping address',
        self::BILLING_ADDRESS => 'Choosing billing address',
        self::SHIPPING_METHOD => 'Choosing shipping method',
        self::PAYMENT_METHOD => 'Choosing payment method',
        self::OVERVIEW => 'Confirmation',
        self::PAYMENT => 'Waiting for Payment',
        self::PMNT_OPEN => 'Payment: Open',
        self::PMNT_PENDING => 'Payment: Pending',
        self::PMNT_CANCELLED => 'Payment: Cancelled',
        self::PMNT_EXPIRED => 'Payment: Expired',
        self::PMNT_PAID => 'Payment: Paid',
        self::PMNT_PAIDOUT => 'Payment: Paidout',
        self::PMNT_REFUNDED => 'Payment: Refunded',
        self::PMNT_CHARGED_BACK => 'Payment: Charged back',
        self::PMNT_FAILED => 'Payment: Failed',
    ];
}