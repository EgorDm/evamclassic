<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 21-Jul-16
 * Time: 23:27
 */

namespace App\Utils;


use App\Services\Payment\Methods\Invoice;
use Cake\I18n\Number;

class PaymentMethods
{
    const
        PAYPAL = 0,
        CREDIT_CARD = 1,
        BANK_TRANSFER = 2,
        IDEAL = 3,
        BANK_TRANSFER_LOCAL = 4,
        REQUEST_INVOICE = 5
    ;

    public static $payment_methods = [
        self::PAYPAL => 'PayPal',
        self::CREDIT_CARD => 'Credit Card',
        self::BANK_TRANSFER => 'Bank Transfer',
        self::IDEAL => 'iDeal',
        self::BANK_TRANSFER_LOCAL => 'Bank Transfer',
        self::REQUEST_INVOICE => 'Request invoice'
    ];

    public static $register = [
        self::REQUEST_INVOICE => Invoice::class
    ];

    public static function getSum($payment_method, $in_eu) {
        $preference = null;
        $ret = '';
        switch ($payment_method) {
            case self::IDEAL:
                $preference = 'commission_ideal';
                break;
            case self::CREDIT_CARD:
                if($in_eu) {
                    $ret .= ' europe';
                    $preference = 'commission_creditcard_eu';
                } else {
                    $ret .= ' international';
                    $preference = 'commission_creditcard_in';
                }
                break;
            case self::BANK_TRANSFER:
                $preference = 'commission_bank_transfer';
                break;
            case self::PAYPAL:
                $preference = 'commission_paypal';
                break;
            case self::REQUEST_INVOICE:
                $preference = 'request_invoice';
                break;
            default:
                return '';
        }
        $pref = CacheUtils::getPreference($preference);
        if(!empty($pref->int_val)) {
            $ret .= ' +'.Number::currency($pref->int_val/100, 'EUR');
        }
        if(!empty($pref->float_val)) {
            $ret .= ' +'.Number::toPercentage($pref->float_val);
        }
        return $ret;
    }

    public static function calcCommission($total, $payment_method, $in_eu) {
        $preference = null;
        $ret = 0;
        switch ($payment_method) {
            case self::IDEAL:
                $preference = 'commission_ideal';
                break;
            case self::CREDIT_CARD:
                if($in_eu) {
                    $ret .= ' europe';
                    $preference = 'commission_creditcard_eu';
                } else {
                    $ret .= ' international';
                    $preference = 'commission_creditcard_in';
                }
                break;
            case self::BANK_TRANSFER:
                $preference = 'commission_bank_transfer';
                break;
            case self::PAYPAL:
                if($in_eu) {
                    $ret .= ' europe';
                    $preference = 'commission_paypal';
                } else {
                    $ret .= ' international';
                    $preference = 'commission_paypal_in';
                }
                break;
            default:
                return 0;
        }
        $pref = CacheUtils::getPreference($preference);
        if(!empty($pref->float_val)) {
            $ret = $total*($pref->float_val/100);
        }
        if(!empty($pref->int_val)) {
            $ret = $ret + ($pref->int_val/100);
        }

        if($in_eu) {
            $ret = $ret*1.21;
        }

        return $ret;
    }
}
