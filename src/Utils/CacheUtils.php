<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 08-Aug-16
 * Time: 17:56
 */

namespace App\Utils;


use Cake\Cache\Cache;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

class CacheUtils
{
    public static function readOrExecuteAll($key, $query, $config = null) {

        //$response = Cache::read($key, $config);
        //if($response == false) {
            $response = $query->all()->toArray();
            //Cache::write($key, $response, $config);
        //}
        return $response;
    }

    public static function getPreference($preference) {
        $prefs = CacheUtils::readOrExecuteAll('allPrefs', TableRegistry::get('Preferences')->find(), 'preferences');
        foreach ($prefs as $pref) {
            if($pref['alias'] == $preference) {
                return $pref;
            }
        }
        return null;
    }
}