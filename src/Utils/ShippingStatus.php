<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 27-Jul-16
 * Time: 12:58
 */

namespace App\Utils;


class ShippingStatus
{
    const
        WAITING_FOR_PAYMENT = 0,
        WAITING_FOR_SHIPMENT = 1,
        SHIPPED = 2;

    public static $status_names = [
        self::WAITING_FOR_PAYMENT => 'Waiting for payment confirmation',
        self::WAITING_FOR_SHIPMENT => 'Waiting for shipment',
        self::SHIPPED => 'Shipped',
    ];
}