<?php

use Customer\Helper\Countries;

?>
<?= $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Address',
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $address->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $address->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $address, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'customer_id', 'name' => 'Customer ID', 'required' => true, 'items' => $customers,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'type', 'name' => 'Type', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'name', 'name' => 'Name', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'street', 'name' => 'Street',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'city', 'name' => 'City',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'country', 'name' => 'Country', 'items' => Countries::$countries,
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'postcode', 'name' => 'Postcode',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'phone', 'name' => 'Phone',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]]

    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);
