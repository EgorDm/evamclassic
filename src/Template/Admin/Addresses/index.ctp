<?php
$threads = array();
foreach ($addresses as $model) {
    array_push($threads, [
        'id' => $model->id,
        'fields' => [
            $this->Number->format($model->id),
            $model->has('customer') ? $this->Html->link($model->customer->name, ['controller' => 'Customers', 'action' => 'view', $model->customer->id]) : '',
            $this->Number->format($model->type),
            h($model->name),
            h($model->street),
            h($model->city),
            h($model->country),
            h($model->postcode),
            h($model->phone),
            h($model->created),
            h($model->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Addresses',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'customer', 'name' => 'Customer', 'width' => 300],
        ['model' => 'type', 'name' => 'Type', 'width' => 200],
        ['model' => 'name', 'name' => 'Name', 'width' => 200],
        ['model' => 'street', 'name' => 'Street', 'width' => 200],
        ['model' => 'city', 'name' => 'City', 'width' => 200],
        ['model' => 'country', 'name' => 'Country', 'width' => 200],
        ['model' => 'postcode', 'name' => 'Postcode', 'width' => 200],
        ['model' => 'phone', 'name' => 'Phone', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);
