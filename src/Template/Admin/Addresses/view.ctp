<?= $this->element('Admin.Partials/crud_view', [
    'title' => h($address->name),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $address->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $address->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $address->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($address->id)],
        ['name' => 'Customer', 'value' => $address->has('customer') ? $this->Html->link($address->customer->name, ['controller' => 'Customers', 'action' => 'view', $address->customer->id]) : ''],
        ['name' => 'Name', 'value' => h($address->name)],
        ['name' => 'Street', 'value' => h($address->street)],
        ['name' => 'City', 'value' => h($address->city)],
        ['name' => 'Country', 'value' => h(\App\Utils\DataUtils::$countries[$address->country])],
        ['name' => 'Postcode', 'value' => h($address->postcode)],
        ['name' => 'Phone', 'value' => h($address->phone)],
        ['name' => 'Type', 'value' => h($address->type)],
        ['name' => 'Created', 'value' => h($address->created)],
        ['name' => 'Modified', 'value' => h($address->modified)]
    ]
]);
