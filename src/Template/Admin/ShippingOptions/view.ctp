<?= $this->element('Admin.Partials/crud_view', [
    'title' => 'Shipping option #'.$shippingOption->id,
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $shippingOption->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $shippingOption->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $shippingOption->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($shippingOption->id)],
        ['name' => 'Min Weight', 'value' => $this->Number->format($shippingOption->min_weight)],
        ['name' => 'Max Weight', 'value' => ($shippingOption->max_weight != null) ?
            $this->Number->format($shippingOption->max_weight) : '>'.$shippingOption->min_weight],
        ['name' => 'Price', 'value' => $this->Number->format($shippingOption->price)],
        ['name' => 'Created', 'value' => h($shippingOption->created)],
        ['name' => 'Modified', 'value' => h($shippingOption->modified)],
    ]
]);
?>
