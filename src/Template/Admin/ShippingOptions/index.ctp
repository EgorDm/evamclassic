<?php
$threads = array();
foreach ($shippingOptions as $shippingOption) {
    array_push($threads, [
        'id' => $shippingOption->id,
        'fields' => [
            $this->Number->format($shippingOption->id),
            $shippingOption->has('shipping_method') ? $this->Html->link($shippingOption->shipping_method->title,
                ['controller' => 'ShippingMethods', 'action' => 'view', $shippingOption->shipping_method->id]) : '',
            $this->Number->format($shippingOption->min_weight) . ' kg',
            ($shippingOption->max_weight != null) ?
                $this->Number->format($shippingOption->max_weight). ' kg' : '>'.$shippingOption->min_weight. ' kg',
            $this->Number->format($shippingOption->price) . ' €',
            h($shippingOption->created),
            h($shippingOption->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Shipping Options',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'shipping_method_id', 'name' => 'Shipping method', 'width' => 150],
        ['model' => 'min_weight', 'name' => 'Min weight', 'width' => 200],
        ['model' => 'max_weight', 'name' => 'Max weight', 'width' => 200],
        ['model' => 'price', 'name' => 'Price', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);


