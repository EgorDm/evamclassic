<?= $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Shipping Option',
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $shippingOption->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $shippingOption->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $shippingOption, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'shipping_method_id', 'name' => 'Full Name', 'required' => true, 'items' => $shippingMethods,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'min_weight', 'name' => 'Min Weight', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'max_weight', 'name' => 'Max Weight',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'price', 'name' => 'Price', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);
