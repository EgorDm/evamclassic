<?php
$threads = array();
foreach ($customers as $model) {
    array_push($threads, [
        'id' => $model->id,
        'fields' => [
            $this->Number->format($model->id),
            h($model->name),
            h($model->email),
            $model->type,
            h($model->created),
            h($model->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Customers',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'name', 'name' => 'Full name', 'width' => 300],
        ['model' => 'email', 'name' => 'Email', 'width' => 300],
        ['model' => 'type', 'name' => 'Type', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);
