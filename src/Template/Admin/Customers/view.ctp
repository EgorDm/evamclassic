<?php echo $this->element('Admin.Partials/crud_view', [
    'title' => h($customer->name),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $customer->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $customer->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($customer->id)],
        ['name' => 'Full Name', 'value' => h($customer->name)],
        ['name' => 'Email', 'value' => h($customer->email)],
        ['name' => 'Type', 'value' => $customer->type],
        ['name' => 'Created', 'value' => h($customer->created)],
        ['name' => 'Modified', 'value' => h($customer->modified)]
    ]
]);

$threads = array();
foreach ($customer->addresses as $model) {
    array_push($threads, [
        'id' => $model->id,
        'fields' => [
            $this->Number->format($model->id),
            $this->Number->format($model->type),
            h($model->name),
            h($model->street),
            h($model->city),
            h($model->country),
            h($model->postcode),
            h($model->phone),
            h($model->created),
            h($model->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Related Addresses',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add', 'controller' => 'Addresses'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'type', 'name' => 'Type', 'width' => 200],
        ['model' => 'name', 'name' => 'Name', 'width' => 200],
        ['model' => 'street', 'name' => 'Street', 'width' => 200],
        ['model' => 'city', 'name' => 'City', 'width' => 200],
        ['model' => 'country', 'name' => 'Country', 'width' => 200],
        ['model' => 'postcode', 'name' => 'Postcode', 'width' => 200],
        ['model' => 'phone', 'name' => 'Phone', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'controller' => 'Addresses', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'Addresses', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'controller' => 'Addresses', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
]);

$threads = array();
foreach ($customer->orders as $order) {
    array_push($threads, [
        'id' => $order->id,
        'fields' => [
            $this->Number->format($order->id),
            h(\App\Utils\PaymentMethods::$payment_methods[$order->payment_method]),
            h(\App\Utils\CheckoutSteps::$status_names[$order->local_status]),
            h(\App\Utils\ShippingStatus::$status_names[$order->shipment_status]),
            h($order->created),
            h($order->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Orders',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'payment_method', 'name' => 'Payment method', 'width' => 300],
        ['model' => 'local_status', 'name' => 'Status', 'width' => 200],
        ['model' => 'shipment_status', 'name' => 'Shipping status', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'controller' => 'Orders', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'Orders', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'controller' => 'Orders', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
]);
