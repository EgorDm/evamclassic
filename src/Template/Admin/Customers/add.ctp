<?= $this->element('Admin.Partials/crud_edit', [
    'title' => 'Create Customer',
    'actions' => [
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $customer, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'name', 'name' => 'Full Name', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'email', 'name' => 'Email', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'password', 'name' => 'Password', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'type', 'name' => 'Type',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]]
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);
