<?php
use Cake\View\View;

/**
 * @var View $this
 */

?>

<div class="x_panel">
    <div class="x_title">
        <h2>All Images</h2>
        <ul class="nav navbar-right panel_toolbox">
            <?php
            echo '<li>' . $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add'], ['escape' => false]) . '</li>';
            ?>
        </ul>
        <form class="nav navbar-right panel_toolbox" method="post">
            <input type="text" placeholder="Search" name="search">
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <?php foreach ($images as $image) { ?>
                <div class="col-md-55">
                    <div class="thumbnail">
                        <div class="image view">
                            <?= $this->AdminPanel->imageLink($image->id, 'style="width: 100%; display: block;"', true) ?>
                        </div>
                        <div class="caption">
                            <p><?= $image->name ?>
                            <ul class="nav panel_toolbox" style="display: inline-block; float: none;">
                                <?php
                                echo '<li>' . $this->Form->postLink('<i class="fa fa-remove"></i>',
                                        ['action' => 'delete', $image->id], ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $image->id)]) . '</li>';
                                echo '<li>' . $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $image->id], ['escape' => false]) . '</li>';
                                echo '<li>' . $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $image->id], ['escape' => false]) . '</li>';
                                ?>
                            </ul>
                            </p>

                        </div>


                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
</div>


<?= $this->element('Admin.Partials/crud_edit', [
    'title' => 'Batch upload images',
    'actions' => [
    ],
    'description' => '<p>Upload a zip with all the images compressed inside. No subfolders allowed an they should all have a unique name.</p>',
    'form' => ['model' => false, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'url' => ['action' => 'batchUpload'], 'type' => 'file']],
    'inputs' => [
        ['model' => 'zip_file', 'name' => 'Zip file', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'file', 'accept' => '.zip']],
        ['model' => 'thumbnail_mask', 'name' => 'Thumbnail mode', 'items' => [0 => 'none', 1 => 'medium', 2 => 'small', 3 => 'medium and small'],
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

