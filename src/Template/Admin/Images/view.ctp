<?= $this->element('Admin.Partials/crud_view', [
    'title' => h($image->name),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $image->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $image->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $image->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($image->id)],
        ['name' => 'Name', 'value' => h($image->name)],
        ['name' => 'Path', 'value' => h($image->path)],
        ['name' => 'Created', 'value' => h($image->created)],
        ['name' => 'Modified', 'value' => h($image->modified)],
    ]
]);
?>

<div class="x_panel">
    <div class="x_title">
        <h2>Image preview</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?=
        $this->AdminPanel->imageLink($image->id, null, false)
        ?>
    </div>
</div>

<?php
$threads = array();
foreach ($image->image_groups as $image_group) {
    array_push($threads, [
        'id' => $image_group->id,
        'fields' => [
            h($image_group->id),
            h($image_group->name),
            h($image_group->type),
            h($image_group->created),
            h($image_group->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Related Image Group Associations',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add', 'controller' => 'ItemGroups'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'name', 'name' => 'Name', 'width' => 300],
        ['model' => 'type', 'name' => 'Type', 'width' => 300],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'controller' => 'ItemGroups', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'ItemGroups', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'controller' => 'ItemGroups', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
]);
?>

