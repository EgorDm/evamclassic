<?php echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Reupload/Change image',
    'actions' => [
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $image, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'type' => 'file']],
    'inputs' => [
        ['model' => 'name', 'name' => 'Name', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'path', 'name' => 'Image', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'file']],
        ['model' => 'thumbnail_mask', 'name' => 'Thumbnail mode', 'items' => [0 => 'none', 1 => 'medium', 2 => 'small', 3 => 'medium and small'],
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'image_groups._ids', 'name' => 'Image group',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'id' => 'image_groups']],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

$this->start('custom_js'); ?>
    <script>
        $("#image_groups").select2();
    </script>
<?php $this->end();?>
