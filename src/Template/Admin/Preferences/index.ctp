<?php
use Cake\View\View;
use Customer\Helper\Countries;

/**
 * @var $this View
 */

$this->start('custom_js'); ?>
<script>
    $("#countries").select2();
</script>
<?php $this->end(); ?>

<style>
    .form-group {
        overflow: hidden;
    }
</style>
<div class="x_panel">
    <div class="x_title">
        <h2>Exact online</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li style="float: right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?= $this->Form->postLink('Logout and reconnect to exact', ['controller' => 'Dashboard', 'action' => 'reloginExact'],
            ['class' => 'btn btn-success', 'confirm' => __('Are you sure you want to do that? This is only required if you want to change connected Exact account.')]) ?>
    </div>
</div>
<div class="x_panel">
    <div class="x_title">
        <h2>Google analytics</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li style="float: right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <?= $this->Form->create($preferences['analytics'], [
            'url' => ['action' => 'edit', $preferences['analytics']->id],
            'data-parsley-validate', 'class' => 'form-horizontal form-label-left'
        ]) ?>
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $preferences['analytics']->title ?></h2>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="text_val">
                        Tracking code
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('text_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <?= $this->Form->button('Submit', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="x_panel">
    <div class="x_title">
        <h2>Europen Countries</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li style="float: right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?= $this->Form->create($preferences['eu_countries'], ['url' => ['action' => 'edit', $preferences['eu_countries']->id]]) ?>
        <div class="form-group" style="overflow: hidden">
            <div class="col-xs-12">
                <?= $this->Form->select('select_val', Countries::$countries, ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'multiple' => true, 'id' => 'countries']) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <?= $this->Form->button('Submit', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="x_panel">
    <div class="x_title">
        <h2>Payment commisions</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li style="float: right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <?= $this->Form->create($preferences['commission_ideal'], [
            'url' => ['action' => 'edit', $preferences['commission_ideal']->id],
            'data-parsley-validate', 'class' => 'form-horizontal form-label-left'
        ]) ?>
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $preferences['commission_ideal']->title ?></h2>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="int_val">
                        Commission costs in cents
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('int_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="float_val">
                        Commission costs variable
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('float_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?= $this->Form->button('Submit', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>

        <?= $this->Form->create($preferences['commission_creditcard_eu'], [
            'url' => ['action' => 'edit', $preferences['commission_creditcard_eu']->id],
            'data-parsley-validate', 'class' => 'form-horizontal form-label-left'
        ]) ?>
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $preferences['commission_creditcard_eu']->title ?></h2>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="int_val">
                        Commission costs in cents
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('int_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="float_val">
                        Commission costs variable
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('float_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?= $this->Form->button('Submit', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>

        <?= $this->Form->create($preferences['commission_creditcard_in'], [
            'url' => ['action' => 'edit', $preferences['commission_creditcard_in']->id],
            'data-parsley-validate', 'class' => 'form-horizontal form-label-left'
        ]) ?>
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $preferences['commission_creditcard_in']->title ?></h2>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="int_val">
                        Commission costs in cents
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('int_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="float_val">
                        Commission costs variable
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('float_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?= $this->Form->button('Submit', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>

        <?= $this->Form->create($preferences['commission_bank_transfer'], [
            'url' => ['action' => 'edit', $preferences['commission_bank_transfer']->id],
            'data-parsley-validate', 'class' => 'form-horizontal form-label-left'
        ]) ?>
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $preferences['commission_bank_transfer']->title ?></h2>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="int_val">
                        Commission costs in cents
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('int_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="float_val">
                        Commission costs variable
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('float_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?= $this->Form->button('Submit', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>

        <?= $this->Form->create($preferences['commission_paypal'], [
            'url' => ['action' => 'edit', $preferences['commission_paypal']->id],
            'data-parsley-validate', 'class' => 'form-horizontal form-label-left'
        ]) ?>
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $preferences['commission_paypal']->title ?></h2>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="int_val">
                        Commission costs in cents
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('int_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="float_val">
                        Commission costs variable
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('float_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?= $this->Form->button('Submit', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
        <?= $this->Form->create($preferences['commission_paypal_in'], [
            'url' => ['action' => 'edit', $preferences['commission_paypal_in']->id],
            'data-parsley-validate', 'class' => 'form-horizontal form-label-left'
        ]) ?>
        <div class="row">
            <div class="col-sm-12">
                <h2><?= $preferences['commission_paypal_in']->title ?></h2>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="int_val">
                        Commission costs in cents
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('int_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="float_val">
                        Commission costs variable
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('float_val', ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?= $this->Form->button('Submit', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
