<?= $this->element('Admin.Partials/crud_view', [
    'title' => h($user->fullname),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $user->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $user->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($user->id)],
        ['name' => 'Full Name', 'value' => h($user->fullname)],
        ['name' => 'Email', 'value' => h($user->email)],
        ['name' => 'Username', 'value' => h($user->username)],
        ['name' => 'Type', 'value' => (($user->type == 1) ? 'Admin' : 'Editor')],
        ['name' => 'Created', 'value' => h($user->created)],
        ['name' => 'Modified', 'value' => h($user->modified)]
    ]
]);
