<?php
$threads = array();
foreach ($users as $user) {
    array_push($threads, [
        'id' => $user->id,
        'fields' => [
            $this->Number->format($user->id),
            h($user->fullname),
            h($user->email),
            h($user->username),
            ($user->type == 1) ? 'Admin' : 'Editor',
            h($user->created),
            h($user->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Users',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'fullname', 'name' => 'Full name', 'width' => 300],
        ['model' => 'email', 'name' => 'Email', 'width' => 300],
        ['model' => 'username', 'name' => 'Username', 'width' => 270],
        ['model' => 'type', 'name' => 'Type', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);
