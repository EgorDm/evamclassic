<?php
$threads = array();
foreach ($shippingMethods as $shippingMethod) {
    array_push($threads, [
        'id' => $shippingMethod->id,
        'fields' => [
            $this->Number->format($shippingMethod->id),
            h($shippingMethod->title),
            h($shippingMethod->countries),
            h($shippingMethod->created),
            h($shippingMethod->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Shipping Methods',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'title', 'name' => 'Title', 'width' => 150],
        ['model' => 'countries', 'name' => 'Countries', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);
