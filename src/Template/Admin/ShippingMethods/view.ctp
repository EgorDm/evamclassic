<?= $this->element('Admin.Partials/crud_view', [
    'title' => h($shippingMethod->title),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $shippingMethod->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $shippingMethod->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $shippingMethod->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($shippingMethod->id)],
        ['name' => 'Countries', 'value' => h($shippingMethod->countries)],
        ['name' => 'Created', 'value' => h($shippingMethod->created)],
        ['name' => 'Modified', 'value' => h($shippingMethod->modified)],
    ]
]);
?>

<div class="x_panel">
    <div class="x_title">
        <h2>Description</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($shippingMethod->description)) {
            echo $this->Text->autoParagraph($shippingMethod->description);
        } else {
            echo 'No description';
        }
        ?>
    </div>
</div>


<?php
$threads = array();
foreach ($shippingMethod->shipping_options as $shippingOptions) {
    array_push($threads, [
        'id' => $shippingOptions->id,
        'fields' => [
            h($shippingOptions->id),
            h($shippingOptions->min_weight),
            h(($shippingOptions->max_weight != null) ?
                $this->Number->format($shippingOptions->max_weight). ' kg' : '>'.$shippingOptions->min_weight. ' kg'),
            h($shippingOptions->price),
            h($shippingOptions->created),
            h($shippingOptions->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Related Shipping Options',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add', 'controller' => 'ShippingOptions'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'min_weight', 'name' => 'Min weight', 'width' => 300],
        ['model' => 'max_weight', 'name' => 'Max weight', 'width' => 300],
        ['model' => 'price', 'name' => 'Price', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'controller' => 'ShippingOptions', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'ShippingOptions', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'controller' => 'ShippingOptions', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
]);
?>


