<?php

use Customer\Helper\Countries;

?>
<?= $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Shipping Method',
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $shippingMethod->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $shippingMethod->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $shippingMethod, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'title', 'name' => 'Title', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'countries', 'name' => 'Countries', 'items' => Countries::$countries,
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'multiple' => true,'id' => 'countries'], 'select' => true],
        ['model' => 'description', 'name' => 'Description', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

$this->start('custom_js'); ?>
    <script>
        $("#countries").select2();
    </script>
<?php $this->end();?>
