<?php
use Cake\View\View;

/**
 * @var View $this
 */

echo json_encode($data);

/*$threads = array();
foreach ($items as $item) {
    array_push($threads, [
        'id' => $item->id,
        'fields' => [
            $this->Number->format($item->id),
            h($item->alias),
            h($item->title),
            $this->Number->format($item->stock),
            $this->Number->format($item->weight) . ' kg',
            $this->Number->format($item->_matchingData['ItemPrices']['price'])
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Items',
    'actions' => [
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'alias', 'name' => 'Alias', 'width' => 200],
        ['model' => 'title', 'name' => 'Title', 'width' => 200],
        ['model' => 'stock', 'name' => 'Stock', 'width' => 50],
        ['model' => 'weight', 'name' => 'Weight', 'width' => 60],
        ['model' => 'price', 'name' => 'Price', 'width' => 60]
    ],
    'item_actions' => [
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'Items', 'options' => []],
        ['title' => 'Add to cart', 'action' => 'addToCart', 'options' => []]
    ],
    'threads' => $threads
]);

if($cart != null) {
    $threads = array();
    foreach ($cart['CartItem'] as $key => $item) {
        array_push($threads, [
            'id' => $key,
            'fields' => [
                $key,
                h($item['title']),
                h($item['quantity']),
                h($item['weight']),
                h($item['subtotal_excl']),
                h($item['subtotal_inc'])
            ]
        ]);
    }
    $shipping_exc = 0;
    $shipping_inc = 0;
    if($shipping != null) {
        $shipping_exc = $shipping['price'];
        $shipping_inc = sprintf('%01.2f', $shipping['price']*1.21);
        array_push($threads, [
            'id' => -1,
            'fields' => [
                -1,
                h($shipping['method']),
                h(1),
                h(-1),
                $shipping_exc,
                $shipping_inc
            ]
        ]);
    }
    array_push($threads, [
        'id' => -1,
        'fields' => [
            -1,
            h('Total'),
            count($cart['CartItem']),
            $cart['Order']['totalweight'],
            sprintf('%01.2f', $cart['Order']['subtotal_excl'] + $shipping_exc),
            sprintf('%01.2f', $cart['Order']['subtotal_inc'] + $shipping_inc)
        ]
    ]);

    echo $this->element('Admin.Partials/crud_index', [
        'title' => 'Cart',
        'actions' => [
            ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'clearCart'], 'options' => ['escape' => false]]
        ],
        'headings' => [
            ['model' => 'id', 'name' => 'ID', 'width' => 100],
            ['model' => 'title', 'name' => 'Title', 'width' => 200],
            ['model' => 'quantity', 'name' => 'Quantity', 'width' => 50],
            ['model' => 'weight', 'name' => 'Weight', 'width' => 60],
            ['model' => 'subtotal_excl', 'name' => 'Subtotal exc btw', 'width' => 60],
            ['model' => 'subtotal_inc', 'name' => 'Subtotal inc btw', 'width' => 60]
        ],
        'item_actions' => [
            ['title' => 'Remove', 'action' => 'removeFromCart', 'options' => []]
        ],
        'threads' => $threads
    ]);
}*/
