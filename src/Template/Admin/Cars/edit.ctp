<?php echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Car',
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $car->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $car->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $car, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'reference', 'name' => 'Reference', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'title', 'name' => 'Title', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'description', 'name' => 'Description', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'textarea']],
        ['model' => 'category', 'name' => 'Category', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'model', 'name' => 'Model', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'year', 'name' => 'Year', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'color', 'name' => 'Color', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'condition', 'name' => 'Condition', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'price', 'name' => 'Price', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'status', 'name' => 'Status', 'required' => true, 'items' => [0 => 'in stock', 1 => 'sold'],
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'image_group_id', 'name' => 'Image Group', 'items' => $imageGroups,
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'empty' => true], 'select' => true],
        ['model' => 'priority', 'name' => 'Priority',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

echo $this->element('tinymce');
