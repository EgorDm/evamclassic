<?php
echo $this->element('Admin.Partials/crud_view', [
    'title' => h($car->title),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $car->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $car->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $car->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($car->id)],
        ['name' => 'Reference', 'value' => h($car->reference)],
        ['name' => 'Alias', 'value' => h($car->alias)],
        ['name' => 'Title', 'value' => h($car->title)],
        ['name' => 'Category', 'value' => h($car->category)],
        ['name' => 'Model', 'value' => h($car->model)],
        ['name' => 'Color', 'value' => h($car->color)],
        ['name' => 'Condition', 'value' => h($car->condition)],
        ['name' => 'Year', 'value' => $this->Number->format($car->year)],
        ['name' => 'Price', 'value' => $this->Number->format($car->price)],
        ['name' => 'Status', 'value' => $this->Number->format($car->status)],
        ['name' => 'Created', 'value' => h($car->created)],
        ['name' => 'Modified', 'value' => h($car->modified)],
        ['name' => 'Priority', 'value' => $this->Number->format($car->priority)],
    ]
]);
?>

<div class="x_panel">
    <div class="x_title">
        <h2>Description</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <p>
        <?php
        if (!empty($car->description)) {
            echo $car->description;
        } else {
            echo 'No description';
        }
        ?>
        </p>
    </div>
</div>

<?php if(!empty($car->image_group)) { ?>
    <div class="x_panel">
        <div class="x_title">
            <h2>Related Images</h2>
            <ul class="nav navbar-right panel_toolbox">
                <?php
                echo '<li>' . $this->Html->link('<i class="fa fa-plus"></i>', ['controller' => 'Images', 'action' => 'add'], ['escape' => false]) . '</li>';
                ?>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <?php foreach ($car->image_group->images as $image) { ?>
                    <div class="col-md-55">
                        <div class="thumbnail">
                            <div class="image view">
                                <?= $this->AdminPanel->imageLink($image->id, 'style="width: 100%; display: block;"', true) ?>
                            </div>
                            <div class="caption">
                                <p><?= $image->name ?>
                                <ul class="nav panel_toolbox" style="display: inline-block; float: none;">
                                    <?php
                                    echo '<li>' . $this->Form->postLink('<i class="fa fa-remove"></i>',
                                            ['controller' => 'Images', 'action' => 'delete', $image->id], ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $image->id)]) . '</li>';
                                    echo '<li>' . $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Images', 'action' => 'edit', $image->id], ['escape' => false]) . '</li>';
                                    echo '<li>' . $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'Images', 'action' => 'view', $image->id], ['escape' => false]) . '</li>';
                                    ?>
                                </ul>
                                </p>

                            </div>


                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>


