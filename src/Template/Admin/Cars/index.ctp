<?php
$threads = array();
foreach ($cars as $car) {
    array_push($threads, [
        'id' => $car->id,
        'fields' => [
            $this->Number->format($car->id),
            h($car->reference),
            h($car->alias),
            h($car->title),
            h($car->category),
            h($car->model),
            h($car->year),
            ($car->status == 0) ? 'IN STOCK' : 'SOLD',
            $car->priority,
            h($car->created),
            h($car->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Cars',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-download"></i>', 'url' => ['action' => 'exportExcel'], 'options' => ['escape' => false, 'target' => '_blank']]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'reference', 'name' => 'Reference', 'width' => 150],
        ['model' => 'alias', 'name' => 'Alias', 'width' => 200],
        ['model' => 'title', 'name' => 'Title', 'width' => 200],
        ['model' => 'category', 'name' => 'Category', 'width' => 200],
        ['model' => 'model', 'name' => 'Model', 'width' => 200],
        ['model' => 'year', 'name' => 'Year', 'width' => 50],
        ['model' => 'status', 'name' => 'Status', 'width' => 60],
        ['model' => 'priority', 'name' => 'Priority', 'width' => 50],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);

echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Import Excel data',
    'actions' => [
    ],
    'description' => '<p>Alle autos vanuit excle bestand importeren. Reference wordt gebruikt om ze te identificeren.</p>',
    'form' => ['model' => false, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'url' => ['action' => 'syncExcel'], 'type' => 'file']],
    'inputs' => [
        ['model' => 'excel_file', 'name' => 'Excel document', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'file', 'accept' => '.xls,.xlsx']],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

?>
