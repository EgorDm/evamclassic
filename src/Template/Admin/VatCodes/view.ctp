<?= $this->element('Admin.Partials/crud_view', [
    'title' => h($vatCode->code_vat),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $vatCode->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $vatCode->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $vatCode->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($vatCode->id)],
        ['name' => 'Code', 'value' => h($vatCode->code_vat)],
        ['name' => 'Percentage', 'value' => $this->Number->format($vatCode->percentage)],
        ['name' => 'Created', 'value' => h($vatCode->created)],
        ['name' => 'Modified', 'value' => h($vatCode->modified)]
    ]
]);

?>

<div class="x_panel">
    <div class="x_title">
        <h2>Description</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($vatCode->description)) {
            echo $this->Text->autoParagraph($vatCode->description);
        } else {
            echo 'No description';
        }
        ?>
    </div>
</div>

