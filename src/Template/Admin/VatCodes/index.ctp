<?php
$threads = array();
foreach ($vatCodes as $vatCode) {
    array_push($threads, [
        'id' => $vatCode->id,
        'fields' => [
            $this->Number->format($vatCode->id),
            strval($vatCode->code_vat),
            h($vatCode->description),
            $this->Number->format($vatCode->percentage),
            h($vatCode->created),
            h($vatCode->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Vat Codes',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-refresh"></i>', 'url' => ['action' => 'synchronize'], 'options' => ['escape' => false]],
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'code', 'name' => 'Code', 'width' => 300],
        ['model' => 'description', 'name' => 'Description', 'width' => 300],
        ['model' => 'percentage', 'name' => 'Percentage', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);

