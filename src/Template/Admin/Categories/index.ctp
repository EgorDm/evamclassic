<?php
$threads = array();
foreach ($categories as $category) {
    array_push($threads, [
        'id' => $category->id,
        'fields' => [
            $this->Number->format($category->id),
            h($category->title),
            (!empty($category->parent_category)) ? h($category->parent_category->title) : 'None',
            h($category->created),
            h($category->modified),
            intval($category->priority)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Categories',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-download"></i>', 'url' => ['action' => 'exportExcel'], 'options' => ['escape' => false, 'target' => '_blank']]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'title', 'name' => 'Title', 'width' => 200],
        ['model' => 'parent_id', 'name' => 'Parent Category', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
        ['model' => 'priority', 'name' => 'Priority', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Move up', 'action' => 'moveUp', 'options' => ['confirm' => __('Are you sure you want to move up # {0}')],
            'post' => true],
        ['title' => 'Move down', 'action' => 'moveDown', 'options' => ['confirm' => __('Are you sure you want to move down # {0}')],
            'post' => true],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);

echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Import Excel data',
    'actions' => [
    ],
    'description' => '<p>Only fields "Groep" and "Sub Groep" are currently supported to import data from.</p>',
    'form' => ['model' => false, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'url' => ['action' => 'syncExcel'], 'type' => 'file']],
    'inputs' => [
        ['model' => 'excel_file', 'name' => 'Excel document', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'file', 'accept' => '.xls,.xlsx']],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);
