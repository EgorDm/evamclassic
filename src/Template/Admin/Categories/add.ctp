<?php echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Add Category',
    'actions' => [
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $category, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'title', 'name' => 'Title', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'urls', 'name' => 'Urls', 'required' => false, 'type' => 'textarea',
            'options' => ['class' => 'form-control col-md-7 col-xs-12 raw', 'label' => false]],
        ['model' => 'description', 'name' => 'Description',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'textarea']],
        ['model' => 'parent_id', 'name' => 'Parent Category', 'items' => $categories,
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'empty' => true], 'select' => true],
        ['model' => 'default_cat', 'name' => 'Default category', 'items' => [false => 'False', true => 'True'],
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'priority', 'name' => 'Priority',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

echo $this->element('tinymce');
