<?php
$threads = array();
foreach ($texts as $text) {
    array_push($threads, [
        'id' => $text->id,
        'fields' => [
            $this->Number->format($text->id),
            h($text->title),
            h($text->slug),
            $text->has('page') ? $this->Html->link($text->page->title, ['controller' => 'Pages', 'action' => 'view', $text->page->id]) : 'None',
            $text->has('image_group') ? $this->Html->link($text->image_group->name, ['controller' => 'ImageGroups', 'action' => 'view', $text->image_group->id]) : 'None'
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Texts',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'title', 'name' => 'Title', 'width' => 300],
        ['model' => 'slug', 'name' => 'Slogan', 'width' => 300],
        ['model' => 'site', 'name' => 'Website', 'width' => 300],
        ['model' => 'page_id', 'name' => 'Page', 'width' => 270],
        ['model' => 'image_group_id', 'name' => 'Image group', 'width' => 270],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);
