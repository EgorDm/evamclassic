<?= $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Text',
    'actions' => [
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $text, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'title', 'name' => 'Title', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'content', 'name' => 'Content',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'textarea']],
        ['model' => 'edit_desc', 'name' => 'Description',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'textarea']],
        ['model' => 'page_id', 'name' => 'Page', 'required' => true, 'items' => $pages,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'image_group_id', 'name' => 'Image Group', 'items' => $imageGroups,
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'empty' => true], 'select' => true],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

echo $this->element('tinymce');
