<?php
echo $this->element('Admin.Partials/crud_view', [
    'title' => h($text->title),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $text->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $text->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $text->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($text->id)],
        ['name' => 'Title', 'value' => h($text->title)],
        ['name' => 'Slogan', 'value' => h($text->slug)],
        ['name' => 'Website', 'value' => h($text->site)],
        ['name' => 'Page', 'value' => $text->has('page') ? $this->Html->link($text->page->title,
            ['controller' => 'Pages', 'action' => 'view', $text->page->id]) : 'None'],
        ['name' => 'Image Group', 'value' => $text->has('image_group') ? $this->Html->link($text->image_group->name,
            ['controller' => 'ImageGroups', 'action' => 'view', $text->image_group->id]) : 'None'],
    ]
]);
?>

<div class="x_panel">
    <div class="x_title">
        <h2>Content</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($text->content)) {
            echo $text->content;
        } else {
            echo 'No content';
        }

        ?>
    </div>
</div>

<div class="x_panel">
    <div class="x_title">
        <h2>Content</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($text->edit_desc)) {
            echo $text->edit_desc;
        } else {
            echo 'No Description';
        }

        ?>
    </div>
</div>

<?php if(!empty($text->image_group) && !empty($text->image_group->images)) { ?>
    <div class="x_panel">
        <div class="x_title">
            <h2>Related Images</h2>
            <ul class="nav navbar-right panel_toolbox">
                <?php
                echo '<li>' . $this->Html->link('<i class="fa fa-plus"></i>', ['controller' => 'Images', 'action' => 'add'], ['escape' => false]) . '</li>';
                ?>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <?php foreach ($text->image_group->images as $image) { ?>
                    <div class="col-md-55">
                        <div class="thumbnail">
                            <div class="image view">
                                <?= $this->AdminPanel->imageLink($image->id, 'style="width: 100%; display: block;"', true) ?>
                            </div>
                            <div class="caption">
                                <p><?= $image->name ?>
                                <ul class="nav panel_toolbox" style="display: inline-block; float: none;">
                                    <?php
                                    echo '<li>' . $this->Form->postLink('<i class="fa fa-remove"></i>',
                                            ['controller' => 'Images', 'action' => 'delete', $image->id], ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $image->id)]) . '</li>';
                                    echo '<li>' . $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Images', 'action' => 'edit', $image->id], ['escape' => false]) . '</li>';
                                    echo '<li>' . $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'Images', 'action' => 'view', $image->id], ['escape' => false]) . '</li>';
                                    ?>
                                </ul>
                                </p>

                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
