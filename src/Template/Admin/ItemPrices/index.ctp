<?php
$threads = array();
foreach ($itemPrices as $itemPrice) {
    array_push($threads, [
        'id' => $itemPrice->id,
        'fields' => [
            $this->Number->format($itemPrice->id),
            $itemPrice->has('item') ? $this->Html->link($itemPrice->item->title, ['controller' => 'Items', 'action' => 'view', $itemPrice->item->id]) : '',
            h($itemPrice->default_unit),
            h($itemPrice->items_per_unit),
            $this->Number->format($itemPrice->quantity),
            $this->Number->format($itemPrice->price) . " €",
            h($itemPrice->start_date),
            h($itemPrice->end_date),
            h($itemPrice->created),
            h($itemPrice->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Item Prices',
    'actions' => [
        ['title' => '<i class="fa fa-refresh"></i>', 'url' => ['action' => 'synchronize'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'item', 'name' => 'Item', 'width' => 300],
        ['model' => 'default_unit', 'name' => 'Unit', 'width' => 300],
        ['model' => 'items_per_unit', 'name' => 'Items per Unit', 'width' => 300],
        ['model' => 'quantity', 'name' => 'Quantity', 'width' => 270],
        ['model' => 'price', 'name' => 'Price', 'width' => 200],
        ['model' => 'start_date', 'name' => 'Start Date', 'width' => 100],
        ['model' => 'end_date', 'name' => 'End Date', 'width' => 100],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);

