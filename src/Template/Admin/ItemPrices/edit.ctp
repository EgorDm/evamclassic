<?php echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Item group',
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $itemPrice->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $itemPrice->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $itemPrice, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'item_id', 'name' => 'Items', 'items' => $items,
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'default_unit', 'name' => 'Default Unit', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'items_per_unit', 'name' => 'Items per Unit', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'quantity', 'name' => 'Quantity', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'price', 'name' => 'Price', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'start_date', 'name' => 'Start date',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'datetime' => true],
        ['model' => 'end_date', 'name' => 'End date',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'datetime' => true],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);
