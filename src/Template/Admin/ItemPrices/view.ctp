<?= $this->element('Admin.Partials/crud_view', [
    'title' => 'Price: #' . $this->Number->format($itemPrice->id),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $itemPrice->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $itemPrice->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $itemPrice->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($itemPrice->id)],
        ['name' => 'Item', 'value' => $itemPrice->has('item') ? $this->Html->link($itemPrice->item->title, ['controller' => 'Items', 'action' => 'view', $itemPrice->item->id]) : ''],
        ['name' => 'Default Unit', 'value' => h($itemPrice->default_unit)],
        ['name' => 'Items Per Unit', 'value' => h($itemPrice->items_per_unit)],
        ['name' => 'Quantity', 'value' => $this->Number->format($itemPrice->quantity)],
        ['name' => 'Price', 'value' => $this->Number->format($itemPrice->price)],
        ['name' => 'Start Date', 'value' => h($itemPrice->start_date)],
        ['name' => 'End Date', 'value' => h($itemPrice->end_date)],
        ['name' => 'Created', 'value' => h($itemPrice->created)],
        ['name' => 'Modified', 'value' => h($itemPrice->modified)],
    ]
]);
?>
