<?php
$threads = array();
foreach ($itemGroups as $itemGroup) {
    array_push($threads, [
        'id' => $itemGroup->id,
        'fields' => [
            $this->Number->format($itemGroup->id),
            h($itemGroup->code),
            h($itemGroup->title),
            h($itemGroup->created),
            h($itemGroup->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Item Groups',
    'actions' => [
        ['title' => '<i class="fa fa-refresh"></i>', 'url' => ['action' => 'synchronize'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'code', 'name' => 'Code', 'width' => 150],
        ['model' => 'title', 'name' => 'Title', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);
