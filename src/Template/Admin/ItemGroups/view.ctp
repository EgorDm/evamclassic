<?= $this->element('Admin.Partials/crud_view', [
    'title' => h($itemGroup->title),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $itemGroup->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $itemGroup->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $itemGroup->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($itemGroup->id)],
        ['name' => 'Code', 'value' => h($itemGroup->code)],
        ['name' => 'Title', 'value' => h($itemGroup->title)],
        ['name' => 'Created', 'value' => h($itemGroup->created)],
        ['name' => 'Modified', 'value' => h($itemGroup->modified)]
    ]
]); ?>

<?php
$threads = array();
foreach ($itemGroup->items as $item) {
    array_push($threads, [
        'id' => $item->id,
        'fields' => [
            $this->Number->format($item->id),
            h($item->code),
            h($item->alias),
            h($item->title),
            $item->has('item_group') ? $this->Html->link($item->item_group->title, ['controller' => 'ItemGroups', 'action' => 'view', $item->item_group->id]) : '',
            $item->has('category') ? $this->Html->link($item->category->title, ['controller' => 'Categories', 'action' => 'view', $item->category->id]) : '',
            $this->Number->format($item->stock),
            $this->Number->format($item->weight) . ' kg',
            h($item->created),
            h($item->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Related Items',
    'actions' => [
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'code', 'name' => 'Code', 'width' => 150],
        ['model' => 'alias', 'name' => 'Alias', 'width' => 200],
        ['model' => 'title', 'name' => 'Title', 'width' => 200],
        ['model' => 'item_group_id', 'name' => 'Group', 'width' => 200],
        ['model' => 'category_id', 'name' => 'Category', 'width' => 200],
        ['model' => 'stock', 'name' => 'Stock', 'width' => 50],
        ['model' => 'weight', 'name' => 'Weight', 'width' => 60],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'controller' => 'Items', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'Items', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'controller' => 'Items', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads
]);

