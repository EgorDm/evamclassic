<?php echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Item group',
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $itemGroup->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $itemGroup->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $itemGroup, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'code', 'name' => 'Code', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'title', 'name' => 'Title', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'description', 'name' => 'Description',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]]
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

echo $this->element('tinymce');
