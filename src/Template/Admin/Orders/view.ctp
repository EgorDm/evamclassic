<?php echo $this->element('Admin.Partials/crud_view', [
    'title' => 'Order #'.$order->id,
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $order->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $order->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $order->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($order->id)],
        ['name' => 'Customer', 'value' => $this->Html->link(h($order->customer->name),
            ['action' => 'view', 'controller' => 'Customers', $order->customer->id])],
        ['name' => 'Billing address', 'value' => $this->Html->link('Address #'.h($order->billing_address->id),
            ['action' => 'view', 'controller' => 'Addresses', $order->billing_address->id])],
        ['name' => 'Shipping address', 'value' => $this->Html->link('Address #'.h($order->shipping_address->id),
            ['action' => 'view', 'controller' => 'Addresses', $order->shipping_address->id])],
        ['name' => 'Status', 'value' => \App\Utils\CheckoutSteps::$status_names[$order->local_status]],
        ['name' => 'Shipment status', 'value' => (!empty($order->shipment_status)) ?
            \App\Utils\ShippingStatus::$status_names[$order->shipment_status] :
            \App\Utils\ShippingStatus::$status_names[0]
        ],
        ['name' => 'Payment method', 'value' => \App\Utils\PaymentMethods::$payment_methods[$order->payment_method]],
        ['name' => 'Shipping method', 'value' => h($order->shipping_method->title)],
        ['name' => 'Shipping option', 'value' => h($order->shipping_option->min_weight
            . (($order->shipping_option->max_weight == null) ? '+' : ' - ' . $order->shipping_option->max_weight) . ' kg')],
        ['name' => 'Payment reference', 'value' => (!empty($order->payment_id) ? h($order->payment_id) : 'Payment not started')],
        ['name' => 'Created', 'value' => h($order->created)],
        ['name' => 'Modified', 'value' => h($order->modified)],
        ['name' => 'Order link', 'value' => $this->Html->link('Click here', ['controller' => 'Orders', 'action' => 'view',
            'prefix' => false, $order->id], ['target' => '_blank'])],
        ['name' => 'Order link', 'value' => $this->Html->link('Synchronize Exact', ['controller' => 'Orders', 'action' => 'sync',
            $order->id])],
        ['name' => 'Doorsturen naar Exact', 'value' => $this->Html->link('Click here', ['controller' => 'Orders', 'action' => 'exact',
            $order->id], [])]
    ]
]);

$threads = array();
foreach ($items as $item) {
    array_push($threads, [
        'id' => $item->id,
        'fields' => [
            $this->Number->format($item->id),
            h($item->code),
            h($item->title),
            h($item->quantity),
            $this->Number->format($item->stock),
            $this->Number->format($item->weight) . ' kg'
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Bought Items',
    'actions' => [
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'code', 'name' => 'Code', 'width' => 150],
        ['model' => 'title', 'name' => 'Title', 'width' => 200],
        ['model' => 'quantity', 'name' => 'Quantity ordered', 'width' => 60],
        ['model' => 'stock', 'name' => 'Left in stock', 'width' => 50],
        ['model' => 'weight', 'name' => 'Weight', 'width' => 60]
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'controller' => 'Items', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'Items', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'controller' => 'Items', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads
]);
