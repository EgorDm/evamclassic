<?php
$threads = array();
foreach ($orders as $order) {
    array_push($threads, [
        'id' => $order->id,
        'fields' => [
            $this->Number->format($order->id),
            $this->Html->link(h($order->customer->name),
                ['action' => 'view', 'controller' => 'Customers', $order->customer->id]),
            h($order->payment_method ? \App\Utils\PaymentMethods::$payment_methods[$order->payment_method] : ''),
            h($order->local_status ?  \App\Utils\CheckoutSteps::$status_names[$order->local_status] : ''),
            h($order->shipment_status ? \App\Utils\ShippingStatus::$status_names[$order->shipment_status] : ''),
            h($order->created),
            h($order->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Orders',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'customer_id', 'name' => 'Customer', 'width' => 300],
        ['model' => 'payment_method', 'name' => 'Payment method', 'width' => 300],
        ['model' => 'local_status', 'name' => 'Status', 'width' => 200],
        ['model' => 'shipment_status', 'name' => 'Shipping status', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);
