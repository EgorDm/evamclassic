<?= $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Order',
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $order->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $order->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $order, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'shipment_status', 'name' => 'Status', 'required' => true, 'items' => \App\Utils\CheckoutSteps::$status_names,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
    ],
    'inputs' => [
        ['model' => 'shipment_status', 'name' => 'Shipment status', 'required' => true, 'items' => \App\Utils\ShippingStatus::$status_names,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

?>
