<?php
$threads = array();
foreach ($pages as $page) {
    array_push($threads, [
        'id' => $page->id,
        'fields' => [
            $this->Number->format($page->id),
            h($page->title),
            h($page->slug),
            ($page->editable == true) ? 'Yes' : 'No'
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Pages',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'title', 'name' => 'Title', 'width' => 300],
        ['model' => 'slug', 'name' => 'Slogan', 'width' => 300],
        ['model' => 'editable', 'name' => 'Editable', 'width' => 270],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);
