<?= $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Page',
    'actions' => [
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $page, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'title', 'name' => 'Title', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'urls', 'name' => 'Urls', 'required' => false, 'type' => 'textarea',
            'options' => ['class' => 'form-control col-md-7 col-xs-12 raw', 'label' => false]],
        ['model' => 'editable', 'name' => 'Editable', 'required' => true, 'items' => [0 => 'No', 1 => 'Yes'],
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'content', 'name' => 'Content',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'textarea']],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

echo $this->element('tinymce');
