<?php
echo $this->element('Admin.Partials/crud_view', [
    'title' => h($page->title),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $page->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $page->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $page->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($page->id)],
        ['name' => 'Title', 'value' => h($page->title)],
        ['name' => 'Slogan', 'value' => h($page->slug)],
        ['name' => 'Editable', 'value' => $page->editable ? __('Yes') : __('No')],
    ]
]);
?>

    <div class="x_panel">
        <div class="x_title">
            <h2>Content</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php
            if (!empty($page->content)) {
                echo $page->content;
            } else {
                echo 'No content';
            }

            ?>
        </div>
    </div>

<?php
$threads = array();
if (!empty($page->texts)) {
    foreach ($page->texts as $text) {
        array_push($threads, [
            'id' => $text->id,
            'fields' => [
                $this->Number->format($text->id),
                h($text->title),
                h($text->slug),
                h($text->site),
                $text->has('image_group') ? $this->Html->link($text->image_group->name, ['controller' => 'ImageGroups', 'action' => 'view', $text->image_group->id]) : 'None'
            ]
        ]);
    }
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Related Texts',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add', 'controller' => 'Texts'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'title', 'name' => 'Title', 'width' => 300],
        ['model' => 'slug', 'name' => 'Slogan', 'width' => 300],
        ['model' => 'site', 'name' => 'Website', 'width' => 300],
        ['model' => 'image_group_id', 'name' => 'Image group', 'width' => 270],
    ],
    'item_actions' => [
        ['title' => 'View', 'controller' => 'Texts', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'controller' => 'Texts', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'controller' => 'Texts', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
]);
