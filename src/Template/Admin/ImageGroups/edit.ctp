<?php echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Item group',
    'actions' => [
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $imageGroup, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'name', 'name' => 'Name', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'group_type', 'name' => 'Type', 'required' => true, 'items' => [0 => 'Auto', 1 => 'Item', 2 => 'Content'],
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false], 'select' => true],
        ['model' => 'description', 'name' => 'Description',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]]
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

echo $this->element('tinymce');
