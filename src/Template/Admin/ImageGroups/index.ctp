<?php
$threads = array();
foreach ($imageGroups as $imageGroup) {
    array_push($threads, [
        'id' => $imageGroup->id,
        'fields' => [
            $this->Number->format($imageGroup->id),
            h($imageGroup->name),
            ($imageGroup->type == 1) ? 'Item' : 'Auto',
            h($imageGroup->created),
            h($imageGroup->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Image Groups',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'name', 'name' => 'Name', 'width' => 150],
        ['model' => 'group_type', 'name' => 'Type', 'width' => 200],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);

