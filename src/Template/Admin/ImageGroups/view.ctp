<?= $this->element('Admin.Partials/crud_view', [
    'title' => h($imageGroup->name),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $imageGroup->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $imageGroup->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $imageGroup->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($imageGroup->id)],
        ['name' => 'name', 'value' => h($imageGroup->name)],
        ['name' => 'Type', 'value' => ($imageGroup->group_type == 1) ? 'Item' : 'Auto'],
        ['name' => 'Created', 'value' => h($imageGroup->created)],
        ['name' => 'Modified', 'value' => h($imageGroup->modified)]
    ]
]); ?>

<div class="x_panel">
    <div class="x_title">
        <h2>Description</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($imageGroup->description)) {
            echo $this->Text->autoParagraph($imageGroup->description);
        } else {
            echo 'No description';
        } ?>
    </div>
</div>

<?php
$threads = array();
foreach ($imageGroup->items as $item) {
    array_push($threads, [
        'id' => $item->id,
        'fields' => [
            $this->Number->format($item->id),
            h($item->code),
            h($item->alias),
            h($item->title),
            $item->has('item_group') ? $this->Html->link($item->item_group->title, ['controller' => 'ItemGroups', 'action' => 'view', $item->item_group->id]) : '',
            $item->has('category') ? $this->Html->link($item->category->title, ['controller' => 'Categories', 'action' => 'view', $item->category->id]) : '',
            $this->Number->format($item->stock),
            $this->Number->format($item->weight) . ' kg',
            h($item->created),
            h($item->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Related Items',
    'actions' => [
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'code', 'name' => 'Code', 'width' => 150],
        ['model' => 'alias', 'name' => 'Alias', 'width' => 200],
        ['model' => 'title', 'name' => 'Title', 'width' => 200],
        ['model' => 'item_group_id', 'name' => 'Group', 'width' => 200],
        ['model' => 'category_id', 'name' => 'Category', 'width' => 200],
        ['model' => 'stock', 'name' => 'Stock', 'width' => 50],
        ['model' => 'weight', 'name' => 'Weight', 'width' => 60],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'controller' => 'Items', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'Items', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'controller' => 'Items', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads
]);
?>


<div class="x_panel">
    <div class="x_title">
        <h2>Related Images</h2>
        <ul class="nav navbar-right panel_toolbox">
            <?php
            echo '<li>' . $this->Html->link('<i class="fa fa-plus"></i>', ['controller' => 'Images', 'action' => 'add'], ['escape' => false]) . '</li>';
            ?>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <?php foreach ($imageGroup->images as $image) { ?>
                <div class="col-md-55">
                    <div class="thumbnail">
                        <div class="image view">
                            <?= $this->AdminPanel->imageLink($image->id, 'style="width: 100%; display: block;"', true) ?>
                        </div>
                        <div class="caption">
                            <p><?= $image->name ?>
                            <ul class="nav panel_toolbox" style="display: inline-block; float: none;">
                                <?php
                                echo '<li>' . $this->Form->postLink('<i class="fa fa-remove"></i>',
                                        ['controller' => 'Images', 'action' => 'delete', $image->id], ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $image->id)]) . '</li>';
                                echo '<li>' . $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Images', 'action' => 'edit', $image->id], ['escape' => false]) . '</li>';
                                echo '<li>' . $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'Images', 'action' => 'view', $image->id], ['escape' => false]) . '</li>';
                                ?>
                            </ul>
                            </p>

                        </div>


                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>


