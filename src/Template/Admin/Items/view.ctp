<?php
$categories = array();
foreach ($item->get('categories') as $category) {
    array_push($categories, $category->title);
}
echo $this->element('Admin.Partials/crud_view', [
    'title' => h($item->title),
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $item->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $item->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-edit"></i>', 'url' => ['action' => 'edit', $item->id], 'options' => ['escape' => false]]
    ],
    'fields' => [
        ['name' => 'ID', 'value' => $this->Number->format($item->id)],
        ['name' => 'status', 'value' => $this->Number->format($item->status)],
        ['name' => 'Code', 'value' => h($item->code)],
        ['name' => 'Alias', 'value' => h($item->alias)],
        ['name' => 'Item Group', 'value' => $item->has('item_group') ? $this->Html->link($item->item_group->title, ['controller' => 'ItemGroups', 'action' => 'view', $item->item_group->id]) : ''],
        ['name' => 'Categories', 'value' => implode(', ', $categories)],
        ['name' => 'Vat Code', 'value' => h($item->vat_code)],
        ['name' => 'Stock', 'value' => $this->Number->format($item->stock)],
        ['name' => 'Weight', 'value' => $this->Number->format($item->weight) . ' kg'],
        ['name' => 'Created', 'value' => h($item->created)],
        ['name' => 'Modified', 'value' => h($item->modified)],
        ['name' => 'IsWebshop', 'value' => h($item->sales ? 'yes' : 'not')],
        ['name' => 'Priority', 'value' => $this->Number->format($item->priority)],
    ]
]);
?>

<div class="x_panel">
    <div class="x_title">
        <h2>Description</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($item->description)) {
            echo $this->Text->autoParagraph($item->description);
        } else {
            echo 'No description';
        }
        ?>
        <br/>
        <h2>English tags:</h2>
        <?php
        if (!empty($item->tags_english)) {
            echo $this->Text->autoParagraph($item->tags_english);
        } else {
            echo 'No description';
        }
        ?>

        <br/>
        <h2>German tags:</h2>
        <?php
        if (!empty($item->tags_german)) {
            echo $this->Text->autoParagraph($item->tags_german);
        } else {
            echo 'No description';
        }
        ?>

        <br/>
        <h2>French tags:</h2>
        <?php
        if (!empty($item->tags_french)) {
            echo $this->Text->autoParagraph($item->tags_french);
        } else {
            echo 'No description';
        }
        ?>
    </div>
</div>

<?php
$threads = array();
foreach ($item->item_prices as $itemPrices) {
    array_push($threads, [
        'id' => $itemPrices->id,
        'fields' => [
            h($itemPrices->id),
            h($itemPrices->default_unit),
            h($itemPrices->items_per_unit),
            h($itemPrices->quantity),
            h($itemPrices->price),
            h($itemPrices->start_date),
            h($itemPrices->end_date),
            h($itemPrices->created),
            h($itemPrices->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'Related Item Prices',
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add', 'controller' => 'ItemPrices'], 'options' => ['escape' => false]]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'default_unit', 'name' => 'Unit', 'width' => 300],
        ['model' => 'items_per_unit', 'name' => 'Items per Unit', 'width' => 300],
        ['model' => 'quantity', 'name' => 'Quantity', 'width' => 270],
        ['model' => 'price', 'name' => 'Price', 'width' => 200],
        ['model' => 'start_date', 'name' => 'Start Date', 'width' => 100],
        ['model' => 'end_date', 'name' => 'End Date', 'width' => 100],
        ['model' => 'created', 'name' => 'Created', 'width' => 100],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 100],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'controller' => 'ItemPrices', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'controller' => 'ItemPrices', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'controller' => 'ItemPrices', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
]);
?>

<?php if(!empty($item->image_group)) { ?>
<div class="x_panel">
    <div class="x_title">
        <h2>Related Images</h2>
        <ul class="nav navbar-right panel_toolbox">
            <?php
            echo '<li>' . $this->Html->link('<i class="fa fa-plus"></i>', ['controller' => 'Images', 'action' => 'add'], ['escape' => false]) . '</li>';
            ?>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <?php foreach ($item->image_group->images as $image) { ?>
                <div class="col-md-55">
                    <div class="thumbnail">
                        <div class="image view">
                            <?= $this->AdminPanel->imageLink($image->id, 'style="width: 100%; display: block;"', true) ?>
                        </div>
                        <div class="caption">
                            <p><?= $image->name ?>
                            <ul class="nav panel_toolbox" style="display: inline-block; float: none;">
                                <?php
                                echo '<li>' . $this->Form->postLink('<i class="fa fa-remove"></i>',
                                        ['controller' => 'Images', 'action' => 'delete', $image->id], ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $image->id)]) . '</li>';
                                echo '<li>' . $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Images', 'action' => 'edit', $image->id], ['escape' => false]) . '</li>';
                                echo '<li>' . $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'Images', 'action' => 'view', $image->id], ['escape' => false]) . '</li>';
                                ?>
                            </ul>
                            </p>

                        </div>


                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>
