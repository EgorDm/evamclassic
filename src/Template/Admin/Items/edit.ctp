<?php echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Edit Item',
    'actions' => [
        ['title' => '<i class="fa fa-trash"></i>', 'url' => ['action' => 'delete', $item->id],
            'options' => ['confirm' => __('Are you sure you want to delete # {0}?', $item->id), 'escape' => false], 'post' => true],
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $item, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left']],
    'inputs' => [
        ['model' => 'status', 'name' => 'Status',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'code', 'name' => 'Code', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'title', 'name' => 'Title', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'urls', 'name' => 'Urls',
            'options' => ['class' => 'form-control col-md-7 col-xs-12 raw', 'label' => false, 'type' => 'textarea', 'required' => false,]],
        ['model' => 'item_group_id', 'name' => 'Item Group', 'items' => $itemGroups,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'empty' => true], 'select' => true],
        ['model' => 'categories._ids', 'name' => 'Categories', 'items' => $categories,
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'id' => 'categories-ids', 'multiple' => true], 'select' => true],
        ['model' => 'image_group_id', 'name' => 'Image Group', 'items' => $imageGroups,
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'empty' => true, 'id' => 'image_groups'], 'select' => true],
        ['model' => 'vat_code', 'name' => 'VAT Code',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'stock', 'name' => 'Stock',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'priority', 'name' => 'Priority',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'weight', 'name' => 'Weight',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false]],
        ['model' => 'description', 'name' => 'Description',
            'options' => ['class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'textarea']],
        ['model' => 'tags_english', 'name' => 'Tags English',
            'options' => ['class' => 'form-control col-md-7 col-xs-12 raw', 'label' => false, 'type' => 'textarea']],
        ['model' => 'tags_german', 'name' => 'Tags German',
            'options' => ['class' => 'form-control col-md-7 col-xs-12 raw', 'label' => false, 'type' => 'textarea']],
        ['model' => 'tags_french', 'name' => 'Tags French',
            'options' => ['class' => 'form-control col-md-7 col-xs-12 raw', 'label' => false, 'type' => 'textarea']],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

echo $this->element('tinymce');

$this->start('custom_js'); ?>
<script>
$("#categories-ids").select2();
$("#image_groups").select2();
</script>
<?php $this->end();?>
