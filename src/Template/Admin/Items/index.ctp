<?php
$threads = array();
foreach ($items as $item) {
    $categories = array();
    foreach ($item->get('categories') as $category) {
        array_push($categories, $category->title);
    }
    array_push($threads, [
        'id' => $item->id,
        'fields' => [
            $this->Number->format($item->id),
            h($item->status),
            h($item->code),
            h($item->alias),
            h($item->title),
            $item->has('item_group') ? $this->Html->link($item->item_group->title, ['controller' => 'ItemGroups', 'action' => 'view', $item->item_group->id]) : '',
            implode(', ', $categories),
            $item->image_group_id,
            $this->Number->format($item->stock),
            $this->Number->format($item->weight) . ' kg',
            h($item->priority),
            h($item->created),
            h($item->modified)
        ]
    ]);
}

echo $this->element('Admin.Partials/crud_index', [
    'title' => 'All Items',
    'actions' => [
        ['title' => '<i class="fa fa-refresh"></i>', 'url' => ['action' => 'synchronize'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-bullseye"></i>', 'url' => ['action' => 'hardsynchronize'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-clone"></i>', 'url' => ['action' => 'hardsynchronize', 'group' => '3e148076-6d50-4304-b4b9-6e5ebb10357d'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-clone"></i>', 'url' => ['action' => 'hardsynchronize', 'group' => '3e6dff89-72bc-4aac-8185-cf3572503b13'], 'options' => ['escape' => false]],
        ['title' => '<i class="fa fa-download"></i>', 'url' => ['action' => 'exportExcel'], 'options' => ['escape' => false, 'target' => '_blank']]
    ],
    'headings' => [
        ['model' => 'id', 'name' => 'ID', 'width' => 100],
        ['model' => 'status', 'name' => 'Status', 'width' => 100],
        ['model' => 'code', 'name' => 'Code', 'width' => 150],
        ['model' => 'alias', 'name' => 'Alias', 'width' => 200],
        ['model' => 'title', 'name' => 'Title', 'width' => 200],
        ['model' => 'item_group_id', 'name' => 'Group', 'width' => 200],
        ['model' => 'category_id', 'name' => 'Category', 'width' => 200],
        ['model' => 'image_group_id', 'name' => 'Image', 'width' => 50],
        ['model' => 'stock', 'name' => 'Stock', 'width' => 50],
        ['model' => 'weight', 'name' => 'Weight', 'width' => 60],
        ['model' => 'priority', 'name' => 'Priority', 'width' => 50],
        ['model' => 'created', 'name' => 'Created', 'width' => 130],
        ['model' => 'modified', 'name' => 'Modified', 'width' => 130],
    ],
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $threads,
    'paginate' => true
]);

echo $this->element('Admin.Partials/crud_edit', [
    'title' => 'Import Excel data',
    'actions' => [
    ],
    'description' => '<p>Only fields "Partnummer" and "Gewicht" are currently supported to import data from. Partnummer is used to identify which model the values are assigned to.</p>',
    'form' => ['model' => false, 'options' => ['data-parsley-validate', 'class' => 'form-horizontal form-label-left', 'url' => ['action' => 'syncExcel'], 'type' => 'file']],
    'inputs' => [
        ['model' => 'excel_file', 'name' => 'Excel document', 'required' => true,
            'options' => ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12', 'label' => false, 'type' => 'file', 'accept' => '.xls,.xlsx']],
    ],
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);

?>
