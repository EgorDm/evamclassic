<?php

use Cake\View\View;

/**
 * @var View $this
 */

?>

    <div class="heading">
        <h3>Thanks for your order.</h3>
    </div>
    <p>Your order has been completed.</p>
<?php if ($bank_transfer) { ?>
    The payment conditions you will recieve in your email.
<?php } ?>
    <p>Your order id is #<?= $order_id ?></p>
<?= $this->Html->link('Order overview', ['action' => 'view', $order_id], ['class' => 'btn btn-primary']) ?>