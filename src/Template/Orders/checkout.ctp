<?php
use App\Utils\CheckoutSteps;
use App\Utils\DataUtils;
use App\Utils\PaymentMethods;
use Cake\View\View;
use Customer\Helper\Countries;

/**
 * @var View $this
 */
\Cake\Log\Log::debug(json_encode($order));

/** @var \App\Model\Entity\Order $order */

$this->start('sidebar_left'); ?>
<div class="checkout-progress-list">
    <h3 class="title">Your Checkout Progress</h3>
    <dl class="checkout-progress-list">
        <dt <?= (!empty($order->shipping_address)) ? 'class="completed"' : '' ?>>
            Shipping Address
            <?php if (!empty($order->shipping_address) && $order->local_status < CheckoutSteps::PAYMENT) {
                echo $this->Html->link('Edit',
                    ['action' => 'checkout', 'step' => CheckoutSteps::SHIPPING_ADDRESS], ['class' => 'btn btn-primary']);
            } ?>
        </dt>
        <?php if (!empty($order->shipping_address)) { ?>
            <dd class="completed">
                <address><?= $order->shipping_address->name ?><br>
                    <?= $order->shipping_address->street ?><br>
                    <?= $order->shipping_address->postcode . ' ' . $order->shipping_address->city ?><br>
                    <?= Countries::$countries[$order->shipping_address->country] ?><br>
                    T: <?= $order->shipping_address->phone ?>
                </address>
            </dd>
        <?php } else { ?>
            <dd>Pending</dd>
        <?php } ?>

        <dt <?= (!empty($order->billing_address)) ? 'class="completed"' : '' ?>>
            Billing Address
            <?php if (!empty($order->billing_address) && $order->local_status < CheckoutSteps::PAYMENT) {
                echo $this->Html->link('Edit',
                    ['action' => 'checkout', 'step' => CheckoutSteps::BILLING_ADDRESS], ['class' => 'btn btn-primary']);
            } ?>
        </dt>
        <?php if (!empty($order->billing_address)) { ?>
            <dd class="completed">
                <address><?= $order->billing_address->name ?><br>
                    <?= $order->billing_address->street ?><br>
                    <?= $order->billing_address->postcode . ' ' . $order->billing_address->city ?><br>
                    <?= Countries::$countries[$order->billing_address->country] ?><br>
                    T: <?= $order->billing_address->phone ?>
                </address>
            </dd>
        <?php } else { ?>
            <dd>Pending</dd>
        <?php } ?>

        <dt <?= ($order->local_status >= CheckoutSteps::SHIPPING_METHOD) ? 'class="completed"' : '' ?>>
            Shipping Method
            <?php if ($order->local_status >= CheckoutSteps::SHIPPING_METHOD && $order->local_status < CheckoutSteps::PAYMENT) {
                echo $this->Html->link('Edit', ['action' => 'checkout', 'step' => CheckoutSteps::SHIPPING_METHOD], ['class' => 'btn btn-primary']);
            } ?>
        </dt>
        <?php if ($order->local_status >= CheckoutSteps::SHIPPING_METHOD && $order->shipping_method) { ?>
            <dd class="completed">
                <?= $order->shipping_method->title/* . ' - ' . $order->shipping_option->min_weight
                . (($order->shipping_option->max_weight == null) ? '+' : ' - ' . $order->shipping_option->max_weight) . ' kg'*/ ?>
                <br>
                <span
                        class="price">
                    <?= \Cake\I18n\Number::currency(\App\Utils\DataUtils::round_up($order->shipping_option->price/1.21, 2), 'EUR') //  ($in_eu ? 1 : 1.21) shpping price adjusted?>
            </dd>
        <?php } else { ?>
            <dd>Pending</dd>
        <?php } ?>

        <dt <?= (isset($order->payment_method)) ? 'class="completed"' : '' ?>>
            Payment Method
            <?php if (isset($order->payment_method) && $order->local_status <= CheckoutSteps::OVERVIEW) {
                echo $this->Html->link('Edit',
                    ['action' => 'checkout', 'step' => CheckoutSteps::PAYMENT_METHOD], ['class' => 'btn btn-primary']);
            } ?>
        </dt>
        <?php if (isset($order->payment_method)) { ?>
            <dd class="completed">
                <?= \App\Utils\PaymentMethods::$payment_methods[$order->payment_method] ?? 'Pending' ?>
            </dd>
        <?php } else { ?>
            <dd>Pending</dd>
        <?php } ?>
    </dl>
</div>
<?php $this->end();
$this->start('custom_js'); ?>
<script>
    $(document).ready(function () {
        var address = $('#address_id');
        if (address) {
            if (address.val() == -1) {
                $('#address-section').collapse('show');
            }
            address.change(function () {
                if (address.val() == -1) {
                    $('#address-section').collapse('show');
                } else {
                    $('#address-section').collapse('hide');
                }
            });
        }

    });
</script>
<?php $this->end(); ?>

<div class="heading">
    <h3>Products</h3>
</div>
<ol class="checkout-progress">
    <li class="step <?php echo (!empty($order->shipping_address) ? 'completed' : '')
        . ' ' . (($step == CheckoutSteps::SHIPPING_ADDRESS) ? 'active' : '') ?>">
        <div class="step-title">
            <h2><span class="number">1</span> Shipping Address</h2>
            <?php if (!empty($order->shipping_address) && $order->local_status < CheckoutSteps::PAYMENT) {
                echo $this->Html->link('Edit',
                    ['action' => 'checkout', 'step' => CheckoutSteps::SHIPPING_ADDRESS], ['class' => 'btn btn-primary']);
            } ?>
        </div>
        <?php if ($step == CheckoutSteps::SHIPPING_ADDRESS) { ?>
            <div class="step-content">
                <?= $this->Form->create($order_form) ?>
                <p>Select from your address book or create a new address</p>
                <?= $this->Form->select('address_id', $addresses,
                    ['templateVars' => ['label' => 'Address: '], 'id' => 'address_id', 'value' => $address_selected]) ?>

                <div class="form-section-content collapse" id="address-section">
                    <h3 class="form-section">New Address</h3>
                    <?= $this->Form->input('contact-name', ['placeholder' => 'Contact Name', 'label' => 'Contact Name',
                        'data-validation-required-message' => 'Please enter a contact name for the address.']) ?>
                    <div class="row">
                        <div class="col-xs-8">
                            <?= $this->Form->input('address-line', ['placeholder' => 'Address', 'label' => 'Address',
                                'data-validation-required-message' => 'Please enter a valid address.']) ?>
                        </div>
                        <div class="col-xs-4">
                            <?= $this->Form->input('address-number', ['placeholder' => 'Number', 'label' => 'Number',
                                'data-validation-required-message' => 'Please enter a valid address number.']) ?>
                        </div>
                    </div>
                    <?= $this->Form->input('zip', ['placeholder' => 'ZIP/Postal code', 'label' => 'ZIP/Postal code',
                        'data-validation-required-message' => 'Please enter a valid ZIP/Postal code.']) ?>
                    <?= $this->Form->input('city', ['placeholder' => 'City', 'label' => 'City',
                        'data-validation-required-message' => 'Please enter your city.']) ?>
                    <?= $this->Form->select('country', Countries::$countries, ['templateVars' => ['label' => 'Country: ']]) ?>
                    <?= $this->Form->input('phone', ['placeholder' => 'Phone number', 'label' => 'Phone number',
                        'data-validation-required-message' => 'Please enter your phone number.', 'type' => 'tel']) ?>
                </div>

                <div class="control-group">
                    <div class="form-group">
                        <?= $this->Form->radio('use_for_billing', [
                            ['value' => 1, 'text' => 'Use this address for billing', 'checked'],
                            ['value' => 0, 'text' => 'Use a different address for billing'],
                        ]) ?>
                    </div>
                </div>

                <?= $this->Form->hidden('step', ['value' => CheckoutSteps::SHIPPING_ADDRESS]) ?>
                <?= $this->Form->submit('Next', ['class' => 'btn btn-primary']) ?>
                <?= $this->Form->end(); ?>
            </div>
        <?php } ?>
    </li>

    <li class="step <?php echo (!empty($order->billing_address) ? 'completed' : '')
        . ' ' . (($step == CheckoutSteps::BILLING_ADDRESS) ? 'active' : '') ?>">
        <div class="step-title">
            <h2><span class="number">2</span> Billing Address</h2>
            <?php if (!empty($order->billing_address) && $order->local_status < CheckoutSteps::PAYMENT) {
                echo $this->Html->link('Edit',
                    ['action' => 'checkout', 'step' => CheckoutSteps::BILLING_ADDRESS], ['class' => 'btn btn-primary']);
            } ?>
        </div>
        <?php if ($step == CheckoutSteps::BILLING_ADDRESS) { ?>
            <div class="step-content">
                <?= $this->Form->create($order_form) ?>
                <p>Select from your address book or create a new address</p>
                <?= $this->Form->select('address_id', $addresses,
                    ['templateVars' => ['label' => 'Address: '], 'id' => 'address_id', 'value' => $address_selected]) ?>

                <div class="form-section-content collapse" id="address-section">
                    <h3 class="form-section">New Address</h3>
                    <?= $this->Form->input('contact-name', ['placeholder' => 'Contact Name', 'label' => 'Contact Name',
                        'data-validation-required-message' => 'Please enter a contact name for the address.']) ?>
                    <div class="row">
                        <div class="col-xs-8">
                            <?= $this->Form->input('address-line', ['placeholder' => 'Address', 'label' => 'Address',
                                'data-validation-required-message' => 'Please enter a valid address.']) ?>
                        </div>
                        <div class="col-xs-4">
                            <?= $this->Form->input('address-number', ['placeholder' => 'Number', 'label' => 'Number',
                                'data-validation-required-message' => 'Please enter a valid address number.']) ?>
                        </div>
                    </div>
                    <?= $this->Form->input('zip', ['placeholder' => 'ZIP/Postal code', 'label' => 'ZIP/Postal code',
                        'data-validation-required-message' => 'Please enter a valid ZIP/Postal code.']) ?>
                    <?= $this->Form->input('city', ['placeholder' => 'City', 'label' => 'City',
                        'data-validation-required-message' => 'Please enter your city.']) ?>
                    <?= $this->Form->select('country', Countries::$countries, ['templateVars' => ['label' => 'Country: ']]) ?>
                    <?= $this->Form->input('phone', ['placeholder' => 'Phone number', 'label' => 'Phone number',
                        'data-validation-required-message' => 'Please enter your phone number.', 'type' => 'tel']) ?>
                </div>
                <?= $this->Form->hidden('step', ['value' => CheckoutSteps::BILLING_ADDRESS]) ?>
                <?= $this->Form->submit('Next', ['class' => 'btn btn-primary']) ?>
                <?= $this->Form->end(); ?>
            </div>
        <?php } ?>
    </li>

    <li class="step <?php echo ($order->local_status >= CheckoutSteps::SHIPPING_METHOD ? 'completed' : '')
        . ' ' . (($step == CheckoutSteps::SHIPPING_METHOD) ? 'active' : '') ?>">
        <div class="step-title">
            <h2><span class="number">3</span> Shipping Method</h2>
            <?php if ($order->local_status >= CheckoutSteps::SHIPPING_METHOD && $order->local_status < CheckoutSteps::PAYMENT) {
                echo $this->Html->link('Edit',
                    ['action' => 'checkout', 'step' => CheckoutSteps::SHIPPING_METHOD], ['class' => 'btn btn-primary']);
            } ?>
        </div>
        <?php if ($step == CheckoutSteps::SHIPPING_METHOD) { ?>
            <div class="step-content">
                <?= $this->Form->create() ?>
                <?php foreach ($shipping_methods as $key => $value) {
                    if ($key == 'selected') continue; ?>
                    <div class="radio">
                        <label>
                            <input type="radio" name="method" value="<?= $key ?>"
                                <?= ($order->shipping_method_id == $key) ? ' checked="checked"' : '' ?>>
                            <p>
                                <?= $value['title'] /*. ' ' . $value['category'] */ ?>&emsp;
                                <?= \Cake\I18n\Number::currency(\App\Utils\DataUtils::round_up($value['price']/1.21, 2), 'EUR') ?>
                            </p>
                        </label>
                    </div>
                <?php } ?>
                <?= $this->Form->hidden('step', ['value' => CheckoutSteps::SHIPPING_METHOD]) ?>
                <?= $this->Form->submit('Next', ['class' => 'btn btn-primary']) ?>
                <?= $this->Form->end(); ?>
            </div>
        <?php } ?>
    </li>

    <li class="step <?php echo (isset($order->payment_method) ? 'completed' : '')
        . ' ' . (($step == CheckoutSteps::PAYMENT_METHOD) ? 'active' : '') ?>">
        <div class="step-title">
            <h2><span class="number">4</span> Payment Method</h2>
            <?php if (isset($order->payment_method) && $order->local_status < CheckoutSteps::PAYMENT) {
                echo $this->Html->link('Edit',
                    ['action' => 'checkout', 'step' => CheckoutSteps::PAYMENT_METHOD], ['class' => 'btn btn-primary']);
            } ?>
        </div>
        <?php if ($step == CheckoutSteps::PAYMENT_METHOD) { ?>
            <div class="step-content">
                <?= $this->Form->create() ?>
                <?= $this->element('Payment.methods', ['order' => $order]) ?>
                <?= $this->Form->hidden('step', ['value' => CheckoutSteps::PAYMENT_METHOD]) ?>
                <?= $this->Form->submit('Next', ['class' => 'btn btn-primary']) ?>
                <?= $this->Form->end(); ?>
            </div>
        <?php } ?>
    </li>

    <li class="step <?php echo (!empty($order->local_status >= CheckoutSteps::OVERVIEW) ? 'completed' : '')
        . ' ' . (($step == CheckoutSteps::OVERVIEW) ? 'active' : '') ?>">
        <div class="step-title">
            <h2><span class="number">5</span> Overview</h2>
            <?php if ($order->local_status >= CheckoutSteps::OVERVIEW && $order->local_status < CheckoutSteps::PAYMENT) {
                echo $this->Html->link('View',
                    ['action' => 'checkout', 'step' => CheckoutSteps::OVERVIEW], ['class' => 'btn btn-primary']);
            } ?>
        </div>
        <?php if ($step == CheckoutSteps::OVERVIEW) { ?>
            <div class="step-content">
                <?= $this->Form->create() ?>
                <div class="checkout-overview">
                    <?= $this->element('Orders.details', ['order' => $order]) ?>
                </div>
                <?= $this->Form->hidden('step', ['value' => CheckoutSteps::OVERVIEW]) ?>
                <?= $this->Form->submit('Next', ['class' => 'btn btn-primary']) ?>
                <?= $this->Form->end(); ?>
            </div>
        <?php } ?>
    </li>

    <li class="step <?php echo (!empty($order->local_status >= CheckoutSteps::PAYMENT) ? 'completed' : '')
        . ' ' . (($step == CheckoutSteps::PAYMENT) ? 'active' : '') ?>">
        <div class="step-title">
            <h2><span class="number">6</span> Payment</h2>
        </div>
        <?php if ($step == CheckoutSteps::PAYMENT) { ?>
            <div class="step-content">
                <p>Please complete the payment, by transferring the full amount to the following bank account.</p>
                <dl >
                    <dt>Beneficiary</dt>
                    <dd><?= $wire_beneficiary->text_val ?></dd>

                    <dt>IBAN</dt>
                    <dd><?= $wire_iban->text_val ?></dd>

                    <dt>Reference</dt>
                    <dd><?= $order->id ?></dd>
                </dl>
                <?= $this->Html->link('Finish', ['action' => 'success', 'controller' => 'Orders', 'order' => $order_id], ['class' => 'btn btn-accent']) ?>
            </div>
        <?php } ?>
    </li>
</ol>
