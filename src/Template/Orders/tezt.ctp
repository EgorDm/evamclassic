<?php
use App\Utils\DataUtils;
use App\Utils\PaymentMethods;
use App\Utils\ShippingStatus;
use Cake\Log\Log;
use Cake\View\View;
use Customer\Helper\Countries;

/**
 * @var View $this
 */

?>
<div class="heading">
    <h3>Order info</h3>
</div>
<div class="checkout-overview">
    <div class="dashboard-block">
        <div class="block-title">
            <h3>Order status</h3>
        </div>
        <div class="block-content">
            <ul>
                <li>Payment status: <?= $payment_status ?></li>
                <li>Shipment status: <?= ShippingStatus::$status_names[$shipment_status] ?></li>
            </ul>
        </div>
    </div>
    <?= $this->element('Orders.details', ['order' => $order]) ?>
</div>
