﻿<?php
use Cake\View\View;

/**
 * @var View $this
 */

$description = getenv('TITLE');

?>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $description . ' - ' . $this->fetch('title') ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
<!--    <?/*= $this->Html->meta('icon') */?>-->
    <?= $this->Html->css(\Core\Helper\Website::resource('style.css')) ?>
    <?= $this->Html->css('lightbox.min.css') ?>
	<?= $this->Html->css('overrides.css') ?>
    <?= $this->Html->script('vendor/modernizr.js') ?>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script>
		window.addEventListener("load", function(){
			window.cookieconsent.initialise({
				"palette": {
					"popup": {
						"background": "#000"
					},
					"button": {
						"background": "#f47a20"
					}
				},
				"theme": "edgeless",
				"content": {
					"message": "By using this website you allow us to place cookies on your computer. They are harmless and never personally identify you.",
					"href": "/main/privacy"
				}
			})});
	</script>
</head>
<body>
<div class="content-wrapper">
    <div class="large-banner banner-top logo-banner">
        <div class="logo"></div>
        <?php if (\Core\Helper\Website::identifier() == \Core\Helper\Website::EVAM): ?>
        <?= $this->Html->image(\Core\Helper\Website::resource('SinceLogo.png')) ?>
        <?php else: ?>
            <img src="/img/site/vwsp/klassic_fab_banner.jpg" alt="">
        <?php endif; ?>
    </div>

    <?= $this->element('UI.Partials/navbar') ?>

    <div class="main-content container-fluid">
        <div class="banner large-banner">
            <?= $this->fetch('top_banner') ?>
        </div>

        <?= $this->Flash->render() ?>
        <div class="row">
            <?= $this->fetch('top_bar') ?>
            <div class="<?= !empty($wide) ? 'col-md-3' : 'col-md-2' ?>">
                <?= $this->element('UI.Partials/sidebar_left') ?>
            </div>
            <div style="overflow: hidden" class="<?= empty($hide) ? (!empty($wide) ? 'col-md-9' : 'col-md-8') : 'col-md-12' ?>">
                <div class="banner mid-banner">
                    <?= $this->fetch('mid_banner') ?>
                </div>

                <?= $this->fetch('content') ?>
            </div>

            <?php if (empty($wide)) { ?>
            <div class="col-md-2">
                <?= $this->element('UI.Partials/sidebar_right') ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<?= $this->element('UI.Partials/footer') ?>

<?= $this->Html->script('main_scripts') ?>
<?= $this->fetch('custom_category_js') ?>
<?= $this->Html->script('vendor/bootstrap_scripts') ?>
<?= $this->Html->script('lightbox.min.js') ?>
<?= $this->fetch('custom_js') ?>
<?= $this->fetch('modal') ?>
<?= $this->element('Partials/Layout/additional_css'); ?>


<?php
// Show blocking modal
if ($_GET['evaminternal']) {
    setcookie("evaminternal", "test", time()+30*24*60*60);
}

if (false && !$_COOKIE["evaminternal"] && isset($modal)) { ?>
    <div
        class="modal fade"
        id="myModal" tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel"
        data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="color: black">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title" id="myModalLabel"><?= $modal->title ?></h4>
                </div>
                <div class="modal-body">
                    <?= $modal->content ?>
                </div>
                <!--<div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Sluiten</button>
                </div>-->
            </div>
        </div>
    </div>
    <script>
        $('#myModal').modal('show')
    </script>
<?php } ?>


?>

<?php
if (!empty(\App\Utils\CacheUtils::getPreference('analytics')->text_val)) {
    ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?=\App\Utils\CacheUtils::getPreference('analytics')->text_val?>', 'auto');
        ga('send', 'pageview');
    </script>
<?php } ?>
<script>
    lightbox.option({
        'resizeDuration': 100,
        'wrapAround': true
    })
</script>
<style>
    .icon-tikkie {
        width: 114px;
        height: 52px;
        background: url(/img/payment_methods/tikkie.jpg) no-repeat;
        overflow: hidden;
        display: inline-block;
        background-size: cover;
    }
    .icon-betaalverzoek {
        width: 97px;
        height: 52px;
        background: url(/img/payment_methods/betaalverzoek.jpg) no-repeat;
        overflow: hidden;
        display: inline-block;
        background-size: cover;
    }
</style>

</body>
</html>
