<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title><?= ((!empty($description) ? $description : '') . ' - ' . $this->fetch('title')) ?></title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('main.css') ?>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                    <p>This page you are looking for does not exist <a href="#">Report this?</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('vendor/bootstrap') ?>
</body>
</html>