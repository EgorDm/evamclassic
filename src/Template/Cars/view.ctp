<?php
use Cake\View\View;

/**
 * @var View $this
 */

$slider_items = array();
$slider_items_big = array();
foreach ($car->image_group['images'] as $image) {
    array_push($slider_items, '<div>' . $this->Html->image('uploads/' . $image->path, ['alt' => $image->name]) . '</div>');
    array_push($slider_items_big, '<div><a href="/img/uploads/'.$image->path.'" data-lightbox="'. $car->reference .'">' . $this->Html->image('uploads/' . $image->path, ['alt' => $image->name]) . '</a></div>');
}

$this->assign('title', $car->title);

$this->start('custom_js'); ?>
<script>
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        infinite: true,
        dots: true,
        centerMode: true,
        variableWidth: true,
        focusOnSelect: true
    });
</script>
<?php $this->end();
$this->start('top_banner'); ?>
<div class="banner-img"
     style="
         background: url('<?php echo $this->Url->build('/img/volkswagen_ship.jpg'); ?>') no-repeat fixed; background-position: center -80px;"></div>
<?php $this->end();?>

<div class="heading">
    <h3><?= $car->title ?></h3>
</div>
<div class="sub-heading">
    <a href="" class="float-left"> back to overview</a>
    <div class="float-right">
        <a href=""> previous </a> - <a href=""> next </a>
    </div>
</div>
<div class="row product-detail">
    <div class="col-md-6 product-carousel">
        <div class="slider slider-for">
            <?php foreach ($slider_items_big as $slide) {
                echo $slide;
            } ?>
        </div>
        <div class="slider slider-nav">
            <?php foreach ($slider_items as $slide) {
                echo $slide;
            } ?>
        </div>
    </div>
    <div class="col-md-6">
        <h3>Description</h3>
        <p><?= $car->description ?></p>

        <div class="purchase-box">
            <div class="row">
                <div class="col-xs-12"><h3><?= \Cake\I18n\Number::currency($car->price, 'EUR') ?></h3></div>
                <div>
                    <div class="col-xs-4">Reference</div>
                    <div class="col-xs-8"><?= $car->reference ?></div>
                </div>
                <div>
                    <div class="col-xs-4">Category</div>
                    <div class="col-xs-8"><?= $car->category ?></div>
                </div>
                <div>
                    <div class="col-xs-4">Model</div>
                    <div class="col-xs-8"><?= $car->model ?></div>
                </div>
                <div>
                    <div class="col-xs-4">Year</div>
                    <div class="col-xs-8"><?= $car->year ?></div>
                </div>
                <div>
                    <div class="col-xs-4">Color</div>
                    <div class="col-xs-8"><?= $car->color ?></div>
                </div>
                <div>
                    <div class="col-xs-4">Condition</div>
                    <div class="col-xs-8"><?= $car->condition ?></div>
                </div>
                <div>
                    <div class="col-xs-4">Availability</div>
                    <div class="col-xs-8">
                        <?php if ($car->status == 0) {
                            echo '<span class="text-success">In stock</span>';
                        } else {
                            echo '<span class="text-danger">Sold</span>';
                        }?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php if ($car->stock == 0) { ?>
                    <hr>
                    <div class="col-xs-12">
                        <?= $this->Html->link('Contact us', ['controller' => 'Main', 'action' => 'contact'],
                            ['class' => 'btn btn-primary pull-left', 'escape' => false]) ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>