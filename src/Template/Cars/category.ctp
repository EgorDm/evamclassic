<?php

use Cake\View\View;

/**
 * @var View $this
 */
$this->assign('title', $title);
$this->start('top_banner'); ?>
<div class="banner-img"
     style="
             background: url('<?php echo $this->Url->build('/img/volkswagen_ship.jpg'); ?>') no-repeat fixed; background-position: center -80px;"></div>
<?php $this->end(); ?>

<div class="heading">
    <h3><?= $title_category ?></h3>
</div>

<div class="row products-grid car">
    <?php foreach ($cars as $car) { ?>
        <div class="col-lg-3 col-md-6">
            <div class="product">
                <a href="/img/uploads/<?= $car->thumbnail[0] ?>" data-lightbox="<?= $car->reference ?>">
                    <?= $this->Html->image('uploads/thumbnail_medium/' . $car->thumbnail[0], ['alt' => $car->thumbnail[1]]) ?>
                </a>
                <div class="product-view">
                    <div class="info ">
                        <small><?= $car->reference ?></small>
                        <h4 class="title"><?= $car->title ?></h4>
                        <?php if ($car->status == 0) {
                            echo "<h5>In stock</h5>";
                        } else {
                            echo "<h5 class='text-danger'>Sold</h5>";
                        } ?>
                    </div>
                    <div class="btn-group actions">
                        <?php
                        if ($car->status == 0) { ?>
                            <a class="btn btn-default"><?= \Cake\I18n\Number::currency($car->price, 'EUR') ?></a>
                        <?php } ?>
                        <?= $this->Html->link('More Info', ['action' => 'view', 'controller' => 'Cars', $car->alias],
                            ['class' => 'btn btn-default', 'rel' => 'View ' . $car->title]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<div class="text-center">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
</div>
