<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acties') ?></li>
        <li><?= $this->Html->link(__('Artikelen bekijken'), ['controller' => 'ExactTest', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Bestellingen bekijken'), ['controller' => 'ExactTest', 'action' => 'orders']) ?></li>
        <li><?= $this->Html->link(__('Customers'), ['controller' => 'Customers', 'prefix' => 'admin', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artikelen index large-9 medium-8 columns content">
    <h3><?= __('Bestellingen') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th>Koper</th>
            <th>Totaal bedrag</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($orders as $order): ?>
            <tr>
                <td><?= h($order->OrderedBy) ?></td>
                <td><?= h($order->AmountDC . ' EUR') ?></td>
                <td><?php
                    switch ($order->Status) {
                        case 20:
                            echo 'Partial';
                            break;
                        case 12:
                            echo 'Open';
                            break;
                        default:
                            echo 'In verwerking';
                            break;
                    }
                    ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
