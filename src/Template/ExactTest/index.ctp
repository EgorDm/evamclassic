<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acties') ?></li>
        <li><?= $this->Html->link(__('Artikelen bekijken'), ['controller' => 'ExactTest', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Bestellingen bekijken'), ['controller' => 'ExactTest', 'action' => 'orders']) ?></li>
        <li><?= $this->Html->link(__('Customers'), ['controller' => 'Customers', 'prefix' => 'admin', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artikelen index large-9 medium-8 columns content">
    <h3><?= __('Artikelen') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th>Titel</th>
            <th>Code</th>
            <th>Beschrijving</th>
            <th>Prijs excl.</th>
            <th>Prijs incl.</th>
            <th>Magazijn</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($items as $item): ?>
            <tr>
                <td><?= h($item->Description) ?></td>
                <td><?= h($item->Code) ?></td>
                <td><?= h($item->Notes) ?></td>
                <td><?= h($item->CostPriceStandard . ' EUR') ?></td>
                <td><?= h((($item->SalesVatCode == 2) ? round($item->CostPriceStandard * 1.21, 2) :  round($item->CostPriceStandard * 1.06, 2)) . ' EUR') ?></td>
                <td><?= h($item->Stock) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
