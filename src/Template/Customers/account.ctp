<?php
use Cake\View\View;
use Customer\Helper\Countries;

/**
 * @var View $this
 */

$address_billing = 'You have not set a default billing address.';
$billing_id = null;
$address_shipping = 'You have not set a default shipping address.';
$shipping_id = null;
if (!empty($addresses)) {
    if (count($addresses) == 1) {
        foreach ($addresses as $address) {
            $billing_id = $address->get('id');
            $shipping_id = $address->get('id');
            $address_billing = $address->get('name') . '<br>'
                . $address->get('street') . '<br>'
                . $address->get('postcode') . ', ' . $address->get('city') . '<br>'
                . Countries::$countries[$address->get('country')] . '<br>'
                . 'T: ' . $address->get('phone');

        }
        $address_shipping = $address_billing;
    } else {
        foreach ($addresses as $address) {
            $billing = false;
            $shipping = false;
            if ($address->get('type') == 2) {
                $shipping = true;
                $shipping_id = $address->get('id');
                $address_shipping = $address->get('name') . '<br>'
                    . $address->get('street') . '<br>'
                    . $address->get('postcode') . ', ' . $address->get('city') . '<br>'
                    . Countries::$countries[$address->get('country')] . '<br>'
                    . 'T: ' . $address->get('phone');
            } else if ($address->get('type') == 1) {
                $billing = true;
                $billing_id = $address->get('id');
                $address_billing = $address->get('name') . '<br>'
                    . $address->get('street') . '<br>'
                    . $address->get('postcode') . ', ' . $address->get('city') . '<br>'
                    . Countries::$countries[$address->get('country')] . '<br>'
                    . 'T: ' . $address->get('phone');
            } else {
                $temp = $address->get('name') . '<br>'
                    . $address->get('street') . '<br>'
                    . $address->get('postcode') . ', ' . $address->get('city') . '<br>'
                    . Countries::$countries[$address->get('country')] . '<br>'
                    . 'T: ' . $address->get('phone');

                if (!$billing) {
                    $billing_id = $address->get('id');
                    $address_billing = $temp;
                }
                if (!$shipping) {
                    $shipping_id = $address->get('id');
                    $address_shipping = $temp;
                }
            }
        }
    }
}

$this->start('sidebar_left'); ?>
<h3 class="title">Actions</h3>
<ul class="sidebar">
    <li><?= $this->Html->link('Edit Account Info', ['action' => 'edit', 'controller' => 'Customers']); ?></li>
    <li><?= $this->Html->link('My orders', ['action' => 'orders', 'controller' => 'Customers']); ?></li>
</ul>
<?php
$this->end(); ?>

<div class="heading">
    <h3>Account overview</h3>
</div>

<div class="dashboard-blocks">
    <div class="dashboard-block">
        <h2>Hello <?= $fullname ?></h2>
    </div>
    <div id="account-info" class="dashboard-block">
        <div class="block-title">
            <h3>Account Info</h3>
        </div>
        <div class="block-content">
            <dl>
                <dt>Name</dt>
                <dd><?= $fullname ?></dd>

                <dt>Email</dt>
                <dd><?= $customer_email ?></dd>

                <dt>Password</dt>
                <dd><?= $this->Html->link('Change Password', ['action' => 'edit', 'controller' => 'Customers', 'change_pw' => true]) ?></dd>

                <?php if(!empty($company)) {?>
                    <dt>Company</dt>
                    <dd><?= $company ?></dd>
                    <dt>VAT Code</dt>
                    <dd><?= $vat_code ?></dd>
                <?php } ?>
            </dl>
        </div>
        <?= $this->Html->link('Edit Account Info', ['action' => 'edit', 'controller' => 'Customers'], ['class' => 'btn btn-accent']) ?>
    </div>
    <div id="account-orders" class="dashboard-block">
        <div class="block-title">
            <h3>My orders</h3>
        </div>
        <div class="block-content">
            <?php if(empty($orders) || count($orders) == 0) { ?>
                <p>You have not placed any orders yet.</p>
            <?php } else { ?>
                <table class="table orders">
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach ($orders as $order) { ?>
                    <tr>
                        <td><?= $order->id ?></td>
                        <td><?= $order->created ?></td>
                        <td>
                            <?php
                            if($order->local_status < \App\Utils\CheckoutSteps::PAYMENT) {
                                echo 'Uncompleted';
                            } else if($order->local_status == \App\Utils\CheckoutSteps::PAYMENT || $order->local_status == \App\Utils\CheckoutSteps::PMNT_OPEN) {
                                echo 'Waiting for payment';
                            } else if($order->local_status == \App\Utils\CheckoutSteps::PMNT_PAID) {
                                echo 'Paid';
                            } else if($order->local_status == \App\Utils\CheckoutSteps::PMNT_PENDING) {
                                echo 'Payment pending';
                            } else {
                                echo 'Failed';
                            }
                            ?>
                        </td>
                        <td>
                            <?= $this->Html->link('View', ['action' => 'view', 'controller' => 'Orders', $order->id]) ?>
                            <?php if($order->local_status <= \App\Utils\CheckoutSteps::PAYMENT) {
                                echo '<br>'.$this->Html->link('Continue', ['action' => 'continueOrder', 'controller' => 'Orders', $order->id]);
                            }?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            <?php } ?>
        </div>
        <?= $this->Html->link('View all', ['action' => 'orders', 'controller' => 'Customers'], ['class' => 'btn btn-accent']) ?>
    </div>
    <div id="address-book" class="dashboard-block">
        <div class="block-title">
            <h3>Address Book</h3>
        </div>

        <div class="block-content">
            <ul class="address-list">
                <li>
                    <h5>Default Billing Address</h5>
                    <address><?= $address_billing ?></address>
                    <?= $this->Html->link('Edit',
                        ['action' => 'editAddress', 'controller' => 'Customers', $billing_id], ['class' => 'btn btn-sm btn-primary']) ?>
                </li>
                <li>
                    <h5>Default Shipping Address</h5>
                    <address><?= $address_shipping ?></address>
                    <?= $this->Html->link('Edit',
                        ['action' => 'editAddress', 'controller' => 'Customers', $shipping_id], ['class' => 'btn btn-sm btn-primary']) ?>
                </li>
            </ul>
        </div>
        <?php
        if (count($addresses) < 2) {
            echo $this->Html->link('Add Address', ['action' => 'editAddress', 'controller' => 'Customers'], ['class' => 'btn btn-accent']);
        }
        ?>
    </div>
</div>
