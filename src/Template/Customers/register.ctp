<?php
use Cake\View\View;
use Customer\Helper\Countries;

/**
 * @var $this View
 */
$this->start('custom_js'); ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<?php $this->end(); ?>

<div class="heading">
    <h3>Register</h3>
</div>
<?= $this->Flash->render('auth') ?>
<div class="row">
    <div class="col-md-8">
        <?= $this->Form->create($customer) ?>
        <h3 class="form-section">Account Info</h3>
        <?= $this->Form->input('name', ['placeholder' => 'Full Name', 'required', 'label' => 'Full Name',
            'data-validation-required-message' => 'Please enter your full name.']) ?>
        <?= $this->Form->input('email', ['placeholder' => 'Email address', 'required',
            'data-validation-required-message' => 'Please enter your email address.']) ?>
        <?= $this->Form->input('password', ['placeholder' => 'Password', 'required',
            'data-validation-required-message' => 'Please enter your password.']) ?>
        <?= $this->Form->input('password-confirm', ['placeholder' => 'Confirm Password', 'required', 'label' => 'Confirm Password',
            'data-validation-required-message' => 'Please confirm your password.', 'type' => 'password']) ?>

        <?= $this->Form->checkbox('is-company', ['templateVars' => ['label' => 'This is a company account.'],
            'hiddenField' => false, 'default' => false]) ?>
        <div class="collapse form-section-content" id="company-section">
            <h3 class="form-section">Company Info</h3>
            <?= $this->Form->input('company', ['placeholder' => 'Company name', 'label' => 'Company name',
                'data-validation-required-message' => 'Please enter a Company name.']) ?>
            <?= $this->Form->input('vat_code', ['placeholder' => 'VAT code', 'label' => 'VAT code',
                'data-validation-required-message' => 'Please enter a VAT code.']) ?>
        </div>

        <?= $this->Form->checkbox('use-address', ['templateVars' => ['label' => 'Add shipment address'],
            'hiddenField' => false, 'default' => false]) ?>
        <div class="collapse form-section-content" id="address-section">
            <h3 class="form-section">Shipment Info</h3>
            <?= $this->Form->input('contact-name', ['placeholder' => 'Contact Name', 'label' => 'Contact Name',
                'data-validation-required-message' => 'Please enter a contact name for the address.']) ?>
            <div class="row">
                <div class="col-xs-8">
                    <?= $this->Form->input('address', ['placeholder' => 'Address', 'label' => 'Address',
                        'data-validation-required-message' => 'Please enter a valid address.']) ?>
                </div>
                <div class="col-xs-4">
                    <?= $this->Form->input('address-number', ['placeholder' => 'Number', 'label' => 'Number',
                        'data-validation-required-message' => 'Please enter a valid address number.']) ?>
                </div>
            </div>
            <?= $this->Form->input('zip', ['placeholder' => 'ZIP/Postal code', 'label' => 'ZIP/Postal code',
                'data-validation-required-message' => 'Please enter a valid ZIP/Postal code.']) ?>
            <?= $this->Form->input('city', ['placeholder' => 'City', 'label' => 'City',
                'data-validation-required-message' => 'Please enter your city.']) ?>
            <?= $this->Form->select('country', Countries::$countries, ['templateVars' => ['label' => 'Country: ']]) ?>
            <?= $this->Form->input('phone', ['placeholder' => 'Phone number', 'label' => 'Phone number',
                'data-validation-required-message' => 'Please enter your phone number.', 'type' => 'tel']) ?>
        </div>
	    <div style="margin-bottom: 8px; font-size: 12px">
		    <div id="tos-error" style="font-size: 16px; color: darkred; display: none">You must agree with the terms and condition to proceed to checkout.</div>
		    <div class="checkbox-input">
			    <input type="checkbox" name="tos" id="tos" value="1">
                <?php if(\Core\Helper\Website::identifier() == \Core\Helper\Website::EVAM): ?>
                    <label for="tos">I agree with <a href="/main/privacy" style="color: gainsboro">privacy policy</a> and
                        <a href="/main/terms" style="color: gainsboro">terms and conditions</a>.</label>
                <?php else: ?>
                    <label for="tos">I agree with <a href="/main/privacy">privacy policy</a> and
                        <a href="/main/terms">terms and conditions</a>.</label>
                <?php endif; ?>
		    </div>
	    </div>
        <div class="form-buttons">
            <?= $this->Recaptcha->display() ?>
            <?= $this->Form->submit('Submit', ['class' => 'btn btn-accent btn-lg', 'id' => 'submitRegister']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="col-md-4">
        <h3>Already have an account?</h3>
        <p>Please login here.</p>
        <?= $this->Html->link('Login', ['action' => 'login', 'controller' => 'Customers'],
            ['class' => 'btn btn-accent btn-lg']) ?>
    </div>
</div>

<?php $this->start('custom_js'); ?>
	<script>
		$('#submitRegister').on('click', function(event) {
			if(!$('#tos').is(':checked')) {
				event.preventDefault();
				$('#tos-error').show();
			}
		});

		$(document).ready(function () {
			if ($('#use-address').is(":checked")) {
				$('#address-section').collapse('show');
			}
			$("#use-address").change(function () {
				if (this.checked) {
					$('#address-section').collapse('show');
				} else {
					$('#address-section').collapse('hide');
				}
			});

			if ($('#is-company').is(":checked")) {
				$('#company-section').collapse('show');
			}
			$("#is-company").change(function () {
				if (this.checked) {
					$('#company-section').collapse('show');
				} else {
					$('#company-section').collapse('hide');
				}
			});
		});
	</script>
<?php $this->end(); ?>
