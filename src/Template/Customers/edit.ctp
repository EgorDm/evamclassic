<?php
use Cake\View\View;

/**
 * @var View $this
 */

$this->start('custom_js'); ?>
<script>
    $(document).ready(function () {
        if ($('#change-password').is(":checked")) {
            $('#password-section').collapse('show');
        }
        $("#change-password").change(function () {
            if (this.checked) {
                $('#password-section').collapse('show');
            } else {
                $('#password-section').collapse('hide');
            }
        });

        if ($('#is-company').is(":checked")) {
            $('#company-section').collapse('show');
        }
        $("#is-company").change(function () {
            if (this.checked) {
                $('#company-section').collapse('show');
            } else {
                $('#company-section').collapse('hide');
            }
        });

    });
</script>
<?php $this->end(); ?>

<div class="heading">
    <h3>Account Info</h3>
</div>
<div class="row">
    <div class="col-md-8">
        <?= $this->Form->create($form) ?>
        <h3 class="form-section">Account Info</h3>
        <?= $this->Form->input('name', ['placeholder' => 'Full Name', 'required', 'label' => 'Full Name',
            'data-validation-required-message' => 'Please enter your full name.']) ?>

        <?= $this->Form->checkbox('is-company', ['templateVars' => ['label' => 'This is a company account.'],
            'hiddenField' => false, 'default' => false]) ?>
        <div class="collapse form-section-content" id="company-section">
            <h3 class="form-section">Company Info</h3>
            <?= $this->Form->input('company', ['placeholder' => 'Company name', 'label' => 'Company name',
                'data-validation-required-message' => 'Please enter a Company name.']) ?>
            <?= $this->Form->input('vat_code', ['placeholder' => 'VAT code', 'label' => 'VAT code',
                'data-validation-required-message' => 'Please enter a VAT code.']) ?>
        </div>

        <?= $this->Form->checkbox('change-password', ['templateVars' => ['label' => 'Change password'],
            'hiddenField' => false, 'default' => false]) ?>
        <div class="collapse form-section-content" id="password-section">
            <h3 class="form-section">Change Password</h3>
            <?= $this->Form->input('current-password', ['placeholder' => 'Current Password', 'label' => 'Current Password',
                'data-validation-required-message' => 'Please enter your current password.', 'type' => 'password']) ?>
            <?= $this->Form->input('new-password', ['placeholder' => 'New Password', 'label' => 'New Password',
                'data-validation-required-message' => 'Please enter your new password.', 'type' => 'password']) ?>
            <?= $this->Form->input('confirm-password', ['placeholder' => 'Confirm Password', 'label' => 'Confirm Password',
                'data-validation-required-message' => 'Please confirm enter your new password.', 'type' => 'password']) ?>
        </div>
        <div class="form-buttons">
            <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary btn-lg']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

