<?php
use Cake\View\View;

/**
 * @var View $this
 */

$this->start('sidebar_left'); ?>
<h3 class="title">Actions</h3>
<ul class="sidebar">
    <li><?= $this->Html->link('Edit Account Info', ['action' => 'edit', 'controller' => 'Customers']); ?></li>
    <li><?= $this->Html->link('My orders', ['action' => 'orders', 'controller' => 'Customers']); ?></li>
</ul>
<?php
$this->end(); ?>

<div class="heading">
    <h3>My orders</h3>
</div>

<div class="dashboard-blocks">
    <div id="account-orders" class="dashboard-block">
        <div class="block-title">
            <h3>My orders</h3>
        </div>
        <div class="block-content">
            <?php if(empty($orders) || count($orders) == 0) { ?>
                <p>You have not placed any orders yet.</p>
            <?php } else { ?>
                <table class="table orders">
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach ($orders as $order) { ?>
                    <tr>
                        <td><?= $order->id ?></td>
                        <td><?= $order->created ?></td>
                        <td>
                            <?php
                            if($order->local_status < \App\Utils\CheckoutSteps::PAYMENT) {
                                echo 'Uncompleted';
                            } else if($order->local_status == \App\Utils\CheckoutSteps::PAYMENT || $order->local_status == \App\Utils\CheckoutSteps::PMNT_OPEN) {
                                echo 'Waiting for payment';
                            } else if($order->local_status == \App\Utils\CheckoutSteps::PMNT_PAID) {
                                echo 'Paid';
                            } else if($order->local_status == \App\Utils\CheckoutSteps::PMNT_PENDING) {
                                echo 'Payment pending';
                            } else {
                                echo 'Failed';
                            }
                            ?>
                        </td>
                        <td>
                            <?= $this->Html->link('View', ['action' => 'view', 'controller' => 'Orders', $order->id]) ?>
                            <?php if($order->local_status <= \App\Utils\CheckoutSteps::PAYMENT) {
                                echo '<br>'.$this->Html->link('Continue', ['action' => 'continueOrder', 'controller' => 'Orders', $order->id]);
                            }?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            <?php } ?>
        </div>
    </div>
</div>
