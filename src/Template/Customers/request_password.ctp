<?php
use Cake\View\View;

/**
 * @var $this View
 */

$this->start('custom_js'); ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<?php $this->end(); ?>

<div class="heading">
    <h3>Reset password</h3>
</div>
<div class="row">
    <div class="col-md-8">
        <?= $this->Form->create(null, ['type' => 'get']) ?>
        <?= $this->Form->input('email', ['placeholder' => 'Email address', 'required',
            'data-validation-required-message' => 'Please enter your email address.']) ?>
        <div class="form-buttons">
            <?= $this->Recaptcha->display() ?>
            <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary btn-lg']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
