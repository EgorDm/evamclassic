<?php
use Cake\View\View;

/**
 * @var $this View
 */

?>

<div class="heading">
    <h3>Login</h3>
</div>
<?= $this->Flash->render('auth') ?>
<div class="row">
    <div class="col-md-4 col-md-offset-1">
        <?= $this->Form->create() ?>
        <?= $this->Form->input('email', ['placeholder' => 'Email address', 'required',
            'data-validation-required-message' => 'Please enter your email address.']) ?>
        <?= $this->Form->input('password', ['placeholder' => 'Password', 'required',
            'data-validation-required-message' => 'Please enter your password.']) ?>
        <div class="form-buttons">
            <?= $this->Form->submit('Submit', ['class' => 'btn btn-accent btn-lg']) ?>
            <?= $this->Html->link('Forgot password?', ['action' => 'requestPassword', 'controller' => 'Customers'],
                ['class' => 'btn btn-clear btn-lg']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="col-md-4 col-md-offset-1">
        <h3>Need an account?</h3>
        <p>Make a new one here.</p>
        <?= $this->Html->link('Register', ['action' => 'register', 'controller' => 'Customers'],
            ['class' => 'btn btn-accent btn-lg']) ?>
    </div>
</div>
