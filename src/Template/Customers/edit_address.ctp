<?php
use Cake\View\View;
use Customer\Helper\Countries;

/**
 * @var $this View
 */

?>

<div class="heading">
    <h3>Account Info</h3>
</div>
<div class="row">
    <div class="col-md-8">
        <?= $this->Form->create($address) ?>

        <?= $this->Form->input('name', ['placeholder' => 'Contact Name', 'label' => 'Contact Name',
            'data-validation-required-message' => 'Please enter a contact name for the address.']) ?>
        <?= $this->Form->input('street', ['placeholder' => 'Address', 'label' => 'Address',
            'data-validation-required-message' => 'Please enter a valid address.']) ?>
        <?= $this->Form->input('postcode', ['placeholder' => 'ZIP/Postal code', 'label' => 'ZIP/Postal code',
            'data-validation-required-message' => 'Please enter a valid ZIP/Postal code.']) ?>
        <?= $this->Form->input('city', ['placeholder' => 'City', 'label' => 'City',
            'data-validation-required-message' => 'Please enter your city.']) ?>
        <?= $this->Form->select('country', Countries::$countries, ['templateVars' => ['label' => 'Country: ']]) ?>
        <?= $this->Form->input('phone', ['placeholder' => 'Phone number', 'label' => 'Phone number',
            'data-validation-required-message' => 'Please enter your phone number.', 'type' => 'tel']) ?>

        <?= $this->Form->checkbox('use-shipping', ['templateVars' => ['label' => 'Use as shipping address'],
            'hiddenField' => false, 'default' =>
                (!empty($address->type) && ($address->type == 2 || $address->type == 3)) ? true : false]) ?>
        <?= $this->Form->checkbox('use-billing', ['templateVars' => ['label' => 'Use as billing address'],
            'hiddenField' => false, 'default' =>
                (!empty($address->type) && ($address->type == 1 || $address->type == 3)) ? true : false]) ?>
        <div class="form-buttons">
            <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary btn-lg']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
