<?php
use Cake\View\View;

/**
 * @var $this View
 */

?>

<div class="heading">
    <h3>Change password</h3>
</div>
<div class="row">
    <div class="col-md-8">
        <?= $this->Form->create(null) ?>
        <?= $this->Form->input('password', ['placeholder' => 'New password', 'required',
            'data-validation-required-message' => 'Please enter your new password.', 'type' => 'password']) ?>
        <?= $this->Form->input('password-confirm', ['placeholder' => 'Password confirm', 'required',
            'data-validation-required-message' => 'Please enter your new password again.', 'type' => 'password']) ?>
        <div class="form-buttons">
            <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary btn-lg']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
