<?php

use Cake\Routing\Router;
use Cake\View\View;

/**
 * @var View $this
 */
$this->assign('title', $title);

//$this->start('top_banner');
//<div style="position: relative; width: 100%">
//    <div class="banner-img"
/*         style="background: url('<?php echo $this->Url->build('/img/parts_pattern.jpg'); ?>') repeat"></div>*/
//    <div
/*            style="background: url('<?php echo $this->Url->build('/img/parts_logo.svg'); ?>') no-repeat; height: 140px; width: 120px; position: absolute; right: 40%; bottom: 0"></div>*/
//</div>
//$this->end();
$this->start('custom_js'); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js"></script>
<script>
    $(function () {
        $('.product').matchHeight();
    });
</script>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "ItemList",
        "url": "<?= Router::url(null, true) ?>",
        "numberOfItems": "<?= count($items) ?>",
        "itemListElement": [
<?php
$i = 0;
foreach ($items as $item) {
$i++;
$vat = 1;
if (!empty($item->vat_code_model['percentage']) && $tax) {
    $vat += $item->vat_code_model['percentage'];
}
?>
    {
        "@context": "http://www.schema.org",
        "@type": "Product",
        "name": "<?= $item->title ?>",
        "sku": "<?= $item->code ?>",
        "productID": "<?= $item->code ?>",
        "url": "<?= Router::url(['action' => 'view', 'controller' => 'Products', $item->alias], true) ?>",
<?php if(!empty($item->image_group['images'])) { ?>
        "image": "<?= Router::url('img/uploads/' .  $item->image_group['images'][0]->path, true) ?>",
<?php } ?>
        "description": "<?= $item->description ?>",
        "offers": {
            "@type": "Offer",
            "priceCurrency": "EUR",
            "price": "<?= $item->price * $vat ?>",
            "availability": "http://schema.org/InStock",
            "url": "<?= Router::url(['action' => 'view', 'controller' => 'Products', $item->alias], true) ?>"
        }
    }
<?php
if($i < count($items)) echo ',';
}
?>
        ]
    }
</script>
<?php $this->end(); ?>

<div class="heading">
    <h3><?= $title_category ?></h3>
</div>

<div class="text-center">
	<ul class="pagination">
		<?= $this->Paginator->prev('< ' . __('previous')) ?>
		<?= $this->Paginator->numbers() ?>
		<?= $this->Paginator->next(__('next') . ' >') ?>
	</ul>
</div>

<div class="row products-grid">
    <?php foreach ($items as $item) { ?>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product">
                <a href="/img/uploads/<?= $item->thumbnail[0] ?>" data-lightbox="<?= $item->code ?>">
                    <?= $this->Html->image('uploads/thumbnail_medium/' . $item->thumbnail[0], ['alt' => $item->thumbnail[1]]) ?>
                </a>
                <div class="product-view">
                    <div class="info">
                        <small><?= $item->code ?></small>
                        <h4 class="title"><?= $item->title ?></h4>
                        <?php if ($item->stock > 0) {
                            //echo "<h5>Stock: $item->stock</h5>";
                        } else {
                            echo "<h5 class='text-danger'>Out of stock</h5>";
                        } ?>

                    </div>
                    <div class="btn-group actions notranslate">
                        <?php
                        $vat = 1;
                        if (!empty($item->vat_code_model['percentage']) && $tax) {
                            $vat += $item->vat_code_model['percentage'];
                        }
                        ?>
                        <a class="btn btn-default" style="font-weight: bold"><?= \Cake\I18n\Number::currency($item->price * $vat, 'EUR') ?></a>
                        <?php if ($item->stock > 0) {
                            echo $this->Form->postLink('Buy', ['action' => 'add', 'controller' => 'Cart', $item->id], ['class' => 'btn btn-primary', 'rel' => 'Add to cart']);
                        } else {
                            echo '<a class="btn btn-primary disabled">Buy</a>';
                        } ?>
                        <?= $this->Html->link('More Info', ['action' => 'view', 'controller' => 'Products', $item->alias],
                            ['class' => 'btn btn-default', 'rel' => 'View ' . $item->title]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<div class="text-center">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
</div>

<?php
$session = $this->request->session();
if (isset($modal) && !$session->check("shown_modal")) {
    $session->write("shown_modal", true);
    $this->start('modal'); ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="color: black">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?= $modal->title ?></h4>
                </div>
                <div class="modal-body">
                    <?= $modal->content ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Sluiten</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#myModal').modal('show')
    </script>
    <?php $this->end();
} ?>

<?//= $this->element('PaymentInvoiceRequest.banner'); ?>
