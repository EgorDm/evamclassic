<?php

use App\Utils\CacheUtils;
use Cake\Routing\Router;
use Cake\View\View;

/**
 * @var View $this
 */

$slider_items = array();
$slider_items_big = array();
if (empty($item->image_group['images'])) {
    array_push($slider_items, '<div>' . $this->Html->image('uploads/' . CacheUtils::getPreference('no_image')->text_val, ['alt' => 'No image found']) . '</div>');
    array_push($slider_items_big, '<div>' . $this->Html->image('uploads/' . CacheUtils::getPreference('no_image')->text_val, ['alt' => 'No image found']) . '</div>');
} else {
    foreach ($item->image_group['images'] as $image) {
        array_push($slider_items, '<div>' . $this->Html->image('uploads/' . $image->path, ['alt' => $image->name]) . '</div>');
        array_push($slider_items_big, '<div><a href="/img/uploads/'.$image->path.'" data-lightbox="'. $item->code .'">' . $this->Html->image('uploads/' . $image->path, ['alt' => $image->name]) . '</a></div>');
    }
}

$this->assign('title', $item->title);

$this->start('custom_js'); ?>
<script>
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        infinite: true,
        dots: true,
        centerMode: true,
        variableWidth: true,
        focusOnSelect: true
    });
    $("input[name='quantity']").TouchSpin({
        min: 0,
        max: 100,
        step: 1,
        boostat: 5,
        maxboostedstep: 10,
    });
</script>

<?php
$vat = 1;
if (!empty($item->vat_code_model['percentage']) && $tax) {
    $vat += $item->vat_code_model['percentage'];
}
?>
<script type='application/ld+json'>
    {
        "@context": "http://www.schema.org",
        "@type": "Product",
        "name": "<?= $item->title ?>",
        "sku": "<?= $item->code ?>",
        "productID": "<?= $item->code ?>",
<?php if(!empty($item->image_group['images'])) { ?>
        "image": "<?= Router::url('img/uploads/' . $item->image_group['images'][0]->path, true) ?>",
<?php } ?>
        "description": "<?= $item->description ?>",
        "offers": {
            "@type": "Offer",
            "priceCurrency": "EUR",
            "price": "<?= number_format($item->price * $vat, 2) ?>",
            "availability": "http://schema.org/InStock",
            "url": "<?= Router::url(['action' => 'view', 'controller' => 'Products', $item->alias], true) ?>"
        }
    }
</script>
<?php $this->end();
/*$this->start('top_banner');
<div style="position: relative; width: 100%">
    <div class="banner-img"
         style="background: url('<?php echo $this->Url->build('/img/parts_pattern.jpg'); ?>') repeat"></div>
    <div
            style="background: url('<?php echo $this->Url->build('/img/parts_logo.svg'); ?>') no-repeat; height: 140px; width: 120px; position: absolute; right: 40%; bottom: 0"></div>
</div>
$this->end();*/ ?>

<div class="heading">
    <h3><?= $item->title ?></h3>
</div>
<div class="row product-detail">
    <div class="col-md-6 product-carousel">
        <div class="slider slider-for">
            <?php foreach ($slider_items_big as $slide) {
                echo $slide;
            } ?>
        </div>
        <div class="slider slider-nav">
            <?php foreach ($slider_items as $slide) {
                echo $slide;
            } ?>
        </div>
    </div>
    <div class="col-md-6">
        <h3>Description</h3>
        <p><?= $item->title ?></p>
        <p><?= $item->description ?></p>

        <div class="purchase-box">
            <div class="row">
                <?php
                $vat = 1;
                if (!empty($item->vat_code_model['percentage']) && $tax) {
                    $vat += $item->vat_code_model['percentage'];
                }
                ?>
                <div class="col-xs-12"><h3><?= \Cake\I18n\Number::currency($item->price * $vat, 'EUR') ?></h3></div>
                <div>
                    <div class="col-xs-4">Part number</div>
                    <div class="col-xs-8"><?= $item->code ?></div>
                </div>
                <div>
                    <div class="col-xs-4">Availability</div>
                    <div class="col-xs-8">
                        <?php if ($item->stock > 0) {
                            if(\Core\Helper\Website::identifier() == \Core\Helper\Website::EVAM) {
                                echo '<span class="text-success">In stock</span>';
                            } else {
                                echo '<span class="text-success">' . $item->stock . ' item' . (($item->stock > 1) ? 's' : '') . ' in stock</span>';
                            }
                        } else {
                            echo '<span class="text-danger">Item is not in stock. Please try again later.</span>';
                        } ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php if ($item->stock > 0) { ?>
                    <hr>
                    <?php //TODO fix color difference ?>
                    <div class="col-xs-12 input-qty-detail">
                        <?php
                        echo $this->Form->create(null, ['url' => ['controller' => 'Cart', 'action' => 'add', $item->id]]);
                        echo '<input type="text" name="quantity" class="item-qty" id="quantity" value="1" style="display: block;">';
                        echo $this->Form->button('<i class="icon icon-basket"></i> Add to Cart',
                            ['class' => 'btn btn-primary pull-left', 'type' => 'submit', 'escape' => false]);
                        echo $this->Form->end();
                        ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
