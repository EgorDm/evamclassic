<?php
use Cake\View\View;

/**
 * @var View $this
 */

$this->start('custom_js'); ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    function AutoGrowTextArea(textField) {
        if (textField.clientHeight < textField.scrollHeight) {
            textField.style.height = textField.scrollHeight + "px";
            if (textField.clientHeight < textField.scrollHeight) {
                textField.style.height =
                    (textField.scrollHeight * 2 - textField.clientHeight) + "px";
            }
        }
    }
</script>
<?php $this->end(); ?>

<div class="heading">
    <h3>Contact</h3>
</div>
<p>Please submit your details.</p>
<div class="row">
    <div class="col-lg-8">
        <?= $this->Form->create($contact, ['id' => 'contact']) ?>
        <?= $this->Form->input('name', ['placeholder' => 'Full Name', 'required', 'label' => 'Full Name',
            'data-validation-required-message' => 'Please enter your full name.']) ?>
        <?= $this->Form->input('company', ['placeholder' => 'Company name', 'label' => 'Company name',
            'data-validation-required-message' => 'Please enter the name of your company.']) ?>
        <?= $this->Form->input('email', ['placeholder' => 'Email address', 'required', 'label' => 'Email address',
            'data-validation-required-message' => 'Please enter your email address.']) ?>
        <?= $this->Form->input('phone', ['placeholder' => 'Phone number', 'label' => 'Phone number',
            'data-validation-required-message' => 'Please enter your phone number.', 'type' => 'tel']) ?>
        <?= $this->Form->textarea('message', ['placeholder' => 'Message', 'required', 'templateVars' => ['label' => 'Message'],
            'data-validation-required-message' => 'Please enter your email address.', 'onkeyup' => 'AutoGrowTextArea(this)']) ?>
        <div class="form-buttons">
            <?= $this->Recaptcha->display() ?>
            <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary btn-lg']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>


