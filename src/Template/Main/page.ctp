<?php
use Cake\View\View;

/**
 * @var View $this
 */

?>

    <div class="heading">
        <h3><?= $page->title ?></h3>
    </div>

<?= $page->content ?>
