<?php
use Cake\View\View;
/**
 *@var View $this
 */

?>

<div class="heading">
    <h3>Contact request</h3>
</div>
<p>Your contact request has been sent. We will contact you as soon as possible.</p>