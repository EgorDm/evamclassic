<?php

use Cake\View\View;

/** @var View $this */
?>
    <div class="heading">
        <h3><?= $page->title ?></h3>
    </div>
    <div class="row products-grid">

        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product">
                <a href="http://evamclassic.com/img/uploads/fp6nxe1528806254.jpg" data-lightbox="fp6nxe1528806254.jpg">
                    <img src="http://evamclassic.com/img/uploads/thumbnail_medium/fp6nxe1528806254.jpg">
                </a>
                <div class="product-view" style="min-height: 1px">
                    <div class="info" style="min-height: 1px">
                        <div class="innerText">Michel
                            <a href="mailto:parts@evamclassic.com?subject=Webshop">Stuur een e-mail</a>
                            <div class="innerText">Parts / Webshop
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product">
                <a href="http://evamclassic.com/img/uploads/n9da5o1593685581.jpg" data-lightbox="n9da5o1593685581.jpg">
                    <img src="http://evamclassic.com/img/uploads/thumbnail_medium/n9da5o1593685581.jpg">
                </a>
                <div class="product-view" style="min-height: 1px">
                    <div class="info" style="min-height: 1px">
                        <div class="innerText">HUAWEI                            <a href="https://wa.me/31617351010">+31617351010</a>
                            <div class="innerText">Stuur jij mij een whatsapp?                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product">
                <a href="http://evamclassic.com/img/uploads/njlm0o1621413326.jpg" data-lightbox="njlm0o16214133262.jpg">
                    <img src="http://evamclassic.com/img/uploads/thumbnail_medium/njlm0o1621413326.jpg">
                </a>
                <div class="product-view" style="min-height: 1px">
                    <div class="info" style="min-height: 1px">
                        <div class="innerText">
                            <div class="innerText">Shipping
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product">
                <a href="http://evamclassic.com/img/uploads/eazd5o1528877821.jpg" data-lightbox="eazd5o1528877821.jpg">
                    <img src="http://evamclassic.com/img/uploads/thumbnail_medium/eazd5o1528877821.jpg">
                </a>
                <div class="product-view" style="min-height: 1px">
                    <div class="info" style="min-height: 1px">
                        <div class="innerText">Marloes
                            <a href="mailto:admin@evamclassic.com?subject=Webshop">Stuur een e-mail</a>
                            <div class="innerText">Finance
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product">
                <a href="http://evamclassic.com/img/uploads/sqiiif1528806295.jpg" data-lightbox="sqiiif1528806295.jpg">
                    <img src="http://evamclassic.com/img/uploads/thumbnail_medium/sqiiif1528806295.jpg">
                </a>
                <div class="product-view" style="min-height: 1px">
                    <div class="info" style="min-height: 1px">
                        <div class="innerText">Etienne
                            <a href="mailto:info@evamclassic.com?subject=Webshop">Stuur een e-mail</a>
                            <div class="innerText">Owner
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><div class="row products-grid">

            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="product">
                    <a href="http://evamclassic.com/img/uploads/qf9pbc1593688071.jpg" data-lightbox="qf9pbc1593688071.jpgg">
                        <img src="http://evamclassic.com/img/uploads/thumbnail_medium/qf9pbc1593688071.jpg">
                    </a>
                    <div class="product-view" style="min-height: 1px">
                        <div class="info" style="min-height: 1px">
                            <div class="innerText">Housekeeping
                                <a href=""></a>
                                <div class="innerText">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?= $page->content ?>
