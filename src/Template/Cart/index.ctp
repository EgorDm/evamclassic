<?php
use Cake\View\View;

/**
 * @var View $this
 */

?>
<div class="heading">
    <h3>Shopping cart</h3>
</div>

<div class="col-xs-12">
    <?php if (!empty($cart_items) && count($cart_items['CartItem']) > 0) { ?>
        <table class="table cart-table">
            <tr>
                <th colspan="3" class="item-product-head">Product</th>
                <th class="item-code-head">Part number</th>
                <th class="item-quantity-head">Count</th>
                <th class="item-price-head">Price</th>
                <th class="item-total-price-head">Price inc. VAT</th>
            </tr>
            <?php foreach ($cart_items['CartItem'] as $item) { ?>
                <tr class="item-row">
                    <td class="item-delete">
                        <?php //TODO fix color difference ?>
                        <?= $this->Form->postLink('×', ['action' => 'remove', 'controller' => 'Cart', $item['id']]) ?>
                    </td>
                    <td class="item-thumbnail">
                        <?= $this->Html->link($this->Html->image('uploads/thumbnail_small/' . $item['thumbnail'][0], ['alt' => $item['thumbnail'][1]]),
                            ['action' => 'view', 'controller' => 'Products', $item['alias']], ['escape' => false]); ?>
                    </td>
                    <td class="item-info">
                        <?= $this->Html->link(h($item['title']), ['action' => 'view', 'controller' => 'Products', $item['alias']], ['class' => 'product-name']); ?>
                    </td>
                    <td class="item-code">
                        <?= $item['code'] ?>
                    </td>
                    <td class="item-quantity">
                        <?php
                        echo $this->Form->create(null, ['url' => ['action' => 'setCount', 'controller' => 'Cart']]);
                        echo $this->Form->hidden('id', ['value' => $item['id']])
                        ?>
                        <select name="quantity" class="item-qty" onchange="this.form.submit()">
                            <?php
                            $start_qty = ($item['quantity'] <= 5) ? 1 : $item['quantity'] - 5;
                            $end_qty = ($item['quantity'] <= 5) ? 11 - $item['quantity'] : $item['quantity'] + 5;
                            for ($i = $start_qty; $i <= $end_qty; $i++) {
                                $addition = ($i == $item['quantity']) ? 'selected' : '';
                                echo "<option value='$i' $addition>$i</option>";
                            } ?>
                        </select>
                        <?= $this->Form->end() ?>
                    </td>
                    <td class="item-price"><?= \Cake\I18n\Number::currency($item['subtotal_excl'], 'EUR') ?></td>
                    <td class="item-total-price"><?= \Cake\I18n\Number::currency($item['subtotal_inc'], 'EUR') ?></td>
                </tr>
            <?php } ?>
        </table>
        <div class="receipt-container">
            <div class="receipt-wrapper col-md-5 col-md-push-7">
                <?php $current_shipping = $cart_items['Order']['shipping']['selected']; ?>
                <table class="table">
                    <tr class="receipt-total">
                        <td>Total</td>
                        <td>
                            <?= \Cake\I18n\Number::currency(\App\Utils\DataUtils::round_up($cart_items['Order']['subtotal_inc'], 2), 'EUR') ?></td>
                    </tr>
                </table>
                <div class="receipt-conclusion">
	                <div style="margin-bottom: 8px; font-size: 12px">
		                <div id="tos-error" style="font-size: 16px; color: darkred; display: none">You must agree with the terms and condition to proceed to checkout.</div>
		                <label for="tos">I agree with <a href="/main/terms">terms and conditions</a>.</label>
		                <input type="checkbox" id="tos" name="tos" style="margin-left: 8px; margin-top: 0" value="1"/>
	                </div>
                    <?= $this->Html->link('Continue Shopping', ['controller' => 'Products', 'action' => 'index'], ['class' => 'btn btn-primary btn-long']) ?>
	                <?= $this->Html->link('Next', ['controller' => 'Orders', 'action' => 'newCheckout'], ['class' => 'btn btn-primary btn-long', 'id' => 'do-order']) ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?php $this->start('custom_js'); ?>
	<script>
		$('#do-order').on('click', function(event) {
			if(!$('#tos').is(':checked')) {
				event.preventDefault();
				$('#tos-error').show();
			}
		});
	</script>
<?php $this->end(); ?>
