<?php
use Cake\View\View;
/**
 *@var View $this
 */

?>

<h3 class="title">Belastingen</h3>
<p>Prijzen worden weergegeven <b><?= ($tax) ? 'met' : 'zonder'?></b> btw.</p>
<?= $this->Form->postLink(('Prijzen weergeven '.((empty($tax)) ? 'met' : 'zonder').' btw'), ['controller' => 'Products', 'action' => 'toggletax'], ['class' => ['btn', 'btn-accent']]) ?>