<?php
use Cake\View\View;

/**
 * @var View $this
 */

if (!empty($cart_items) && count($cart_items['CartItem']) > 0) { ?>
    <?= $this->Html->link('Cart<span class="cart-amount">'.count($cart_items['CartItem']).'</span>',
        ['action' => 'index', 'controller' => 'Cart'], ['class' => 'chart-link icon-basket', 'id' => 'navbar-cart-icon', 'escape' => false]); ?>
    <div class="cart-items-wrapper collapse" id="navbar-cart-wrapper">
        <div class="cart-items">
            <p class="summary"><?= count($cart_items['CartItem']) ?> items in your cart:</p>
            <ol>
                <?php foreach ($cart_items['CartItem'] as $item) { ?>
                    <li>
                        <?= $this->Html->link(h($item['title']), ['action' => 'view', 'controller' => 'Products', $item['alias']], ['class' => 'product-name']); ?>
                        <?= $this->Html->link($this->Html->image('uploads/thumbnail_small/' . $item['thumbnail'][0], ['alt' => $item['thumbnail'][1]]),
                            ['action' => 'view', 'controller' => 'Products', $item['alias']], ['class' => 'product-image', 'escape' => false]); ?>
                        <dl>
                            <dt><abbr title="Quantity">Qty</abbr>:</dt>
                            <dd><?= $item['quantity'] ?></dd>
                            <dt class="label-price">Price:</dt>
                            <dd>
                                <span class="price"><?= \Cake\I18n\Number::currency($item['subtotal_inc'], 'EUR') ?></span>
                            </dd>
                        </dl>
                        <?= $this->Form->postLink('×', ['action' => 'remove', 'controller' => 'Cart', $item['id']], ['class' => 'btn-remove']) ?>
                    </li>
                    <?php
                } ?>
            </ol>

            <!--<p class="shipment">
                <?php /*$current_shipping = $cart_items['Order']['shipping']['selected']; */?>
                <small>Shipment: <?/*= \Cake\I18n\Number::currency($cart_items['Order']['shipping'][$current_shipping]['price'], 'EUR') */?></small>
            </p>-->

            <p class="subtotal">
                Subtotal:
                <span class="price"><?= \Cake\I18n\Number::currency($cart_items['Order']['subtotal_inc']/* + $cart_items['Order']['shipping'][$current_shipping]['price']*/, 'EUR') ?></span>
            </p>
            <div class="actions">
                <?= $this->Html->link(h('Go to cart'), ['action' => 'index', 'controller' => 'Cart'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
<?php } else { ?>
    <a class="chart-link icon-basket" id="navbar-cart-icon">Cart<span class="cart-amount empty">0</span></a>
    <div class="cart-items-wrapper collapse" id="navbar-cart-wrapper">
        <div class="cart-items">
            <p class="empty">Your cart is empty.</p>
        </div>
    </div>
<?php } ?>

