<?php
use Cake\Routing\Router;
use Cake\View\View;

/**
 * @var View $this
 */

?>

<script src="https://cdn.tiny.cloud/1/8hllfw0iz9q4m56hvcwn8bgnhclzmqrd0x9xitwnrucjzwob/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({
        selector: 'textarea:not(.raw)',
        plugins: "code table paste textcolor imagetools image preview link lists",
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image preview forecolor',
        content_css: [
            "<?= Router::url('/', true).'/css/style.css' ?>"
        ]
    });</script>
