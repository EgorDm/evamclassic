<?php
use Cake\View\View;

/**
 * @var View $this
 */
$parent_tree = array();
foreach ($categories as $cat) {
        $parent_tree = $parent_tree + createParent($cat);
}
function createParent($category)
{
    $tree = array();
    foreach ($category->children as $child) {
        $tree[$child->id] = $category->id;
        $tree = $tree + createParent($child);
    }
    return $tree;
}

$showing_categories = array();
if (!empty($category)) {
    array_push($showing_categories, $category->id);
    $curr_id = $category->id;
    while (!empty($parent_tree[$curr_id])) {
        $curr_id = $parent_tree[$curr_id];
        array_push($showing_categories, $curr_id);
    }

}
function createCat($category, $html_temp, $showing)
{
    echo '<li>';
    if (!empty($category->children)) {
        echo '<input type="checkbox" id="category-' . $category->id . '" ' . (in_array($category->id, $showing) ? 'checked' : '') . '/>';
        echo '<label for="category-' . $category->id . '"></label>';
        echo $html_temp->link($category->title, ['action' => 'category', 'controller' => 'Products', $category->alias]);
        echo '<ol>';
        foreach ($category->children as $child) {
            createCat($child, $html_temp, $showing);
        }
        echo '</ol>';
    } else {
        echo $html_temp->link($category->title, ['action' => 'category', 'controller' => 'Products', $category->alias]);
    }
    echo '</li>';
}
?>
<h3 class="title">Categories</h3>
<ol class="treelist sidebar notranslate">
    <?php foreach ($categories as $cat) {
        createCat($cat, $this->Html, $showing_categories);
    } ?>
</ol>
