<style>
    .icon-invoice {
        width: 60px;
        height: 52px;
        background: url(/img/payment_methods/invoice.png) no-repeat;
        background-size: cover;
        overflow: hidden;
        display: inline-block;
    }

    .banner {
        text-align: center;
        background-color: #3f3f3f;
    }
</style>
