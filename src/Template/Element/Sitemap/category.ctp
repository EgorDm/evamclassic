<?php

use Cake\I18n\Time;
use Cake\Routing\Router;
/** @var \App\Model\Entity\Category $category */
?>

<url>
    <loc><?= Router::url(['action' => 'category', 'controller' => 'Products', $category->alias], true) ?></loc>
    <lastmod><?= Time::now()->format('Y-m-d') ?></lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.8</priority>
</url>