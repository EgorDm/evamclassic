<?php

use Cake\Routing\Router;

/** @var \App\Model\Entity\Item $item */
?>

<url>
    <loc><?= Router::url(['action' => 'view', 'controller' => 'Products', $item->alias], true) ?></loc>
    <lastmod><?= $item->modified->format('Y-m-d') ?></lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.6</priority>
    <?php
    if (!empty($item) && !empty($item->image_group['images'])) {
        foreach ($item->image_group['images'] as $image) {
            ?>
            <image:image>
                <image:loc><?= Router::url('img/uploads/' . $image->path, true) ?></image:loc>
                <image:title><![CDATA[<?= $item->title ?>]]></image:title>
            </image:image>
            <?php
        }
    }
    ?>
</url>