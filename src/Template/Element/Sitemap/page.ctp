<?php

use Cake\I18n\Time;
use Cake\Routing\Router;
/** @var \App\Model\Entity\Page $page */
?>

<url>
    <loc><?= Router::url($page->slug, true) ?></loc>
    <lastmod><?= Time::now()->format('Y-m-d') ?></lastmod>
    <changefreq>monthly</changefreq>
    <priority>1</priority>
</url>