<?php
namespace App\Form;

use App\Controller\Component\ExactSyncComponent;
use App\Utils\SystemUtils;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * CustomerRegister Form.
 */
class CustomerRegisterForm extends Form
{
    use MailerAwareTrait;

    /**
     * Builds the schema for the modelless form
     *
     * @param Schema $schema From schema
     * @return Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema
            ->addField('name', 'string')
            ->addField('email', 'string')
            ->addField('password', 'string')
            ->addField('password-confirm', 'string')
            ->addField('use-address', 'boolean')
            ->addField('contact-name', 'string')
            ->addField('address', 'string')
            ->addField('address-number', 'string')
            ->addField('zip', 'string')
            ->addField('city', 'string')
            ->addField('country', 'string')
            ->addField('phone', 'string');
    }

    /**
     * Form validation builder
     *
     * @param Validator $validator to use against the form
     * @return Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        return $validator
            ->notEmpty('name')
            ->notEmpty('email')
            ->notEmpty('password')
            ->notEmpty('password-confirm')
            ->add('password-confirm', 'no-misspelling', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Passwords are not equal',
            ])
            ->allowEmpty('contact-name', function ($context) {
                return empty($context['data']['use-address']);
            })
            ->allowEmpty('company', function ($context) {
                return empty($context['data']['is-company']);
            })
            ->allowEmpty('vat_code', function ($context) {
                return empty($context['data']['is-company']);
            })
            ->allowEmpty('address', function ($context) {
                return empty($context['data']['use-address']);
            })
            ->allowEmpty('address-number', function ($context) {
                return empty($context['data']['use-address']);
            })
            ->allowEmpty('zip', function ($context) {
                return empty($context['data']['use-address']);
            })
            ->allowEmpty('city', function ($context) {
                return empty($context['data']['use-address']);
            })
            ->allowEmpty('country', function ($context) {
                return empty($context['data']['use-address']);
            })
            ->allowEmpty('phone', function ($context) {
                return empty($context['data']['use-address']);
            });
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data
     * @return null|array
     */
    protected function _execute(array $data)
    {
        $customers_table = TableRegistry::get('Customers');
        $customer = $customers_table->patchEntity($customers_table->newEntity(), $data);
        $customer->set('active', SystemUtils::generateRandomString(16));
        if (!$customers_table->save($customer)) {
            return null;
        }
        if (!empty($data['use-address'])) {
            $addresses_table = TableRegistry::get('Addresses');
            $address_data = [
                'customer_id' => $customer->id,
                'type' => 0,
                'name' => $data['contact-name'],
                'street' => $data['address'] . ' ' . $data['address-number'],
                'city' => $data['city'],
                'country' => $data['country'],
                'postcode' => $data['zip'],
                'phone' => $data['phone'],
            ];
            $address = $addresses_table->patchEntity($addresses_table->newEntity(), $address_data);
            if (!$addresses_table->save($address)) {
                return null;
            }
        }
        $customer->unsetProperty('password-confirm');
        $customer->unsetProperty('password');
        $this->getMailer('CustomerMail')->send('sendConfirm', [$customer]);
        return $customer->toArray();
    }
}
