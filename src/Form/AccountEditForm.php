<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * AccountEdit Form.
 */
class AccountEditForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param Schema $schema From schema
     * @return $this
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema
            ->addField('name', 'string')
            ->addField('change-password', 'boolean')
            ->addField('current-password', 'string')
            ->addField('new-password', 'string')
            ->addField('confirm-password', 'string');
    }

    /**
     * Form validation builder
     *
     * @param Validator $validator to use against the form
     * @return Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        return $validator
            ->notEmpty('name')
            ->allowEmpty('current-password', function ($context) {
                return empty($context['data']['change-password']);
            })
            ->allowEmpty('new-password', function ($context) {
                return empty($context['data']['change-password']);
            })
            ->allowEmpty('confirm-password', function ($context) {
                return empty($context['data']['change-password']);
            })
            ->add('confirm-password', 'no-misspelling', [
                'rule' => ['compareWith', 'new-password'],
                'message' => 'Passwords are not equal',
            ]);
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data
     * @return bool|array
     */
    protected function _execute(array $data)
    {
        return $data;
    }
}
