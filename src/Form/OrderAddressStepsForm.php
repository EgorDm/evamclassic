<?php
namespace App\Form;

use App\Utils\CheckoutSteps;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * OrderAddressSteps Form.
 */
class OrderAddressStepsForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param Schema $schema From schema
     * @return Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema
            ->addField('step', 'integer')
            ->addField('address_id', 'integer')
            ->addField('use_for_billing', 'boolean')
            ->addField('contact-name', 'string')
            ->addField('address-line', 'string')
            ->addField('address-number', 'string')
            ->addField('zip', 'string')
            ->addField('city', 'string')
            ->addField('country', 'string')
            ->addField('phone', 'string');
    }

    /**
     * Form validation builder
     *
     * @param Validator $validator to use against the form
     * @return Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        return $validator
            ->requirePresence('step')
            ->requirePresence('address_id')
            ->requirePresence('use_for_billing', function ($context) {
                return $context['data']['step'] == CheckoutSteps::SHIPPING_ADDRESS;
            })
            ->allowEmpty('contact-name', function ($context) {
                return (empty($context['data']['address_id']) || $context['data']['address_id'] != -1);
            })
            ->allowEmpty('address-line', function ($context) {
                return (empty($context['data']['address_id']) || $context['data']['address_id'] != -1);
            })
            ->allowEmpty('address-number', function ($context) {
                return (empty($context['data']['address_id']) || $context['data']['address_id'] != -1);
            })
            ->allowEmpty('zip', function ($context) {
                return (empty($context['data']['address_id']) || $context['data']['address_id'] != -1);
            })
            ->allowEmpty('city', function ($context) {
                return (empty($context['data']['address_id']) || $context['data']['address_id'] != -1);
            })
            ->allowEmpty('country', function ($context) {
                return (empty($context['data']['address_id']) || $context['data']['address_id'] != -1);
            })
            ->allowEmpty('phone', function ($context) {
                return (empty($context['data']['address_id']) || $context['data']['address_id'] != -1);
            });
    }

    /**
     * Return null on error otherwise returns array with data top patch order with.
     *
     * @param array $data
     * @return array|null
     */
    protected function _execute(array $data)
    {
        if ($data['step'] == 0 || $data['step'] == 1) {
            if ($data['address_id'] == -1) {
                $addresses_table = TableRegistry::get('Addresses');
                $address_data = [
                    'customer_id' => $data['customer_id'],
                    'type' => ($data['use_for_billing'] && $data['use_for_billing'] == true) ? 3 : 2,
                    'name' => $data['contact-name'],
                    'street' => $data['address-line'] . ' ' . $data['address-number'],
                    'city' => $data['city'],
                    'country' => $data['country'],
                    'postcode' => $data['zip'],
                    'phone' => $data['phone'],
                ];
                $address = $addresses_table->patchEntity($addresses_table->newEntity(), $address_data);
                if (!$addresses_table->save($address)) {
                    return null;
                }
                $data['address_id'] = $address->id;
            }
            $ret = array();
            if ($data['step'] == CheckoutSteps::SHIPPING_ADDRESS) {
                $ret['shipping_id'] = $data['address_id'];
                if ($data['use_for_billing'] == true) {
                    $ret['billing_id'] = $data['address_id'];
                }
            } else if ($data['step'] == CheckoutSteps::BILLING_ADDRESS) {
                $ret['billing_id'] = $data['address_id'];

            }
            return $ret;
        }
        return null;
    }
}
