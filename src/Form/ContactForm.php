<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Mailer\MailerAwareTrait;
use Cake\Validation\Validator;

/**
 * Contact Form.
 */
class ContactForm extends Form
{
    use MailerAwareTrait;

    /**
     * Builds the schema for the modelless form
     *
     * @param Schema $schema From schema
     * @return Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema
            ->addField('name', 'string')
            ->addField('company', 'string')
            ->addField('email', 'string')
            ->addField('phone', 'string')
            ->addField('message', 'text');
    }

    /**
     * Form validation builder
     *
     * @param Validator $validator to use against the form
     * @return Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        return $validator
            ->notEmpty('name')
            ->notEmpty('email')
            ->notEmpty('phone')
            ->notEmpty('message')
            ->allowEmpty('company');
    }

    /**
     * Defines what to execute once the From is being processed
     *
     * @param array $data
     * @return bool
     */
    protected function _execute(array $data)
    {
        $this->getMailer('CustomerMail')->send('customerContacted', [$data['name'], $data['email'], $data['phone'], $data['message']]);
        $this->getMailer('AdminMail')->send('sendContact', [$data['name'], $data['email'], $data['phone'], $data['message']]);
        return true;
    }
}
