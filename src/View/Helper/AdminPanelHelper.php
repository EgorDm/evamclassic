<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 6/10/2016
 * Time: 9:01 PM
 */

namespace App\View\Helper;


use Cake\View\Helper;

/**
 * @property Helper\HtmlHelper Html
 * @property Helper\UrlHelper Url
 */
class AdminPanelHelper extends Helper
{
    public $helpers = ['Html', 'Url'];

    public function sidebarLink($title = null, $url = null, $options = array())
    {
        if ($title == null || $url == null)
            return;


        $class = ($this->request->param('controller') == $url['controller']) ? 'active' : '';

        echo "<li class='" . $class . "'>" . $this->Html->link($title, $url, $options) . "</li>";
    }

    public function sidebarGroup($group, $data_array) {
        $active = '';
        $expanded = '';
        $ret = '';
        foreach ($data_array as $data) {
            if($data['controller'] == $this->request->param('controller')) {
                $active = 'class="active"';
                $expanded = 'style="display: block;"';
                $ret.= "<li $active>".$this->Html->link($data['title'], ['controller' => $data['controller'], 'action' => $data['action']])."</li>";
            } else {
                $ret.= "<li>".$this->Html->link($data['title'], ['controller' => $data['controller'], 'action' => $data['action']])."</li>";
            }
        }
        if(isset($group['extra_controllers'] )) {
            foreach ($group['extra_controllers'] as $controller) {
                if ($controller == $this->request->param('controller')) {
                    $active = 'class="active"';
                    $expanded = 'style="display: block;"';
                }
            }
        }
        $ret = "<li $active><a><i class=\"fa $group[icon]\"></i> $group[title] <span class=\"fa fa-chevron-down\"></span></a>"
            ."<ul $expanded class=\"nav child_menu\">$ret</ul></li>";
        echo $ret;
    }

    public function imageLink($id, $attrs, $medium) {
        $this->Url->build(["controller" => "Images", "action" => "rawView", $id]);
        return "<img $attrs src='". $this->Url->build(["controller" => "Images", "action" => "rawView", $id, 'medium' => $medium]) ."'/>";
    }
}