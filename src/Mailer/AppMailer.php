<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 06-Aug-16
 * Time: 18:04
 */

namespace App\Mailer;


use Cake\Log\Log;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;

abstract class AppMailer extends Mailer
{
    public function sendTextMail($to, $slug, $variables, $from = 'info@evamclassic.com', $replyto = null)
    {
        Log::debug('Sendign mail to' . $to);
        $text_table = TableRegistry::get('Texts');
        $text = $text_table->find()->where(['slug' => $slug])->first();
        //TODO: acc text caching
        if (empty($text)) {
            return null;
        }
        $subject = str_replace(array_keys($variables), array_values($variables), $text->title);
        $message = str_replace(array_keys($variables), array_values($variables), $text->content);
        $this
            ->template('main', 'default')
            ->emailFormat('html')
            ->subject($subject)
            ->to($to)
            ->from($from)
            ->replyTo($replyto)
            ->viewVars(['message' => $message]);
    }

}
