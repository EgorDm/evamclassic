<?php
namespace App\Mailer;

use Cake\Routing\Router;

/**
 * CustomerMail mailer.
 */
class AdminMailMailer extends AppMailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'AdminMail';

    public function sendContact($name, $email, $phone, $message)
    {
        $this->sendTextMail(
            ['info@evamclassic.com', 'info@vwsplitparts.com', 'parts@evamclassic.com', 'sales@evamclassic.com'],
            'admin_contacted',
            ['%name%' => $name, '%email%' => $email, '%phone%' => $phone, '%message%' => $message],
            $email
        );

    }

    public function order($order_id)
    {
        $this->sendTextMail(
            ['info@evamclassic.com', 'info@vwsplitparts.com', 'parts@evamclassic.com', 'sales@evamclassic.com'],
            'admin_order',
            [
                '%order_id%' => $order_id,
                '%short_link%' => '<a href="' . Router::url(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'view', $order_id, '_full' => true])
                    . '">click this link</a>',
                '%full_link%' => '<a href="' . Router::url(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'view', $order_id, '_full' => true])
                    . '">' . Router::url(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'view', $order_id, '_full' => true]) . '</a>',
            ]
        );
    }

}
