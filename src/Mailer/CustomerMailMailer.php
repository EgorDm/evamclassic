<?php
namespace App\Mailer;

use Cake\Routing\Router;

/**
 * CustomerMail mailer.
 */
class CustomerMailMailer extends AppMailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'CustomerMail';

    public function sendConfirm($customer)
    {
        $this->sendTextMail($customer->email, 'customer_registered', [
            '%name%' => $customer->name,
            '%short_link%' => '<a href="' .
                Router::url(['controller' => 'Customers', 'action' => 'confirm', 'token' => $customer->active, '_full' => true])
                . '">click this link</a>',
            '%full_link%' => '<a href="' . Router::url(['controller' => 'Customers', 'action' => 'confirm', 'token' => $customer->active, '_full' => true])
                . '">' . Router::url(['controller' => 'Customers', 'action' => 'confirm', 'token' => $customer->active, '_full' => true]) . '</a>',
        ]);

    }

    public function confirmed($customer)
    {
        $this->sendTextMail($customer->email, 'customer_confirmed', [
            '%name%' => $customer->name,
            '%short_link%' => '<a href="' .
                Router::url(['controller' => 'Customers', 'action' => 'account', '_full' => true])
                . '">click this link</a>',
            '%full_link%' => '<a href="' . Router::url(['controller' => 'Customers', 'action' => 'account', '_full' => true])
                . '">' . Router::url(['controller' => 'Customers', 'action' => 'account', '_full' => true]) . '</a>',
        ]);
    }

    public function passwordReset($customer)
    {
        $this->sendTextMail($customer->email, 'customer_password_reset', [
            '%name%' => $customer->name,
            '%short_link%' => '<a href="' .
                Router::url(['controller' => 'Customers', 'action' => 'resetPassword', 'token' => $customer->pass_reset, '_full' => true])
                . '">click this link</a>',
            '%full_link%' => '<a href="' . Router::url(['controller' => 'Customers', 'action' => 'resetPassword', 'token' => $customer->pass_reset, '_full' => true])
                . '">' . Router::url(['controller' => 'Customers', 'action' => 'resetPassword', 'token' => $customer->pass_reset, '_full' => true]) . '</a>',
        ]);
    }

    public function passwordChanged($customer)
    {

        $this->sendTextMail($customer->email, 'customer_password_changed', [
            '%name%' => $customer->name,
            '%short_link%' => '<a href="' .
                Router::url(['controller' => 'Customers', 'action' => 'requestPassword', 'email' => $customer->email, '_full' => true])
                . '">click this link</a>',
            '%full_link%' => '<a href="' . Router::url(['controller' => 'Customers', 'action' => 'requestPassword', 'email' => $customer->email, '_full' => true])
                . '">' . Router::url(['controller' => 'Customers', 'action' => 'requestPassword', 'email' => $customer->email, '_full' => true]) . '</a>',
        ]);
    }

    public function orderPlaced($order)
    {
        $this->sendTextMail($order->customer->email, 'customer_order_placed', [
            '%order_id%' => $order->id,
            '%name%' => $order->customer->name,
            '%short_link%' => '<a href="' . Router::url(['controller' => 'Orders', 'action' => 'view', $order->id, '_full' => true])
                . '">click this link</a>',
            '%full_link%' => '<a href="' . Router::url(['controller' => 'Orders', 'action' => 'view', $order->id, '_full' => true])
                . '">' . Router::url(['controller' => 'Orders', 'action' => 'view', $order->id, '_full' => true]) . '</a>',
        ]);
    }

    public function orderShipped($order)
    {
        $this->sendTextMail($order->customer->email, 'order_shipped', [
            '%order_id%' => $order->id,
            '%name%' => $order->customer->name,
            '%short_link%' => '<a href="' . Router::url(['controller' => 'Orders', 'action' => 'view', $order->id, '_full' => true])
                . '">click this link</a>',
            '%full_link%' => '<a href="' . Router::url(['controller' => 'Orders', 'action' => 'view', $order->id, '_full' => true])
                . '">' . Router::url(['controller' => 'Orders', 'action' => 'view', $order->id, '_full' => true]) . '</a>',
        ]);
    }

    public function customerContacted($name, $email, $phone, $message)
    {
        $this->sendTextMail($email, 'customer_contacted', ['%name%' => $name, '%email%' => $email, '%phone%' => $phone, '%message%' => $message]);
    }
}
