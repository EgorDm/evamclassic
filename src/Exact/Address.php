<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 5/28/2016
 * Time: 11:27 PM
 */

namespace App\Exact;


use Picqer\Financials\Exact\Model;
use Picqer\Financials\Exact\Persistance\Storable;
use Picqer\Financials\Exact\Query\Findable;

class Address extends Model
{
    use Findable;
    use Storable;

    protected $fillable = [
        'ID',
        'Account',
        'AddressLine1',
        'City',
        'ContactName',
        'Country',
        'Created',
        'Modified',
        'Phone',
        'Postcode',
        'Type'
    ];

     protected $url = 'crm/Addresses';
}