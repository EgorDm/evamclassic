<?php
/**
 * Created by PhpStorm.
 * User: egordm
 * Date: 29-12-18
 * Time: 12:32
 */

namespace App\Shell;


use App\Model\Entity\Item;
use App\Model\Entity\Page;
use Cake\Console\Shell;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\View\View;

/**
 * Class CreateSitemapShell
 * @package App\Shell
 *
 * @property Table Items
 * @property Table Pages
 * @property Table Categories
 */
class CreateSitemapShell extends Shell
{
    protected $_ignoredPages = ['emails', 'admin_emails', 'off'];

    /** @var View */
    protected $_view;

    public function initialize()
    {
        parent::initialize();
        $this->_view = new View();

        $this->loadModel('Pages');
        $this->loadModel('Items');
        $this->loadModel('Categories');
    }

    public function main()
    {
        $this->out('Creating the sitemap');
        $urls = [];
        $urls += $this->getPageSitemapUrls();
        $urls += $this->getCategorySitemapUrls();
        $urls += $this->getItemSitemapUrls();

        $sitemap = $this->_view->element('Sitemap/sitemap', compact('urls'));

        $sitemapPath = WWW_ROOT . '/sitemap.xml';
        $this->out('Saved sitemap in: ' . $sitemapPath);
        file_put_contents($sitemapPath, $sitemap);
    }

    /**
     * @return array
     */
    public function getPageSitemapUrls()
    {
        $pages = $this->Pages->find('all');
        $ret = [];

        foreach ($pages as $page) {
            if (in_array($page->slug, $this->_ignoredPages)) continue;
            $ret[] = $this->_view->element('Sitemap/page', compact('page'));
        }

        $this->out(sprintf('Added %d page urls.', count($ret)));
        return $ret;
    }

    /**
     * @return array
     */
    public function getCategorySitemapUrls()
    {
        $categories = $this->Categories->find('all');
        $ret = [];

        foreach ($categories as $category) {
            $ret[] = $this->_view->element('Sitemap/category', compact('category'));
        }

        $this->out(sprintf('Added %d category urls.', count($ret)));
        return $ret;
    }

    /**
     * @return array
     */
    public function getItemSitemapUrls()
    {
        $items = $this->Items->find()->contain([
            'ImageGroups' => function ($q) {
                return $q->contain(['Images']);
            }])->all();
        $ret = [];

        foreach ($items as $item) {
            $ret[] = $this->_view->element('Sitemap/item', compact('item'));
        }

        $this->out(sprintf('Added %d item urls.', count($ret)));
        return $ret;
    }
}