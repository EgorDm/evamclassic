<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\AppController;

/**
 * ImageGroups Controller
 *
 * @property \App\Model\Table\ImageGroupsTable $ImageGroups
 */
class ImageGroupsController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ($this->request->is('post')) {
            $keyword = $this->request->data['search'];
            $this->paginate = [
                'conditions' => [
                    'OR' => array(
                        'ImageGroups.id' => $keyword,
                        'ImageGroups.name LIKE' => "%$keyword%",
                    )
                ],
                'limit' => 30
            ];
        } else {
            $this->paginate = [
                'limit' => 30
            ];
        }

        $imageGroups = $this->paginate($this->ImageGroups);

        $this->set(compact('imageGroups'));
        $this->set('_serialize', ['imageGroups']);
    }

    /**
     * View method
     *
     * @param string|null $id Image Group id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $imageGroup = $this->ImageGroups->get($id, [
            'contain' => ['Images', 'Items']
        ]);

        $this->set('imageGroup', $imageGroup);
        $this->set('_serialize', ['imageGroup']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $imageGroup = $this->ImageGroups->newEntity();
        if ($this->request->is('post')) {
            $imageGroup = $this->ImageGroups->patchEntity($imageGroup, $this->request->data);
            if ($this->ImageGroups->save($imageGroup)) {
                $this->Flash->success(__('The image group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The image group could not be saved. Please, try again.'));
            }
        }
        $images = $this->ImageGroups->Images->find('list', ['limit' => 200]);
        $this->set(compact('imageGroup', 'images'));
        $this->set('_serialize', ['imageGroup']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Image Group id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $imageGroup = $this->ImageGroups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $imageGroup = $this->ImageGroups->patchEntity($imageGroup, $this->request->data);
            if ($this->ImageGroups->save($imageGroup)) {
                $this->Flash->success(__('The image group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The image group could not be saved. Please, try again.'));
            }
        }
        $images = $this->ImageGroups->Images->find('list', ['limit' => 200]);
        $this->set(compact('imageGroup', 'images'));
        $this->set('_serialize', ['imageGroup']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Image Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $imageGroup = $this->ImageGroups->get($id);
        if ($this->ImageGroups->delete($imageGroup)) {
            $this->Flash->success(__('The image group has been deleted.'));
        } else {
            $this->Flash->error(__('The image group could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
