<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 6/13/2016
 * Time: 9:27 PM
 */

namespace App\Controller\Admin;

use Admin\Controller\AdminController;
use App\Controller\Component\ExactSyncComponent;
use Cake\ORM\TableRegistry;

/**
 * Dashboard Controller
 *
 * @property ExactSyncComponent ExactSync
 */
class DashboardController extends AdminController
{
    public function initialize()
    {
        $this->loadComponent('ExactSync');
        parent::initialize();
    }

    public function general() {
        $prefs = TableRegistry::get('Preferences');
        $date_synced = $prefs->find()->where(['alias' => 'sync_products_last'])->first();
        $this->set('synced', $date_synced->text_val);
    }

    public function reloginExact() {
        if (!$this->request->is('post')) {
            $this->Flash->error(__('Wrong request!'));
            return $this->redirect(['action' => 'general']);
        }
        $this->ExactSync->resetCredentials();
        $result = $this->ExactSync->tryConnect();
        if($result) {
            $this->Flash->success(__('Account succesfully connected'));
        } else {
            $this->Flash->error(__('Error connectiong account'));
        }
        return $this->redirect(['action' => 'general']);
    }

    public function logoff() {
        $this->ExactSync->resetCredentials();
        return $this->redirect(['action' => 'general']);
    }



}
