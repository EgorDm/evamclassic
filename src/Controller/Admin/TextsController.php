<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;
use Cake\ORM\TableRegistry;
use Core\Helper\Website;

/**
 * Texts Controller
 *
 * @property \App\Model\Table\TextsTable $Texts
 */
class TextsController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Pages', 'ImageGroups']
        ];
        $texts = $this->paginate($this->Texts);

        $this->set(compact('texts'));
        $this->set('_serialize', ['texts']);
    }

    /**
     * View method
     *
     * @param string|null $id Text id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $text = $this->Texts->get($id, [
            'contain' => ['Pages']
        ]);
        if(!empty($text->image_group_id)) {
            $table_images = TableRegistry::get('ImageGroups');
            $image_group = $table_images->find()->where(['id' => $text->image_group_id])->contain(['Images'])->first();
            if(!empty($image_group)) {
                $text->set('image_group', $image_group);
            }
        }

        $this->set('text', $text);
        $this->set('_serialize', ['text']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $text = $this->Texts->newEntity();
        if ($this->request->is('post')) {
            $text = $this->Texts->patchEntity($text, $this->request->data);
            $text->site = Website::identifier();
            if ($this->Texts->save($text)) {
                $this->Flash->success(__('The text has been saved.'));
                return $this->redirect(['action' => 'view', 'controller' => 'Pages', $text->page_id]);
            } else {
                $this->Flash->error(__('The text could not be saved. Please, try again.'));
            }
        }
        $pages = $this->Texts->Pages->find('list', ['limit' => 200]);
        $imageGroups = $this->Texts->ImageGroups->find('list', ['limit' => 200]);
        $this->set(compact('text', 'pages', 'imageGroups'));
        $this->set('_serialize', ['text']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Text id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $text = $this->Texts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $text = $this->Texts->patchEntity($text, $this->request->data);
            if ($this->Texts->save($text)) {
                $this->Flash->success(__('The text has been saved.'));
                return $this->redirect(['action' => 'view', 'controller' => 'Pages', $text->page_id]);
            } else {
                $this->Flash->error(__('The text could not be saved. Please, try again.'));
            }
        }
        $pages = $this->Texts->Pages->find('list', ['limit' => 200]);
        $imageGroups = $this->Texts->ImageGroups->find('list', ['limit' => 200]);
        $this->set(compact('text', 'pages', 'imageGroups'));
        $this->set('_serialize', ['text']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Text id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $text = $this->Texts->get($id);
        if ($this->Texts->delete($text)) {
            $this->Flash->success(__('The text has been deleted.'));
        } else {
            $this->Flash->error(__('The text could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
