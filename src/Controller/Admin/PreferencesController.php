<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\AppController;
use App\Utils\CacheUtils;
use App\Utils\DataUtils;
use Cake\Cache\Cache;
use Cake\Log\Log;

/**
 * Preferences Controller
 *
 * @property \App\Model\Table\PreferencesTable $Preferences
 */
class PreferencesController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $preferences = DataUtils::indexEntityMap(
            CacheUtils::readOrExecuteAll('allPrefs', $this->Preferences->find(), 'preferences'), 'alias');
        //Log::debug($preferences);
        //Log::debug(json_encode($this->Preferences->find()->all()));
        $preferences['eu_countries']->set('select_val', explode(',', $preferences['eu_countries']->text_val));

        $this->set(compact('preferences'));
        $this->set('_serialize', ['preferences']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Preference id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $preference = $this->Preferences->get($id, [
                'contain' => []
            ]);
            if($preference->alias == 'eu_countries') {
                $this->request->data['text_val'] = implode(',',$this->request->data['select_val']);
            }
            $preference = $this->Preferences->patchEntity($preference, $this->request->data);
            if ($this->Preferences->save($preference)) {
                Cache::write('allPrefs', $this->Preferences->find()->all(), 'preferences');
                $this->Flash->success(__('The preference has been saved.'));
            } else {
                $this->Flash->error(__('The preference could not be saved. Please, try again.'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
