<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\AppController;

/**
 * ShippingOptions Controller
 *
 * @property \App\Model\Table\ShippingOptionsTable $ShippingOptions
 */
class ShippingOptionsController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ShippingMethods']
        ];
        $shippingOptions = $this->paginate($this->ShippingOptions);

        $this->set(compact('shippingOptions'));
        $this->set('_serialize', ['shippingOptions']);
    }

    /**
     * View method
     *
     * @param string|null $id Shipping Option id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $shippingOption = $this->ShippingOptions->get($id, [
            'contain' => ['ShippingMethods']
        ]);

        $this->set('shippingOption', $shippingOption);
        $this->set('_serialize', ['shippingOption']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $shippingOption = $this->ShippingOptions->newEntity();
        if ($this->request->is('post')) {
            $shippingOption = $this->ShippingOptions->patchEntity($shippingOption, $this->request->data);
            if ($this->ShippingOptions->save($shippingOption)) {
                $this->Flash->success(__('The shipping option has been saved.'));
                return $this->redirectBack(['controller' => 'ShippingOptions', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The shipping option could not be saved. Please, try again.'));
            }
        }
        $shippingMethods = $this->ShippingOptions->ShippingMethods->find('list', ['limit' => 200]);
        $this->set(compact('shippingOption', 'shippingMethods'));
        $this->set('_serialize', ['shippingOption']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Shipping Option id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $shippingOption = $this->ShippingOptions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $shippingOption = $this->ShippingOptions->patchEntity($shippingOption, $this->request->data);
            if ($this->ShippingOptions->save($shippingOption)) {
                $this->Flash->success(__('The shipping option has been saved.'));
                return $this->redirectBack(['controller' => 'ShippingOptions', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The shipping option could not be saved. Please, try again.'));
            }
        }
        $shippingMethods = $this->ShippingOptions->ShippingMethods->find('list', ['limit' => 200]);
        $this->set(compact('shippingOption', 'shippingMethods'));
        $this->set('_serialize', ['shippingOption']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Shipping Option id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $shippingOption = $this->ShippingOptions->get($id);
        if ($this->ShippingOptions->delete($shippingOption)) {
            $this->Flash->success(__('The shipping option has been deleted.'));
        } else {
            $this->Flash->error(__('The shipping option could not be deleted. Please, try again.'));
        }
        return $this->redirectBack(['controller' => 'ShippingOptions', 'action' => 'index']);
    }
}
