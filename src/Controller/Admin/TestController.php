<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\Component\CartComponent;
use App\Controller\Component\ExactSyncComponent;
use App\Controller\Component\ExcelComponent;
use App\Controller\Component\MollieComponent;
use App\Model\Entity\Customer;
use App\Utils\CacheUtils;
use Cake\Cache\Cache;
use Cake\Log\Log;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Mollie_API_Client;
use Picqer\Financials\Exact\Item;

/**
 * Test Controller
 *
 * @property CartComponent Cart
 * @property ExcelComponent Excel
 * @property MollieComponent Mollie
 * @property ExactSyncComponent ExactSync
 */
class TestController extends AdminController
{
    use MailerAwareTrait;

    public function initialize()
    {
        $this->loadComponent('ExactSync');
        $this->loadComponent('Cart');
        $this->loadComponent('Excel');
        $this->loadComponent('Mollie');
        parent::initialize();
    }

    public function index() {
        $this->set('data', $this->ExactSync->pushAccounts(TableRegistry::get('Customers')->get(2)));

    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function indexz()
    {
        $items = TableRegistry::get('Items')->find()->select(['id', 'code', 'title', 'weight', 'priority', 'stock'])
            ->contain(['ImageGroups' => function ($q) {
                return $q
                    ->select(['id', 'thumbnail'])
                    ->contain(['Images']);
            },
                'Categories'])->all();
        $items_excel = array();
        $sync_counts = (CacheUtils::getPreference('excel_for_stock')->int_val == 1);
        foreach ($items as $item) {
            $item_excel = array();
            $item_excel['id'] = h($item->get('id'));
            $item_excel['code'] = h($item->get('code'));
            $item_excel['title'] = h($item->get('title'));
            $item_excel['weight'] = strval(floatval($item->get('weight')));
            $item_excel['priority'] = strval(intval($item->get('priority')));
            if($sync_counts) {
                $item_excel['stock'] = strval(intval($item->get('stock')));
            }
            $categories = array();
            foreach ($item->get('categories') as $category) {
                array_push($categories, $category->id);
            }
            $item_excel['categories'] = implode(',', $categories);
            if(empty($item_excel['categories'])) {
                $item_excel['categories'] = null;
            }
            $img_group = $item->toArray()['image_group'];
            $img_list = $img_group['images'];
            $images = '';
            $item_excel['thumbnail'] = null;
            for ($i = 0; $i < count($img_list); $i++) {
                //::debug('no'.$img_list[$i]['id'].' vs '.$img_group['thumbnail']);
                if ($img_list[$i]['id'] == $img_group['thumbnail']) {
                    $item_excel['thumbnail'] = $img_list[$i]['name'];
                    //Log::debug('oye');
                } else {
                    //Log::debug('aye');
                    $image_name = $img_list[$i]['name'];
                    $images .= "$image_name=$image_name;";
                }
            }
            $item_excel['images'] = $images;
            if(empty($item_excel['images'])) {
                $item_excel['images'] = null;
            }
            array_push($items_excel, $item_excel);
        }

        $this->set('data', $items_excel);

        /*Cache::write('allPrefs',TableRegistry::get('Preferences')->find()->all()->toArray(), 'preferences');
        $dat = Cache::read('allPrefs', 'preferences');
        foreach ($dat as $pref) {
            $this->set('data', $pref['alias']);
        }*/
        //$this->set('data', json_encode());
       /* $Items = TableRegistry::get('Items');
        $items = $Items->find()
            ->matching('ItemPrices', function($q) {
            return $q->where([
                'ItemPrices.price >=' => '20',
                'ItemPrices.start_date <=' => new \DateTime(),
                'OR' => [['ItemPrices.end_date >' => new \DateTime()], ['ItemPrices.end_date IS' => null]]
            ])->limit(1);
        })->all();

        //TODO refresh all amounts, antihax
        $shipping = $this->Cart->calcShipping('NL');
        $cart = $this->Cart->getCart();*/

        //$this->set(compact('test', 'items', 'cart', 'shipping'));

        /*$this->ExactSync->tryConnect();
        $exact_model = new Item($this->ExactSync->connection);
        $data = $exact_model->filter('');

        $this->set('data', $data);
        $this->set('_serialize', ['test']);*/

        //$this->getMailer('AdminMail')->send('order', [-1]);
    }

    public function addToCart($id) {
        $this->Cart->add($id, 1);
        $this->Flash->success(__('Added item to cart'));
        return $this->redirect(['action' => 'index']);
    }

    public function removeFromCart($id) {
        $this->Cart->remove($id, 1);
        $this->Flash->success(__('Removed item from cart'));
        return $this->redirect(['action' => 'index']);
    }

    public function clearCart() {
        $this->Cart->clear();
        $this->Flash->success(__('Cleared the cart'));
        return $this->redirect(['action' => 'index']);
    }

    public function test() {
        $this->ExactSync->syncOrder(26);
    }

    public function mollie() {
        $mollie = new Mollie_API_Client;
        $mollie->setApiKey("test_QGWVRjTeyxAvJAwC7djuQ2rtWK4uap");
        $payment = $mollie->payments->create(array(
            "amount"      => 10.00,
            "description" => "My first API payment",
            "redirectUrl" => "https://exact.egordmitriev.net/admin/test/mollie2",
        ));
        Log::debug('Mollie payment:' . json_encode($payment));
    }

    public function mollie2() {
        Log::debug(json_encode($this->request));
    }

    protected function allowedPages()
    {
        return ['mollie2'];
    }


}
