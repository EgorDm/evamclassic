<?php

namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\Component\ExcelComponent;
use App\Utils\DataUtils;
use Aura\Intl\Exception;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 * @property ExcelComponent Excel
 */
class CategoriesController extends AdminController
{
    public function initialize()
    {
        $this->loadComponent('Excel');
        parent::initialize();

    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ($this->request->is('post')) {
            $keyword = $this->request->data['search'];
            $this->paginate = [
                'conditions' => [
                    'OR' => array(
                        'Categories.id' => $keyword,
                        'Categories.title LIKE' => "%$keyword%",
                        'Categories.description LIKE' => "%$keyword%"
                    )
                ],
                'limit' => 30
            ];
        }
        $categories = $this->paginate($this->Categories->find()->contain(['ParentCategories']));

        $categories_tree = $this->Categories->find('treeList');
        $this->set(compact('categories', 'categories_tree'));
        $this->set('_serialize', ['categories']);
    }

    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $category = $this->Categories->get($id, [
            'contain' => ['Items', 'ParentCategories']
        ]);

        $this->set('category', $category);
        $this->set('_serialize', ['category']);
    }

    public function syncExcel()
    {
        if ($this->request->is('post')) {
            if (!empty($this->request->data['excel_file']['tmp_name'])) {
                try {
                    $this->Excel->syncCategories($this->Categories, $this->request->data['excel_file']['tmp_name']);
                    $this->Flash->success(__('Succesfully synchronized categories with excel.'));
                } catch (\Exception $e) {
                    $this->Flash->error(__('Error occurred while synchronizing categories with excel.'));
                    Log::error($e->getMessage());
                }
            } else {
                $this->Flash->error(__('Error occurred while synchronizing categories with excel.'));
            }
        }
        return $this->redirectBack(['controller' => 'Categories', 'action' => 'index']);
    }

    public function exportExcel()
    {
        $ret = [];
        try {

            $categories = $this->Categories->find()->select(['id', 'title', 'parent_id'])->all();
            $local_id = DataUtils::indexEntityMap($categories, 'id');
            foreach ($categories as $data) {
                $cat_dat = ['ID' => $data->id, 'Name' => $data->title, 'Parent Name' => null, 'Parent ID' => null];
                if (!empty($data->parent_id)) {
                    if (!empty($local_id[$data->parent_id])) {
                        $cat_dat['Parent Name'] = $local_id[$data->parent_id]->title;
                        $cat_dat['Parent ID'] = $local_id[$data->parent_id]->id;
                    }
                }

                array_push($ret, $cat_dat);
            }
        } catch (Exception $e) {
            echo $e;
        }

        $writer = $this->Excel->writeDocument([
            'Name' => 'Name',
            'ID' => 'ID',
            'Parent Name' => 'Parent Name',
            'Parent ID' => 'Parent ID'
        ], $ret);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="webshop_categories.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
        die();
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $category = $this->Categories->newEntity();
        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->request->data);
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The category could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Categories->find('treeList');
        $this->set(compact('category', 'categories'));
        $this->set('_serialize', ['category']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $category = $this->Categories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->data);
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The category could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Categories->find('treeList');
        $this->set(compact('category', 'categories'));
        $this->set('_serialize', ['category']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Categories->get($id);
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function moveUp($id = null)
    {
        $this->request->allowMethod(['post', 'put']);
        $category = $this->Categories->get($id);
        if ($this->Categories->moveUp($category)) {
            $this->Flash->success('The category has been moved Up.');
        } else {
            $this->Flash->error('The category could not be moved up. Please, try again.');
        }
        return $this->redirect($this->referer(['action' => 'index']));
    }

    public function moveDown($id = null)
    {
        $this->request->allowMethod(['post', 'put']);
        $category = $this->Categories->get($id);
        if ($this->Categories->moveDown($category)) {
            $this->Flash->success('The category has been moved down.');
        } else {
            $this->Flash->error('The category could not be moved down. Please, try again.');
        }
        return $this->redirect($this->referer(['action' => 'index']));
    }

    public function clean()
    {
        $category_assoc_table = TableRegistry::get('ItemCategoryAssociations');
        $ents = $category_assoc_table->find()->all();
        $del_ids = array();
        $bent = array_keys(DataUtils::indexEntityMap($ents, 'item_id'));
        foreach ($bent as $item) {
            $alhave = array();
            foreach ($ents as $asoc) {
                if ($asoc->item_id == $item) {
                    if (in_array($asoc->category_id, $alhave)) {
                        array_push($del_ids, $asoc->id);
                    } else {
                        array_push($alhave, $asoc->category_id);
                    }
                }
            }
        }
        if (!empty($del_ids)) {
            Log::debug(json_encode($del_ids));
            $category_assoc_table->deleteAll(['id IN' => $del_ids]);
        }
    }

    public function aliase()
    {
        $categories = $this->Categories->find()->where(['alias' => ''])->all();
        $updt = array();
        foreach ($categories as $category) {
            array_push($updt, [
                'id' => $category->id,
                'alias' => "$category->parent_id-" . Text::slug($category->title)
            ]);
        }
        DataUtils::UpdateMultipleInto($this->Categories, ['id', 'alias'], $updt);
    }
}
