<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\AppController;

/**
 * ItemPrices Controller
 *
 * @property \App\Model\Table\ItemPricesTable $ItemPrices
 */
class ItemPricesController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Items']
        ];
        $itemPrices = $this->paginate($this->ItemPrices);

        $this->set(compact('itemPrices'));
        $this->set('_serialize', ['itemPrices']);
    }

    /**
     * View method
     *
     * @param string|null $id Item Price id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemPrice = $this->ItemPrices->get($id, [
            'contain' => ['Items']
        ]);

        $this->set('itemPrice', $itemPrice);
        $this->set('_serialize', ['itemPrice']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $itemPrice = $this->ItemPrices->newEntity();
        if ($this->request->is('post')) {
            $itemPrice = $this->ItemPrices->patchEntity($itemPrice, $this->request->data);
            if ($this->ItemPrices->save($itemPrice)) {
                $this->Flash->success(__('The item price has been saved.'));
                return $this->redirectBack(['controller' => 'ItemPrices', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The item price could not be saved. Please, try again.'));
            }
        }
        $items = $this->ItemPrices->Items->find('list');
        $this->set(compact('itemPrice', 'items'));
        $this->set('_serialize', ['itemPrice']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Item Price id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemPrice = $this->ItemPrices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemPrice = $this->ItemPrices->patchEntity($itemPrice, $this->request->data);
            if ($this->ItemPrices->save($itemPrice)) {
                $this->Flash->success(__('The item price has been saved.'));
                return $this->redirectBack(['controller' => 'ItemPrices', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The item price could not be saved. Please, try again.'));
            }
        }
        $items = $this->ItemPrices->Items->find('list');
        $this->set(compact('itemPrice', 'items'));
        $this->set('_serialize', ['itemPrice']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Item Price id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $itemPrice = $this->ItemPrices->get($id);
        if ($this->ItemPrices->delete($itemPrice)) {
            $this->Flash->success(__('The item price has been deleted.'));
        } else {
            $this->Flash->error(__('The item price could not be deleted. Please, try again.'));
        }
        return $this->redirectBack(['controller' => 'ItemPrices', 'action' => 'index']);
    }
}
