<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\AppController;

/**
 * ItemGroups Controller
 *
 * @property \App\Model\Table\ItemGroupsTable $ItemGroups
 */
class ItemGroupsController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $itemGroups = $this->paginate($this->ItemGroups);

        $this->set(compact('itemGroups'));
        $this->set('_serialize', ['itemGroups']);
    }

    /**
     * View method
     *
     * @param string|null $id Item Group id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itemGroup = $this->ItemGroups->get($id, [
            'contain' => ['Items']
        ]);

        $this->set('itemGroup', $itemGroup);
        $this->set('_serialize', ['itemGroup']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $itemGroup = $this->ItemGroups->newEntity();
        if ($this->request->is('post')) {
            $itemGroup = $this->ItemGroups->patchEntity($itemGroup, $this->request->data);
            if ($this->ItemGroups->save($itemGroup)) {
                $this->Flash->success(__('The item group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The item group could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('itemGroup'));
        $this->set('_serialize', ['itemGroup']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Item Group id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $itemGroup = $this->ItemGroups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemGroup = $this->ItemGroups->patchEntity($itemGroup, $this->request->data);
            if ($this->ItemGroups->save($itemGroup)) {
                $this->Flash->success(__('The item group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The item group could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('itemGroup'));
        $this->set('_serialize', ['itemGroup']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Item Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $itemGroup = $this->ItemGroups->get($id);
        if ($this->ItemGroups->delete($itemGroup)) {
            $this->Flash->success(__('The item group has been deleted.'));
        } else {
            $this->Flash->error(__('The item group could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
