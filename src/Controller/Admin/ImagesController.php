<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Utils\DataUtils;
use App\Utils\SystemUtils;
use Cake\Log\Log;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Association\BelongsToMany;
use ZipArchive;

/**
 * Images Controller
 *
 * @property \App\Model\Table\ImagesTable $Images
 */
class ImagesController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if($this->request->is('post')) {
            $keyword = $this->request->data['search'];
            $this->paginate = [
                'conditions' => [
                    'OR' => array(
                        'Images.id' => $keyword,
                        'Images.name LIKE' => "%$keyword%",
                        'Images.file_name LIKE' => "%$keyword%",
                        'Images.path LIKE' => "%$keyword%"
                    )
                ],
                'limit' => 30
            ];
        }
        $images = $this->paginate($this->Images);

        $this->set(compact('images'));
        $this->set('_serialize', ['images']);
    }

    /**
     * View method
     *
     * @param string|null $id Image id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => ['ImageGroups']
        ]);

        $this->set('image', $image);
        $this->set('_serialize', ['image']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $image = $this->Images->newEntity();
        if ($this->request->is('post')) {
            $image = $this->Images->patchEntity($image, $this->request->data, ['associated'=>['ImageGroups']]);
            if ($this->Images->save($image, ['associated'=>['ImageGroups']])) {
                $this->Flash->success(__('The image has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The image could not be saved. Please, try again.'));
            }
        }
        $imageGroups = $this->Images->ImageGroups->find('list', ['limit' => 200]);
        $this->set(compact('image', 'imageGroups'));
        $this->set('_serialize', ['image']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Image id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => ['ImageGroups']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $image = $this->Images->patchEntity($image, $this->request->data, ['associated'=>['ImageGroups']]);
            if ($this->Images->save($image, ['associated'=>['ImageGroups']])) {
                $this->Flash->success(__('The image has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The image could not be saved. Please, try again.'));
            }
        }
        $imageGroups = $this->Images->ImageGroups->find('list', ['limit' => 200]);
        $this->set(compact('image', 'imageGroups'));
        $this->set('_serialize', ['image']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Image id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $image = $this->Images->get($id);
        if ($this->Images->delete($image)) {
            $this->Flash->success(__('The image has been deleted.'));
        } else {
            $this->Flash->error(__('The image could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function rawView($id)
    {
        $this->autoRender = false;
        $image = $this->Images->find()->where(['id' => $id])
            ->select(['id', 'path', 'thumbnail_mask'])->first();

        if ($this->request->query('medium') == 1 && ($image->get('thumbnail_mask') == 1 || $image->get('thumbnail_mask') == 3)) {
            $file = 'img' . DS . 'uploads' . DS . 'thumbnail_medium' . DS . $image->get('path');
        } else {
            $file = 'img' . DS . 'uploads' . DS . $image->get('path');
        }

        if (!file_exists($file)) {
            throw new NotFoundException('Image could not be found.');
        }

        header("Content-Type: image/jpeg");
        header("Content-Length: " . filesize($file));
        readfile($file);
    }

    const extension_whitelist = ['png', 'jpg', 'jpeg'];

    public function batchUpload()
    {
        if ($this->request->is('post')) {
            set_time_limit(0);
            if (!empty($this->request->data['zip_file']['tmp_name'])) {
                $zip = new ZipArchive;
                $path = $this->request->data['zip_file']['tmp_name'];
                if ($zip->open($path) === TRUE) {
                    $target_dir = pathinfo($this->request->data['zip_file']['tmp_name'],PATHINFO_DIRNAME).DS.'test';
                    if (!file_exists($target_dir)) {
                        mkdir($target_dir.DS, 0777, true);
                    }
                    for ($i = 0; $i < $zip->numFiles; $i++) {
                        $filename = $zip->getNameIndex($i);
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if (in_array($ext, self::extension_whitelist)) {
                            copy("zip://" . $path . "#" . $filename, $target_dir . DS . pathinfo($filename, PATHINFO_BASENAME));
                        }
                    }
                    $zip->close();
                    $files = glob($target_dir.'/*.{jpg,png,gif,jpeg}', GLOB_BRACE);
                    $names = array();
                    foreach($files as $file) {
                        array_push($names, pathinfo($file, PATHINFO_BASENAME));
                    }
                    $images = $this->Images->find()->where(['name IN' => $names])->all();
                    $images =  DataUtils::indexEntityMap($images, 'name');
                    $updated = 0;
                    $inserted = 0;
                    foreach($files as $file) {
                        $create_data = [
                            'name' => pathinfo($file, PATHINFO_BASENAME),
                            'path' => [
                                'name' => pathinfo($file, PATHINFO_BASENAME),
                                'tmp_name' => $file,
                                'error' => 0
                            ],
                            'thumbnail_mask' => $this->request->data['thumbnail_mask']
                        ];
                        Log::debug('Create Data: ' . json_encode($create_data));
                        if(!empty($images[pathinfo($file, PATHINFO_BASENAME)])) {
                            $image = $images[pathinfo($file, PATHINFO_BASENAME)];
                            $updated++;
                        } else {
                            $image = $this->Images->newEntity();
                            $inserted++;
                        }
                        $image = $this->Images->patchEntity($image, $create_data);
                        $this->Images->save($image);
                    }

                    SystemUtils::deldir($target_dir);
                    $this->Flash->success(__("$updated images were updated and $inserted were created."));
                }
            }
        }
        return $this->redirectBack(['controller' => 'Items', 'action' => 'index']);
    }

}
