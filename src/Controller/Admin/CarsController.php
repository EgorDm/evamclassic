<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\Component\ExcelComponent;
use Cake\Event\Event;
use Cake\Log\Log;

/**
 * Cars Controller
 *
 * @property \App\Model\Table\CarsTable $Cars
 * @property ExcelComponent Excel
 */
class CarsController extends AdminController
{
    public function initialize()
    {
        $this->loadComponent('Excel');
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if($this->request->is('post')) {
            $keyword = $this->request->data['search'];
            $this->paginate = [
                'conditions' => [
                    'OR' => array(
                        'Cars.id' => $keyword,
                        'Cars.reference LIKE' => "%$keyword%",
                        'Cars.title LIKE' => "%$keyword%",
                        'Cars.description LIKE' => "%$keyword%"
                    )
                ],
                'limit' => 30
            ];
        }
        $cars = $this->paginate($this->Cars);

        $this->set(compact('cars'));
        $this->set('_serialize', ['cars']);
    }

    /**
     * View method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $car = $this->Cars->get($id, [
            'contain' => [
                'ImageGroups' => function ($q) {
                    return $q
                        ->autoFields(true)
                        ->contain(['Images']);
                }
            ]
        ]);

        $this->set('car', $car);
        $this->set('_serialize', ['car']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $car = $this->Cars->newEntity();
        if ($this->request->is('post')) {
            $car = $this->Cars->patchEntity($car, $this->request->data);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('The car has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The car could not be saved. Please, try again.'));
            }
        }
        $imageGroups = $this->Cars->ImageGroups->find('list', ['limit' => 200]);
        $this->set(compact('car', 'imageGroups'));
        $this->set('_serialize', ['car']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $car = $this->Cars->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $car = $this->Cars->patchEntity($car, $this->request->data);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('The car has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The car could not be saved. Please, try again.'));
            }
        }
        $imageGroups = $this->Cars->ImageGroups->find('list');
        $this->set(compact('car', 'imageGroups'));
        $this->set('_serialize', ['car']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $car = $this->Cars->get($id);
        if ($this->Cars->delete($car)) {
            $this->Flash->success(__('The car has been deleted.'));
        } else {
            $this->Flash->error(__('The car could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function syncExcel()
    {
        if ($this->request->is('post')) {
            if (!empty($this->request->data['excel_file']['tmp_name'])) {
                try {
                    $this->Excel->syncCars($this->Cars, $this->request->data['excel_file']['tmp_name']);
                    $this->Flash->success(__('Succesfully synchronized cars with excel.'));
                } catch (\Exception $e) {
                    $this->Flash->error(__('Error occurred while synchronizing cars with excel.'));
                    Log::error(json_encode($e));
                }
            } else {
                $this->Flash->error(__('Error occurred while synchronizing cars with excel.'));
            }
        }
        return $this->redirectBack(['controller' => 'Cars', 'action' => 'index']);
    }

    public function exportExcel()
    {
        $this->autoRender = false;
        $this->layout = false;
        $this->render(false);
        try {
            $cars = $this->Cars->find()->select([
                'id', 'reference', 'title', 'description', 'category', 'model', 'year', 'color',
                'condition', 'price', 'status', 'image_group_id', 'priority'
            ])
                ->contain(['ImageGroups' => function ($q) {
                    return $q
                        ->select(['id', 'thumbnail'])
                        ->contain(['Images']);
                }])->all();
            $cars_excel = array();
            foreach ($cars as $car) {
                $car_excel = array();
                $car_excel['id'] = h($car->get('id'));
                $car_excel['reference'] = h($car->get('reference'));
                $car_excel['title'] = h($car->get('title'));
                $car_excel['description'] = $car->get('description');
                $car_excel['category'] = h($car->get('category'));
                $car_excel['model'] = h($car->get('model'));
                $car_excel['year'] = h($car->get('year'));
                $car_excel['color'] = h($car->get('color'));
                $car_excel['condition'] = h($car->get('condition'));
                $car_excel['price'] = h($car->get('price'));
                $car_excel['status'] = strval(intval($car->get('status')));
                $car_excel['priority'] = strval(intval($car->get('priority')));
                $img_group = $car->toArray()['image_group'];
                $img_list = $img_group['images'];
                $images = '';
                $car_excel['thumbnail'] = '';
                for ($i = 0; $i < count($img_list); $i++) {
                    if ($img_list[$i]['id'] == $img_group['thumbnail']) {
                        $car_excel['thumbnail'] = $img_list[$i]['name'];
                    } else {
                        $image_name = $img_list[$i]['name'];
                        $images .= "$image_name=$image_name;";
                    }
                }
                $car_excel['images'] = $images;
                array_push($cars_excel, $car_excel);
            }

            $writer = $this->Excel->writeDocument([
                'id' => 'ID',
                'reference' => 'reference',
                'title' => 'title',
                'description' => 'description',
                'category' => 'category',
                'model' => 'model',
                'year' => 'year',
                'color' => 'color',
                'condition' => 'condition',
                'price' => 'price',
                'status' => 'status',
                'thumbnail' => 'Image1',
                'images' => 'Image2',
                'priority' => 'Priority'
            ], $cars_excel);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="webshop_cars.xlsx"');
            header('Cache-Control: max-age=0');
            ob_end_clean();//Important or corrupt
            $writer->save('php://output');
        } catch (\Exception $e) {
            $this->Flash->error(__('Error occurred while exporting cars to excel.'));
            return $this->redirectBack(['controller' => 'Cars', 'action' => 'index']);
        }
    }
}
