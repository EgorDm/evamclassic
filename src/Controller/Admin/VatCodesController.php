<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\AppController;
use App\Controller\Component\ExactSyncComponent;
/**
 * VatCodes Controller
 *
 * @property \App\Model\Table\VatCodesTable $VatCodes
 * @property ExactSyncComponent ExactSync
 */
class VatCodesController extends AdminController
{

    public function initialize()
    {
        $this->loadComponent('ExactSync');
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $vatCodes = $this->paginate($this->VatCodes);

        $this->set(compact('vatCodes'));
        $this->set('_serialize', ['vatCodes']);
    }

    public function synchronize()
    {

        $success = $this->ExactSync->syncModel('Picqer\Financials\Exact\VatCode', 'VatCodes', false, true);

        if ($success == true) {
            $this->Flash->success(__('Succesfully synchronized items with exact.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Error occurred while synchronizing items with exact.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Vat Code id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vatCode = $this->VatCodes->get($id, [
            'contain' => []
        ]);

        $this->set('vatCode', $vatCode);
        $this->set('_serialize', ['vatCode']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vatCode = $this->VatCodes->newEntity();
        if ($this->request->is('post')) {
            $vatCode = $this->VatCodes->patchEntity($vatCode, $this->request->data);
            if ($this->VatCodes->save($vatCode)) {
                $this->Flash->success(__('The vat code has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The vat code could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('vatCode'));
        $this->set('_serialize', ['vatCode']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vat Code id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vatCode = $this->VatCodes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vatCode = $this->VatCodes->patchEntity($vatCode, $this->request->data);
            if ($this->VatCodes->save($vatCode)) {
                $this->Flash->success(__('The vat code has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The vat code could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('vatCode'));
        $this->set('_serialize', ['vatCode']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vat Code id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vatCode = $this->VatCodes->get($id);
        if ($this->VatCodes->delete($vatCode)) {
            $this->Flash->success(__('The vat code has been deleted.'));
        } else {
            $this->Flash->error(__('The vat code could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
