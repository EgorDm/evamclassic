<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\AppController;
use Cake\Log\Log;

/**
 * ShippingMethods Controller
 *
 * @property \App\Model\Table\ShippingMethodsTable $ShippingMethods
 */
class ShippingMethodsController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $shippingMethods = $this->paginate($this->ShippingMethods);

        $this->set(compact('shippingMethods'));
        $this->set('_serialize', ['shippingMethods']);
    }

    /**
     * View method
     *
     * @param string|null $id Shipping Method id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $shippingMethod = $this->ShippingMethods->get($id, [
            'contain' => ['ShippingOptions']
        ]);

        $this->set('shippingMethod', $shippingMethod);
        $this->set('_serialize', ['shippingMethod']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $shippingMethod = $this->ShippingMethods->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['countries'] = implode(',', $this->request->data['countries']);
            $shippingMethod = $this->ShippingMethods->patchEntity($shippingMethod, $this->request->data);
            if ($this->ShippingMethods->save($shippingMethod)) {
                $this->Flash->success(__('The shipping method has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The shipping method could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('shippingMethod'));
        $this->set('_serialize', ['shippingMethod']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Shipping Method id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $shippingMethod = $this->ShippingMethods->get($id, [
            'contain' => []
        ]);
        $shippingMethod->countries = explode(',', $shippingMethod->countries);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['countries'] = implode(',', $this->request->data['countries']);
            $shippingMethod = $this->ShippingMethods->patchEntity($shippingMethod, $this->request->data);
            if ($this->ShippingMethods->save($shippingMethod)) {
                $this->Flash->success(__('The shipping method has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The shipping method could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('shippingMethod'));
        $this->set('_serialize', ['shippingMethod']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Shipping Method id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $shippingMethod = $this->ShippingMethods->get($id);
        if ($this->ShippingMethods->delete($shippingMethod)) {
            $this->Flash->success(__('The shipping method has been deleted.'));
        } else {
            $this->Flash->error(__('The shipping method could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
