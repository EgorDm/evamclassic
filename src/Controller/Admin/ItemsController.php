<?php
namespace App\Controller\Admin;

use Admin\Controller\AdminController;

use App\Controller\Component\ExactSyncComponent;
use App\Controller\Component\ExcelComponent;
use App\Utils\CacheUtils;
use Cake\Chronos\Date;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use DebugKit\Routing\Filter\DebugBarFilter;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 * @property ExactSyncComponent ExactSync
 * @property ExcelComponent Excel
 */
class ItemsController extends AdminController
{

    public function initialize()
    {
        $this->loadComponent('ExactSync');
        $this->loadComponent('Excel');
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ($this->request->is('post')) {
            $keyword = $this->request->data['search'];
            $this->paginate = [
                'contain' => ['ItemGroups', 'Categories'],
                'conditions' => [
                    'OR' => array(
                        'Items.id' => $keyword,
                        'Items.code LIKE' => "%$keyword%",
                        'Items.title LIKE' => "%$keyword%",
                        'Items.description LIKE' => "%$keyword%",
                        'Items.tags_english LIKE' => "%$keyword%",
                        'Items.tags_german LIKE' => "%$keyword%",
                        'Items.tags_french LIKE' => "%$keyword%",
                    )
                ],
                'limit' => 30
            ];
        } else {
            $this->paginate = [
                'contain' => ['ItemGroups', 'Categories'],
                'limit' => 30
            ];
        }

        $items = $this->paginate($this->Items);

        $this->set(compact('items'));
        $this->set('_serialize', ['items']);
    }

    public function synchronize() {
        return $this->softsynchronize();
    }

    public function hardsynchronize()//hardsynchronize()
    {
        $group = $this->request->query['group'];
        $filter = $group ? "ItemGroup eq guid'$group'" : '';
        $success = $this->ExactSync->syncModel('Picqer\Financials\Exact\ItemGroup', 'ItemGroups', true, true);

        if ($success == true) {
            Log::debug('HardSync Items');
            $success = $this->ExactSync->syncModel('Picqer\Financials\Exact\Item', 'Items', false, true,
                ['item_group_id' => ['ItemGroups', 'ItemGroup', false]], $filter);
        }
        if ($success == true) {
            $success = $this->ExactSync->syncPrices(true);
        }
        if ($success == true) {
            $success = $this->ExactSync->syncWarehouses();
        }

        $prefs = TableRegistry::get('Preferences');
        $prefs->updateAll(['text_val' => Time::now()->toDateTimeString()], ['alias' => 'sync_products_last']);

        if ($success == true) {
            $this->Flash->success(__('Succesfully synchronized items with exact.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Error occurred while synchronizing items with exact.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function softsynchronize() //synchronize()
    {
        //Log::debug('SoftSync Items');
        $success = $this->ExactSync->syncModel('Picqer\Financials\Exact\ItemGroup', 'ItemGroups', false, false);

        if ($success == true) {
            $success = $this->ExactSync->syncModel('Picqer\Financials\Exact\Item', 'Items', false, false,
                ['item_group_id' => ['ItemGroups', 'ItemGroup', false]]);
        }
        if ($success == true) {
            $success = $this->ExactSync->syncPrices(true);
        }
        if ($success == true) {
            $success = $this->ExactSync->syncWarehouses();
        }

        $prefs = TableRegistry::get('Preferences');
        $prefs->updateAll(['text_val' => Time::now()->toDateTimeString()], ['alias' => 'sync_products_last']);

        if ($success == true) {
            $this->Flash->success(__('Succesfully synchronized items with exact.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Error occurred while synchronizing items with exact.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function syncExcel()
    {
        if ($this->request->is('post')) {
            if (!empty($this->request->data['excel_file']['tmp_name'])) {
                try {
                    $this->Excel->syncItems($this->Items, $this->request->data['excel_file']['tmp_name']);
                    $this->Flash->success(__('Succesfully synchronized items with excel.'));
                } catch (\Exception $e) {
                    $this->Flash->error(__('Error occurred while synchronizing items with excel.'));
                    Log::error($e->getMessage());
                }
            } else {
                $this->Flash->error(__('Error occurred while synchronizing items with excel.'));
            }
        }
        return $this->redirectBack(['controller' => 'Items', 'action' => 'index']);
    }

    public function exportExcel()
    {
        $this->autoRender = false;
        $this->layout = false;
        $this->render(false);
        try {
            $items = $this->Items->find()->select(['id', 'status', 'code', 'title', 'weight', 'priority',
                'stock', 'tags_english', 'tags_german', 'tags_french', 'urls', 'status'])
                ->contain(['ImageGroups' => function ($q) {
                    return $q
                        ->select(['id', 'thumbnail'])
                        ->contain(['Images']);
                },
                    'Categories'])->all();
            $items_excel = array();
            $sync_counts = (CacheUtils::getPreference('excel_for_stock')->int_val == 1);
            foreach ($items as $item) {
                $item_excel = array();
                $item_excel['id'] = h($item->get('id'));
                $item_excel['status'] = h($item->get('status'));
                $item_excel['code'] = h($item->get('code'));
                $item_excel['title'] = h($item->get('title'));
                $item_excel['weight'] = strval(floatval($item->get('weight')));
                $item_excel['priority'] = strval(intval($item->get('priority')));
                $item_excel['tags_english'] = h(strip_tags($item->get('tags_english')));
                $item_excel['tags_german'] = h(strip_tags($item->get('tags_german')));
                $item_excel['tags_french'] = h(strip_tags($item->get('tags_french')));
                $item_excel['urls'] = h(strip_tags($item->get('urls')));
                //if($sync_counts) {
                    $item_excel['stock'] = strval(intval($item->get('stock')));
                //}
                $categories = array();
                foreach ($item->get('categories') as $category) {
                    array_push($categories, $category->id);
                }
                $item_excel['categories'] = implode(',', $categories);
                if(empty($item_excel['categories'])) {
                    $item_excel['categories'] = null;
                }
                $img_group = $item->toArray()['image_group'];
                $img_list = $img_group['images'];
                $images = '';
                $item_excel['thumbnail'] = null;
                for ($i = 0; $i < count($img_list); $i++) {
                    if ($img_list[$i]['id'] == $img_group['thumbnail']) {
                        $item_excel['thumbnail'] = $img_list[$i]['name'];
                    } else {
                        $image_name = $img_list[$i]['name'];
                        $images .= "$image_name=$image_name;";
                    }
                }
                $item_excel['images'] = $images;
                if(empty($item_excel['images'])) {
                    $item_excel['images'] = null;
                }
                array_push($items_excel, $item_excel);
            }

            $writer = $this->Excel->writeDocument(
                ['id' => 'ID', 'status' => 'Status', 'code' => 'Partnummer', 'title' => 'Titel', 'categories' => 'Categories', 'weight' => 'Gewicht', 'priority' => 'Priority',
                    'thumbnail' => 'Image1', 'images' => 'Image2','stock' => 'Aantal', 'tags_english' => 'TagsEnglish', 'tags_german' => 'TagsGerman', 'tags_french' => 'TagsFrench',
                    'urls' => 'Urls'
                ],
                $items_excel);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="webshop_items.xlsx"');
            header('Cache-Control: max-age=0');
            ob_end_clean();//Important or corrupt
            $writer->save('php://output');
            exit();
        } catch (\Exception $e) {
            $this->Flash->error(__('Error occurred while exporting items to excel.'));
            return $this->redirectBack(['controller' => 'Items', 'action' => 'index']);
        }
    }

    public function cleanimages() {
        //return $this->redirectBack(['controller' => 'Items', 'action' => 'index']);
    }

    /**
     * View method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => ['ItemGroups', 'ItemPrices', 'Categories',
                'ImageGroups' => function ($q) {
                    return $q
                        ->autoFields(true)
                        ->contain(['Images']);
                }
            ]
        ]);

        $this->set('item', $item);
        $this->set('_serialize', ['item']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $item = $this->Items->newEntity();
        if ($this->request->is('post')) {
            $item = $this->Items->patchEntity($item, $this->request->data);
            if ($this->Items->save($item)) {
                $this->Flash->success(__('The item has been saved.'));
                return $this->redirectBack(['controller' => 'Items', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The item could not be saved. Please, try again.'));
            }
        }
        $itemGroups = $this->Items->ItemGroups->find('list', ['limit' => 200]);
        $categories = $this->Items->Categories->find('treeList');
        $imageGroups = $this->Items->ImageGroups->find('list');
        $this->set(compact('item', 'itemGroups', 'imageGroups', 'categories'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => ['Categories']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $item = $this->Items->patchEntity($item, $this->request->data);
            if ($this->Items->save($item)) {
                $this->Flash->success(__('The item has been saved.'));
                $this->redirectBack(['controller' => 'Items', 'action' => 'index']);
                return;
            } else {
                $this->Flash->error(__('The item could not be saved. Please, try again.'));
            }
        }
        $itemGroups = $this->Items->ItemGroups->find('list');
        $categories = $this->Items->Categories->find('treeList');
        $imageGroups = $this->Items->ImageGroups->find('list');
        $model = $this->Items;
        $this->set(compact('item', 'itemGroups', 'imageGroups', 'categories', 'model'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $item = $this->Items->get($id);
        if ($this->Items->delete($item)) {
            $this->Flash->success(__('The item has been deleted.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }
        return $this->redirectBack(['controller' => 'Items', 'action' => 'index']);
    }

    protected function allowedPages()
    {
        return ['synchronize', 'softsynchronize', 'hardsynchronize', 'test'];
    }


}
