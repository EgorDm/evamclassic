<?php
namespace App\Controller;

use App\Controller\Component\ExactSyncComponent;
use App\Controller\Component\MollieComponent;
use App\Form\OrderAddressStepsForm;
use App\Model\Entity\Order;
use App\Utils\CacheUtils;
use App\Utils\CheckoutSteps;
use App\Utils\DataUtils;
use App\Utils\PaymentMethods;
use App\Utils\ShippingStatus;
use Cake\Log\Log;
use Cake\Mailer\MailerAwareTrait;
use Cake\Network\Exception\UnauthorizedException;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Customer\Helper\Countries;
use Mollie_API_Object_Payment;
use Orders\Model\Total;
use PaymentInvoiceRequest\Helper\InvoiceRequest;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Orders Controller
 * @property ExactSyncComponent ExactSync
 * @property MollieComponent Mollie
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{
    use MailerAwareTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Mollie');
        $this->loadComponent('ExactSync');
        $this->set('wide', true);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        //$orders = $this->paginate($this->Orders);

        /* $this->set(compact('orders'));
         $this->set('_serialize', ['orders']);*/
    }

    public function reset()
    {
        $session = $this->request->session();
        $session->delete('Order');
        return $this->redirect(['action' => 'checkout']);
    }

    public function view($id)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['BillingAddresses', 'ShippingAddresses', 'ShippingMethods', 'ShippingOptions']
        ]);
        $session = $this->request->session();
        $user_id = $this->Auth->user('id');
        //Log::debug('test '.$session->check('Auth.User.fullname'));
        if ($order->customer_id == $user_id || $session->check('Auth.User.fullname')) {
            $total = \Orders\Helper\Total::calculate($order);

            if (empty($order->payment_id)) { //TODO: bank Transfer
                try {
                    $payment_data = [
                        "amount" => $total->getFinal(),
                        "description" => "Betaling voor ordernr. $order->id bij Evam Classic webshop.",
                        "redirectUrl" => Router::url(["controller" => "Orders", "action" => "success", 'order' => $order->id], true),
                        "webhookUrl" => Router::url(["controller" => "Orders", "action" => "webhook"], true),
                        "metadata" => array(
                            "order_id" => $order->id,
                        ),
                    ];
                    $payment_method = $this->Mollie->convertPaymentMethod($order->payment_method);
                    if (!empty($payment_method)) {
                        $payment_data['method'] = $payment_method;
                    }
                    $payment = $this->Mollie->createPayment($payment_data);
                    $order->set('payment_id', $payment->id);
                    $this->Orders->save($order);
                } catch (\Exception $e) {
                    Log::error($e->getMessage() . $e->getTraceAsString());
                }
            }
        } else {
            throw new UnauthorizedException();
        }

        $payment_status = $this->checkPaymentStatus($order->payment_id, $order);
        $shipment_status = $order->shipment_status;
        $this->set(compact(['order', 'payment_status', 'shipment_status']));
    }

    public function tezt($id)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['BillingAddresses', 'ShippingAddresses', 'ShippingMethods', 'ShippingOptions']
        ]);
        $session = $this->request->session();
        $user_id = $this->Auth->user('id');
        //Log::debug('test '.$session->check('Auth.User.fullname'));
        if ($order->customer_id == $user_id || $session->check('Auth.User.fullname')) {
            $total = \Orders\Helper\Total::calculate($order);

            if (empty($order->payment_id)) { //TODO: bank Transfer
                try {
                    $payment_data = [
                        "amount" => $total->getFinal(),
                        "description" => "Betaling voor ordernr. $order->id bij Evam Classic webshop.",
                        "redirectUrl" => Router::url(["controller" => "Orders", "action" => "success", 'order' => $order->id], true),
                        "webhookUrl" => Router::url(["controller" => "Orders", "action" => "webhook"], true),
                        "metadata" => array(
                            "order_id" => $order->id,
                        ),
                    ];
                    $payment_method = $this->Mollie->convertPaymentMethod($order->payment_method);
                    if (!empty($payment_method)) {
                        $payment_data['method'] = $payment_method;
                    }
                    $payment = $this->Mollie->createPayment($payment_data);
                    $order->set('payment_id', $payment->id);
                    $this->Orders->save($order);
                } catch (\Exception $e) {
                    Log::error($e->getMessage() . $e->getTraceAsString());
                }
            }
        } else {
            throw new UnauthorizedException();
        }

        $asscoiations_table = TableRegistry::get('OrderItemAssociations');
        $associations = $asscoiations_table->find()->where(['order_id' => $order->id])->all();
        $associations = DataUtils::indexEntityMap($associations, 'item_id');
        $items = $this->Orders->Items->find()
            ->where(['Items.id IN' => array_keys($associations), 'stock >' => 0])
            ->matching('ItemPrices', function ($q) {
                return $q->where([
                    'ItemPrices.start_date <=' => new \DateTime(),
                    'OR' => [['ItemPrices.end_date >' => new \DateTime()], ['ItemPrices.end_date IS' => null]]
                ])->limit(1);
            })
            ->contain(['VatCodes' => function ($q) {
                return $q->select(['code_vat', 'percentage']);
            }])
            ->all();

        $in_eu = (strpos(CacheUtils::getPreference('eu_countries')->text_val, $order->shipping_address->country) !== false);
        $vat_prices = $this->calculateVat($order, $items, $associations);

        dd([$in_eu, $vat_prices, $this->calculateTotal($order, $items, $vat_prices, $in_eu)]); die();

        $payment_status = $this->checkPaymentStatus($order->payment_id, $order);
        $shipment_status = $order->shipment_status;
        $this->set(compact(['order', 'payment_status', 'shipment_status']));
    }

    public function success()
    {
        $session = $this->request->session();
        $session->delete('Order');
        $session->delete('Cart');
        $order_id = $this->request->query['order'];

        //TODO: move into webhook function once it works
        $order = $this->Orders->get($order_id, ['contain' => ['Customers', 'Items']]);
        if (!empty($order) && empty($order->mailed)) {
            $order->mailed = true;
            $this->Orders->save($order);
            $this->getMailer('CustomerMail')->send('orderPlaced', [$order]);
            $this->getMailer('AdminMail')->send('order', [$order_id]);

            $associations_table = TableRegistry::get('OrderItemAssociations');
            $assoc = $associations_table->find()->where(['order_id' => $order->id])->all();
            $item_ids = array();
            foreach ($assoc as $asso) {
                array_push($item_ids, $asso->item_id);
            }
            $items_table = TableRegistry::get('Items');
            $items = DataUtils::indexEntityMap($items_table->find()->where(['id IN' => $item_ids])->all(), 'id');
            $update_me = array();
            foreach ($assoc as $asso) {
                array_push($update_me, ['id' => $asso->item_id, 'stock' => $items[$asso->item_id]->stock - $asso->quantity]);
            }
            DataUtils::UpdateMultipleInto($items_table, ['id', 'stock'], $update_me);
        }

        $this->set(compact(['order_id']));
        $this->set('bank_transfer', $order->payment_method == PaymentMethods::BANK_TRANSFER);
    }

    public function continueOrder($id)
    {
        $order = $this->Orders->get($id);
        if($order->get('local_status') == CheckoutSteps::PAYMENT) {
            $order->set('local_status', CheckoutSteps::OVERVIEW);
            $order->set('payment_id', null);
            $this->Orders->save($order);
        }
        $user_id = $this->Auth->user('id');
        if ($order->customer_id != $user_id) {
            throw new UnauthorizedException();
        }
        $session = $this->request->session();
        $session->write('Order.id', $order->id);
        return $this->redirect(['action' => 'checkout']);
    }

    public function checkout()
    {
        $session = $this->request->session();
        if (!$session->read('Auth.Customer.id')) {
            $session->write('Auth.redirect', Router::url(['action' => 'checkout', 'controller' => 'Orders'], true));
            $this->Flash->set(__('Login or register to continue with your order.'));
            return $this->redirect(['action' => 'login', 'controller' => 'Customers']);
        }

        $user_id = $this->Auth->user('id');
        $result = $this->checkoutSteps($user_id);

        $this->set('order', $result[0]);
        $this->set('step', $result[1]);
    }

    public function newCheckout()
    {
        $session = $this->request->session();
        $session->delete('Order');
        $this->redirect(['action' => 'checkout']);
    }

    public function webhook()
    {
        $builder = $this->viewBuilder();
        $builder->layout('ajax');
        $builder->template('ajax_success');
        Log::debug('Yes webhook: ' . json_encode($this->request));
        if ($this->request->is('post')) {
            $payment_id = $this->request->data['id'];
            $this->checkPaymentStatus($payment_id);
        }
    }

    private function checkPaymentStatus($payment_id, $order = null)
    {
        if (empty($order)) {
            $order = $this->Orders->find()->where(['payment_id' => $payment_id])->first();
        }

        if($order->payment_method == PaymentMethods::BANK_TRANSFER_LOCAL)  {
            if($order->shipment_status == ShippingStatus::SHIPPED) {
                return Mollie_API_Object_Payment::STATUS_PAID;
            }
            return Mollie_API_Object_Payment::STATUS_PENDING;
        }

        $payment = $this->Mollie->getPayment($payment_id, $order->payment_method);
        if (empty($payment)) return Mollie_API_Object_Payment::STATUS_PENDING;
        $payment_status = $this->Mollie->mollieStatusConvert($payment->status);

        if (empty($order->exact_guid) && $payment_status == CheckoutSteps::PMNT_PAID) {
            $this->ExactSync->syncOrder($order->id);
        }

        if ($order->local_status != $payment_status) {
            $order->set('local_status', $payment_status);
            $this->Orders->save($order);
        }
        if (!empty($order)) {
            return $this->Mollie->convertStatusToMollie($order->local_status);
        }
        return Mollie_API_Object_Payment::STATUS_PENDING;
    }

    private function checkoutSteps($user_id)
    {
        $session = $this->request->session();
        if ($session->check('Order')) {
            $order_id = $session->read('Order.id');
            $order = $this->Orders->find()->where(['Orders.id' => $order_id, 'Orders.customer_id' => $user_id])
                ->contain(['BillingAddresses', 'ShippingAddresses', 'ShippingMethods', 'ShippingOptions'])->first();
        } else {
            $order = $this->Orders->newEntity(['customer_id' => $this->Auth->user('id')]);
        }

        $step = $order->get('local_status');
        if ($step != null && $step < CheckoutSteps::PAYMENT && isset($this->request->query['step'])
            && abs($this->request->query['step']) < $step
        ) {
            $step = intval($this->request->query['step']);
        }

        if (empty($order->local_status) || $step == CheckoutSteps::SHIPPING_ADDRESS) {
            $result = $this->stepOne($order);
        }
        if ($step == CheckoutSteps::BILLING_ADDRESS) {
            $result = $this->stepTwo($order);
        }
        if ($step == CheckoutSteps::SHIPPING_METHOD) {
            $result = $this->stepThree($order);
        }
        if ($step == CheckoutSteps::SHIPPING_METHOD) {
            $result = $this->stepThree($order);
        }
        if ($step == CheckoutSteps::PAYMENT_METHOD) {
            $result = $this->stepFour($order);
        }
        if ($step == CheckoutSteps::OVERVIEW) {
            $result = $this->stepFive($order);
        }
        if ($step == CheckoutSteps::PAYMENT) {
            $result = $this->stepSix($order);
        }
        if (isset($result)) {
            if ($result === -1) {
                return [$order, $step];
            }
            if (!empty($result)) {
                unset($this->request->query['step']);
                return $this->checkoutSteps($user_id);
            }
        }
        return [$order, $step];
    }

    private function stepOne($order)
    {
        $order_form = new OrderAddressStepsForm();
        if ($this->request->is('post') && $this->request->data['step'] == CheckoutSteps::SHIPPING_ADDRESS) {
            $cart = $this->Cart->getCart();
            if ((empty($cart) || count($cart['CartItem']) == 0) && empty($order->shipping_method_id)) {
                $this->Flash->error(__('You have no items in your cart.'));
                $this->redirect(['action' => 'index', 'controller' => 'Products']);
                return -1;
            }
            $data = $this->request->data;
            $data['customer_id'] = $order->customer_id;
            $result = $order_form->execute($data);
            if (!empty($result)) {
                $session = $this->request->session();
                $next_step = CheckoutSteps::BILLING_ADDRESS;
                if (isset($result['billing_id'])) {
                    $next_step = CheckoutSteps::SHIPPING_METHOD;
                }
                if (empty($order->local_status) || $order->local_status < $next_step) {
                    $result['local_status'] = $next_step;
                }
                $order = $this->Orders->patchEntity($order, $result);
                if (!empty($order->shipping_method_id)) {
                    $order = $this->patchShipping($order);
                    if ($order == null) {
                        $this->Flash->error(__('Something went wrong while saving the shipping method. Please try again later.'));
                        $this->redirect(['action' => 'index', 'controller' => 'Products']);
                        return -1;
                    }
                } else {
                    if ($this->Orders->save($order)) {
                        $session->write('Order.cart', $cart);
                        $order = $this->saveCart($order);
                        if ($order == null) {
                            $this->Flash->error(__('Something went wrong while saving your cart.'));
                            $this->redirect(['action' => 'index', 'controller' => 'Products']);
                            return -1;
                        }
                    }
                }
                $order->set('payment_id', null);
                if ($this->Orders->save($order)) {
                    $session->write('Order.id', $order->id);
                    unset($this->request->data['address_id']);
                    return $order;
                }
            }
        }
        $this->loadAddressList($order->customer_id, CheckoutSteps::SHIPPING_ADDRESS);
        if (isset($this->request->data['address_id'])) {
            $this->set('address_selected', $this->request->data['address_id']);
        }
        $this->set(compact('order_form'));
        return null;
    }

    private function stepTwo($order)
    {
        $order_form = new OrderAddressStepsForm();
        if ($this->request->is('post') && $this->request->data['step'] == CheckoutSteps::BILLING_ADDRESS) {
            $data = $this->request->data;
            $data['customer_id'] = $order->customer_id;
            $result = $order_form->execute($data);
            if (!empty($result)) {
                $next_step = CheckoutSteps::SHIPPING_METHOD;
                if (empty($order->local_status) || $order->local_status < $next_step) {
                    $result['local_status'] = $next_step;
                }
                $order = $this->Orders->patchEntity($order, $result);
                $order->set('payment_id', null);
                if ($this->Orders->save($order)) {
                    return $order;
                }
            }
        }
        $this->loadAddressList($order->customer_id, CheckoutSteps::BILLING_ADDRESS);
        if (isset($this->request->data['address_id'])) {
            $this->set('address_selected', $this->request->data['address_id']);
        }
        $this->set(compact('order_form'));
        return null;
    }

    private function stepThree($order)
    {
        $items_table = TableRegistry::get('OrderItemAssociations');
        $result = $items_table->find()->where(['order_id' => $order->id])->all();
        $items = array();
        foreach ($result as $item) {
            $items[$item->item_id] = $item->quantity;
        }
        $weight = $this->Cart->calcWeight($items);
        $methods = $this->Cart->getShipping($order->shipping_address->country, $weight);
        $this->set('shipping_methods', $methods);

        if ($this->request->is('post') && $this->request->data['step'] == CheckoutSteps::SHIPPING_METHOD) {
            if (empty($order->local_status) || $order->local_status < CheckoutSteps::PAYMENT_METHOD) {
                $order->set('local_status', CheckoutSteps::PAYMENT_METHOD);
            }
            $method = $this->request->data['method'];
            if ($methods[$method]) {
                $order->set('shipping_method_id', $methods[$method]['method_id']);
                $order->set('shipping_option_id', $methods[$method]['option_id']);
            } else {
                return null;
            }

            $order->set('payment_id', null);
            if ($this->Orders->save($order)) {
                return $order;
            }
        }

        return null;
    }

    /**
     * @param Entity|null $order
     * @return null
     */
    private function stepFour($order)
    {
        $this->set('user_id', $this->Auth->user('id'));
        if ($this->request->is('post') && $this->request->data['step'] == CheckoutSteps::PAYMENT_METHOD) {
            if (isset($this->request->data['payment_method'])
                && $this->request->data['payment_method'] <= PaymentMethods::REQUEST_INVOICE
            ) {
                if (empty($order->local_status) || $order->local_status < CheckoutSteps::OVERVIEW) {
                    $order->set('local_status', CheckoutSteps::OVERVIEW);
                }
                $order->set('payment_method', intval($this->request->data['payment_method']));
                $order->set('payment_id', null);
                if ($this->Orders->save($order)) {
                    return $order;
                }
            }
        }
        return null;
    }

    /**
     * @param Order $order
     * @return |null
     */
    private function stepFive($order)
    {
        if ($this->request->is('post') && $this->request->data['step'] == CheckoutSteps::OVERVIEW) {
            if (empty($order->local_status) || $order->local_status < CheckoutSteps::PAYMENT) {
                $order->set('local_status', CheckoutSteps::PAYMENT);
                if ($this->Orders->save($order)) {
                    return $order;
                }
            }
        }

        $asscoiations_table = TableRegistry::get('OrderItemAssociations');
        $associations = $asscoiations_table->find()->where(['order_id' => $order->id])->all();
        $associations = DataUtils::indexEntityMap($associations, 'item_id');
        $items = $this->Orders->Items->find()
            ->where(['Items.id IN' => array_keys($associations), 'stock >' => 0])
            ->matching('ItemPrices', function ($q) {
                return $q->where([
                    'ItemPrices.start_date <=' => new \DateTime(),
                    'OR' => [['ItemPrices.end_date >' => new \DateTime()], ['ItemPrices.end_date IS' => null]]
                ])->limit(1);
            })
            ->contain(['VatCodes' => function ($q) {
                return $q->select(['code_vat', 'percentage']);
            }])
            ->all();

        $in_eu = (strpos(CacheUtils::getPreference('eu_countries')->text_val, $order->shipping_address->country) !== false);
        $vat_prices = $this->calculateVat($order, $items, $associations);

        //TODO: wire transfer
        if($order->payment_method != PaymentMethods::BANK_TRANSFER_LOCAL && $order->payment_method != PaymentMethods::REQUEST_INVOICE) {
            if (empty($order->payment_id) || $this->Mollie->getPayment($order->payment_id, $order->payment_method) == null
                || $this->Mollie->getPayment($order->payment_id, $order->payment_method)->isExpired()
            ) {
                $payment_data = [
                    "amount" => $this->calculateTotal($order, $items, $vat_prices, $in_eu),
                    "description" => "Betaling voor ordernr. $order->id bij Evam Classic webshop.",
                    "redirectUrl" => Router::url(["controller" => "Orders", "action" => "success", 'order' => $order->id], true),
                    "webhookUrl" => Router::url(["controller" => "Orders", "action" => "webhook"], true),
                    "metadata" => array(
                        "order_id" => $order->id,
                    ),
                ];
                $payment_method = $this->Mollie->convertPaymentMethod($order->payment_method);
                if (!empty($payment_method)) {
                    $payment_data['method'] = $payment_method;
                }

                Log::debug(json_encode($payment_data), json_encode($vat_prices), json_encode($in_eu));
                $payment = $this->Mollie->createPayment($payment_data);
                $order->set('payment_id', $payment->id);
                $this->Orders->save($order);
            }
        }

        $this->set('items', $items);
       // Log::debug(' Pay ' . $vat_prices);
        $this->set('payment_method_sum', PaymentMethods::getSum($order->payment_method, $in_eu));
        $this->set('in_eu', $in_eu);
        $this->set('vat_prices', $vat_prices);
        return null;
    }

    private function stepSix($order)
    {
        if($order->payment_method == PaymentMethods::REQUEST_INVOICE) {
            $handler = new InvoiceRequest();
            $handler->handle($this, $order);
            return -1;
        }

        //TODO: wire transfer
        if($order->payment_method == PaymentMethods::BANK_TRANSFER_LOCAL) {
            $this->set('wire_beneficiary', CacheUtils::getPreference('beneficiary'));
            $this->set('wire_iban', CacheUtils::getPreference('iban'));
            $this->set('order_id', $order->id);
            return -1;
        }

        if (empty($order->payment_id) || $this->Mollie->getPayment($order->payment_id, $order->payment_method)->isExpired()) {
            return $this->stepFive($order);
        }
        $payment = $this->Mollie->getPayment($order->payment_id, $order->payment_method);
        if (empty($payment)) {
            return $this->stepFive($order);
        }
        $this->set('payment_url', $payment->getPaymentUrl());
        $this->redirect($payment->getPaymentUrl());

        return -1;
    }

    private function calculateVat($order, $items, $associations) {
        $vat_prices = array();
        $in_eu = (strpos(CacheUtils::getPreference('eu_countries')->text_val, $order->shipping_address->country) !== false);
        foreach ($items as $item) {
            $item->set('quantity', $associations[$item->id]->quantity);
            $vat_percentage = $in_eu ? strval($item->vat_code_model->percentage) : '0';
            if (!empty($vat_prices[$vat_percentage])) {
                $vat_prices[$vat_percentage] += $item->_matchingData['ItemPrices']['price'] * $item->quantity;
            } else {
                $vat_prices[$vat_percentage] = $item->_matchingData['ItemPrices']['price'] * $item->quantity;
            }
        }
        if (!empty($order->shipping_option->price)) {
            $vat_percentage = $in_eu ? '0.21' : '0.21';
            if (!empty($vat_prices[$vat_percentage])) {
                $vat_prices[$vat_percentage] += $order->shipping_option->price/1.21;
            } else {
                $vat_prices[$vat_percentage] = $order->shipping_option->price/1.21;
            }
        }

        return $vat_prices;
    }

    private function calculateTotal($order, $items, $vat_prices, $in_eu)
    {
        //TODO done? non european counties = no vat
        $total_vat = 0;
        $total_price = 0;
        foreach ($vat_prices as $percentage => $vat_price) {
            $total_vat += $vat_price * $percentage;
            $total_price += $vat_price;
        }

       //Log::debug(\json_encode($vat_prices));

        $commission = DataUtils::round_up_no_format(
            PaymentMethods::calcCommission($total_price + $total_vat, $order->payment_method, $in_eu), 2);
        Log::debug('amount' . DataUtils::round_up_no_format($total_price + $total_vat + $commission, 2));
        return DataUtils::round_up_no_format($total_price + $total_vat + $commission, 2);
    }

    private function loadAddressList($customer_id, $step)
    {
        $addresses_table = TableRegistry::get('Addresses');
        $addresses_result = $addresses_table->find()->where(['customer_id' => $customer_id])->all();
        $addresses = array();
        $address_selected = null;
        foreach ($addresses_result as $address) {
            $addresses[$address->id] = $address->name . ', ' . $address->street . ', ' . $address->city
                . ', ' . $address->postcode . ', ' . Countries::$countries[$address->country];
            if ($step == CheckoutSteps::BILLING_ADDRESS && $address->type == 1) {
                $address_selected = $address->id;
            } else if ($step == CheckoutSteps::SHIPPING_ADDRESS && $address->type == 2) {
                $address_selected = $address->id;
            }
        }
        $addresses[-1] = 'New Address';
        if ($address_selected == null) {
            if (count($address_selected) > 1) {
                foreach ($addresses as $key => $address) {
                    if ($key != -1) {
                        $address_selected = $key;
                    }
                }
            } else {
                $address_selected = -1;
            }
        }
        $this->set(compact('addresses', 'address_selected'));
    }

    protected function loadCategories()
    {
    }

    /**
     * @param Entity $order
     * @param Entity $address
     * @return Entity
     */
    protected function saveCart($order, $address = null)
    {
        $session = $this->request->session();
        $cart = $session->read('Order.cart');
        $cart_items = $cart['CartItem'];
        $item_associations = array();
        $item_quantities = array();
        if (!empty($cart)) {
            foreach ($cart_items as $item) {
                array_push($item_associations, ['order_id' => $order->id, 'item_id' => $item['id'], 'quantity' => $item['quantity']]);
                $item_quantities[$item['id']] = $item['quantity'];
            }
        }
        if (count($item_associations) == 0) {
            return null;
        }
        $associations_table = TableRegistry::get('OrderItemAssociations');
        if (DataUtils::InsertMultipleInto($associations_table, ['order_id', 'item_id', 'quantity'], $item_associations)) {
            return $this->patchShipping($order, $address, $item_quantities);
        }
        return null;
    }

    protected function patchShipping($order, $address = null, $items = null)
    {
        if ($items == null) {
            $items_table = TableRegistry::get('OrderItemAssociations');
            $result = $items_table->find()->where(['order_id' == $order->id])->all();
            $items = array();
            foreach ($result as $item) {
                $items[$item->item_id] = $item->quantity;
            }
        }
        if ($address == null) {
            $addresses_table = TableRegistry::get('Addresses');
            $address = $addresses_table->get($order->shipping_id);
        }
        $weight = $this->Cart->calcWeight($items);
        $shipping_method = $this->Cart->getShipping($address->country, $weight);
        $method = $shipping_method['selected'];
        if ($method && isset($shipping_method[$method])) {
            $order->set('shipping_method_id', $shipping_method[$method]['method_id']);
            $order->set('shipping_option_id', $shipping_method[$method]['option_id']);
            return $order;
        } else {
            foreach ($shipping_method as $key => $method) {
                if ($key == 'selected') continue;

                $order->set('shipping_method_id', $method['method_id']);
                $order->set('shipping_option_id', $method['option_id']);
                return $order;
            }
        }
        return null;
    }

    protected function allowedPages()
    {
        return ['newCheckout', 'checkout', 'index', 'webhook', 'view', 'test', 'tezt'];
    }
}
