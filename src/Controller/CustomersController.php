<?php

namespace App\Controller;

use App\Form\AccountEditForm;
use App\Form\CustomerRegisterForm;
use App\Utils\SystemUtils;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Log\Log;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;

/**
 * Customers Controller
 * @property ExactSyncComponent ExactSync
 * @property \App\Model\Table\CustomersTable $Customers
 */
class CustomersController extends AppController
{
    use MailerAwareTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('ExactSync');
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,
            'sitekey' => '6Ldt97kUAAAAAAOV0hhWTQs7_POXedGi2CLn5qyj',
            'secret' => '6Ldt97kUAAAAAMR1EusVNPr6xitmNWZyDNh5u5qs',
            'type' => 'image',
            'theme' => 'dark',
            'lang' => 'en',
            'size' => 'normal'
        ]);
        $this->set('wide', true);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Username or password is incorrect'), [
                    'key' => 'auth'
                ]);
            }
        }
    }

    public function register()
    {
        $customer = new CustomerRegisterForm();
        if ($this->request->is('post')) {
            if ($this->Recaptcha->verify()) {
                $result = $customer->execute($this->request->data);
                if ($result) {
                    $customerz = $this->Customers->get($result['id']);
                    $guid = $this->ExactSync->pushAccounts($customerz);

                    $this->Flash->success('You are succesfully registered. A confirmation has been sent to your email.');
                    $this->Auth->setUser($result);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error('There was a problem submitting your form.');
                }
            } else {
                $this->Flash->error('Please pass Google Recaptcha first');
            }
        }
        $this->set('customer', $customer);
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function account()
    {
        $user_id = $this->Auth->user('id');
        $customer_email = $this->Auth->user('email');
        $company = $this->Auth->user('company');
        $vat_code = $this->Auth->user('vat_code');
        $address_table = TableRegistry::get('Addresses');
        $addresses = $address_table->find()->where(['customer_id' => $user_id])->all();
        $orders_table = TableRegistry::get('Orders');
        $orders = $orders_table->find()->where(['customer_id' => $user_id])
            ->select(['id', 'created', 'local_status', 'customer_id'])->order(['id' => 'DESC'])->limit(5)->all();
        $this->set(compact('customer_email', 'addresses', 'orders', 'company','vat_code'));
    }

    public function edit()
    {
        $user_id = $this->Auth->user('id');
        $form = new AccountEditForm();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $result = $form->execute($this->request->data);
            if (!empty($result)) {
                $customer = $this->Customers->get($user_id);
                $continue = true;
                if (!empty($result['change-password'])) {
                    if ((new DefaultPasswordHasher)->check($result['current-password'], $customer->password)) {
                        $result['password'] = $result['new-password'];
                    } else {
                        $continue = false;
                    }
                }
                if ($continue) {
                    $customer = $this->Customers->patchEntity($customer, $result);
                    if ($this->Customers->save($customer)) {
                        if (!empty($result['change-password'])) {
                            $this->getMailer('CustomerMail')->send('passwordChanged', [$customer]);
                        }
                        $data = $customer->toArray();
                        unset($data['password']);
                        $this->Auth->setUser($data);
                        $this->Flash->success('Succesfully saved new info.');
                        return $this->redirect(['action' => 'account', 'controller' => 'Customers']);
                    } else {
                        $this->Flash->error('Please check all the fields and try again.');
                    }
                } else {
                    $this->Flash->error('Given password does not match your current password.');
                }
            } else {
                $this->Flash->error('Please check all the fields and try again.');
            }
            $this->request->data['current-password'] = null;
            $this->request->data['new-password'] = null;
            $this->request->data['confirm-password'] = null;
        }
        if ($this->request->is('get')) {
            if ($this->request->query('change_pw') == true) {
                $this->request->data['change-password'] = true;
            }
            $this->request->data['name'] = $this->Auth->user('name');
            if(!empty($this->Auth->user('company'))) {
                $this->request->data['is-company'] = true;
                $this->request->data['company'] = $this->Auth->user('company');
                $this->request->data['vat_code'] = $this->Auth->user('vat_code');
            }
        }
        $this->set(compact('form'));
    }

    public function editAddress($id = null)
    {
        $address_table = TableRegistry::get('Addresses');
        if (!empty($id)) {
            $address = $address_table->get($id);
            if (empty($address) || $address->customer_id != $this->Auth->user('id')) {
                $this->Flash->error(__('Something went wrong, please try again.'));
                return $this->redirect(['action' => 'account']);
            }
        } else {
            $address = $address_table->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!empty($this->request->data['use-shipping']) && empty($this->request->data['use-billing'])) {
                $this->request->data['type'] = 2;
            } else if (!empty($this->request->data['use-billing']) && empty($this->request->data['use-shipping'])) {
                $this->request->data['type'] = 1;
            } else {
                $this->request->data['type'] = 3;
            }
            $this->request->data['customer_id'] = $this->Auth->user('id');
            $address = $address_table->patchEntity($address, $this->request->data);
            if ($address_table->save($address)) {
                $this->Flash->success(__('Your address has been succesfully saved.'));
                $cust = $this->Customers->get($this->Auth->user()->id);
                if(!empty($cust->exact_guid)) {
                    $this->ExactSync->pushAddress($cust->exact_guid, $address);
                }

                $this->Flash->error(__('Your address could not be saved.'));
            }
        }

        $this->set(compact('address'));
    }

    public function confirm()
    {
        if (empty($this->request->query['token'])) {
            $this->Flash->error(__('Invalid confirmation token.'));
        } else {
            $customer = $this->Customers->find()->where(['active' => $this->request->query['token']])->first();
            if (empty($customer)) {
                $this->Flash->error(__('Invalid confirmation token.'));
            } else {
                $customer->set('active', null);
                if ($this->Customers->save($customer)) {
                    $this->getMailer('CustomerMail')->send('confirmed', [$customer]);
                    $this->Flash->success(__('Your email has been succesfully confirmed.'));
                } else {
                    $this->Flash->error(__('Invalid confirmation token.'));
                }
            }
        }
        $session = $this->request->session();
        if (!$session->read('Auth.Customer.id')) {
            return $this->redirect(['action' => 'login']);
        } else {
            return $this->redirect(['action' => 'account']);
        }
    }

    public function requestPassword()
    {
        if (!empty($this->request->query['email'])) {
            $customer = $this->Customers->find()->where(['email' => $this->request->query['email']])->first();
            if (empty($customer)) {
                $this->Flash->error(__('There is no account with this email registered.'));
            } else {
                $customer->set('pass_reset', SystemUtils::generateRandomString(16));
                if ($this->Customers->save($customer)) {
                    $this->getMailer('CustomerMail')->send('passwordReset', [$customer]);
                    $this->Flash->success(__('Please check your email for further instructions.'));
                    return $this->redirect(['controller' => 'Products', 'action' => 'index']);
                } else {
                    $this->Flash->error(__('Something went wrong. Please try again later.'));
                }
            }
        }
    }

    public function resetPassword()
    {
        $session = $this->request->session();
        if ($session->read('Auth.Customer.id')) {
            return $this->redirect(['action' => 'login']);
        }
        if (empty($this->request->query['token'])) {
            $this->Flash->error(__('Invalid or Expired password reset token.'));
            return $this->redirect(['action' => 'login']);
        } else {
            if ($this->request->is(['post'])) {
                if ($this->request->data['password'] === $this->request->data['password-confirm']) {
                    $customer = $this->Customers->find()->where(['pass_reset' => $this->request->query['token']])->first();
                    if (empty($customer)) {
                        $this->Flash->error(__('Invalid or Expired password reset token.'));
                        return $this->redirect(['action' => 'login']);
                    }
                    $customer->set('pass_reset', null);
                    $customer = $this->Customers->patchEntity($customer,
                        ['password' => $this->request->data['password']],
                        [
                            'validate' => 'updatePassword'
                        ]);
                    if ($customer->errors('password')) {
                        $this->Flash->error(__('The password is invalid please try another one.'));
                    } else {
                        if ($this->Customers->save($customer)) {
                            $this->getMailer('CustomerMail')->send('passwordChanged', [$customer]);
                            $this->Flash->success(__('Your password has been changed. You cna login now with your new password.'));
                            return $this->redirect(['action' => 'login']);
                        } else {
                            $this->Flash->error(__('Something went wrong. Please try again later.'));
                        }
                    }
                } else {
                    $this->Flash->error(__('Passwords are not equal.'));
                }
            }
        }
    }

    public function orders()
    {
        $user_id = $this->Auth->user('id');
        $orders_table = TableRegistry::get('Orders');
        $orders = $orders_table->find()->where(['customer_id' => $user_id])
            ->select(['id', 'created', 'local_status', 'customer_id'])->limit(5)->all();

        $this->set(compact('orders'));
    }

    protected function loadCategories()
    {
    }


    protected function allowedPages()
    {
        return ['login', 'register', 'logout', 'confirm', 'requestPassword', 'resetPassword'];
    }

}
