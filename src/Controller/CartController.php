<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 6/4/2016
 * Time: 10:05 PM
 */

namespace App\Controller;


use Cake\Event\Event;
use Cake\Routing\Router;

class CartController extends AppController
{

    public function beforeFilter(Event $event)
    {
        if (in_array($this->request->params['action'], ['index'])) {
            $this->request->session()->write('content_referrer', Router::reverse(Router::getRequest(), true));
        }
        return parent::beforeFilter($event);
    }

    public function index()
    {
        $cart_items = $this->Cart->getCart();
        $this->set(compact('cart_items'));
    }

    public function add($id)
    {
        if ($this->request->is('post')) {
            $this->request->allowMethod(['post']);
            $quantity = 1;
            if (isset($this->request->data['quantity'])) {
                $quantity = $this->request->data['quantity'];
            }
            $this->Cart->add($id, $quantity);
        }
        return $this->redirect(['controller' => 'Cart', 'action' => 'index']);
    }

    public function setCount()
    {
        if ($this->request->is('post')) {
            $this->request->allowMethod(['post']);
            $id = $this->request->data['id'];
            $quantity = $this->request->data['quantity'];
            $this->Cart->setCount($id, $quantity);
        }
        return $this->redirect(['controller' => 'Cart', 'action' => 'index']);
    }

    public function remove($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->Cart->remove($id);
        return $this->redirectBack(['controller' => 'Main', 'action' => 'index']);
    }

    public function editShipping() {
        $this->request->allowMethod(['post']);
        $method = $this->request->data['method'];
        if($this->Cart->setShipping($method)) {
            $this->Flash->success('Uw verzend optie is gewijzigd.');
        } else {
            $this->Flash->error('Er is iets fout gegaan.');
        }
        return $this->redirectBack(['action' => 'index']);
    }

    protected function allowedPages()
    {
        return ['cart', 'add', 'index', 'setCount', 'remove', 'editShipping'];
    }

}