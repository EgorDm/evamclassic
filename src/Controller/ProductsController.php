<?php
namespace App\Controller;

use App\Utils\CacheUtils;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Core\Helper\Website;

/**
 * Products Controller
 *
 * @property Table Items
 */
class ProductsController extends AppController
{
    public $paginate = [
        'limit' => 16,
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');

        $builder = $this->viewBuilder();
        $builder->layout('web_main');

        $this->loadModel('Items');

    }

    public function beforeFilter(Event $event)
    {
        $this->request->session()->write('content_referrer', Router::reverse(Router::getRequest(), true));
        $this->set('menu_active', 2);
        return parent::beforeFilter($event);
    }

    public function index()
    {
        $builder = $this->viewBuilder();
        $builder->template('category');
        $this->set('title', 'Home page');
        $this->set('title_category', 'Product');
        $this->loadPage('index');

        $items_list = $this->paginate(
            $this->Items->find()
                ->matching('ItemPrices', function ($q) {
                    return $q->where([
                        'ItemPrices.price >=' => '0',
                        'ItemPrices.start_date <=' => new \DateTime(),
                        'OR' => [['ItemPrices.end_date >' => new \DateTime()], ['ItemPrices.end_date IS' => null]]
                    ])->limit(1);
                })
                ->contain([
                    'ImageGroups' => function ($q) {
                        return $q->contain(['Images']);
                    },
                    'VatCodes'])
                ->order(['Items.priority' => 'DESC','code' => 'ASC'])
                ->distinct(['Items.id'])
                ->where(['stock >' => 0, 'sales' => 1, 'status >=' => env('STATUS_PRIO', 1)])->limit(16));
        $items = array();
        $session = $this->request->session();
        foreach ($items_list as $item) {
            if ($item->get('image_group')['images'] != null) {
                foreach ($item->get('image_group')['images'] as $image) {
                    if ($image->id == $item->image_group->thumbnail) {
                        $item->set('thumbnail', [$image->get('path'), $image->get('name')]);
                        break;
                    }
                }
            } else {
                $item->set('thumbnail', [CacheUtils::getPreference('no_image')->text_val, 'No image found']);
            }
            $item->set('price', $item->get('_matchingData')['ItemPrices']['price']);
            $qty = $session->read("Cart.CartItem.$item->id.quantity");
            $item->set('stock', $item->get('stock') - $qty);
            $item->unsetProperty('image_group');
            $item->unsetProperty('_matchingData');
            array_push($items, $item);
        }
        $this->set('items', $items);
        $this->set('menu_active', 1);
    }

    public function view($alias)
    {
        $item = $this->Items->find()
            ->matching('ItemPrices', function ($q) {
                return $q->where([
                    'ItemPrices.price >=' => '0',
                    'ItemPrices.start_date <=' => new \DateTime(),
                    'OR' => [['ItemPrices.end_date >' => new \DateTime()], ['ItemPrices.end_date IS' => null]]
                ])->limit(1);
            })
            ->contain([
                'ImageGroups' => function ($q) {
                    return $q->contain(['Images']);
                },
                'VatCodes'])
            ->where(['alias' => $alias, 'status >=' => env('STATUS_PRIO', 1)])->first();
        if (empty($item)) {
            $this->Flash->error('Product not found.');
            throw new NotFoundException('Product not found.');
        }
        $item->set('price', $item->get('_matchingData')['ItemPrices']['price']);
        $qty = $this->request->session()->read("Cart.CartItem.$item->id.quantity");
        $item->set('stock', $item->get('stock') - $qty);
        $item->unsetProperty('_matchingData');

        $this->set('item', $item);
    }

    public function search()
    {
        if (empty($this->request->query['query'])) {
            $this->redirect(['action' => 'index']);
        }
        $query = $this->request->query['query'];

        $builder = $this->viewBuilder();
        $builder->template('category');
        $this->set('title', 'Home page');
        $this->set('title_category', 'Results for: ' . $query);
        $items_list = $this->paginate(
            $this->Items->find()
                ->matching('ItemPrices', function ($q) {
                    return $q->where([
                        'ItemPrices.price >' => '0',
                        'ItemPrices.start_date <=' => new \DateTime(),
                        'OR' => [['ItemPrices.end_date >' => new \DateTime()], ['ItemPrices.end_date IS' => null]],
                    ])->limit(1);
                })->contain(['ImageGroups' => function ($q) {
                    return $q->contain(['Images']);
                }, 'VatCodes'])
                ->where([
                        'OR' => [
                            ['code LIKE' => "%$query%"],
                            ['title LIKE' => "%$query%"],
                            ['Items.description LIKE' => "%$query%"],
                            ['Items.tags_english LIKE' => "%$query%"],
                            ['Items.tags_german LIKE' => "%$query%"],
                            ['Items.tags_french LIKE' => "%$query%"],
                        ],
                        'sales' => 1,
                    'status >=' => env('STATUS_PRIO', 1)
                ])
                ->order(['Items.priority' => 'DESC','code' => 'ASC'])
                ->limit(16));
        $items = array();
        $session = $this->request->session();
        foreach ($items_list as $item) {
            if ($item->get('image_group')['images'] != null) {
                foreach ($item->get('image_group')['images'] as $image) {
                    if ($image->id == $item->image_group->thumbnail) {
                        $item->set('thumbnail', [$image->get('path'), $image->get('name')]);
                        break;
                    }
                }
            } else {
                $item->set('thumbnail', [CacheUtils::getPreference('no_image')->text_val, 'No image found']);
            }
            $item->set('price', $item->get('_matchingData')['ItemPrices']['price']);
            $qty = $session->read("Cart.CartItem.$item->id.quantity");
            $item->set('stock', $item->get('stock') - $qty);
            $item->unsetProperty('image_group');
            $item->unsetProperty('_matchingData');
            array_push($items, $item);

        }
        $this->set('items', $items);
    }

    public function category($alias)
    {
        $categories = TableRegistry::get('Categories');
        $category = $categories->find()->where(['alias' => $alias])->order(['Categories.priority' => 'DESC'])->first();

        if (empty($category)) {
            return $this->redirect(['action' => 'index']);
        }
        $this->set('title', 'Parts - ' . $category->title);
        $items = $this->getCategoryItems($category);

        $this->set('items', $items);
        $this->set('category', $category);
        $this->set('title_category', $category->title);
    }

    public function toggletax() {
        if ($this->request->is('post')) {
            $this->request->session()->write('Tax', !$this->request->session()->read('Tax'));
        }
        return $this->redirect($this->referer());
    }

    protected function getCategoryItems($category)
    {
        $category_ids = [$category->id];
        $categories = TableRegistry::get('Categories');
        $cat_tree = $categories
            ->find('children', [
                'for' => $category->id, 'fields' => ['id', 'parent_id'],
                'conditions' => [
                    'site' => Website::identifier()
                ]
            ])
            ->find('threaded', [
                'conditions' => [
                    'site' => Website::identifier()
                ]
            ]);

        if (!empty($cat_tree)) {
            foreach ($cat_tree as $child) {
                $category_ids = $this->loopChildren($child, $category_ids);
            }
        }
        $items_list = $this->paginate(
            $this->Items->find()
                ->matching('ItemPrices', function ($q) {
                    return $q->where([
                        'ItemPrices.price >' => '0',
                        'ItemPrices.start_date <=' => new \DateTime(),
                        'OR' => [['ItemPrices.end_date >' => new \DateTime()], ['ItemPrices.end_date IS' => null]]
                    ])->limit(1);
                })->matching('Categories', function ($q) use ($category_ids) {
                    return $q->where([
                        'Categories.id IN' => $category_ids,
                    ])->limit(1);
                })
                ->contain([
                    'ImageGroups' => function ($q) {
                        return $q->contain(['Images']);
                    },
                    'VatCodes'])
                ->where(['status >=' => env('STATUS_PRIO', 1)])
                ->where([
                    //'stock >' => 0,
                    'sales' => 1,
                    'status >=' => env('STATUS_PRIO', 1)
                ])
                ->distinct(['Items.id'])
                ->order(['Items.priority' => 'DESC','code' => 'ASC'])
                ->limit(16));
        $items = array();
        $session = $this->request->session();
        foreach ($items_list as $item) {
            if ($item->get('image_group')['images'] != null) {
                foreach ($item->get('image_group')['images'] as $image) {
                    if ($image->id == $item->image_group->thumbnail) {
                        $item->set('thumbnail', [$image->get('path'), $image->get('name')]);
                        break;
                    }
                }
            } else {
                $item->set('thumbnail', [CacheUtils::getPreference('no_image')->text_val, 'No image found']);
            }
            $item->set('price', $item->get('_matchingData')['ItemPrices']['price']);
            $qty = $session->read("Cart.CartItem.$item->id.quantity");
            $item->set('stock', $item->get('stock') - $qty);
            $item->unsetProperty('image_group');
            $item->unsetProperty('_matchingData');
            array_push($items, $item);

        }
        //$this->loadCategories($category->id);
        return $items;
    }

    private function loopChildren($parent, $resp)
    {
        array_push($resp, $parent->id);
        if (!empty($parent->children)) {
            foreach ($parent->children as $child) {
                $resp = $this->loopChildren($child, $resp);
            }
        }
        return $resp;
    }

    protected function loadCategories($target = null)
    {
        if ($target == null &&
            ($this->request->params['action'] == 'splitBus' || $this->request->params['action'] == 'bayWindow')
        ) {
            return;
        }
        $category_table = TableRegistry::get('Categories');
        $categories = $category_table->find(
            'threaded',
            [
                'order' => ['Categories.priority' => 'DESC'],
                'conditions' => [
                    'site' => Website::identifier()
                ]
            ]);
        if ($target != null) {
            foreach ($categories as $category) {

                if ($category->id == $target) {
                    $temp = array();
                    foreach ($category->children as $child) {
                        $child->parent_id = null;
                        array_push($temp, $child);
                    }
                    $categories = $temp;
                    break;
                }
            }
        }


        $this->set(compact('categories', 'cart_items'));
    }

    protected function allowedPages()
    {
        return ['index', 'view', 'search', 'category', 'splitBus', 'bayWindow', 'toggletax'];
    }
}
