<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\View\Helper\NumberHelper;

/**
 * Cars Controller
 *
 * @property \App\Model\Table\CarsTable $Cars
 */
class CarsController extends AppController
{

    public $paginate = [
        'limit' => 12,
        'order' => [
            'Cars.priority' => 'DESC',
            'Cars.status' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();

        $this->set('wide', true);
        $this->set('hide', true);
    }

    public function beforeFilter(Event $event)
    {
        $this->request->session()->write('content_referrer', Router::reverse(Router::getRequest(), true));
        $this->set('menu_active', 3);
        return parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $builder = $this->viewBuilder();
        $builder->template('category');
        $this->set('title', 'Cars for Sale');
        $this->set('title_category', 'Cars for Sale');

        $cars_list = $this->paginate(
            $this->Cars->find()
                ->contain(['ImageGroups' => function ($q) {
                    return $q->contain(['Images']);
                }])->limit(9));


        $cars = array();
        foreach ($cars_list as $car) {
            if ($car->get('image_group')['images'] != null) {
                foreach ($car->get('image_group')['images'] as $image) {
                    if ($image->id == $car->image_group->thumbnail) {
                        $car->set('thumbnail', [$image->get('path'), $image->get('name')]);
                        break;
                    }
                }
                $car->unsetProperty('image_group');
                $car->unsetProperty('_matchingData');
                array_push($cars, $car);
            }
        }

        $this->set(compact('cars'));
    }

    /**
     * View method
     *
     * @param $alias
     * @return \Cake\Network\Response|null
     * @internal param null|string $id Car id.
     */
    public function view($alias)
    {
        $car = $this->Cars->find()
            ->contain([
                'ImageGroups' => function ($q) {
                    return $q->contain(['Images']);
                }])
            ->where(['alias' => $alias])->first();

        if (empty($car)) {
            //$this->Flash->error('Product not found.');
            return $this->redirectBack(['controller' => 'Cars', 'action' => 'index']);
        }
        $car->unsetProperty('_matchingData');

        $this->set('car', $car);
    }

    protected function allowedPages()
    {
        return ['index', 'view'];
    }
}
