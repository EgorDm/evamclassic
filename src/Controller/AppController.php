<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 5/28/2016
 * Time: 10:45 PM
 */

namespace App\Controller;


use App\Controller\Component\CartComponent;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Core\Helper\Website;
use IPTrace\Controller\Component\IPTraceComponent;

/**
 * @property IPTraceComponent IPTrace
 * @property CartComponent Cart
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('IPTrace.IPTrace');
        $this->loadComponent('Cart');
        $this->loadComponent('Auth', [
            'userModel' => 'Customers',
            'loginRedirect' => [
                'controller' => 'Products',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Customers',
                'action' => 'login'
            ],
            'loginAction' => [
                'controller' => 'Customers',
                'action' => 'login'
            ],
            'authenticate' => [
                'Basic' => [
                    'userModel' => 'Customers'
                ],
                'Form' => [
                    'fields' => ['username' => 'email'],
                    'userModel' => 'Customers'
                ]
            ],
            'storage' => [
                'className' => 'Session',
                'key' => 'Auth.Customer',
            ],
            'authError', "You are not allowed to visit this page."
        ]);
        $builder = $this->viewBuilder();
        $builder->helpers(['Form' => ['templates' => 'form_templates']]);
        $builder->layout('web_main');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        if($this->request->clientIp() != '::1') {
            //Log::debug(json_encode($this->IPTrace->get($this->request->clientIp())));
        }
        $cart_items = $this->Cart->getCart();
        $this->set('Auth', $this->Auth);
        $this->set(compact('cart_items'));
        $this->loadCategories();

        $texts_table = TableRegistry::get('Texts');
        $text = $texts_table->find()->where(['slug' => 'modal', 'site' => Website::identifier()])->first();
        $this->set($text->slug, $text);
    }

    protected function loadCategories() {
        $category_table = TableRegistry::get('Categories');
        $categories = $category_table->find('threaded', [
            'conditions' => [
                'site' => Website::identifier()
            ]
        ]);
        $this->set(compact('categories'));
    }

    public function redirectBack($default_url) {
        if($this->request->session()->check('content_referrer')) {
            $this->redirect($this->request->session()->read('content_referrer'));
            $this->request->session()->delete('content_referrer');
            return;
        }
        $this->redirect($default_url);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        if ($this->request->params['action'] != 'login') {
            $this->Auth->allow($this->allowedPages());
        }

        if (in_array($this->request->params['action'], ['view', 'index'])) {
            $this->request->session()->write('content_referrer', Router::reverse(Router::getRequest(), true));
        }
        $this->set('fullname', $this->Auth->user()['name']);
        $this->set('tax', $this->Cart->applyTax());
    }

    /**
     * Getting array of pages which are free to visit for everyone.
     * @return array
     */
    protected function allowedPages() {
        return [];
    }

    protected function loadPage($slug) {
        $pages_table = TableRegistry::get('Pages');
        $page = $pages_table->find()
            ->where(['slug' => $slug])
            ->select(['id', 'title', 'slug', 'content'])
            /*->contain(['Texts' => function($q) {
            return $q->contain(['ImageGroups' => function ($q) {
                return $q
                    ->autoFields(true)
                    ->contain(['Images']);
            }]);
        }])*/->first();
        if(!empty($page)) {
            $texts_table = TableRegistry::get('Texts');
            $texts = $texts_table->find()
                /*->contain(['ImageGroups' => function ($q) {
                    return $q->contain(['Images']);
                }])*/->where(['page_id =' => $page->id, 'site' => Website::identifier()])->all();
            foreach ($texts as $text) {
                $this->set($text->slug, $text);
            }
            //$page->unsetProperty('texts');
            $this->set('page', $page);
        }
    }
}
