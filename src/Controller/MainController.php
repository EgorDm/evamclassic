<?php
namespace App\Controller;

use App\Form\ContactForm;
use Cake\Event\Event;
use Cake\Routing\Router;
use Recaptcha\Controller\Component\RecaptchaComponent;

/**
 * Main Controller
 */
class MainController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('Paginator');
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,
            'sitekey' => '6Ldt97kUAAAAAAOV0hhWTQs7_POXedGi2CLn5qyj',
            'secret' => '6Ldt97kUAAAAAMR1EusVNPr6xitmNWZyDNh5u5qs',
            'type' => 'image',
            'theme' => 'dark',
            'lang' => 'en',
            'size' => 'normal'
        ]);

        $builder = $this->viewBuilder();
        $builder->layout('web_main');

    }

    public function beforeFilter(Event $event)
    {
        $this->request->session()->write('content_referrer', Router::reverse(Router::getRequest(), true));
        return parent::beforeFilter($event);
    }

    public function index()
    {

    }

    public function company() {
        $this->loadPage('company');
    }

    public function privacy() {
        $this->loadPage('privacy');
    }

    public function terms() {
        $this->loadPage('terms');
    }

    public function page($alias) {
        if($alias == 'contact') {
            $this->viewBuilder()->template('contact');
            return $this->contact();
        }

        $alias = preg_replace("/[^A-Za-z0-9 ]/", '', $alias);
        $template = APP.join(DS, ['Template', 'Main', $alias]).'.ctp';
        if(file_exists($template)) {
           $this->viewBuilder()->template($alias);
        }
        $this->loadPage($alias);
    }

    public function contact() {
        $contact = new ContactForm();
        if ($this->request->is('post')) {
	        if ($this->Recaptcha->verify()) {
		        $result = $contact->execute($this->request->data);
		        if ($result) {
			        $this->redirect(['action' => 'contactSuccess']);
		        }
	        } else {
		        $this->Flash->error('Please pass Google Recaptcha first');
	        }
        }
        $this->set('contact', $contact);
    }

    public function contactSuccess() {

    }

    protected function allowedPages()
    {
        return ['contactSuccess','contact', 'privacy', 'terms', 'company', 'index', 'page'];
    }
}
