<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 6/4/2016
 * Time: 10:49 PM
 */

namespace App\Controller\Component;

use App\Utils\CacheUtils;
use Cake\Controller\Component;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use IPTrace\Controller\Component\IPTraceComponent;

/**
 * @property IPTraceComponent IPTrace
 * @property Component\FlashComponent Flash
 */
class CartComponent extends Component
{
    public $maxQuantity = 99;

    public $controller = null;
    /**
     * Session
     * @var Session
     */
    public $session = null;

    public $components = ['IPTrace.IPTrace', 'Flash'];

    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
        $this->session = $this->controller->request->session();
    }

    public function add($id, $quantity)
    {
        if ($this->session->check("Cart.CartItem.$id.quantity")) {
            $quantity += $this->session->read("Cart.CartItem.$id.quantity");
        }
        $this->setCount($id, $quantity);
    }

    /**
     * @param $id
     * @param int $quantity
     * @return bool
     */
    public function setCount($id, $quantity = 1)
    {
        if (!is_numeric($quantity)) {
            $quantity = 1;
        }
        $quantity = abs($quantity);

        if ($quantity > $this->maxQuantity) {
            $quantity = $this->maxQuantity;
        }

        $ProductTable = TableRegistry::get('Items');
        $product = $ProductTable->find()->where(['Items.id' => $id])->contain([
            'ItemPrices',
            'ImageGroups' => function ($q) {
                return $q->contain(['Images']);
            }
        ])->first();
        if (empty($product)) {
            return false;
        }
        if ($product->stock < $quantity) {
            $this->Flash->error(__('Could not add the item to your cart because there is not enough in stock.'));
            return false;
        }

        $in_eu = true;
        if ($this->request->clientIp() != '::1') {
            $country = $this->IPTrace->get($this->request->clientIp())['countryCode'];
            $in_eu = (strpos(CacheUtils::getPreference('eu_countries')->text_val, $country) !== false);
        }

        $vat_code = 1;
        if ($in_eu) {
            if(!empty($product->vat_code)) {
                $VatCodesTable = TableRegistry::get('VatCodes');
                $result = $VatCodesTable->find()->where(['code_vat' => $product->vat_code])->first();
                if (!empty($result)) {
                    $vat_code = 1 + $result->percentage;
                } else if ($in_eu) {
                    $vat_code = 1.21;
                }
            } else {
                $vat_code = 1.21;
            }
        }
        $time_now = Time::now();
        foreach ($product->item_prices as $item_price) {
            if ($time_now->lt($item_price->start_date)) {
                continue;
            }
            if ($item_price->end_date != null && $time_now->gt($item_price->end_date)) {
                continue;
            }
            if ($quantity < $item_price->quantity) {
                continue;
            }
            $price = $item_price;

        }
        if (!isset($price)) {
            return false;
        }

        $thumbnail = null;
        foreach ($product->get('image_group')['images'] ?? [] as $image) {
            if ($image->id == $product->image_group->thumbnail) {
                $thumbnail = [$image->path, $image->name];
            }
        }

        $data['id'] = $product->id;
        $data['title'] = $product->title;
        $data['alias'] = $product->alias;
        $data['code'] = $product->code;
        $data['weight'] = sprintf('%01.2f', $product->weight);
        $data['quantity'] = $quantity;
        $data['price'] = $price->price;
        $data['thumbnail'] = $thumbnail;
        $data['subtotal_excl'] = sprintf('%01.2f', $price->price * $quantity);
        $data['subtotal_inc'] = $this->applyTax() ? sprintf('%01.2f', $price->price * $quantity * $vat_code) : $data['subtotal_excl'];
        $this->session->write('Cart.CartItem.' . $id, $data);
        $this->session->write('Cart.Order.shop', 1);
        $this->cart();
        return true;
    }

    public function remove($id, $quantity = -1)
    {
        if ($this->session->check('Cart.CartItem.' . $id)) {
            $current_qty = $this->session->read("Cart.CartItem.$id.quantity");
            if ($quantity == -1 || $current_qty <= $quantity) {
                $this->session->delete('Cart.CartItem.' . $id);
                $this->cart();
                return true;
            } else {
                $this->session->write("Cart.CartItem.$id.quantity", $current_qty - $quantity);
                $this->add($id, 0);
                return true;
            }
        }
        return false;
    }

    public function cart()
    {
        //Log::debug('Test!');
        $shop = $this->session->read('Cart');
        $quantity = 0;
        $totalweight = 0;
        $subtotal_inc = 0;
        $subtotal_excl = 0;
        $order_item_count = 0;
        if (count($shop['CartItem']) > 0) {
            foreach ($shop['CartItem'] as $item) {
                $quantity += $item['quantity'];
                $subtotal_inc += $item['subtotal_inc'];
                $subtotal_excl += $item['subtotal_excl'];
                $totalweight += $item['quantity'] * $item['weight'];
                $order_item_count++;
            }
            $d['order_item_count'] = $order_item_count;
            $d['quantity'] = $quantity;
            $d['subtotal_inc'] = sprintf('%01.2f', $subtotal_inc);
            $d['subtotal_excl'] = sprintf('%01.2f', $subtotal_excl);
            $d['totalweight'] = sprintf('%01.2f', $totalweight);
            $country = null;
            if ($this->request->clientIp() != '::1') {
                $country = $this->IPTrace->get($this->request->clientIp())['countryCode'];
            }
            $d['shipping'] = $this->getShipping($country, $totalweight);

            if (isset($shop['Order'])) $d = $d + $shop['Order'];
            $this->session->write('Cart.Order', $d);
            return true;
        } else {
            $this->session->delete('Cart');
            return false;
        }
    }

    public function setShipping($shipping) {
        if($this->session->check('Cart.Order.shipping.'.$shipping)) {
            $this->session->write('Cart.Order.shipping.selected', $shipping);
            return true;
        }
        return false;
    }

    public function clear()
    {
        $this->session->delete('Cart');
    }

    public function getCart()
    {
        return $this->session->read('Cart');
    }

    /**
     * Calculates which shipping method will be used.
     * @param $country
     * @param null $weight
     * @return array|null
     */
    public function getShipping($country, $weight = null)
    {
        if ($weight == null) return null;
        $ShippingMethods = TableRegistry::get('ShippingMethods');
        $methods = $ShippingMethods->find()->where([
            'OR' => [['FIND_IN_SET(:country,ShippingMethods.countries)'], 'ShippingMethods.countries IS' => null]
        ])->bind(':country', $country, 'string')->matching('ShippingOptions', function ($q) use ($weight) {
            return $q->where([
                'ShippingOptions.min_weight <=' => $weight,
                'OR' => [['ShippingOptions.max_weight >' => $weight], ['ShippingOptions.max_weight IS' => null]]
            ])->limit(1);
        })->order(['countries' => 'DESC']);
        if (empty($methods)) return null;

        $ret_arr = array();
        if($this->session->check('Cart.Order.shipping.selected')) {
            $ret_arr['selected'] = $this->session->read('Cart.Order.shipping.selected');
        }
        $shipping_groups = array();
        foreach ($methods as $method) {
            if(empty($ret_arr['selected'])) {
                $ret_arr['selected'] = $method->id;
            }
            if(in_array($method->group, $shipping_groups)) {
                continue;
            }
            array_push($shipping_groups, $method->group);
            $ret = ['weight' => $weight];
            if ($method->_matchingData['ShippingOptions']['max_weight'] == null) {
                $category = '>' . $method->_matchingData['ShippingOptions']['min_weight'] . ' kg';
            } else {
                $category = $method->_matchingData['ShippingOptions']['min_weight']
                    . '-' . $method->_matchingData['ShippingOptions']['max_weight'] . ' kg';
            }

            $ret['method_id'] = $method->id;
            $ret['option_id'] = $method->_matchingData['ShippingOptions']['id'];
            $ret['title'] = $method->title;
            $ret['description'] = $method->description;
            $ret['category'] = $category;

            $ret['price'] = $method->_matchingData['ShippingOptions']['price'];
            $ret_arr[$method->id] = $ret;
        }


        return $ret_arr;
    }

    /**
     * @param array $item_quantities an array with as Key the item_id and as value the quantity
     * @return int total weight in kg
     */
    public function calcWeight($item_quantities)
    {
        $items_table = TableRegistry::get('Items');
        $items = $items_table->find()->where(['id IN' => array_keys($item_quantities)])->select(['id', 'weight'])->all();
        $weight = 0;
        foreach ($items as $item) {
            $weight += $item_quantities[$item->id] * $item->weight;
        }
        return $weight;
    }

    public function applyTax()
    {
        if ($this->session->check('Tax')) {
            return $this->session->read('Tax');
        }
        /*if ($this->request->clientIp() != '::1') {
            $country = $this->IPTrace->get($this->request->clientIp())['countryCode'];
            $in_eu = (strpos(CacheUtils::getPreference('eu_countries')->text_val, $country) !== false);
            $this->session->write('Tax', $in_eu);
            return $in_eu;
        }*/
        return true;
    }


    //https://github.com/andraskende/cakephp-shopping-cart/blob/master/app/Controller/Component/CartComponent.php
    //http://www.shoppingcartcore.com/shop/cart
}
