<?php
namespace App\Controller\Component;

use App\Utils\CheckoutSteps;
use App\Utils\PaymentMethods;
use Cake\Controller\Component;
use Cake\Log\Log;
use Mollie_API_Client;
use Mollie_API_Exception;
use Mollie_API_Object_Payment;

/**
 * Mollie component
 */
class MollieComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * @var Mollie_API_Client
     */
    protected $mollie = null;

    public static $API_CC = 'live_76ckvj5t4EkWsdWqqR3x8pky9Np9N9';
    public static $API_REST_CC = 'live_mpaSq3RjcCVqURBjqUDKr4QRk2UVNf';

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function tryConnect($cc = false) {
        if(empty($this->mollie)) {
            $this->mollie = new Mollie_API_Client;
            $this->mollie->setApiKey(($cc == true) ? MollieComponent::$API_CC : MollieComponent::$API_REST_CC);
        }
    }

    public function createPayment(array $options)
    {
        $cc = ($options['method'] == \Mollie_API_Object_Method::CREDITCARD);
        Log::debug("Creating Payment; Is cc?: " . $cc);
        $this->tryConnect($cc);
        return $this->mollie->payments->create($options);
    }

    public function getPayment($payment_id, $method = null)
    {
        $cc = ($this->convertPaymentMethod($method) == \Mollie_API_Object_Method::CREDITCARD);
        Log::debug("Getting Payment; Is cc?: " . $cc);
        $this->tryConnect($cc);
        try {
            return $this->mollie->payments->get($payment_id);
        } catch (Mollie_API_Exception $e) {
            if(!$cc) {
                return $this->getPayment($payment_id, PaymentMethods::CREDIT_CARD);
            }
            return null;
        }
    }

    public function convertPaymentMethod($payment_method)
    {
        switch ($payment_method) {
            case PaymentMethods::PAYPAL:
                return \Mollie_API_Object_Method::PAYPAL;
            case PaymentMethods::CREDIT_CARD:
                return \Mollie_API_Object_Method::CREDITCARD;
            case PaymentMethods::BANK_TRANSFER:
                return \Mollie_API_Object_Method::BANKTRANSFER;
            case PaymentMethods::IDEAL:
                return \Mollie_API_Object_Method::IDEAL;
        }
        return null;
    }

    public function mollieStatusConvert($payment_status)
    {
        switch ($payment_status) {
            case Mollie_API_Object_Payment::STATUS_OPEN:
                return CheckoutSteps::PMNT_OPEN;
            case Mollie_API_Object_Payment::STATUS_PENDING:
                return CheckoutSteps::PMNT_PENDING;
            case Mollie_API_Object_Payment::STATUS_CANCELLED:
                return CheckoutSteps::PMNT_CANCELLED;
            case Mollie_API_Object_Payment::STATUS_EXPIRED:
                return CheckoutSteps::PMNT_EXPIRED;
            case Mollie_API_Object_Payment::STATUS_PAID:
                return CheckoutSteps::PMNT_PAID;
            case Mollie_API_Object_Payment::STATUS_PAIDOUT:
                return CheckoutSteps::PMNT_PAIDOUT;
            case Mollie_API_Object_Payment::STATUS_REFUNDED:
                return CheckoutSteps::PMNT_REFUNDED;
            case Mollie_API_Object_Payment::STATUS_CHARGED_BACK:
                return CheckoutSteps::PMNT_CHARGED_BACK;
            case Mollie_API_Object_Payment::STATUS_FAILED:
                return CheckoutSteps::PMNT_FAILED;
        }
        return CheckoutSteps::PMNT_FAILED;
    }

    public function convertStatusToMollie($payment_status)
    {
        switch ($payment_status) {
            case CheckoutSteps::PMNT_OPEN:
                return Mollie_API_Object_Payment::STATUS_OPEN;
            case CheckoutSteps::PMNT_PENDING:
                return Mollie_API_Object_Payment::STATUS_PENDING;
            case CheckoutSteps::PMNT_CANCELLED:
                return Mollie_API_Object_Payment::STATUS_CANCELLED;
            case CheckoutSteps::PMNT_EXPIRED:
                return Mollie_API_Object_Payment::STATUS_EXPIRED;
            case CheckoutSteps::PMNT_PAID:
                return Mollie_API_Object_Payment::STATUS_PAID;
            case CheckoutSteps::PMNT_PAIDOUT:
                return Mollie_API_Object_Payment::STATUS_PAIDOUT;
            case CheckoutSteps::PMNT_REFUNDED:
                return Mollie_API_Object_Payment::STATUS_REFUNDED;
            case CheckoutSteps::PMNT_CHARGED_BACK:
                return Mollie_API_Object_Payment::STATUS_CHARGED_BACK;
            case CheckoutSteps::PMNT_FAILED:
                return Mollie_API_Object_Payment::STATUS_FAILED;
        }
        return Mollie_API_Object_Payment::STATUS_PENDING;
    }


}
