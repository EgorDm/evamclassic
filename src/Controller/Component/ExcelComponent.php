<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 6/12/2016
 * Time: 7:36 PM
 */

namespace App\Controller\Component;


use App\Model\Table\CarsTable;
use App\Model\Table\CategoriesTable;
use App\Model\Table\ItemsTable;
use App\Utils\CacheUtils;
use App\Utils\DataUtils;
use Cake\Controller\Component;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use PHPExcel;
use PHPExcel_IOFactory;

class ExcelComponent extends Component
{

    public function readDocument($file, $columns)
    {
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $columnNames = $sheet->rangeToArray('A1:' . $highestColumn . '1', NULL, TRUE, FALSE);
        $select_cols = array();
        for ($i = 0; $i < count($columnNames[0]); $i++) {
            if (in_array($columnNames[0][$i], $columns)) {
                $select_cols[$i] = $columnNames[0][$i];
            }
        }
        $ret = array();
        for ($row = 2; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $data = array();
            foreach ($select_cols as $key_index => $key) {
                $data[$key] = $rowData[0][$key_index];
            }
            array_push($ret, $data);
        }
        return $ret;
    }

    /**
     * @param array $columns
     * @param array $data
     * @return \PHPExcel_Writer_IWriter
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function writeDocument($columns, $data)
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Evam Classic");
        $objPHPExcel->getProperties()->setLastModifiedBy("Evam Classic");
        $objPHPExcel->getProperties()->setTitle("Evam Classic - Webshop Items");

        $objPHPExcel->setActiveSheetIndex(0);
        $row = 1;

        $columns_my = array_keys($data[0]);
        $columns_do = array();
        foreach ($columns_my as $column) {
            array_push($columns_do, strval($columns[$column]));
        }
        $objPHPExcel->getActiveSheet()->fromArray($columns_do);
        $row++;
        foreach ($data as $item) {
            $rowvals = array();
            foreach (array_values($item) as $arrval) {
                array_push($rowvals, empty($arrval) ? null : strval($arrval));
            }
            $objPHPExcel->getActiveSheet()->fromArray($rowvals, null, 'A' . $row);
            $row++;
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        return $objWriter;
    }

    /**
     * @param ItemsTable $table
     * @param $file
     */
    public function syncItems($table, $file)
    {
        set_time_limit(0);
        $sync_counts = (CacheUtils::getPreference('excel_for_stock')->int_val == 1);

        if ($sync_counts) {
            $excel_data = $this->readDocument($file, ['ID', 'Status', 'Partnummer', 'Gewicht', 'Omschrijving', 'Aantal', 'Priority',
                'Image1', 'Image2', 'Categories', 'TagsEnglish', 'TagsGerman', 'TagsFrench', 'Urls']);
        } else {
            $excel_data = $this->readDocument($file, ['ID', 'Status', 'Partnummer', 'Gewicht', 'Omschrijving', 'Priority', 'Image1',
                'Image2', 'Categories', 'TagsEnglish', 'TagsGerman', 'TagsFrench', 'Urls']);
        }
        $items = DataUtils::indexEntityArray($table->find()->select(['id', 'code', 'image_group_id'])->all(), 'code');
        $update_array = array();
        $image_groups_table = TableRegistry::get('ImageGroups');
        $images_table = TableRegistry::get('Images');
        $images = DataUtils::indexEntityArray($images_table->find()->select(['id', 'name'])->all(), 'name');

        $image_groups_insert = array();
        $image_groups_update = array();
        $image_groups_ids = array();
        $image_group_image_ids = array();

        $item_ids = array();
        $category_ids = array();
        foreach ($excel_data as $data) {
            $item_code = $data['Partnummer'];
            if (isset($items[$item_code]) && !empty($data['Image1'])) {
                $image_ids = array();
                $thumbnail = -1;
                if (!empty($images[$data['Image1']])) {
                    $thumbnail = $images[$data['Image1']]['id'];
                    array_push($image_ids, $thumbnail);
                }
                if (!empty($data['Image2'])) {
                    $img_split = explode(';', $data['Image2']);
                    foreach ($img_split as $img_raw) {
                        if (!empty($images[explode('=', $img_raw)[0]])) {
                            array_push($image_ids, $images[explode('=', $img_raw)[0]]['id']);
                            if ($thumbnail == -1) {
                                $thumbnail = $images[explode('=', $img_raw)[0]]['id'];
                            }
                        }
                    }
                }
                if (empty($items[$item_code]['image_group_id'])) {
                    $image_groups_insert[$item_code] = [
                        'name' => $item_code,
                        'description' => null,
                        'group_type' => 0,
                        'thumbnail' => $thumbnail,
                        'created' => Time::now(),
                        'modified' => Time::now(),
                    ];
                } else {
                    $image_groups_update[$item_code] = ['id' => $items[$item_code]['image_group_id'], 'thumbnail' => $thumbnail];
                    array_push($image_groups_ids, $items[$item_code]['image_group_id']);
                }
                $image_group_image_ids[$item_code] = $image_ids;
            } else {}
            if (!empty($data['ID'])) {
                array_push($item_ids, intval($data['ID']));
            }
            if (!empty($data['Categories'])) {

                if (empty($data['ID']) && isset($items[$item_code])) {
                    if (!empty($category_ids[$items[$item_code]['id']])) {
                        $category_ids[$items[$item_code]['id']] = array_merge($category_ids[$items[$item_code]['id']],
                            explode(',', $data['Categories']));
                    } else {
                        $category_ids[$items[$item_code]['id']] = explode(',', $data['Categories']);
                    }
                } else if (isset($items[$item_code])) {
                    if (!empty($category_ids[intval($data['ID'])])) {
                        $category_ids[intval($data['ID'])] = array_merge($category_ids[intval($data['ID'])],
                            explode(',', $data['Categories']));
                    } else {
                        $category_ids[intval($data['ID'])] = explode(',', $data['Categories']);
                    }
                    $category_ids[intval($data['ID'])] = explode(',', $data['Categories']);
                } else {
                }

            }
        }
        $image_groups = array();
        if (!empty($image_groups_insert)) {
            $insert_vals = array_values($image_groups_insert);
            $response = DataUtils::InsertMultipleIntoResp($image_groups_table,
                ['name', 'description', 'group_type', 'thumbnail', 'created', 'modified'], $insert_vals);
            $last_id = $response->lastInsertId('image_groups');
            $index_name = 0;
            $group_keys = array_keys($image_groups_insert);

            for ($i = $last_id; $i <= (intval($last_id) + $response->rowCount() - 1); $i++) {
                $image_groups[$group_keys[$index_name]] = $i;
                $index_name++;
            }
        }
        if (!empty($image_groups_update)) {
            DataUtils::UpdateMultipleInto($image_groups_table, ['id', 'thumbnail'], array_values($image_groups_update));
            foreach ($image_groups_update as $key => $value) {
                $image_groups[$key] = $value['id'];
            }
        }
        if (!empty($image_groups_update)) {
            $image_assoc_table = TableRegistry::get('ImageGroupAssociations');
            $image_assoc_table->deleteAll(['image_group_id IN' => $image_groups_ids]);
            $insert_assocs = array();
            foreach ($image_group_image_ids as $key => $values) {
                if (!empty($image_groups[$key])) {
                    $group_id = $image_groups[$key];
                    foreach ($values as $image) {
                        array_push($insert_assocs, ['image_group_id' => $group_id, 'image_id' => $image]);
                    }
                }
            }
            DataUtils::InsertMultipleInto($image_assoc_table, ['image_group_id', 'image_id'], $insert_assocs);
        }
        $category_assoc_table = TableRegistry::get('ItemCategoryAssociations');
        if (!empty($item_ids)) {
            $category_assoc_table->deleteAll(['item_id IN' => $item_ids]);
        }
        $category_values = array();
        foreach ($category_ids as $key => $value) {
            foreach ($value as $category) {
                array_push($category_values, [
                    'item_id' => $key,
                    'category_id' => $category
                ]);
            }
        }
        DataUtils::InsertMultipleInto($category_assoc_table, ['item_id', 'category_id'], $category_values);

        foreach ($excel_data as $data) {
            $item_code = $data['Partnummer'];
            if (isset($items[$item_code])) {
                $insert_blank = [
                    'id' => $items[$item_code]['id'],
                    'status' => $data['Status'],
                    'weight' => $data['Gewicht'],
                    'priority' => $data['Priority'],
                    'tags_english' => $data['TagsEnglish'],
                    'tags_german' => $data['TagsGerman'],
                    'tags_french' => $data['TagsFrench'],
                    'urls' => $data['Urls'],
                    'image_group_id' => (!empty($image_groups[$item_code])) ? $image_groups[$item_code] : 0
                ];
                if ($sync_counts) {
                    $insert_blank['stock'] = $data['Aantal'];
                }

                array_push($update_array, $insert_blank);
            }
        }
        if ($sync_counts) {
            DataUtils::UpdateMultipleInto($table, ['id', 'status', 'weight', 'priority', 'image_group_id', 'stock', 'tags_english', 'tags_german', 'tags_french', 'urls'], $update_array);
        } else {
            DataUtils::UpdateMultipleInto($table, ['id', 'status', 'weight', 'priority', 'image_group_id', 'tags_english', 'tags_german', 'tags_french', 'urls'], $update_array);
        }

        $event = new Event('Import.Excel.afterImport', $this, []);
        EventManager::instance()->dispatch($event);
    }

    /**
     * @param CarsTable $table
     * @param $file
     */
    public function syncCars($table, $file)
    {
        $excel_data = $this->readDocument($file, [
            'reference', 'title', 'description', 'category', 'model', 'year', 'color', 'condition', 'price', 'status', 'Image1', 'Image2', 'Priority'
        ]);
        $cars = DataUtils::indexEntityArray($table->find()->select(['id', 'reference', 'image_group_id'])->all(), 'reference');

        $image_groups_table = TableRegistry::get('ImageGroups');
        $images_table = TableRegistry::get('Images');
        $images = DataUtils::indexEntityArray($images_table->find()->select(['id', 'name'])->all(), 'name');

        $image_groups_insert = array();
        $image_groups_update = array();
        $image_groups_ids = array();
        $image_group_image_ids = array();

        foreach ($excel_data as $data) {
            $car_reference = $data['reference'];
            if (!empty($data['Image1'])) {
                $image_ids = array();
                $thumbnail = -1;
                if (!empty($images[$data['Image1']])) {
                    $thumbnail = $images[$data['Image1']]['id'];
                    array_push($image_ids, $thumbnail);
                }
                if (!empty($data['Image2'])) {
                    $img_split = explode(';', $data['Image2']);
                    foreach ($img_split as $img_raw) {
                        if (!empty($images[explode('=', $img_raw)[0]])) {
                            array_push($image_ids, $images[explode('=', $img_raw)[0]]['id']);
                            if ($thumbnail == -1) {
                                $thumbnail = $images[explode('=', $img_raw)[0]]['id'];
                            }
                        }
                    }
                }
                if (empty($cars[$car_reference]['image_group_id'])) {
                    $image_groups_insert[$car_reference] = [
                        'name' => $car_reference,
                        'description' => null,
                        'group_type' => 0,
                        'thumbnail' => $thumbnail,
                        'created' => Time::now(),
                        'modified' => Time::now(),
                    ];
                } else {
                    $image_groups_update[$car_reference] = ['id' => $cars[$car_reference]['image_group_id'], 'thumbnail' => $thumbnail];
                    array_push($image_groups_ids, $cars[$car_reference]['image_group_id']);
                }
                $image_group_image_ids[$car_reference] = $image_ids;
            }
        }

        $image_groups = array();
        if (!empty($image_groups_insert)) {
            $insert_vals = array_values($image_groups_insert);
            $response = DataUtils::InsertMultipleIntoResp($image_groups_table,
                ['name', 'description', 'group_type', 'thumbnail', 'created', 'modified'], $insert_vals);
            $last_id = $response->lastInsertId('image_groups');
            $index_name = 0;
            $group_keys = array_keys($image_groups_insert);

            for ($i = $last_id; $i <= (intval($last_id) + $response->rowCount() - 1); $i++) {
                $image_groups[$group_keys[$index_name]] = $i;
                $index_name++;
            }
        }
        if (!empty($image_groups_update)) {
            DataUtils::UpdateMultipleInto($image_groups_table, ['id', 'thumbnail'], array_values($image_groups_update));
            foreach ($image_groups_update as $key => $value) {
                $image_groups[$key] = $value['id'];
            }
        }
        $image_assoc_table = TableRegistry::get('ImageGroupAssociations');
        if (!empty($image_groups_update)) {
            $image_assoc_table->deleteAll(['image_group_id IN' => $image_groups_ids]);
        }
        if (!empty($image_groups_update) || !empty($image_groups_insert)) {
            $insert_assocs = array();
            foreach ($image_group_image_ids as $key => $values) {
                if (!empty($image_groups[$key])) {
                    $group_id = $image_groups[$key];
                    foreach ($values as $image) {
                        array_push($insert_assocs, ['image_group_id' => $group_id, 'image_id' => $image]);
                    }
                }
            }
            DataUtils::InsertMultipleInto($image_assoc_table, ['image_group_id', 'image_id'], $insert_assocs);
        }

        $cars_insert = array();
        $cars_update = array();
        foreach ($excel_data as $data) {
            $car_reference = $data['reference'];
            if (isset($cars[$car_reference])) {
                array_push($cars_update, [
                    'id' => $cars[$car_reference]['id'],
                    'image_group_id' => (!empty($image_groups[$car_reference])) ? $image_groups[$car_reference] : 0,
                    'alias' => Text::slug($data['reference'] . ' ' . $data['title']),
                    'title' => $data['title'],
                    'description' => nl2br($data['description']),
                    'category' => $data['category'],
                    'model' => $data['model'],
                    'year' => intval($data['year']),
                    'color' => $data['color'],
                    'condition' => $data['condition'],
                    'price' => $data['price'],
                    'status' => $data['status'],
                    'priority' => $data['Priority'],
                ]);
            } else {
                array_push($cars_insert, [
                    'image_group_id' => (!empty($image_groups[$car_reference])) ? $image_groups[$car_reference] : 0,
                    'reference' => $data['reference'],
                    'alias' => Text::slug($data['reference'] . ' ' . $data['title']),
                    'title' => $data['title'],
                    'description' => nl2br($data['description']),
                    'category' => $data['category'],
                    'model' => $data['model'],
                    'year' => intval($data['year']),
                    'color' => $data['color'],
                    'condition' => $data['condition'],
                    'price' => $data['price'],
                    'status' => $data['status'],
                    'priority' => $data['Priority'],
                ]);
            }
        }
        if (!empty($cars_update)) {
            DataUtils::UpdateMultipleInto($table, [
                'id', 'image_group_id', 'alias', 'title', 'description', 'category', 'model',
                'year', 'color', 'condition', 'price', 'status', 'priority'
            ], $cars_update);
        }
        if (!empty($cars_insert)) {
            DataUtils::InsertMultipleInto($table, [
                'reference', 'image_group_id', 'alias', 'title', 'description', 'category', 'model',
                'year', 'color', 'condition', 'price', 'status', 'priority'
            ], $cars_insert);
        }

    }

    /**
     * @param CategoriesTable $table
     * @param $file
     * @param $parent
     */
    public function syncCategories($table, $file)
    {
        $excel_data = $this->readDocument($file, ['Name', 'ID', 'Parent Name', 'Parent ID']);
        $local = $table->find()->select(['id', 'title'])->all()->indexBy('id')->toArray();
        foreach ($excel_data as $data) {
            $parent_id = null;
            if (!empty($data['Parent ID'])) {
                if (!isset($local[intval($data['Parent ID'])])) {
                    $entity = $table->newEntity([
                        'title' => $data['Parent Name'],
                    ]);
                    $entity->set('id', intval($data['Parent ID']));
                    if ($table->save($entity)) {
                        $local[$data['Parent Name']] = $entity;
                    }

                } else {
                    $local[intval($data['Parent Name'])]->title = $data['Parent Name'];
                }
                $parent_id = intval($data['Parent ID']);
            }
            if (!empty($data['ID'])) {
                if (!isset($local[intval($data['ID'])])) {
                    $entity = $table->newEntity([
                        'title' => $data['Name'],
                        'parent_id' => $parent_id
                    ]);
                    $entity->set('id', intval($data['ID']));
                    if ($table->save($entity)) {
                        $local[$data['Name']] = $entity;
                    }
                } else {
                    $local[intval($data['ID'])]->title = $data['Name'];
                    $local[intval($data['ID'])]->parent_id = $parent_id;
                    $ret = $table->save($local[intval($data['ID'])]);
                }
            }
        }
    }

    /**
     * @param CategoriesTable $table
     * @param $file
     * @param $parent
     * @return array
     */
    public function syncWriteCategories($table, $file, $parent = null)
    {
        $excel_data = $this->readDocument($file, ['Groep', 'Sub Groep']);
        $local = $table->find()->select(['id', 'title', 'parent_id'])->all()->toList();
        $local_id = DataUtils::indexEntityMap($table->find()->select(['id', 'title', 'parent_id'])->all(), 'id');
        $ret = array();
        foreach ($excel_data as $data) {
            $my_id = $data['Sub Groep'];
            foreach ($local as $local_dat) {
                if ($local_dat->title == $data['Sub Groep']) {
                    $dat = $local_dat;
                    while (!empty($dat->parent_id)) {
                        if ($local_id[$dat->parent_id]->title == $data['Groep']) {
                            $my_id = $local_dat->id;
                            break;
                        }
                        $dat = $local_id[$dat->parent_id];
                    }
                }
            }
            array_push($ret, ['Groep' => $data['Groep'], 'Sub Groep' => $my_id, 'origin' => $data['Sub Groep']]);
        }
        return $ret;
    }

}
