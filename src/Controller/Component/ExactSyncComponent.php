<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 5/28/2016
 * Time: 11:01 PM
 */

namespace App\Controller\Component;


use App\Exact\Address;
use App\Exact\ItemWarehouses;
use App\Model\Table\ExactConnectedTable;
use App\Model\Table\ItemsTable;
use App\Utils\DataUtils;
use App\Utils\ExactUtils;
use Cake\Cache\Cache;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Datasource\ResultSetInterface;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Customer\Helper\Countries;
use Picqer\Financials\Exact\Account;
use Picqer\Financials\Exact\Connection;
use Picqer\Financials\Exact\Item;
use Picqer\Financials\Exact\Model;
use Picqer\Financials\Exact\Query\Findable;
use Picqer\Financials\Exact\SalesOrders;

class ExactSyncComponent extends Component
{
    /**
     * Connection
     * @var Connection
     */
    public $connection = null;
    protected $cred_storage = null;

    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        Configure::load('credentials', 'default');
        $temp = Cache::read('exact_credentials', '_exact_credentials_');
        if ($temp == false) {
            $this->cred_storage = array();
        } else {
            $this->cred_storage = $temp;
        }

        if (isset($this->request->query['code']) && is_null($this->getValue('authorizationcode'))) {
            $this->setValue('authorizationcode', $this->request->query['code']);
            Cache::write('exact_credentials', $this->cred_storage, '_exact_credentials_');
        }

        parent::initialize($config);
    }

    public function tryConnect()
    {
        if (empty($this->connection)) {
            if ($this->getValue('authorizationcode') == null) {
                $this->authorize();
            }
            $this->connection = $this->connect();
            Log::debug("Connection :" . json_encode($this->connection));
        }
        return !empty($this->connection);
    }

    public function resetCredentials()
    {
        $this->cred_storage = null;
        $this->connection = null;
        Cache::delete('exact_credentials', '_exact_credentials_');
    }

    /**
     * Function to retrieve persisted data for the example
     * @param string $key
     * @return null|string
     */
    private function getValue($key)
    {
        if ($this->cred_storage != null && array_key_exists($key, $this->cred_storage)) {
            return $this->cred_storage[$key];
        }
        return null;
    }

    /**
     * Function to persist some data for the example
     * @param string $key
     * @param string $value
     */
    private function setValue($key, $value)
    {
        $this->cred_storage[$key] = $value;
    }

    /**
     * Authorize user because there is no info at all!
     */
    private function authorize()
    {
        if(!$this->checkAuth()) return;
        Cache::delete('exact_credentials', '_exact_credentials_');
        $connection = new Connection();
        $temp_config = Configure::read('ExactCredentials');
        $connection->setDivision($temp_config['DIVISION']);
        $connection->setRedirectUrl($temp_config['REDIRECT_BASE']);
        $connection->setExactClientId($temp_config['CLIENT_ID']);
        $connection->setExactClientSecret($temp_config['CLIENT_SECRET']);
        $connection->redirectForAuthorization();
    }

    /**
     * Function to connect to Exact, this creates the client and automatically retrieves oAuth tokens if needed
     *
     * @return \Picqer\Financials\Exact\Connection
     * @throws Exception
     */
    private function connect()
    {
        $connection = new Connection();
        $temp_config = Configure::read('ExactCredentials');
        $connection->setDivision($temp_config['DIVISION']);
        $connection->setRedirectUrl($temp_config['REDIRECT_BASE']);
        $connection->setExactClientId($temp_config['CLIENT_ID']);
        $connection->setExactClientSecret($temp_config['CLIENT_SECRET']);


        if ($this->getValue('authorizationcode')) // Retrieves authorizationcode from database
        {
            $connection->setAuthorizationCode($this->getValue('authorizationcode'));
        }

        if ($this->getValue('accesstoken')) // Retrieves accesstoken from database
        {
            $connection->setAccessToken($this->getValue('accesstoken'));
        }

        if ($this->getValue('refreshtoken')) // Retrieves refreshtoken from database
        {
            $connection->setRefreshToken($this->getValue('refreshtoken'));
        }

        if ($this->getValue('expires_in')) // Retrieves expires timestamp from database
        {
            $connection->setTokenExpires($this->getValue('expires_in'));
        }

        // Make the client connect and exchange tokens
        try {
            $connection->connect();
        } catch (\Exception $e) {
            $this->authorize();
            return;
            // TODO: if admin authorize
            throw new Exception('Could not connect to Exact: ' . $e->getMessage());
        }

        // Save the new tokens for next connections
        $this->setValue('accesstoken', $connection->getAccessToken());
        $this->setValue('refreshtoken', $connection->getRefreshToken());

        // Save expires time for next connections
        $this->setValue('expires_in', $connection->getTokenExpires());
        Cache::write('exact_credentials', $this->cred_storage, '_exact_credentials_');

        return $connection;
    }

    public function checkAuth($admin = true) {
        //return true;
        if(!$this->isAuthorized(Router::getRequest(true)->session()->read('Auth.User'), $admin)) {
            return false;
        }
        return true;
    }

    public function isAuthorized($user, $admin = true) {
        if(isset($user['type'])) {
            if($admin === true) {
                if($user['type'] == 1) {
                    return true;
                }
            } elseif($user['type'] >= 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $exact_class
     * @param $cake_class
     * @param $upstream
     * @param bool $force
     * @param null|array $foreign_keys
     * Array goes as following: name_cake_key => [CakeTable, KeyFromExact, delete if no association]
     * @param string $initialFilter
     * @return bool
     */
    public function syncModel($exact_class, $cake_class, $upstream, $force = false, $foreign_keys = null, $initialFilter = '')
    {
        Log::debug("Syncing Model: " . $exact_class );
        set_time_limit(0);
        $this->tryConnect();
        if(empty($this->connection)) {
            echo 'cannot connect to exact. Try again and/or re-authenticate.'; die();
        }
        /** @var ExactConnectedTable $table */
        $table = TableRegistry::get($cake_class);
        try {
            $filter = $initialFilter;
            $readfrom = explode('\\', $exact_class);
            if (!$force) {
                $date_time = Cache::read('exact_' . $readfrom[count($readfrom) - 1] . '_sync_date', '_exact_credentials_');
                Log::debug($cake_class . 'Datetime ' . $date_time);
                $filter = (($date_time != false) ? 'Modified ge DateTime\'' . $date_time->format('Y-m-d\Th:i:s') . '\'' : '');
            }
            //Log::debug("syncModel ".$readfrom[count($readfrom)-1]." FILTER $filter");

            /** @var Model|Findable $exact_model */
            $exact_model = new $exact_class($this->connection);
            $exact_result = DataUtils::indexExactArray(
                $exact_model->filter($filter, '', implode(",", $table->getExactFields())),
                'ID');
            Cache::write('exact_' . $readfrom[count($readfrom) - 1] . '_sync_date', Time::now(), '_exact_credentials_');
        } catch (\Exception $e) {
            Log::error(json_encode($e));
            return false;
        }
        $ret = true;
        $in_update_array = array();
        $in_delete_array = array();
        if (!empty($upstream)) {
            $out_update_array = array();
            $out_insert_array = array();
        }
        /** @var ResultSetInterface $local_result */
        $local_result = $table->find('all');
        foreach ($local_result as $local_data) {
            if ($local_data->get('exact_guid') == null) {
                if (!empty($upstream)) array_push($out_insert_array, $local_data);
                continue;
            }
            if (empty($exact_result[$local_data->get('exact_guid')])) {
                array_push($in_delete_array, $local_data->get('id'));
                continue;
            }

            $exact_model = $exact_result[$local_data->get('exact_guid')];
            $exact_modified = DataUtils::ExactUnixConvertFrom($exact_model['Modified']) * ($force);
            //Log::debug("Syncing $cake_class: ($force) and ".$exact_modified > $local_data->get('modified')->format('U'));
            if ($force || $exact_modified > $local_data->get('modified')->format('U')) {
                $exact_model['id'] = $local_data->get('id');
                array_push($in_update_array, $exact_model);
            } else if ($exact_modified < $local_data->get('modified')->format('U')) {
                if (!empty($upstream)) array_push($out_update_array, $local_data);
            }
            unset($exact_result[$local_data->get('exact_guid')]);
        }
        $in_insert_array = array();
        if ((count($exact_result) > 0 || count($in_update_array) > 0)) {
            $foreign_data_list = array();
            if ($foreign_keys != null && count($foreign_keys) > 0) {
                foreach ($foreign_keys as $key => $key_data) {
                    $foreign_table = TableRegistry::get($key_data[0]);
                    $foreign_models = DataUtils::indexEntityArray(
                        $foreign_table->find()->select(['id', 'exact_guid'])->all(), 'exact_guid');
                    $foreign_data_list[$key] = [$foreign_models, $key_data[1], $key_data[2]];
                }
            }

            foreach ($exact_result as $item) {
                $skip = false;
                foreach ($foreign_data_list as $key => $key_data) {
                    if (isset($item[$key_data[1]]) && isset($key_data[0][$item[$key_data[1]]])) {
                        $item[$key] = $key_data[0][$item[$key_data[1]]]['id'];
                    } else if ($key_data[2] == true) {
                        $skip = true;
                    }
                }
                if ($skip == true) continue;
                array_push($in_insert_array, $table->ConvertFromExact($item));
            }
            for ($i = 0; $i < count($in_update_array); $i++) {
                $skip = false;
                foreach ($foreign_data_list as $key => $key_data) {
                    if (isset($in_update_array[$i][$key_data[1]])) {
                        $in_update_array[$i][$key] = $key_data[0][$in_update_array[$i][$key_data[1]]]['id'];
                    } else if ($key_data[2] == true) {
                        $skip = true;
                    }
                }
                if ($skip == true) continue;
                $in_update_array[$i] = $table->ConvertFromExact($in_update_array[$i]);
            }
        }

        if (!DataUtils::InsertMultipleInto($table, $table->getInsertColumns(), $in_insert_array)) {
            $ret = true;
        }


        if (!DataUtils::UpdateMultipleInto($table, $table->getUpdateColumns(), $in_update_array)) {
            $ret = true;
        }

        if ($force == true && !$initialFilter && !empty($in_delete_array)) {
//            $table->deleteAll(['id IN' => $in_delete_array]);
            if(count($in_delete_array) > 20) {
                Log::debug('Fishy delete during sync');
                Log::debug(json_encode([
                    'in_delete_array' => $in_delete_array,
                    'exact_result' => $exact_result,
                ]));
            }

        }

        return $ret;
    }

    public function syncPrices($force)
    {
        $this->tryConnect();
        return $this->syncModel('App\Exact\SalesItemPrices', 'ItemPrices', false, $force,
            ['item_id' => ['Items', 'Item', true]]);
    }

    public function test()
    {
        $this->tryConnect();
        $table = TableRegistry::get('Items');
        $local_result = $table->find()->select(['id', 'exact_guid', 'stock', 'warehouse_guid']);
        foreach ($local_result as $result) {
            if ($result->exact_guid == 'a710094b-9757-49bf-862d-129e122774a0') {
                $item_wares = new ItemWarehouses($this->connection);
                $resultz = $item_wares->filter("Item eq guid'a710094b-9757-49bf-862d-129e122774a0'", '', 'ID,Item,Warehouse');
                $myid = null;
                if (!empty($resultz)) {
                    foreach ($resultz as $res) {
                        $myid = $res->ID;
                    }
                }
                $item_wares = new ItemWarehouses($this->connection);
                $item_wares->ID = $myid;
                $item_wares->Item = $result->exact_guid;
                $item_wares->Warehouse = $result->warehouse_guid;
                $item_wares->SafetyStock = $result->stock;
                $item_wares->update();
            }

        }

    }

    public function syncOrder($id)
    {
        $this->tryConnect();
        $orders_table = TableRegistry::get('Orders');
        $order = $orders_table->find()->where(['Orders.id' => $id])
            ->contain(['BillingAddresses', 'ShippingAddresses', 'Customers', 'ShippingMethods', 'ShippingOptions'])->first();
        if (empty($order)) {
            return false;
        }

        if ($order->billing_id != $order->shipping_id) {
            $billing_ad = $order->billing_address;
        } else {
            $billing_ad = $order->shipping_address;
        }

        $account = $this->pushAccounts($order->customer, $billing_ad);
        $shipping = $this->pushAddress($account, $order->shipping_address);
        if ($order->billing_id != $order->shipping_id) {
            $billing = $this->pushAddress($account, $order->billing_address);
        } else {
            $billing = $shipping;
        }

        $warehouses = array();
        $assoc_table = TableRegistry::get('OrderItemAssociations');
        $assocs = $assoc_table->find()->where(['order_id' => $id])->contain(['Items' => function ($q) {
            return $q->select(['id', 'exact_guid', 'warehouse_guid']);
        }])->all();
        foreach ($assocs as $item) {
            if (empty($warehouses[$item->item->warehouse_guid])) {
                $warehouses[$item->item->warehouse_guid] = array();
            }
            array_push($warehouses[$item->item->warehouse_guid], [
                'Item' => $item->item->exact_guid,
                'Quantity' => $item->quantity
            ]);
        }

        if (!empty($order->exact_guid)) {
            $order_guids = explode(';', $order->exact_guid);
        } else {
            $order_guids = array();
        }
        $index = 0;
        foreach ($warehouses as $warehouse => $items) {
            $order_data = new SalesOrders($this->connection);
            if (!empty($order_guids[$index])) {
                $order_data->OrderID = $order_guids[$index];
            }
            if (empty($order_guids[$index])) {
                $order_data->Description = 'Webshop order #' . $order->id;
                $order_data->OrderDate = $order->modified->format('Y-m-d\TH:i:s');
                $order_data->OrderedBy = $account;
                $order_data->DeliveryAddress = $shipping;
                $order_data->YourRef = "WSH_$order->id";
                $order_data->WarehouseID = $warehouse;
                $order_data->SalesOrderLines = $items;
                $order_data->Remarks = $order->shipping_address->street . "\n" .
                    $order->shipping_address->postcode . ' '.$order->shipping_address->city.
                    "\n".Countries::$countries[$order->shipping_address->country];
            }
            $order_data->PaymentReference = $order->payment_id;
            $order_data->save();
            if (empty($order_guids[$index])) {
                array_push($order_guids, $order_data->OrderID);
            }
            $index++;
        }
        $order->set('exact_guid', implode(';', $order_guids));
        if ($orders_table->save($order)) {
            return true;
        }
        return false;
    }

    /**
     * @param Entity $account
     * @param null|Entity $address
     * @return bool|string
     */
    public function pushAccounts($account, $address = null)
    {
        $this->tryConnect();
        if(!$this->connection) {
            Log::critical('No connection found');
            return false;
        }

        $data = new Account($this->connection);
        if (!empty($account->exact_guid)) {
            $data->ID = $account->exact_guid;
        }
        $data->Name = $account->get('name');
        $data->Email = $account->get('email');
        $data->Status = 'C';
        if ($address != null) {
            $data->AddressLine1 = $address->get('street');
            $data->City = $address->get('city');
            $data->Country = $address->get('country');
            $data->Phone = $address->get('phone');
            $data->Postcode = $address->get('postcode');
        }
        if(!empty($account->get('company'))) {
            $data->Remarks = "Company: ".$account->get('company')." | \n".
            "Vat Code: ".$account->get('vat_code');
        }
        $data->save();
        $account->set('exact_guid', $data->ID);
        $account_table = TableRegistry::get('Customers');
        if ($account_table->save($account)) {
            return $data->ID;
        }
        return false;
    }

    /**
     * @param string $account_guid
     * @param Entity $address
     * @return string
     */
    public function pushAddress($account_guid, $address)
    {
        $this->tryConnect();
        $data = new Address($this->connection);
        if (!empty($address->exact_guid)) {
            $data->ID = $address->exact_guid;
        }
        $data->Account = $account_guid;
        $data->AddressLine1 = $address->get('street');
        $data->City = $address->get('city');
        $data->Country = $address->get('country');
        $data->Phone = $address->get('phone');
        $data->Postcode = $address->get('postcode');
        if ($address->get('type') == 1) {
            $data->Type = 3;
        } else {
            $data->Type = 4;
        }
        $data->save();
        $address->set('exact_guid', $data->ID);
        $addresses_table = TableRegistry::get('Addresses');
        if ($addresses_table->save($address)) {
            return $data->ID;
        }
        return false;
    }

    public function syncWarehouses($force = false)
    {
        $warehouse = env('DEFAULT_WAREHOUSE', null);
        if ($warehouse) {
            /** @var ItemsTable $itemsTable */
            $itemsTable = TableRegistry::get('Items');
            $itemsTable->updateAll(['warehouse_guid' => $warehouse], ['warehouse_guid !=' => $warehouse]);
            return;
        }

        $this->tryConnect();
        $item_wares = new ItemWarehouses($this->connection);

        $filter = '';
        $key = 'warehouse';
        if (!$force) {
            $date_time = Cache::read('exact_' . $key . '_sync_date', '_exact_credentials_');
            Log::debug($key . ': Datetime ' . $date_time);
            $filter = (($date_time != false) ? 'Modified ge DateTime\'' . $date_time->format('Y-m-d\Th:i:s') . '\'' : '');
        }
        $result = $item_wares->filter($filter, '', 'ID,Item,Warehouse');
        Cache::write('exact_' . $key . '_sync_date', Time::now(), '_exact_credentials_');
        $items_table = TableRegistry::get('Items');
        $items = DataUtils::indexEntityArray($items_table->find()->select(['id', 'exact_guid'])->all(), 'exact_guid');
        $insert_array = array();

        foreach ($result as $ware) {
            if (!empty($items[$ware->Item])) {
                $insert_array[$items[$ware->Item]['id']] = [
                    'id' => $items[$ware->Item]['id'],
                    'warehouse_guid' => $ware->Warehouse
                ];
            }
        }

        DataUtils::UpdateMultipleInto($items_table, ['id', 'warehouse_guid'], array_values($insert_array));
        return true;
    }
}
