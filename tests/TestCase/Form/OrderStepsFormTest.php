<?php
namespace App\Test\TestCase\Form;

use App\Form\OrderStepsForm;
use Cake\TestSuite\TestCase;

/**
 * App\Form\OrderStepsForm Test Case
 */
class OrderStepsFormTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Form\OrderStepsForm
     */
    public $OrderSteps;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->OrderSteps = new OrderStepsForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderSteps);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
