<?php
namespace App\Test\TestCase\Form;

use App\Form\CustomerRegisterForm;
use Cake\TestSuite\TestCase;

/**
 * App\Form\CustomerRegisterForm Test Case
 */
class CustomerRegisterFormTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Form\CustomerRegisterForm
     */
    public $CustomerRegister;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->CustomerRegister = new CustomerRegisterForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CustomerRegister);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
