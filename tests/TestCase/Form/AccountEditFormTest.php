<?php
namespace App\Test\TestCase\Form;

use App\Form\AccountEditForm;
use Cake\TestSuite\TestCase;

/**
 * App\Form\AccountEditForm Test Case
 */
class AccountEditFormTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Form\AccountEditForm
     */
    public $AccountEdit;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->AccountEdit = new AccountEditForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccountEdit);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
