<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\MollieComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\MollieComponent Test Case
 */
class MollieComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\MollieComponent
     */
    public $Mollie;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Mollie = new MollieComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Mollie);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
