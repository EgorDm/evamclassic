<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\OrdersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\OrdersController Test Case
 */
class OrdersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.orders',
        'app.customers',
        'app.addresses',
        'app.billing_addresses',
        'app.shipping_addresses',
        'app.shipping_methods',
        'app.shipping_options',
        'app.items',
        'app.item_groups',
        'app.categories',
        'app.item_category_associations',
        'app.image_groups',
        'app.images',
        'app.image_group_associations',
        'app.vat_codes',
        'app.item_prices',
        'app.order_item_associations'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
