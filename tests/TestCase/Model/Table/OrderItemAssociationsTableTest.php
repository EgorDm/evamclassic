<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderItemAssociationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderItemAssociationsTable Test Case
 */
class OrderItemAssociationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderItemAssociationsTable
     */
    public $OrderItemAssociations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.order_item_associations',
        'app.orders',
        'app.users',
        'app.billings',
        'app.shippings',
        'app.items',
        'app.item_groups',
        'app.categories',
        'app.item_category_associations',
        'app.image_groups',
        'app.images',
        'app.image_group_associations',
        'app.item_prices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrderItemAssociations') ? [] : ['className' => 'App\Model\Table\OrderItemAssociationsTable'];
        $this->OrderItemAssociations = TableRegistry::get('OrderItemAssociations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderItemAssociations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
