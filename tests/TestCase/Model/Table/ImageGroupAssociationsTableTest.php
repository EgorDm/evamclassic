<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ImageGroupAssociationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ImageGroupAssociationsTable Test Case
 */
class ImageGroupAssociationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ImageGroupAssociationsTable
     */
    public $ImageGroupAssociations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.image_group_associations',
        'app.image_groups',
        'app.images',
        'app.items',
        'app.item_groups',
        'app.categories',
        'app.item_prices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ImageGroupAssociations') ? [] : ['className' => 'App\Model\Table\ImageGroupAssociationsTable'];
        $this->ImageGroupAssociations = TableRegistry::get('ImageGroupAssociations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ImageGroupAssociations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
