<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemPricesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemPricesTable Test Case
 */
class ItemPricesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemPricesTable
     */
    public $ItemPrices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.item_prices',
        'app.items',
        'app.item_groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemPrices') ? [] : ['className' => 'App\Model\Table\ItemPricesTable'];
        $this->ItemPrices = TableRegistry::get('ItemPrices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemPrices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
