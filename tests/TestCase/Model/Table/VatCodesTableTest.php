<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VatCodesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VatCodesTable Test Case
 */
class VatCodesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VatCodesTable
     */
    public $VatCodes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vat_codes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('VatCodes') ? [] : ['className' => 'App\Model\Table\VatCodesTable'];
        $this->VatCodes = TableRegistry::get('VatCodes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VatCodes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
