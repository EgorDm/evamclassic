<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemCategoryAssociationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemCategoryAssociationsTable Test Case
 */
class ItemCategoryAssociationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemCategoryAssociationsTable
     */
    public $ItemCategoryAssociations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.item_category_associations',
        'app.items',
        'app.item_groups',
        'app.categories',
        'app.image_groups',
        'app.images',
        'app.image_group_associations',
        'app.item_prices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemCategoryAssociations') ? [] : ['className' => 'App\Model\Table\ItemCategoryAssociationsTable'];
        $this->ItemCategoryAssociations = TableRegistry::get('ItemCategoryAssociations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemCategoryAssociations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
