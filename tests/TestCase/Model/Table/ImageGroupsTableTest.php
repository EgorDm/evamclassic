<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ImageGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ImageGroupsTable Test Case
 */
class ImageGroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ImageGroupsTable
     */
    public $ImageGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.image_groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ImageGroups') ? [] : ['className' => 'App\Model\Table\ImageGroupsTable'];
        $this->ImageGroups = TableRegistry::get('ImageGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ImageGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
