<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemGroupsTable Test Case
 */
class ItemGroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemGroupsTable
     */
    public $ItemGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.item_groups',
        'app.items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemGroups') ? [] : ['className' => 'App\Model\Table\ItemGroupsTable'];
        $this->ItemGroups = TableRegistry::get('ItemGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test ConvertFromExact method
     *
     * @return void
     */
    public function testConvertFromExact()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getExactFields method
     *
     * @return void
     */
    public function testGetExactFields()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getInsertColumns method
     *
     * @return void
     */
    public function testGetInsertColumns()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getUpdateColumns method
     *
     * @return void
     */
    public function testGetUpdateColumns()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
