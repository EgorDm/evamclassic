<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AddressesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AddressesTable Test Case
 */
class AddressesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AddressesTable
     */
    public $Addresses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.addresses',
        'app.customers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Addresses') ? [] : ['className' => 'App\Model\Table\AddressesTable'];
        $this->Addresses = TableRegistry::get('Addresses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Addresses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test ConvertFromExact method
     *
     * @return void
     */
    public function testConvertFromExact()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test ConvertToExact method
     *
     * @return void
     */
    public function testConvertToExact()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getExactFields method
     *
     * @return void
     */
    public function testGetExactFields()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getInsertColumns method
     *
     * @return void
     */
    public function testGetInsertColumns()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getUpdateColumns method
     *
     * @return void
     */
    public function testGetUpdateColumns()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
