<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\AliasBehavior;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\AliasBehavior Test Case
 */
class AliasBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Behavior\AliasBehavior
     */
    public $Alias;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Alias = new AliasBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Alias);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
