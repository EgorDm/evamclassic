<?php
use Migrations\AbstractMigration;

class AddSiteIdentifierToTexts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('texts')
            ->addColumn('site', 'string', ['limit' => 255, 'null' => false, 'default' => 'evam']);
        $table->update();
    }
}
