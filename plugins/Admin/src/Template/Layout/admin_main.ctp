<?php
use Cake\View\View;

/**
 * @var View $this
 */

$description = 'Admin Panel';
$this->assign('fullname', isset($fullname) ? $fullname : 'Admin');
$this->assign('isAdmin', isset($isAdmin) ? $isAdmin : 0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title><?= $description . ' - ' . $this->fetch('title') ?></title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('main.css') ?>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view nav-actions">
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <?= $this->element('Admin.navigation') ?>
                        <?= $this->fetch('sidebar') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <?= $this->fetch('fullname') ?>
                                <span class="fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"><i class="fa fa-user pull-right"></i> Profile</a></li>
                                <li>
                                    <?= $this->Html->link('<i class="fa fa-cog pull-right"></i> Settings',
                                        ['action' => 'index', 'controller' => 'Preferences'], ['escape' => false]) ?>
                                </li>
                                <li><?= $this->Html->link('<i class="fa fa-sign-out pull-right"></i> Log Out',
                                        ['action' => 'logout', 'controller' => 'Users'], ['escape' => false]) ?></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>
                <?= $this->Flash->render() ?>
                <div class="page-title">
                    <div class="title_left">
                        <h3><?= $this->fetch('title') ?></h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $this->fetch('content') ?>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="pull-right">
                <a href="http://egordmitriev.net">Webdesign by Egor Dmitriev</a>
            </div>
            <div class="clearfix"></div>
        </footer>
    </div>
</div>

<?= $this->Html->script('vendor/bootstrap') ?>
<?= $this->Html->script('extras') ?>
<?= $this->Html->script('statistics') ?>
<?= $this->Html->script('main') ?>
<?= $this->fetch('custom_js') ?>
</body>
</html>
