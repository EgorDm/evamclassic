<?php
use Cake\View\View;

/**
 * @var View $this
 */

$description = 'Admin Panel';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title><?= $description . ' - ' . $this->fetch('title') ?></title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('main.css') ?>
</head>

<body class="login">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <?= $this->Form->create(null, ['class' => 'login-form']) ?>
                <h1>Admin Login</h1>
                <?= $this->Flash->render('auth') ?>
                <?= $this->Flash->render() ?>
                <div>
                    <?= $this->Form->input('username', ['class' => 'form-control', 'required' => '', 'placeholder' => 'Username']); ?>
                </div>
                <div>
                    <?= $this->Form->input('password', ['class' => 'form-control', 'required' => '', 'placeholder' => 'Password']); ?>
                </div>
                <div>
                    <?= $this->Form->button(__('Login'), ['class' => 'btn btn-default submit']) ?>
                    <a class="reset_pass" href="#">Lost your password?</a>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <div>
                        <a href="http://egordmitriev.net">Webdesign by Egor Dmitriev</a>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </section>
        </div>
    </div>
</div>

<?= $this->Html->script('vendor/bootstrap') ?>
</body>
</html>
