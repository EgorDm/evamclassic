<ul class="nav side-menu">
    <?= $this->element('Admin.Partials/navigation_group', [
        'group' => ['icon' => 'fa-home', 'title' => 'General'],
        'data_array' => [
            ['title' => 'Dashboard', 'controller' => 'Dashboard', 'action' => 'general', 'plugin' => null],
            ['title' => 'All Pages', 'controller' => 'Pages', 'action' => 'index', 'plugin' => null],
            ['title' => 'All Texts', 'controller' => 'Texts', 'action' => 'index', 'plugin' => null],
            ['title' => 'Image groups', 'controller' => 'ImageGroups', 'action' => 'index', 'plugin' => null],
            ['title' => 'All Images', 'controller' => 'Images', 'action' => 'index', 'plugin' => null],
        ]
    ]); ?>
    <?= $this->element('Admin.Partials/navigation_group', [
        'group' => ['icon' => 'fa-shopping-cart', 'title' => 'Webshop', 'extra_controllers' => ['ItemPrices']],
        'data_array' => [
            ['title' => 'All Items', 'controller' => 'Items', 'action' => 'index', 'plugin' => null],
            ['title' => 'All Cars', 'controller' => 'Cars', 'action' => 'index', 'plugin' => null],
            ['title' => 'Categories', 'controller' => 'Categories', 'action' => 'index', 'plugin' => null],
            ['title' => 'Item groups', 'controller' => 'ItemGroups', 'action' => 'index', 'plugin' => null],
        ]
    ]); ?>
    <?= $this->element('Admin.Partials/navigation_group', [
        'group' =>  ['icon' => 'fa-database', 'title' => 'Data'],
        'data_array' => [
            ['title' => 'Customers', 'controller' => 'Customers', 'action' => 'index', 'plugin' => null],
            ['title' => 'Orders', 'controller' => 'Orders', 'action' => 'index', 'plugin' => null],
        ]
    ]); ?>
    <?= $this->element('Admin.Partials/navigation_group', [
        'group' =>  ['icon' => 'fa-credit-card', 'title' => 'Purchasing', 'extra_controllers' => ['ShippingOptions']],
        'data_array' => [
            ['title' => 'Shipping', 'controller' => 'ShippingMethods', 'action' => 'index', 'plugin' => null],
            ['title' => 'Vat Codes', 'controller' => 'VatCodes', 'action' => 'index', 'plugin' => null],
        ]
    ]); ?>
    <?= $this->element('Admin.Partials/navigation_group', [
        'group' =>  ['icon' => 'fa-plug', 'title' => 'Admin menu'],
        'data_array' => [
            ['title' => 'Users', 'controller' => 'Users', 'action' => 'index', 'plugin' => null],
            ['title' => 'Test', 'controller' => 'Test', 'action' => 'index', 'plugin' => null],
        ]
    ]); ?>
    <?= $this->element('Seo.Admin/navigation')?>
</ul>
