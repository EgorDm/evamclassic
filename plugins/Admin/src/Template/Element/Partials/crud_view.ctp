<?php
use Cake\View\View;

/**
 * @var View $this
 */

?>
<div class="x_panel">
    <div class="x_title">
        <h2><?= $title ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <?php
            foreach ($actions as $action) {
                if (isset($action['post'])) {
                    echo '<li>' . $this->Form->postLink($action['title'], $action['url'], $action['options']) . '</li>';
                } else {
                    echo '<li>' . $this->Html->link($action['title'], $action['url'], $action['options']) . '</li>';
                }
            }
            ?>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
            <div class="row">
                <table class="table table-hover table-striped">
                    <?php for ($i = 0; $i < count($fields); $i++) { ?>
                        <tr>
                            <th><?= $fields[$i]['name'] ?></th>
                            <td><?= $fields[$i]['value'] ?></td>
                            <?php if (count($fields) > 8) {
                                if ($i + 1 < count($fields)) {
                                    $i++; ?>
                                    <th><?= $fields[$i]['name'] ?></th>
                                    <td><?= $fields[$i]['value'] ?></td>
                                <?php } else {
                                    echo '<th></th><td></td>';
                                }
                            } ?>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>
