<?php
use Cake\View\View;

/**
 * @var View $this
 */

?>

<div class="x_panel">
    <div class="x_title">
        <h2><?= $title ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <?php
            foreach ($actions as $action) {
                if (isset($action['post'])) {
                    echo '<li>' . $this->Form->postLink($action['title'], $action['url'], $action['options']) . '</li>';
                } else {
                    echo '<li>' . $this->Html->link($action['title'], $action['url'], $action['options']) . '</li>';
                }
            }
            ?>
        </ul>
        <form class="nav navbar-right panel_toolbox" method="post">
            <input type="text" placeholder="Search" name="search">
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
            <div class="row">
                <div class="col-sm-12">
                    <table id="datatable" class="table table-striped dataTable no-footer table-hover" role="grid"
                           aria-describedby="datatable_info">
                        <thead>
                        <tr role="row">
                            <?php foreach ($headings as $heading) { ?>
                                <th class="sorting" aria-controls='datatable' rowspan="1" colspan="1"
                                    aria-sort="ascending">
                                    <?= $this->Paginator->sort($heading['model'], $heading['name']) //style="<?= "width:" . $heading['width'] . "px"    ?>
                                </th>
                            <?php }
                            if (isset($actions)) { ?>
                                <th class="actions"><?= __('Actions') ?></th>
                            <?php } ?>
                        </tr>
                        </thead>
                        <tbody>

                        <?php foreach ($threads as $thread): ?>
                            <tr role="row">
                                <?php foreach ($thread['fields'] as $field): ?>
                                    <td><?= $field ?></td>
                                <?php endforeach; ?>
                                <?php if (isset($actions)) {
                                    echo ' <td class="actions">';
                                    foreach ($item_actions as $item_action) {
                                        $url = \Admin\Helper\Generation::extractUrlParams($item_action);
                                        $url[] = $thread['id'];
                                        if (isset($item_action['post'])) {
                                            echo $this->Form->postLink($item_action['title'], $url, $item_action['options']) . ' ';
                                        } else {
                                            echo $this->Html->link($item_action['title'], $url, $item_action['options']) . ' ';
                                        }
                                    }
                                    echo '</td>';
                                } ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php if (isset($paginate)) { ?>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                    <p><?= $this->Paginator->counter() ?></p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
