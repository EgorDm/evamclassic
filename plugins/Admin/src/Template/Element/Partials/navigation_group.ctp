<?php
/** @var array $group */
/** @var array $data_array */

$active = '';
$expanded = '';
$ret = '';
foreach ($data_array as $data) {
    $urlKeyMask = ['controller' => true, 'action' => true, 'plugin' => true, 'prefix' => true];
    $urlParams = array_intersect_key($data, $urlKeyMask);

    if($data['controller'] == $this->request->param('controller')) {
        $active = 'class="active"';
        $expanded = 'style="display: block;"';
        $ret.= "<li $active>".$this->Html->link($data['title'], $urlParams)."</li>";
    } else {
        $ret.= "<li>".$this->Html->link($data['title'], $urlParams)."</li>";
    }
}
if(isset($group['extra_controllers'] )) {
    foreach ($group['extra_controllers'] as $controller) {
        if ($controller == $this->request->param('controller')) {
            $active = 'class="active"';
            $expanded = 'style="display: block;"';
        }
    }
}
$ret = "<li $active><a><i class=\"fa $group[icon]\"></i> $group[title] <span class=\"fa fa-chevron-down\"></span></a>"
    ."<ul $expanded class=\"nav child_menu\">$ret</ul></li>";
echo $ret;
