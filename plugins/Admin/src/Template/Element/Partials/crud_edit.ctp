<?php
use Cake\View\View;

/**
 * @var View $this
 */

?>

    <div class="x_panel">
        <div class="x_title">
            <h2><?= $title ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <?php
                foreach ($actions as $action) {
                    if (isset($action['post'])) {
                        echo '<li>' . $this->Form->postLink($action['title'], $action['url'], $action['options']) . '</li>';
                    } else {
                        echo '<li>' . $this->Html->link($action['title'], $action['url'], $action['options']) . '</li>';
                    }
                }
                ?>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php if(isset($description)) {
                echo $description;
            }?>
            <br/>
            <?= $this->Form->create($form['model'], $form['options']) ?>
            <?php
            $datepickers = array();
            foreach ($inputs as $input) {
                $inputType = $input['type'] ?? 'string';

                if (isset($input['select'])) { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                               for="<?= $input['model'] ?>"><?= $input['name'] ?>
                            <?= isset($input['required']) ? ' <span class="required">*</span>' : '' ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?= $this->Form->select($input['model'], $input['items'], $input['options']) ?>
                        </div>
                    </div>
                <?php } else if (isset($input['datetime']) || $inputType == 'datetime') { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                               for="<?= $input['model'] ?>"><?= $input['name'] ?>
                            <?= isset($input['required']) ? ' <span class="required">*</span>' : '' ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php $input['options']['id'] = $input['model'] ?>
                            <?= $this->Form->text($input['model'], $input['options']) ?>
                        </div>
                    </div>
                    <?php
                    array_push($datepickers, $input['model']);
                } else if ($inputType == 'string') { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                               for="<?= $input['model'] ?>"><?= $input['name'] ?>
                            <?= isset($input['required']) ? ' <span class="required">*</span>' : '' ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?= $this->Form->text($input['model'], $input['options']) ?>
                        </div>
                    </div>
                <?php } else if($inputType == 'boolean') { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                               for="<?= $input['model'] ?>"><?= $input['name'] ?>
                            <?= isset($input['required']) ? ' <span class="required">*</span>' : '' ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?= $this->Form->checkbox($input['model'], $input['options']) ?>
                        </div>
                    </div>
                <?php } else if($inputType == 'integer') { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                               for="<?= $input['model'] ?>"><?= $input['name'] ?>
                            <?= isset($input['required']) ? ' <span class="required">*</span>' : '' ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?= $this->Form->number($input['model'], $input['options']) ?>
                        </div>
                    </div>
                <?php } else if($inputType == 'select') { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                               for="<?= $input['model'] ?>"><?= $input['name'] ?>
                            <?= isset($input['required']) ? ' <span class="required">*</span>' : '' ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?= $this->Form->select($input['model'], $input['select_options'], $input['options']) ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                               for="<?= $input['model'] ?>"><?= $input['name'] ?>
                            <?= isset($input['required']) ? ' <span class="required">*</span>' : '' ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?= $this->Form->input($input['model'], $input['options']) ?>
                        </div>
                    </div>
                <?php }
            } ?>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <?= $this->Form->button($submit['title'], $submit['options']) ?>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>

<?php if (count($datepickers) > 0) {
    $this->start('custom_js');
    ?>
    <script> $(document).ready(function () {
            <?php foreach ($datepickers as $picker) {?>
            $('#<?= $picker ?>').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1",
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YY HH:mm'
                }
            });
            $(<?= $picker ?>).on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
            <?php } ?>
        });</script>
    <?php
    $this->end();
}

