<?php
/** @var \App\View\AppView $this */
/** @var \Cake\ORM\Table $model */
/** @var \Cake\ORM\Entity $item */
/** @var array $types */
$schema = $model->schema();

$title = h("Edit " . \Cake\Utility\Inflector::singularize(\Admin\Helper\Generation::toTitleCase($schema->name())));
$fields = \Admin\Helper\Generation::prepareInputs($this, $model, $schema, isset($types) ? $types : []);

echo $this->element('Admin.Partials/crud_edit', [
    'title' => $title,
    'actions' => [
        ['title' => '<i class="fa fa-home"></i>', 'url' => ['action' => 'index'], 'options' => ['escape' => false]]
    ],
    'form' => ['model' => $item, 'options' => ['class' => 'form-horizontal form-label-left']],
    'inputs' => $fields,
    'submit' => ['title' => 'Submit', 'options' => ['class' => 'btn btn-success']]
]);
