<?php
/** @var \App\View\AppView $this */
/** @var \Cake\ORM\Table $model */
/** @var \Cake\ORM\Entity[] $items */
$schema = $model->schema();

$formatted = \Admin\Helper\Generation::formatItems($this, $schema, $items);
$headings = \Admin\Helper\Generation::prepareHeadings($this, $schema);


$title =h("All " . \Admin\Helper\Generation::toTitleCase($schema->name()));

echo $this->element('Admin.Partials/crud_index', [
    'title' => $title,
    'actions' => [
        ['title' => '<i class="fa fa-plus"></i>', 'url' => ['action' => 'add', 'plugin' => 'Seo'], 'options' => ['escape' => false]]
    ],
    'headings' => $headings,
    'item_actions' => [
        ['title' => 'View', 'action' => 'view', 'plugin' => 'Seo', 'options' => []],
        ['title' => 'Edit', 'action' => 'edit', 'plugin' => 'Seo', 'options' => []],
        ['title' => 'Delete', 'action' => 'delete', 'plugin' => 'Seo', 'options' => ['confirm' => __('Are you sure you want to delete?')],
            'post' => true],
    ],
    'threads' => $formatted,
    'paginate' => true
]);
