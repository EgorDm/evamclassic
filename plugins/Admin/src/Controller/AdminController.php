<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 6/10/2016
 * Time: 8:50 PM
 */

namespace Admin\Controller;


use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;

class AdminController extends Controller
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Items',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'authenticate' => [
                'Basic' => [
                    'userModel' => 'Users'
                ],
                'Form' => [
                    'userModel' => 'Users'
                ]
            ],
            'storage' => [
                'className' => 'Session',
                'key' => 'Auth.User',
            ],
        ]);
        $this->Auth->config('authError', "You are not allowed to visit this page.");

        $builder = $this->viewBuilder();
        $builder->helpers([
            'AdminPanel',
        ]);
        $builder->layout('Admin.admin_main');
    }

    public function isAuthorized($user, $admin = true) {
        if(isset($user['type'])) {
            if($admin === true) {
                if($user['type'] == 1) {
                    return true;
                }
            } elseif($user['type'] >= 0) {
                return true;
            }
        }
        return false;
    }

    public function checkAuth($admin = true) {
        //return true;
        if(!$this->isAuthorized($this->Auth->user(), $admin)) {
            $this->Flash->error(__('You are not allowed to visit this page.'));
            $this->redirect(['controller' => 'Users', 'action' => 'login']);
            return false;
        }
        return true;
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        if($this->request->params['action'] != 'login') {
            $this->Auth->allow($this->allowedPages());
        }
        $user = $this->Auth->user();
        if($user == null) {
            return;
        }
        if($user['type'] == 1) {
            $this->set('isAdmin', true);
        }

        $this->set('fullname', $user['fullname']);

        if(in_array($this->request->params['action'], ['view', 'index'])) {
            $this->request->session()->write('content_referrer', Router::reverse(Router::getRequest(),true));
        }
    }

    public function redirectBack($default_url) {
        if($this->request->session()->check('content_referrer')) {
            $default_url = $this->request->session()->read('content_referrer');
            $this->request->session()->delete('content_referrer');
        }
        return $this->redirect($default_url);
    }

    /**
     * Getting array of pages which are free to visit for everyone.
     * @return array
     */
    protected function allowedPages() {
        return [];
    }
}
