<?php


namespace Admin\Controller;


use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Network\Response;
use Cake\ORM\Table;

abstract class AdminCrudController extends AdminController
{
    /**
     * @return Table
     */
    protected abstract function getModel();

    protected function searchParams() {
        $keyword = $this->request->data['search'];
        $schema = $this->getModel()->schema();

        $model = str_replace('_', '', ucwords($schema->name(), '_'));

        $fields = [];
        foreach ($schema->columns() as $col) {
            if($col != 'id' && !in_array($schema->columnType($col), ['text', 'string'])) continue;

            if ($col == 'id') {
                $fields["{$model}.{$col}"] = $keyword;
            } else {
                $fields["{$model}.{$col} LIKE"] = "%$keyword%";
            }
        }

         $this->paginate = [
             'conditions' => [
                 'OR' => $fields
             ],
             'limit' => 30
         ];
    }

    public function beforeFilter(Event $event)
    {
        $this->set('model', $this->getModel());
        return parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return Response|null
     */
    public function index()
    {
        if($this->request->is('post')) $this->searchParams();
        $items = $this->paginate($this->getModel());

        $this->set(compact('items'));
        $this->set('_serialize', ['items']);
    }

    /**
     * View method
     *
     * @param string|null $id Customer id.
     * @return void
     */
    public function view($id = null)
    {
        $item = $this->getModel()->get($id);

        $this->set('item', $item);
        $this->set('_serialize', ['item']);
    }

    /**
     * Add method
     *
     * @return Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $item = $this->getModel()->newEntity();
        if ($this->request->is('post'))  $this->addPost($item);

        $this->set(compact('item'));
        $this->set('_serialize', ['item']);
    }

    protected function addPost(&$item)
    {
        $item = $this->getModel()->patchEntity($item, $this->request->data);
        if ($this->getModel()->save($item)) {
            $this->Flash->success(__('The item has been saved.'));
            $plugin = $this->request->params['plugin'] ?? null;
            return $this->redirect(['action' => 'index', 'plugin' => $plugin]);
        } else {
            $this->Flash->error(__('The item could not be saved. Please, try again.'));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Customer id.
     * @return Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $item = $this->getModel()->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) $this->editPost($item);

        $this->set(compact('item'));
        $this->set('_serialize', ['item']);
    }

    protected function editPost(&$item)
    {
        $item = $this->getModel()->patchEntity($item, $this->request->data);
        if ($this->getModel()->save($item)) {
            $this->Flash->success(__('The item has been saved.'));
            $plugin = $this->request->params['plugin'] ?? null;
            return $this->redirect(['action' => 'index', 'plugin' => $plugin]);
        } else {
            $this->Flash->error(__('The item could not be saved. Please, try again.'));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Customer id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $item = $this->getModel()->get($id);

        if ($this->getModel()->delete($item)) {
            $this->Flash->success(__('The item has been deleted.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index', 'plugin' => $plugin]);
    }
}
