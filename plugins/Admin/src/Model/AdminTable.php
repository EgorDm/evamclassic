<?php


namespace Admin\Model;


use Cake\ORM\Table;

class AdminTable extends Table
{
    private $fieldOptionsCache = null;

    protected function _getFieldsOptions()
    {
        return [];
    }

    public function getFieldsOptions()
    {
        if(is_null($this->fieldOptionsCache)) {
            $this->fieldOptionsCache = $this->_getFieldsOptions();
        }
        return $this->fieldOptionsCache;
    }

    public function getFieldOptions($field)
    {
        return $this->getFieldsOptions()[$field] ?? null;
    }

    public function getReadonlyFields()
    {
        return ['created', 'modified'];
    }
}
