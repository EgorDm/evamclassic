<?php


namespace Admin\Helper;


use Admin\Model\AdminTable;
use Cake\Database\Schema\Table;
use Cake\ORM\Entity;
use Cake\View\View;

class Generation
{
    public static function toTitleCase($s)
    {
        return str_replace('_', ' ', ucwords($s, '_'));
    }

    /**
     * @param View $view
     * @param Table $schema
     * @param Entity[] $items
     * @return array
     */
    public static function formatItems($view, $schema, $items)
    {
        $ret = [];
        foreach ($items as $item) {
            $fields = [];
            foreach ($schema->columns() as $col) {
                $type = $schema->columnType($col);
                if(in_array($type, ['integer'])) {
                    $fields[] = $view->Number->format($item->get($col));
                } else {
                    $fields[] = h($item->get($col));
                }
            }

            array_push($ret, [
                'id' => $item->id,
                'fields' => $fields
            ]);
        }

        return $ret;
    }

    /**
     * @param View $view
     * @param Table $schema
     * @return array
     */
    public static function prepareHeadings($view, $schema)
    {
        $ret = [];
        foreach ($schema->columns() as $col) {
            $type = $schema->columnType($col);

            $width = 200;
            if(in_array($type, ['integer'])) $width = 100;

            $model = $col;
            $name = h(self::toTitleCase($col));

            $ret[] = compact('model', 'name', 'width');
        }
        return $ret;
    }

    /**
     * @param View $view
     * @param AdminTable $table
     * @param Table $schema
     * @return array
     */
    public static function prepareInputs($view, $table, $schema, $typeHints = [])
    {
        $defaults = $schema->defaultValues();

        $hidden = $table->newEntity()->hiddenProperties();
        $readonlyFields = $table->getReadonlyFields();

        $ret = [];
        foreach ($schema->columns() as $col) {
            if(in_array($col, ['id'])) continue;
            if(in_array($col, $hidden)) continue;
            if(in_array($col, $readonlyFields)) continue;
            $colType = $schema->columnType($col);

            $model = $col;
            $name = h(self::toTitleCase($col));
            $required = !$schema->isNullable($col) && !isset($defaults[$col]);

            $options = ['class' => 'form-control col-md-7 col-xs-12', 'label' => false];
            if($required) $options['required'] = 'required';

            $type = 'string';
            $select_options = null;
            if ($table->getFieldOptions($col) != null) {
                $type = 'select';
                $select_options = $table->getFieldOptions($col);
            } else if(in_array($colType, ['datetime'])) {
                $type = 'datetime';
            } else if(in_array($colType, ['boolean', 'bool'])) {
                $type = 'boolean';
            } else if(in_array($colType, ['integer'])) {
                $type = 'integer';
            } else {
                $type = 'string';
            }

            if(isset($typeHints[$model])) $type = $typeHints[$model];

            $ret[] = compact('model', 'name', 'required', 'options', 'type', 'select_options');
        }
        return $ret;
    }

    /**
     * @param array $a
     * @return array
     */
    public static function extractUrlParams($a)
    {
        $urlKeyMask = ['controller' => true, 'action' => true, 'plugin' => true, 'prefix' => true];
        return array_intersect_key($a, $urlKeyMask);
    }
}
