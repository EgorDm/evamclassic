<?php

use Cake\View\View;
use Orders\Helper\Price;
use Payment\Helper\Methods;

/**  @var View $this */
/** @var \App\Model\Entity\Order $order */

$total = \Orders\Helper\Total::calculate($order);
?>

<table class="table data-table order-items-table">
    <tr>
        <th>Part number</th>
        <th>Title</th>
        <th>Quantity</th>
        <th>Price</th>
    </tr>
    <?php foreach ($order->getItems() as $item): ?>
        <tr>
            <td><?= $item->code ?></td>
            <td><?= $item->title ?></td>
            <td><?= $item->quantity ?></td>
            <td><?= Price::format($item->getPrice($item->quantity)) ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<div class="receipt-wrapper">
    <table class="receipt data-table">
        <tr>
            <td class="receipt-vat">
                <table class="data-table data-table-num">
                    <?php foreach ($total->getVatSubtotals() as $percentage => $vatTotal): ?>
                        <tr>
                            <td><?= $percentage * 100 ?> % Vat of <?= Price::format($vatTotal) ?></td>
                            <td><?= Price::format($vatTotal * $percentage) ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr class="hidden-compact">
                        <td class="divider" colspan="2"></td>
                    </tr>
                    <tr>
                        <td>Total price VAT</td>
                        <td><?= Price::format($total->getVat()) ?></td>
                    </tr>
                </table>
            </td>
            <?= isset($compact) ? '</tr><tr>' : '' ?>
            <td class="receipt-sum">
                <table class="data-table data-table-num">
                    <tr>
                        <td>Shipping cost</td>
                        <td class="price" colspan="2"><?=  Price::format($total->getShipping()) ?></td>
                    </tr>
                    <?php if (isset($order->payment_method)): ?>
                        <tr>
                            <td>Payment method (<?= Methods::format($order->getPaymentMethod(), $order->shipping_address->inEu())?>)</td>
                            <td class="price" colspan="2"><?= Price::format($total->getComission()) ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr class="receipt-total">
                        <td>Total price</td>
                        <td class="price" colspan="2"><?= Price::format($total->getFinal()) ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $this->element('Customer.address', ['address' => $order->shipping_address, 'title' => 'Delivery address']) ?>
    </div>
    <div class="col-md-6">
        <?= $this->element('Customer.address', ['address' => $order->shipping_address, 'title' => 'Billing address']) ?>
    </div>
    <div class="col-md-6">
        <div class="dashboard-block">
            <div class="block-title">
                <h3>Shipping method</h3>
            </div>
            <div class="block-content">
                <ul><li><?= $order->shipping_method->title ?></li></ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="dashboard-block">
            <div class="block-title">
                <h3>Payment method</h3>
            </div>
            <div class="block-content">
                <ul><li><?= $order->getPaymentMethod() ? $order->getPaymentMethod()->getTitle() : '' ?></li></ul>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="dashboard-block">
            <div class="block-title">
                <h3>Email</h3>
            </div>
            <div class="block-content">
                <ul><li><?= $order->getCustomer()->email ?></li></ul>
            </div>
        </div>
    </div>
</div>
