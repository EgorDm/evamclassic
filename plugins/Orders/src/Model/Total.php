<?php

namespace Orders\Model;

class Total
{
    /** @var float */
    protected $subtotal;

    /** @var float */
    protected $shipping;

    /** @var float */
    protected $vat;

    /** @var array */
    protected $vatSubtotals;

    /** @var float */
    protected $comission;

    /** @var float */
    protected $final;

    /**
     * Total constructor.
     * @param float $subtotal
     * @param float $shipping
     * @param float $vat
     * @param array $vatSubtotals
     * @param float $comission
     * @param float $final
     */
    public function __construct(float $subtotal, float $shipping, float $vat, array $vatSubtotals, float $comission, float $final)
    {
        $this->subtotal = $subtotal;
        $this->shipping = $shipping;
        $this->vat = $vat;
        $this->vatSubtotals = $vatSubtotals;
        $this->comission = $comission;
        $this->final = $final;
    }

    /**
     * @return float
     */
    public function getSubtotal(): float
    {
        return $this->subtotal;
    }

    /**
     * @return float
     */
    public function getShipping(): float
    {
        return $this->shipping;
    }

    /**
     * @return float
     */
    public function getVat(): float
    {
        return $this->vat;
    }

    /**
     * @return float
     */
    public function getComission(): float
    {
        return $this->comission;
    }

    /**
     * @return float
     */
    public function getFinal(): float
    {
        return $this->final;
    }

    /**
     * @return array
     */
    public function getVatSubtotals(): array
    {
        return $this->vatSubtotals;
    }
}
