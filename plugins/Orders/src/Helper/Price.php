<?php
namespace Orders\Helper;


class Price
{
    /**
     * @param float $value
     * @param int $places
     * @param bool $showCurrency
     * @return string
     */
    public static function format($value, $places = 2, $showCurrency = true)
    {
        if ($places < 0) {
            $places = 0;
        }
        $mult = pow(10, $places);
        $ret = number_format(ceil($value * $mult) / $mult, 2);
        if($showCurrency) {
            $ret = '€ ' . $ret;
        }

        return $ret;
    }

    /**
     * @param float $value
     * @param int $places
     * @return float
     */
    public static function round($value, $places = 2)
    {
        if ($places < 0) {
            $places = 0;
        }
        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }
}
