<?php
namespace Orders\Helper;

use App\Model\Entity\Order;
use App\Utils\DataUtils;
use App\Utils\PaymentMethods;
use Cake\Log\Log;

class Total
{

    /**
     * @param $order
     * @return \Orders\Model\Total
     */
    public static function calculate($order)
    {
        $inEU = $order->shipping_address->inEu();
        $vat = Vat::calculate($order);
        $shipping = Vat::getShippingPrice($order);

        $totalVat = 0;
        $totalPrice = 0;
        foreach ($vat as $percentage => $vatTotal) {
            $totalVat += $vatTotal * $percentage;
            $totalPrice += $vatTotal;
        }

        $commission = Price::round(PaymentMethods::calcCommission($totalPrice + $totalVat, $order->payment_method, $inEU));
        $finalPrice = Price::round($totalVat + $totalPrice + $commission);
        Log::debug("Calculated Order #$order->id (final price: $finalPrice, subtotal: $totalPrice, vat: $totalVat, comisssion: $commission)");
        return new \Orders\Model\Total($totalPrice, $shipping, $totalVat, $vat, $commission, $finalPrice);
    }
}
