<?php
namespace Orders\Helper;

use App\Utils\CacheUtils;
use Core\Helper\Preference;

class Vat
{
    /**
     * Vat [percentage => total]
     * @param \App\Model\Entity\Order $order
     * @return array
     */
    public static function calculate($order)
    {
        $inEU = $order->shipping_address->inEu();
        $vatPrices = array();

        foreach ($order->getItems() as $item) {
            $vat_percentage = $inEU ? strval($item->vat_code_model->percentage) : '0';
            $vatPrices[$vat_percentage] = $vatPrices[$vat_percentage] ?? 0;
            $vatPrices[$vat_percentage] += $item->getPrice($item->quantity);
        }

        if (!empty($order->shipping_option->price)) {
            $vat_percentage = $inEU ? '0.21' : '0.21';
            $vatPrices[$vat_percentage] = $vatPrices[$vat_percentage] ?? 0;
            $vatPrices[$vat_percentage] += self::getShippingPrice($order);
        }

        return $vatPrices;
    }

    public static function getShippingPrice($order)
    {
        return $order->shipping_option->price / 1.21;
    }
}
