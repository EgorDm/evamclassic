<?php


namespace Payment\Model\Methods;



use App\Model\Entity\Customer;
use App\Model\Entity\Order;
use Payment\Model\Methods\PaymentMethodInterface;

class PaymentMethod implements PaymentMethodInterface
{
    protected $identifier;

    protected $title;

    /**
     * AbstractPaymentMethod constructor.
     * @param $identifier
     * @param $title
     */
    public function __construct($identifier, $title)
    {
        $this->identifier = $identifier;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param float $total
     * @param bool $in_eu
     * @return float
     */
    public function calculateComission($total, $in_eu)
    {
        return 0;
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function checkValid($order)
    {
        return true;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function canUse($customer)
    {
        return true;
    }

    public function getExtraHtml($order) {
        return '';
    }
}
