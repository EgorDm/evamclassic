<?php


namespace Payment\Model\Methods;


use Payment\Model\Methods\PaymentMethod;

class Mollie extends PaymentMethod
{
    protected $mollieIdentifier;

    public function __construct($identifier, $title, $mollieIdentifier)
    {
        $this->mollieIdentifier = $mollieIdentifier;
        parent::__construct($identifier, $title);
    }


    /**
     * @return mixed
     */
    public function getMollieIdentifier()
    {
        return $this->mollieIdentifier;
    }
}
