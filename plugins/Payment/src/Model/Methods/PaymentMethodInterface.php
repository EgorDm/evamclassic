<?php


namespace Payment\Model\Methods;

use App\Model\Entity\Customer;
use App\Model\Entity\Order;

interface PaymentMethodInterface
{
    /**
     * @return string
     */
    public function getIdentifier();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param float $total
     * @param bool $in_eu
     * @return float
     */
    public function calculateComission($total, $in_eu);

    /**
     * @param Order $order
     * @return bool
     */
    public function checkValid($order);

    /**
     * @param Customer $customer
     * @return bool
     */
    public function canUse($customer);

    /**
     * @param Order $order
     * @return string
     */
    public function getExtraHtml($order);

}
