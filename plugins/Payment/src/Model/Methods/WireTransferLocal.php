<?php

namespace Payment\Model\Methods;

class WireTransferLocal extends PaymentMethod
{
    public function canUse($customer)
    {
        $id = is_array($customer) ? $customer['id'] : $customer->id;
        return $id == 2;
    }

}
