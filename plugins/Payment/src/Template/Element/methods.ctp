<?php

use Cake\View\View;
use Payment\Helper\Methods;

/** @var View $this */
/** @var \Cake\Controller\Component\AuthComponent $Auth */
/** @var int $selected */

$icons = [
    Methods::PAYPAL => 'paypal',
    Methods::CREDIT_CARD => 'master-card',
    Methods::BANK_TRANSFER => 'bank-transfer',
    Methods::IDEAL => 'ideal',
    Methods::REQUEST_INVOICE => 'invoice',
    Methods::BANK_TRANSFER_LOCAL => 'transfer',
];

/** @var \App\Model\Entity\Order $order */
/** @var \App\Model\Entity\Customer $customer */
$customer = $Auth->user();
 ?>

<ul class="clearfix row">
    <?php foreach (Methods::getMethods() as $method): ?>
        <?php if(!$method->canUse($customer)) continue; ?>
        <li class="col-sm-6 col-md-4">
            <label class="payment-method">
                    <span class="title">
                      <input type="radio" name="payment_method" value="<?= $method->getIdentifier() ?>"
                          <?= ($order->payment_method == $method->getIdentifier()) ? 'checked' : '' ?>>
                      <span class="name"><?= $method->getTitle() ?></span>
                        <span><?= $method->getExtraHtml($order) ?></span>
                    </span>
                <span class="image text-center">
                      <span class="icon icon-<?= $icons[$method->getIdentifier()] ?>"></span>
                    </span>
            </label>
        </li>
    <?php endforeach; ?>
</ul>
