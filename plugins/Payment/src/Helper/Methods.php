<?php

namespace Payment\Helper;

use App\Utils\PaymentMethods;
use Payment\Model\Methods\Mollie;
use Payment\Model\Methods\PaymentMethod;
use Payment\Model\Methods\PaymentMethodInterface;
use Payment\Model\Methods\WireTransferLocal;
use PaymentInvoiceRequest\Model\Methods\InvoiceRequest;

class Methods
{
    const
        PAYPAL = 0,
        CREDIT_CARD = 1,
        BANK_TRANSFER = 2,
        IDEAL = 3,
        BANK_TRANSFER_LOCAL = 4,
        REQUEST_INVOICE = 5
    ;

    /**
     * @var PaymentMethodInterface[]
     */
    protected static $methods = null;

    /**
     * @return PaymentMethodInterface[]
     */
    public static function getMethods()
    {
        if(!self::$methods) {
            self::$methods = [
                self::REQUEST_INVOICE => new InvoiceRequest(self::REQUEST_INVOICE, 'Black Friday'),
                self::PAYPAL => new Mollie(self::PAYPAL, 'PayPal', \Mollie_API_Object_Method::PAYPAL),
                self::CREDIT_CARD => new Mollie(self::CREDIT_CARD, 'Credit Card', \Mollie_API_Object_Method::CREDITCARD),
                self::BANK_TRANSFER => new Mollie(self::BANK_TRANSFER, 'Bank Transfer', \Mollie_API_Object_Method::BANKTRANSFER),
                self::IDEAL => new Mollie(self::IDEAL, 'iDeal', \Mollie_API_Object_Method::IDEAL),
                self::BANK_TRANSFER_LOCAL => new WireTransferLocal(self::BANK_TRANSFER_LOCAL, 'Bank Transfer'),
            ];
        }

        return self::$methods;
    }

    /**
     * @param int $method
     * @return PaymentMethodInterface
     */
    public static function getMethod($method)
    {
        return self::getMethods()[$method] ?? null;
    }

    public static function format($method, $inEU)
    {
        if(!($method instanceof PaymentMethodInterface)) $method = self::getMethod($method);
        return $method->getTitle() . PaymentMethods::getSum($method->getIdentifier(), $inEU);
    }
}
