<?php
namespace Seo\Controller\Admin;

use Admin\Controller\AdminController;
use Admin\Controller\AdminCrudController;
use Cake\ORM\Table;
use Seo\Controller\AppController;

/**
 * UrlRewrites Controller
 *
 * @property \Seo\Model\Table\UrlRewritesTable $UrlRewrites
 */
class UrlRewritesController extends AdminCrudController
{
    /**
     * @inheritDoc
     */
    protected function getModel()
    {
        return $this->UrlRewrites;
    }
}
