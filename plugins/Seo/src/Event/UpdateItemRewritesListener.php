<?php


namespace Seo\Event;


use App\Model\Entity\Item;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Seo\Model\Entity\UrlRewrite;
use Seo\Model\Table\UrlRewritesTable;

class UpdateItemRewritesListener implements EventListenerInterface
{
    /**
     * @inheritDoc
     */
    public function implementedEvents()
    {
       return [
           'Model.afterSave' => 'afterSave',
           'Model.afterDelete' => 'afterDelete',
           'Import.Excel.afterImport' => 'afterImport',
       ];
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function afterSave($event)
    {
        /** @var Item $entity */
        $entity = $event->data()['entity'];
        /** @var UrlRewritesTable $table */
        $table = TableRegistry::get('Seo.UrlRewrites');

        try {
            $table->connection()->transactional(function ($conn) use ($entity, $table) {
                $table->deleteForEntity($entity);
                $table->createForEntity($entity);
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage() . $e->getTraceAsString());
        }
    }

    /**
     * @param Event $event
     */
    public function afterDelete($event)
    {
        /** @var Item $entity */
        $entity = $event->data()['entity'];
        /** @var UrlRewritesTable $table */
        $table = TableRegistry::get('Seo.UrlRewrites');
        $table->deleteForEntity($entity);
    }
}
