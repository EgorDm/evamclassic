<?php


namespace Seo\Event;


use App\Model\Entity\Category;
use App\Model\Entity\Item;
use App\Model\Entity\Page;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Seo\Model\Entity\UrlRewrite;
use Seo\Model\Table\UrlRewritesTable;

class UpdatePageRewritesListener implements EventListenerInterface
{
    /**
     * @inheritDoc
     */
    public function implementedEvents()
    {
       return [
           'Model.afterSave' => 'afterSave',
           'Model.afterDelete' => 'afterDelete',
       ];
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function afterSave($event)
    {
        /** @var Page $entity */
        $entity = $event->data()['entity'];
        /** @var UrlRewritesTable $table */
        $table = TableRegistry::get('Seo.UrlRewrites');

        try {
            $table->connection()->transactional(function ($conn) use ($entity, $table) {
                $table->deleteForEntity($entity);
                $table->createForEntity($entity);
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage() . $e->getTraceAsString());
        }
    }

    /**
     * @param Event $event
     */
    public function afterDelete($event)
    {
        /** @var Category $entity */
        $entity = $event->data()['entity'];
        /** @var UrlRewritesTable $table */
        $table = TableRegistry::get('Seo.UrlRewrites');
        $table->deleteForEntity($entity);
    }
}
