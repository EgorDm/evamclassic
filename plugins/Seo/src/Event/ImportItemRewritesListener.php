<?php


namespace Seo\Event;


use App\Model\Table\ItemsTable;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Seo\Model\Entity\UrlRewrite;
use Seo\Model\Table\UrlRewritesTable;

class ImportItemRewritesListener implements EventListenerInterface
{
    /**
     * @inheritDoc
     */
    public function implementedEvents()
    {
        return [
            'Import.Excel.afterImport' => 'afterImport',
        ];
    }

    public function afterImport($event)
    {
        /** @var UrlRewritesTable $table */
        $table = TableRegistry::get('Seo.UrlRewrites');
        /** @var ItemsTable $itemsTable */
        $itemsTable = TableRegistry::get('Items');

        try {
            $table->connection()->transactional(function ($conn) use ($table, $itemsTable) {
                $table->deleteAll([
                    'entity_type' => UrlRewrite::ENTITY_TYPE_PRODUCT,
                    'generated' => 1,
                ]);

                $items = $itemsTable->find()->all();
                foreach ($items as $entity) {
                    $table->createForEntity($entity, UrlRewrite::ENTITY_TYPE_PRODUCT);
                }
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage() . $e->getTraceAsString());
        }
    }
}
