<?php

namespace Seo\Filter;

use Cake\Network\Request;
use Cake\ORM\TableRegistry;
use Cake\Routing\DispatcherFilter;
use Seo\Model\Entity\UrlRewrite;

/**
 * Class RewriteFilter
 * @package Seo\Filter
 */
class RewriteFilter extends DispatcherFilter
{
    protected $_priority = 9;

    public function beforeDispatch(\Cake\Event\Event $event)
    {
        /** @var Request $request */
        $request = $event->data['request'];
        $url = $request->url;

        if(starts_with($url, ['img/', 'js/', 'favicon.ico'])) return;

        /** @var \Seo\Model\Table\UrlRewritesTable $rewriteTable */
        $rewriteTable = TableRegistry::get('Seo.UrlRewrites');

        /** @var UrlRewrite $rewrite */
        $rewrite = $rewriteTable->find()->where(['request_path' => rtrim($url, '/')])->first();
        if(is_null($rewrite)) return;

        if($rewrite->redirect_type == UrlRewrite::REDIRECT_TYPE_REWRITE) {
            $request->url = $rewrite->target_path;
        } else if($rewrite->redirect_type == UrlRewrite::REDIRECT_TYPE_REDIRECT) {
            $response = $event->data['response'];
            $response->statusCode(302);
            $response->location(\Cake\Routing\Router::url($rewrite->target_path, true));
            return $response;
        }
    }
}
