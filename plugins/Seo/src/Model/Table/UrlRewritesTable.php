<?php
namespace Seo\Model\Table;

use Admin\Model\AdminTable;
use App\Model\Entity\Category;
use App\Model\Entity\Item;
use App\Model\Entity\Page;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use Seo\Model\Entity\UrlRewrite;

/**
 * UrlRewrites Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Entities
 *
 * @method \Seo\Model\Entity\UrlRewrite get($primaryKey, $options = [])
 * @method \Seo\Model\Entity\UrlRewrite newEntity($data = null, array $options = [])
 * @method \Seo\Model\Entity\UrlRewrite[] newEntities(array $data, array $options = [])
 * @method \Seo\Model\Entity\UrlRewrite|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Seo\Model\Entity\UrlRewrite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Seo\Model\Entity\UrlRewrite[] patchEntities($entities, array $data, array $options = [])
 * @method \Seo\Model\Entity\UrlRewrite findOrCreate($search, callable $callback = null, $options = [])
 */
class UrlRewritesTable extends AdminTable
{
    const PREFIXES = [
        UrlRewrite::ENTITY_TYPE_PAGE  => [''],
        UrlRewrite::ENTITY_TYPE_PRODUCT => ['', 'product/'],
        UrlRewrite::ENTITY_TYPE_CATEGORY => ['', 'category/'],
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('url_rewrites');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * @param Item|Page|Category $entity
     * @return string|null
     */
    protected function getEntityType($entity)
    {
        if ($entity instanceof Page) return UrlRewrite::ENTITY_TYPE_PAGE;
        if ($entity instanceof Item) return UrlRewrite::ENTITY_TYPE_PRODUCT;
        if ($entity instanceof Category) return UrlRewrite::ENTITY_TYPE_CATEGORY;
        return null;
    }

    public function deleteForEntity($entity, $type = null)
    {
        if(is_null($type)) $type = $this->getEntityType($entity);

        $this->deleteAll([
            'entity_type' => $type,
            'entity_id' => $entity->id,
            'generated' => 1,
        ]);
    }

    /**
     * @param Item|Page|Category $entity
     * @param $type
     * @return string|void
     */
    protected function getUrlForType($entity, $type)
    {
        switch ($type) {
            case UrlRewrite::ENTITY_TYPE_PAGE:
                return Router::url(['controller' => 'Main', 'action' => 'page', 'prefix' => false, $entity->slug, 'plugin' => null]);
            case UrlRewrite::ENTITY_TYPE_PRODUCT:
                return Router::url(['controller' => 'Products', 'action' => 'view', 'prefix' => false, $entity->alias, 'plugin' => null]);
            case UrlRewrite::ENTITY_TYPE_CATEGORY:
                return Router::url(['controller' => 'Products', 'action' => 'category', 'prefix' => false, $entity->alias, 'plugin' => null]);
        }
    }

    /**
     * @param Item|Page|Category $entity
     * @param null $type
     */
    public function createForEntity($entity, $type = null)
    {
        if(!empty(trim($entity->urls))) {
            if(is_null($type)) $type = $this->getEntityType($entity);
            $prefixes = self::PREFIXES[$type];

            $lines = explode("\n", trim($entity->urls));
            foreach ($prefixes as $prefix) {
                foreach ($lines as $line) {
                    $rewrite = new UrlRewrite([
                        'entity_type' => $type,
                        'entity_id' => $entity->id,
                        'target_path' => $this->getUrlForType($entity, $type),
                        'request_path' => $prefix . Text::slug($line),
                        'redirect_type' => UrlRewrite::REDIRECT_TYPE_REWRITE,
                        'generated' => 1,
                    ]);
                    $this->save($rewrite);
                }
            }
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('entity_type', 'create')
            ->notEmpty('entity_type');

        $validator
            ->integer('entity_id')
            ->requirePresence('entity_id', 'create')
            ->notEmpty('entity_id');

        $validator
            ->requirePresence('request_path', 'create')
            ->notEmpty('request_path');

        $validator
            ->requirePresence('target_path', 'create')
            ->notEmpty('target_path');

        $validator
            ->integer('redirect_type')
            ->requirePresence('redirect_type', 'create')
            ->notEmpty('redirect_type');

        $validator
            ->boolean('generated')
            ->requirePresence('generated', 'create')
            ->notEmpty('generated');

        $validator
            ->allowEmpty('metadata');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }


    protected function _getFieldsOptions()
    {
        return [
            'entity_type' => [
                'product' => 'product',
                'category' => 'category',
                'page' => 'page',
            ],
            'redirect_type' => [
                UrlRewrite::REDIRECT_TYPE_REWRITE => 'Rewrite Page',
                UrlRewrite::REDIRECT_TYPE_REDIRECT => 'Redirect 301',
            ]
        ];
    }


}
