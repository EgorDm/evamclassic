<?php
namespace Seo\Model\Entity;

use Cake\ORM\Entity;

/**
 * UrlRewrite Entity
 *
 * @property int $id
 * @property string $entity_type
 * @property int $entity_id
 * @property string $request_path
 * @property string $target_path
 * @property int $redirect_type
 * @property bool $generated
 * @property string $metadata
 *
 * @property \Seo\Model\Entity\Entity $entity
 */
class UrlRewrite extends Entity
{
    const ENTITY_TYPE_PRODUCT = 'product';
    const ENTITY_TYPE_CATEGORY = 'category';
    const ENTITY_TYPE_PAGE = 'page';

    const REDIRECT_TYPE_REWRITE = 0;
    const REDIRECT_TYPE_REDIRECT = 1;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
