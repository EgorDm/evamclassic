<?= $this->element('Admin.Partials/navigation_group', [
    'group' => ['icon' => 'fa-search', 'title' => 'Seo'],
    'data_array' => [
        ['title' => 'Url Rewrites', 'controller' => 'UrlRewrites', 'action' => 'index', 'plugin' => 'Seo'],
    ]
]);
