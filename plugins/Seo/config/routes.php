<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'Seo',
    ['path' => '/seo'],
    function (RouteBuilder $routes) {
        $routes->prefix('Admin', function ($routes) {
            $routes->fallbacks(DashedRoute::class);
        });
    }
);
