<?php

use Cake\Routing\DispatcherFactory;

DispatcherFactory::add(new \Seo\Filter\RewriteFilter());

\Cake\ORM\TableRegistry::get('Items')->eventManager()
    ->on(new \Seo\Event\UpdateItemRewritesListener());
\Cake\ORM\TableRegistry::get('Categories')->eventManager()
    ->on(new \Seo\Event\UpdateCategoryRewritesListener());
\Cake\ORM\TableRegistry::get('Pages')->eventManager()
    ->on(new \Seo\Event\UpdatePageRewritesListener());
\Cake\Event\EventManager::instance()->on(new \Seo\Event\ImportItemRewritesListener());
