<?php
use Migrations\AbstractMigration;

class UrlRewrites extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('url_rewrites')
            ->addColumn('entity_type', 'string', ['limit' => 60, 'null' => false])
            ->addColumn('entity_id', 'integer', ['null' => false])
            ->addColumn('request_path', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('target_path', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('redirect_type', 'integer', ['limit' => 2, 'null' => false, 'default' => 0])
            ->addColumn('generated', 'boolean', ['null' => false, 'default' => false])
            ->addColumn('metadata', 'text', ['null' => true])
            ->save();
    }
}
