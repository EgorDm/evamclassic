<?php
namespace Seo\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Seo\Model\Table\UrlRewritesTable;

/**
 * Seo\Model\Table\UrlRewritesTable Test Case
 */
class UrlRewritesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Seo\Model\Table\UrlRewritesTable
     */
    public $UrlRewrites;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.seo.url_rewrites',
        'plugin.seo.entities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UrlRewrites') ? [] : ['className' => 'Seo\Model\Table\UrlRewritesTable'];
        $this->UrlRewrites = TableRegistry::get('UrlRewrites', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UrlRewrites);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
