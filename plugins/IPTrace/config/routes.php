<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'IPTrace',
    ['path' => '/i-p-trace'],
    function (RouteBuilder $routes) {
        $routes->fallbacks('DashedRoute');
    }
);
