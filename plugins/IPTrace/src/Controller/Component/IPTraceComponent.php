<?php
namespace IPTrace\Controller\Component;

use Cake\Controller\Component;
use Exception;
use IPTrace\Lib\IPTraceLib;

class IPTraceComponent extends Component
{
    /**
     * Find
     *
     * @param $ip
     * @param array $query
     * @return mixed Array of location data or null if no location found.
     * @access public
     */
    public function get($ip, $query = array())
    {
        $obj = new IPTraceLib(dirname(dirname(dirname(__FILE__))) . DS . 'data' . DS . 'IP2LOCATION.BIN', IPTraceLib::FILE_IO);
        try {
            $records = $obj->lookup($ip, IPTraceLib::COUNTRY);
        } catch (Exception $e) {
            return null;
        }
        return $records;
    }
}