<?php
namespace Exact\Test\TestCase\Shell\Task;

use Cake\TestSuite\TestCase;
use Exact\Shell\Task\SyncPricesTask;

/**
 * Exact\Shell\Task\SyncPricesTask Test Case
 */
class SyncPricesTaskTest extends TestCase
{

    /**
     * ConsoleIo mock
     *
     * @var \Cake\Console\ConsoleIo|\PHPUnit_Framework_MockObject_MockObject
     */
    public $io;

    /**
     * Test subject
     *
     * @var \Exact\Shell\Task\SyncPricesTask
     */
    public $SyncPrices;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')->getMock();

        $this->SyncPrices = $this->getMockBuilder('Exact\Shell\Task\SyncPricesTask')
            ->setConstructorArgs([$this->io])
            ->getMock();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SyncPrices);

        parent::tearDown();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
