<?php
namespace Exact\Test\TestCase\Shell\Task;

use Cake\TestSuite\TestCase;
use Exact\Shell\Task\SyncProductsTask;

/**
 * Exact\Shell\Task\SyncProductsTask Test Case
 */
class SyncProductsTaskTest extends TestCase
{

    /**
     * ConsoleIo mock
     *
     * @var \Cake\Console\ConsoleIo|\PHPUnit_Framework_MockObject_MockObject
     */
    public $io;

    /**
     * Test subject
     *
     * @var \Exact\Shell\Task\SyncProductsTask
     */
    public $SyncProducts;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')->getMock();

        $this->SyncProducts = $this->getMockBuilder('Exact\Shell\Task\SyncProductsTask')
            ->setConstructorArgs([$this->io])
            ->getMock();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SyncProducts);

        parent::tearDown();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
