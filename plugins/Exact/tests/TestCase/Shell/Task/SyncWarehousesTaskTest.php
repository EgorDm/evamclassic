<?php
namespace Exact\Test\TestCase\Shell\Task;

use Cake\TestSuite\TestCase;
use Exact\Shell\Task\SyncWarehousesTask;

/**
 * Exact\Shell\Task\SyncWarehousesTask Test Case
 */
class SyncWarehousesTaskTest extends TestCase
{

    /**
     * ConsoleIo mock
     *
     * @var \Cake\Console\ConsoleIo|\PHPUnit_Framework_MockObject_MockObject
     */
    public $io;

    /**
     * Test subject
     *
     * @var \Exact\Shell\Task\SyncWarehousesTask
     */
    public $SyncWarehouses;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')->getMock();

        $this->SyncWarehouses = $this->getMockBuilder('Exact\Shell\Task\SyncWarehousesTask')
            ->setConstructorArgs([$this->io])
            ->getMock();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SyncWarehouses);

        parent::tearDown();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
