<?php


namespace Exact\Helpers;



use App\Model\Table\ExactConnectedTable;
use App\Utils\DataUtils;
use Cake\Cache\Cache;
use Cake\Datasource\ResultSetInterface;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

class SyncHelper
{
    /** @var Client  */
    protected $client = null;

    /**
     * SyncHelper constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
        $this->init();
    }


    public function init()
    {
        $this->client->connect();
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param $exactClass
     * @param $cakeModel
     * @param $upstream
     * @param bool $force
     * @param null|array $foreign_keys
     * Array goes as following: name_cake_key => [CakeTable, KeyFromExact, delete if no association]
     * @return bool
     */
    public function syncModel($exactClass, $cakeModel, $upstream, $force = false, $foreign_keys = null)
    {
        Log::debug("Syncing Model: " . $exactClass );
        set_time_limit(0);

        /** @var ExactConnectedTable $table */
        $table = TableRegistry::get($cakeModel);

        $filter = '';
        $readfrom = explode('\\', $exactClass);
        if (!$force) {
            $date_time = Cache::read('exact_' . $readfrom[count($readfrom) - 1] . '_sync_date', '_exact_credentials_');
            Log::debug($cakeModel . ': Datetime ' . $date_time);
            $filter = (($date_time != false) ? 'Modified ge DateTime\'' . $date_time->format('Y-m-d\Th:i:s') . '\'' : '');
        }
        //Log::debug("syncModel ".$readfrom[count($readfrom)-1]." FILTER $filter");

        $resource = $this->client->getResource($exactClass);
        $exact_result = DataUtils::indexExactArray(
            $resource->filter($filter, '', implode(",", $table->getExactFields())),
            'ID');
        Cache::write('exact_' . $readfrom[count($readfrom) - 1] . '_sync_date', Time::now(), '_exact_credentials_');
        Log::debug('Updating ' . count($exact_result) . ' items');

        // Database part
        $ret = true;
        $in_update_array = array();
        $in_delete_array = array();
        if (!empty($upstream)) {
            $out_update_array = array();
            $out_insert_array = array();
        }
        /** @var ResultSetInterface $local_result */
        $local_result = $table->find('all');
        foreach ($local_result as $local_data) {
            if ($local_data->get('exact_guid') == null) {
                if (!empty($upstream)) array_push($out_insert_array, $local_data);
                continue;
            }
            if (empty($exact_result[$local_data->get('exact_guid')])) {
                array_push($in_delete_array, $local_data->get('id'));
                continue;
            }

            $resource = $exact_result[$local_data->get('exact_guid')];
            $exact_modified = DataUtils::ExactUnixConvertFrom($resource['Modified']) * ($force);
            //Log::debug("Syncing $cake_class: ($force) and ".$exact_modified > $local_data->get('modified')->format('U'));
            if ($force || $exact_modified > $local_data->get('modified')->format('U')) {
                $resource['id'] = $local_data->get('id');
                array_push($in_update_array, $resource);
            } else if ($exact_modified < $local_data->get('modified')->format('U')) {
                if (!empty($upstream)) array_push($out_update_array, $local_data);
            }
            unset($exact_result[$local_data->get('exact_guid')]);
        }
        $in_insert_array = array();
        if ((count($exact_result) > 0 || count($in_update_array) > 0)) {
            $foreign_data_list = array();
            if ($foreign_keys != null && count($foreign_keys) > 0) {
                foreach ($foreign_keys as $key => $key_data) {
                    $foreign_table = TableRegistry::get($key_data[0]);
                    $foreign_models = DataUtils::indexEntityArray(
                        $foreign_table->find()->select(['id', 'exact_guid'])->all(), 'exact_guid');
                    $foreign_data_list[$key] = [$foreign_models, $key_data[1], $key_data[2]];
                }
            }

            foreach ($exact_result as $item) {
                $skip = false;
                foreach ($foreign_data_list as $key => $key_data) {
                    if (isset($item[$key_data[1]]) && isset($key_data[0][$item[$key_data[1]]])) {
                        $item[$key] = $key_data[0][$item[$key_data[1]]]['id'];
                    } else if ($key_data[2] == true) {
                        $skip = true;
                    }
                }
                if ($skip == true) continue;
                array_push($in_insert_array, $table->ConvertFromExact($item));
            }
            for ($i = 0; $i < count($in_update_array); $i++) {
                $skip = false;
                foreach ($foreign_data_list as $key => $key_data) {
                    if (isset($in_update_array[$i][$key_data[1]])) {
                        $in_update_array[$i][$key] = $key_data[0][$in_update_array[$i][$key_data[1]]]['id'];
                    } else if ($key_data[2] == true) {
                        $skip = true;
                    }
                }
                if ($skip == true) continue;
                $in_update_array[$i] = $table->ConvertFromExact($in_update_array[$i]);
            }
        }

        if (!DataUtils::InsertMultipleInto($table, $table->getInsertColumns(), $in_insert_array)) {
            $ret = true;
        }


        if (!DataUtils::UpdateMultipleInto($table, $table->getUpdateColumns(), $in_update_array)) {
            $ret = true;
        }

        if ($force == true && !empty($in_delete_array)) {
            $table->deleteAll(['id IN' => $in_delete_array]);
        }

        return $ret;
    }
}
