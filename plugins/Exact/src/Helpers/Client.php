<?php


namespace Exact\Helpers;


use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Log\Log;
use Cake\Routing\Router;
use Exact\Models\Credentials;
use Picqer\Financials\Exact\Connection;
use Picqer\Financials\Exact\Model;
use Picqer\Financials\Exact\Query\Findable;

class Client
{
    /** @var Connection  */
    public $connection = null;

    /** @var Credentials */
    protected $credentials;

    /**
     * SyncHelper constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     *
     */
    public function init()
    {
        Configure::load('credentials', 'default');
        $this->credentials = Credentials::load();
    }

    /**
     * @param string $className
     * @return Findable|Model
     */
    public function getResource($className)
    {
        return new $className($this->connection);
    }

    /**
     * @return bool
     */
    public function tryConnect()
    {
        try {
            if (empty($this->connection)) {
                if(!$this->credentials->getAuthorizationcode()) {
                    $this->authorize();
                }
                $this->connect();
                Log::debug("Connection :" . json_encode($this->connection));
            }
            return $this->connected();
        } catch (\Exception $e) {
            Log::critical($e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function reset()
    {
        $this->connection = null;
        $this->credentials = new Credentials();
        $this->credentials->save();
    }

    public function connect()
    {
        $this->connection = $this->buildConnection();
        $this->credentials->apply($this->connection);

        // Make the client connect and exchange tokens
        try {
            $this->connection->connect();
        } catch (\Exception $e) {
            $this->authorize();
            throw new Exception('Could not connect to Exact: ' . $e->getMessage());
        }

        // Save the new tokens for next connections
        $this->credentials->extract($this->connection)->save();
        return $this->connected();
    }

    public function connected()
    {
        return !empty($this->connection);
    }

    public function checkUpdateCredentials()
    {
        $request = Router::getRequest(true);

        if ($request->query('code') && !$this->credentials->getAuthorizationcode()) {
            $this->credentials->setAuthorizationcode($request->query('code'))->save();
        }
    }

    /**
     * Authorize user because there is no info at all!
     */
    private function authorize()
    {
        if(!$this->checkAuth()) return;
        Credentials::delete();
        $connection = $this->buildConnection();
        $connection->redirectForAuthorization();
    }

    /**
     * @return \Picqer\Financials\Exact\Connection
     */
    protected function buildConnection() {
        $connection = new Connection();
        $config = Configure::read('ExactCredentials');
        $connection->setDivision($config['DIVISION']);
        $connection->setRedirectUrl($config['REDIRECT_BASE']);
        $connection->setExactClientId($config['CLIENT_ID']);
        $connection->setExactClientSecret($config['CLIENT_SECRET']);
        return $connection;
    }

    /**
     * @param bool $admin
     * @return bool
     */
    protected function checkAuth($admin = true) {
        if(!Router::getRequest(true)) return false;
        return $this->isAuthorized(Router::getRequest(true)->session()->read('Auth.User'), $admin);
    }

    /**
     * @param $user
     * @param bool $admin
     * @return bool
     */
    protected function isAuthorized($user, $admin = true) {
        if(isset($user['type'])) {
            if($admin === true) {
                if($user['type'] == 1) {
                    return true;
                }
            } elseif($user['type'] >= 0) {
                return true;
            }
        }
        return false;
    }
}
