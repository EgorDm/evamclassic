<?php
namespace Exact\Shell;

use Cake\Console\ConsoleOutput;
use Cake\Console\Shell;
use Cake\Log\Log;
use Exact\Shell\Task\SyncPricesTask;
use Exact\Shell\Task\SyncProductsTask;
use Exact\Shell\Task\SyncWarehousesTask;

/**
 * Synchronize shell command.
 * @property SyncProductsTask SyncProducts
 * @property SyncPricesTask SyncPrices
 * @property SyncWarehousesTask SyncWarehouses
 */
class SynchronizeShell extends Shell
{
    public $tasks = array('Exact.SyncProducts', 'Exact.SyncPrices', 'Exact.SyncWarehouses');
    const TASK_TYPE_NAME = 'task';

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        return parent::getOptionParser()
            ->addArgument(self::TASK_TYPE_NAME, [
                'default' => array('products', 'prices', 'warehouses', 'all'),
                'required' => true,
                'choices' => array('products', 'prices', 'warehouses', 'all')]);
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $tasks = $this->args[0];
        if($tasks == 'all') $tasks = array('products', 'prices', 'warehouses');
        if(!is_array($tasks)) $tasks = [$tasks];

        foreach ($tasks as $task) {
            Log::info('Syncing exact type: ' . $task);
            $this->runTask($task);
        }

    }

    protected function runTask($task)
    {
        switch ($task) {
            case 'products':
                $this->SyncProducts->main();
                break;
            case 'prices':
                $this->SyncPrices->main();
                break;
            case 'warehouses':
                $this->SyncWarehouses->main();
                break;
            default:
                break;
        }
    }
}
