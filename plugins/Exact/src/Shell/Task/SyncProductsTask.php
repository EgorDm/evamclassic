<?php
namespace Exact\Shell\Task;

use App\Controller\Component\ExactSyncComponent;
use Cake\Console\Shell;
use Exact\Helpers\Client;
use Exact\Helpers\SyncHelper;
use Items;

/**
 * SyncProducts shell task.
 * @property ExactSyncComponent ExactSync
 */
class SyncProductsTask extends Shell
{
    /** @var SyncHelper  */
    protected $sync = null;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Items');

        $this->sync = new SyncHelper();
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->sync->syncModel(\Picqer\Financials\Exact\ItemGroup::class, 'ItemGroups', false, false);

        $this->sync->syncModel(\Picqer\Financials\Exact\Item::class, 'ItemGroups', false, false,
           ['item_group_id' => ['ItemGroups', 'ItemGroup', false]]);
    }
}
