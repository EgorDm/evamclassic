<?php
namespace Exact\Shell\Task;

use App\Controller\Component\ExactSyncComponent;
use App\Exact\ItemWarehouses;
use App\Model\Table\ItemsTable;
use App\Utils\DataUtils;
use Cake\Cache\Cache;
use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Exact\Helpers\SyncHelper;

/**
 * SyncWarehouses shell task.
 * @property ExactSyncComponent ExactSync
 */
class SyncWarehousesTask extends Shell
{
    /** @var SyncHelper  */
    protected $sync = null;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Items');

        $this->sync = new SyncHelper();
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $warehouse = env('DEFAULT_WAREHOUSE', null);
        if ($warehouse) {
            /** @var ItemsTable $itemsTable */
            $itemsTable = TableRegistry::get('Items');
            $itemsTable->updateAll(['warehouse_guid' => $warehouse], ['warehouse_guid !=' => $warehouse]);
            return;
        }

        $resource = $this->sync->getClient()->getResource(ItemWarehouses::class);

        $force = false;
        $filter = '';
        $key = 'warehouse';
        if (!$force) {
            $date_time = Cache::read('exact_' . $key . '_sync_date', '_exact_credentials_');
            Log::debug($key . ': Datetime ' . $date_time);
            $filter = (($date_time != false) ? 'Modified ge DateTime\'' . $date_time->format('Y-m-d\Th:i:s') . '\'' : '');
        }
        $result = $resource->filter($filter, '', 'ID,Item,Warehouse');
        Cache::write('exact_' . $key . '_sync_date', Time::now(), '_exact_credentials_');

        Log::debug('Updating data for ' . count($result) . ' warehouses');

        $itemsTable = TableRegistry::get('Items');
        $items = DataUtils::indexEntityArray($itemsTable->find()->select(['id', 'exact_guid'])->all(), 'exact_guid');
        $insert_array = array();

        foreach ($result as $ware) {
            if (!empty($items[$ware->Item])) {
                $insert_array[$items[$ware->Item]['id']] = [
                    'id' => $items[$ware->Item]['id'],
                    'warehouse_guid' => $ware->Warehouse
                ];
            }
        }

        DataUtils::UpdateMultipleInto($itemsTable, ['id', 'warehouse_guid'], array_values($insert_array));
    }
}
