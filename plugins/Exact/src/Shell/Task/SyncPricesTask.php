<?php
namespace Exact\Shell\Task;

use Cake\Console\Shell;
use Exact\Helpers\SyncHelper;

/**
 * SyncPrices shell task.
 */
class SyncPricesTask extends Shell
{
    /** @var SyncHelper  */
    protected $sync = null;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Items');

        $this->sync = new SyncHelper();
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->sync->syncModel(\App\Exact\SalesItemPrices::class, 'ItemPrices', false, false,
            ['item_id' => ['Items', 'Item', true]]);
    }
}
