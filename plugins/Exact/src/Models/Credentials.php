<?php


namespace Exact\Models;


use Cake\Cache\Cache;

class Credentials
{
    protected $authorizationcode = null;
    protected $accesstoken = null;
    protected $refreshtoken = null;
    protected $expires_in = -1;

    /**
     * Credentials constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->authorizationcode = $data['authorizationcode'] ?? $this->authorizationcode;
        $this->accesstoken = $data['accesstoken'] ?? $this->accesstoken;
        $this->refreshtoken = $data['refreshtoken'] ?? $this->refreshtoken;
        $this->expires_in = $data['expires_in'] ?? $this->expires_in;
    }

    public function getData()
    {
        return [
            'authorizationcode' => $this->authorizationcode,
            'accesstoken' => $this->accesstoken,
            'refreshtoken' => $this->refreshtoken,
            'expires_in' => $this->expires_in,
        ];
    }


    /**
     * @return null
     */
    public function getAuthorizationcode()
    {
        return $this->authorizationcode;
    }

    /**
     * @param null $authorizationcode
     * @return Credentials
     */
    public function setAuthorizationcode($authorizationcode)
    {
        $this->authorizationcode = $authorizationcode;
        return $this;
    }

    /**
     * @return null
     */
    public function getAccesstoken()
    {
        return $this->accesstoken;
    }

    /**
     * @param null $accesstoken
     * @return Credentials
     */
    public function setAccesstoken($accesstoken)
    {
        $this->accesstoken = $accesstoken;
        return $this;
    }

    /**
     * @return null
     */
    public function getRefreshtoken()
    {
        return $this->refreshtoken;
    }

    /**
     * @param null $refreshtoken
     * @return Credentials
     */
    public function setRefreshtoken($refreshtoken)
    {
        $this->refreshtoken = $refreshtoken;
        return $this;
    }

    /**
     * @return int
     */
    public function getExpiresIn()
    {
        return $this->expires_in;
    }

    /**
     * @param int $expires_in
     * @return Credentials
     */
    public function setExpiresIn($expires_in)
    {
        $this->expires_in = $expires_in;
        return $this;
    }

    /**
     * @param \Picqer\Financials\Exact\Connection $connection
     */
    public function apply($connection)
    {
        $connection->setAuthorizationCode($this->getAuthorizationcode());
        $connection->setAccessToken($this->getAccesstoken());
        $connection->setRefreshToken($this->getRefreshtoken());
        $connection->setTokenExpires($this->getExpiresIn());
    }

    /**
     * @param \Picqer\Financials\Exact\Connection $connection
     * @return Credentials
     */
    public function extract($connection)
    {
        $this->setAccesstoken($connection->getAccessToken());
        $this->setRefreshtoken($connection->getRefreshToken());
        $this->setExpiresIn($connection->getTokenExpires());
        return $this;
    }

    /**
     *
     */
    public function save()
    {
        Cache::write('exact_credentials', $this->getData(), '_exact_credentials_');
    }

    public static function delete()
    {
        Cache::delete('exact_credentials', '_exact_credentials_');
    }

    /**
     * @return Credentials
     */
    public static function load()
    {
        return new self(Cache::read('exact_credentials', '_exact_credentials_') ?? []);
    }
}
