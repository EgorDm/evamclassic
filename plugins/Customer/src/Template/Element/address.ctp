<?php
use Customer\Helper\Countries;

/** @var string $title */
/** @var \App\Model\Entity\Address $address */
?>

<div class="dashboard-block">
    <div class="block-title">
        <h3><?= $title ?></h3>
    </div>
    <div class="block-content">
        <ul>
            <li><?= h($address->name) ?></li>
            <li> <?= h($address->street) ?></li>
            <li><?= h($address->postcode) . ' ' . h($address->city) ?></li>
            <li><?= h(Countries::$countries[$address->country]) ?></li>
            <li>Phone <?= h($address->phone) ?></li>
        </ul>
    </div>
</div>
