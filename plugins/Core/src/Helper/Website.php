<?php


namespace Core\Helper;

class Website
{
    const EVAM = 'evam';
    const VWSP = 'vwsp';

    const DEFAULT_CONFIG = [
        'site_regex' => [
            self::EVAM => '/.*(evamclassic).*/',
            self::VWSP => '/.*(vwsplitparts).*/',
        ]
    ];

    public static function config() {
        return self::DEFAULT_CONFIG;
    }

    public static function identifier() {
        static $website = null;

        if(!$website) {
            $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            $website = self::EVAM;
            foreach (self::config()['site_regex'] as $site => $pattern) {
                if(preg_match($pattern, $url)) $website = $site;
            }
        }

        return $website;
    }

    public static function resource($path) {
        $site = self::identifier();
        return "site/$site/$path";
    }
}
