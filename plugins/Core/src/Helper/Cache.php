<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 08-Aug-16
 * Time: 17:56
 */

namespace Core\Helper;


use Cake\ORM\TableRegistry;

class Cache
{
    public static function readOrExecuteAll($key, $query, $config = null) {

        //$response = Cache::read($key, $config);
        //if($response == false) {
            $response = $query->all()->toArray();
            //Cache::write($key, $response, $config);
        //}
        return $response;
    }
}
