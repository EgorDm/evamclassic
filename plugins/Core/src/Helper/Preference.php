<?php


namespace Core\Helper;


use Cake\ORM\TableRegistry;

class Preference
{
    /** @var \App\Model\Entity\Preference[] */
    protected static $preferences;

    public static function load()
    {
        self::$preferences = [];
        $result = TableRegistry::get('Preferences')->find()->all();
        /** @var \App\Model\Entity\Preference $item */
        foreach ($result as $item) {
            self::$preferences[$item->alias] = $item;
        }
    }

    /**
     * @param $name
     * @return \App\Model\Entity\Preference|null
     */
    public static function get($name)
    {
        if(!self::$preferences) self::load();
        return self::$preferences[$name] ?? null;
    }

    /**
     * @param $name
     * @return string
     */
    public static function text($name)
    {
        return self::get($name)->text_val;
    }

    /**
     * @param $name
     * @return int
     */
    public static function int($name)
    {
        return self::get($name)->int_val;
    }
}
