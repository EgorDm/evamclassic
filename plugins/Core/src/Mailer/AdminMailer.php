<?php

namespace Core\Mailer;

use Cake\Core\Configure;

/**
 * Trait AdminMailer
 * @package Core\Mailer
 * @method sendTemplated($to, $template, $variables, $from = null, $replyto = null)
 */
trait AdminMailer
{
    protected function sendTemplatedAdmin($template, $variables, $from = null, $replyto = null)
    {
        $to = Configure::read('Core.Mail.admin_email');
        $this->sendTemplated($to, $template, $variables, $from, $replyto);
    }
}
