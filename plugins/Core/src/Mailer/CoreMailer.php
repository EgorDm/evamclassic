<?php

namespace Core\Mailer;

use Cake\Log\Log;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\View\View;

class CoreMailer extends Mailer
{
    /**
     * @param $slug
     * @return \App\Model\Entity\Text|null
     */
    private function getTemplate($slug)
    {
        $ret = TableRegistry::get('Texts')->find()->where(['slug' => $slug])->first();
        return empty($text) ? $ret : null;
    }

    /**
     * @param $text
     * @param $variables
     * @return string
     */
    private function substituteVariables($text, $variables) {
        $keys = array_map(function ($s) { return "%$s%";}, array_keys($variables));
        return str_replace($keys, array_values($variables), $text);
    }

    /**
     * @param $to
     * @param $template
     * @param $variables
     * @param $from
     * @param null $replyto
     */
    protected function sendTemplated($to, $template, $variables, $from = null, $replyto = null)
    {
        Log::debug('Sending mail: ' . json_encode($to));
        $template = $this->getTemplate($template);

        if(!$template) {
            Log::critical('Cannot find template: ' . $template);
            return;
        }

        $subject = $this->substituteVariables($template->title, $variables);
        $message = $this->substituteVariables($template->content, $variables);
        $this
            ->template('main', 'default')
            ->emailFormat('html')
            ->subject($subject)
            ->to($to)
            ->from($from)
            ->replyTo($replyto)
            ->viewVars(['message' => $message]);
    }

    /**
     * @param string $name
     * @param array $variables
     * @return string|null
     */
    protected function renderView($name, $variables)
    {
        $view = new View(Router::getRequest(true), null,null);
        $view->set($variables);
        return $view->render($name, 'ajax');
    }
}
