<?php
return [
    'Core' => [
        'Mail' => [
            'admin_email' => ['info@evamclassic.com', 'info@vwsplitparts.com', 'parts@evamclassic.com', 'sales@evamclassic.com'],
        ]
    ]
];
