<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'PaymentInvoiceRequest',
    ['path' => '/invoice-request'],
    function (RouteBuilder $routes) {
        $routes->fallbacks('DashedRoute');
    }
);
