<?php

use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class AddInvoiceTexts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = TableRegistry::get('Texts');
        /** @var \App\Model\Entity\Text $text */
        $text = $table->newEntity();
        $text->title = 'New invoice request';
        $text->slug = 'admin_invoicerequest';
        $text->page_id = 8;
        $text->content = "<p>You have a new invoice request from %name%.</p><p>%details%</p><p>Kind regards,<br />Evam Classic</p>";
        $table->save($text);

        /** @var \App\Model\Entity\Text $text */
        $text = $table->newEntity();
        $text->title = 'Your invoice request';
        $text->slug = 'customer_invoicerequest';
        $text->page_id = 5;
        $text->content = "<p>Hello %name%.</p><p>We have recieved your invoice request and will contact you shortly.</p><p>%details%</p><p>Kind regards,<br />Evam Classic</p>";
        $table->save($text);
    }
}
