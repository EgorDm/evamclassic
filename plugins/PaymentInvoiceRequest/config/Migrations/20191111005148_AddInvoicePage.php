<?php

use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class AddInvoicePage extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $pages = TableRegistry::get('Pages');
        /** @var \App\Model\Entity\Page $page */
        $page = $pages->newEntity();
        $page->title = "Invoice Request Texts";
        $page->slug = 'invoice-request-texts';
        $page->editable = 0;
        $pages->save($page);


        $texts = TableRegistry::get('Texts');
        /** @var \App\Model\Entity\Text $text */
        $text = $texts->newEntity();
        $text->page_id = $page->id;
        $text->title = "Invoice Request Succeeded";
        $text->slug = 'invoice-request-success';
        $text->content = 'We have recieved your invoice request. We will contact you as soon as possible.';
        $texts->save($text);

        $text = $texts->newEntity();
        $text->page_id = $page->id;
        $text->title = "Black friday policy";
        $text->slug = 'black-friday';
        $text->content = 'Placeholder text for a black friday policy.';
        $texts->save($text);

    }
}
