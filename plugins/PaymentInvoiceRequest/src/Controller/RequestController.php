<?php


namespace PaymentInvoiceRequest\Controller;


use App\Controller\AppController;
use App\Model\Entity\Text;
use Cake\ORM\TableRegistry;

class RequestController extends AppController
{
    public function index() {
        /** @var Text $text */
        $text = TableRegistry::get('Texts')->find()->where(['slug' => 'invoice-request-success'])->first();
        $this->set('page', $text);
    }
}
