<?php


namespace PaymentInvoiceRequest\Model\Methods;


use Cake\Chronos\Chronos;
use Payment\Model\Methods\PaymentMethod;

class InvoiceRequest extends PaymentMethod
{
    public function canUse($customer)
    {
        $dateFrom = Chronos::createFromDate(2019, 11, 27);
        $dateTo = Chronos::createFromDate(2019, 12, 2);
        $now = Chronos::now();

        //return $now > $dateFrom && $now < $dateTo;
        return false;
    }

    public function getExtraHtml($order)
    {
        return "<a class='btn-success btn-sm' style='cursor: pointer; background-color: #0f8837; font-weight: 700;' onclick='openblackfriday()'>View Policy</a>";
    }
}
