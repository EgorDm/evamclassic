<?php
/** @var \App\Model\Entity\Page $page */
?>

<div class="heading">
    <h3><?= $page->title ?></h3>
</div>

<?= $page->content ?>
