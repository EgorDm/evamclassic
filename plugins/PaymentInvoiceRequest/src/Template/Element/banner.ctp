<?php
/** @var \Cake\View\View $this */
?>

<?php $this->start('mid_banner') ?>
<div class="black-friday" onclick="openblackfriday()">
    <h1>Black Friday <span class="highlight">Sale</span></h1>
    <h4><span class="highlight">29 + 30 November 2019</span> (Click for policy details)</h4>
</div>
<style>
    .black-friday {
        background-color: #1c1c1c;
        padding: 14px;
        cursor: pointer;
    }

    .black-friday h1 {
        font-weight: 700;
    }

    .black-friday h4 {
        margin-bottom: 0;
        font-weight: 600;
    }

    .black-friday .highlight {
        color: #f47a20;
    }
</style>
<?php $this->end(); ?>
