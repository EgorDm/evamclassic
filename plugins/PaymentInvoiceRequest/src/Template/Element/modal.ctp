<?php
/** @var \Cake\View\View $this */
/** @var Text $text */

use App\Model\Entity\Text;
use Cake\ORM\TableRegistry;

$text = TableRegistry::get('Texts')->find()->where(['slug' => 'black-friday'])->first();
?>

<?= $this->element('UI.modal', [
    'identifier' => 'blackfriday',
    'title' => $text->title,
    'content' => $text->content,
]) ?>

<div>
    <div class="block text-center" style="padding-bottom: 16px"><?= $this->Html->image('PaymentInvoiceRequest.banner_small.jpg') ?></div>
    <a class='btn-lg btn-success text-center'
       style="font-size: 26px; background-color: #0f8837; font-weight: 700; cursor: pointer; display: block"
       onclick='openblackfriday()'>
        <span>Black Friday Policy</span>
    </a>
</div>
