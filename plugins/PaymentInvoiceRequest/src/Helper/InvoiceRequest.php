<?php


namespace PaymentInvoiceRequest\Helper;


use App\Model\Entity\Order;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;

class InvoiceRequest
{
    use MailerAwareTrait;

    /**
     * @param Controller $controller
     * @param Order $order
     * @return \Cake\Network\Response|null
     * @throws \Exception
     */
    public function handle($controller, $order)
    {
        try {
            $this->getMailer('PaymentInvoiceRequest.Invoice')->send('adminInvoice', [$order]);
            $this->getMailer('PaymentInvoiceRequest.Invoice')->send('customerInvoice', [$order]);

            $orders = TableRegistry::get('Orders');
            //$orders->delete($order);

            return $controller->redirect(['action' => 'index', 'controller' => 'Request', 'plugin' => 'PaymentInvoiceRequest']);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
