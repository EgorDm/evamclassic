<?php
namespace PaymentInvoiceRequest\Mailer;

use Cake\Routing\Router;
use Core\Mailer\CoreMailer;

class InvoiceMailer extends CoreMailer
{
    use \Core\Mailer\AdminMailer;

    /**
     * @param \App\Model\Entity\Order $order
     */
    public function adminInvoice($order)
    {
        $detailsView = $this->renderView('Orders.Element/details', ['order' => $order, 'compact' => true]);
        $this->sendTemplatedAdmin('admin_invoicerequest', [
            'name' => $order->getCustomer()->name,
            'details' => $detailsView,
            'customer_mail' => $order->getCustomer()->email
        ]);
    }

    /**
     * @param \App\Model\Entity\Order $order
     */
    public function customerInvoice($order)
    {
        $detailsView = $this->renderView('Orders.Element/details', ['order' => $order, 'compact' => true]);
        $this->sendTemplated($order->getCustomer()->email, 'customer_invoicerequest', [
            'name' => $order->getCustomer()->name,
            'details' => $detailsView
        ]);
    }
}
