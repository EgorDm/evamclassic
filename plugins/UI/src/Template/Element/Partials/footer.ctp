<?php
/** @var \Cake\View\View $this */

use Core\Helper\Website;

?>

<footer class="text-center <?= Website::identifier() == Website::EVAM ? 'text-light' : ''?>">
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="footer-col col-md-4">
                    <h3>Information</h3>
                    <ul class="level-one">
                        <li><?= $this->Html->link('Company', ['action' => 'company', 'controller' => 'Main']) ?></li>
                        <li><?= $this->Html->link('Contact', ['action' => 'contact', 'controller' => 'Main']) ?></li>
                        <li><?= $this->Html->link('Privacy Policy', ['action' => 'privacy', 'controller' => 'Main']) ?></li>
                        <li><?= $this->Html->link('Terms and Conditions', ['action' => 'terms', 'controller' => 'Main']) ?></li>
                    </ul>
                </div>
                <div class="footer-col col-md-4">
                    <h3>You can pay with</h3>
                    <span class="icon icon-ideal"></span>
                    <span class="icon icon-paypal"></span>
                    <span class="icon icon-bank-transfer"></span>
                    <span class="icon icon-master-card"></span>
                    <span class="icon icon-maestro"></span>
                    <span class="icon icon-visa"></span>
                    <span class="icon icon-tikkie"></span>
                    <span class="icon icon-betaalverzoek"></span>
                </div>
                <div class="footer-col col-md-4">
                    <h3>Location</h3>
                    <p>
                        Edisonweg 13-C<br>
                        3442 AC Woerden The Netherlands<br>
                        +31 (0)348-480240<br>
                        +31 (0)6-17351010<br>
                        IBAN - NL37RABO 0140 8947 99<br>
                        BIC/SWIFT - RABO NL 2U<br>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">© 2016 All rights reserved Evam Classic -
                    <a href="http://egordmitriev.net" target="_blank">Webdesign by Egor Dmitriev</a>
                    <div class="col-lg-12">* We cannot accept responsibility or liability for any misinformation *</a>
                        <div class="col-lg-12">* Typesetting errors reserved * </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
