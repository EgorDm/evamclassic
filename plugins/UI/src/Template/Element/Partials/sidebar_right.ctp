<?php
/** @var \Cake\View\View $this */
?>

<?php //= $this->element('PaymentInvoiceRequest.modal') ?>
<?= $this->element('taxtoggle', ['tax' => $tax]) ?>
<?= $this->element('UI.Partials/sidebar/languages') ?>
<?= $this->fetch('sidebar_right') ?>
<?= $this->element('UI.Partials/sidebar/logos') ?>
<?= $this->Html->image('PaymentInvoiceRequest.recycle.jpg', ['class' => 'img-responsive banner-side']) ?>
<?= $this->Html->link(
    $this->Html->image(
        "banner_t2.png",
        ['class' => 'img-responsive banner-side']
    ),
    'https://paulcamper.nl/camper-huren/leiden/vw-t1-uit-1965-49973/',
    ['escape' => false]
) ?>
