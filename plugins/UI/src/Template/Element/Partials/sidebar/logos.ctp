<?php
/** @var \Cake\View\View $this */
?>

<?= $this->Html->link(
    $this->Html->image("like.jpg", ['class' => 'img-responsive banner-side']),
    'https://www.facebook.com/Evam-Classic-VW-splitparts-1758424324390759/',
    ['escape' => false, 'target' => '_blank']
) ?>
<?= $this->Html->link(
    $this->Html->image("instagram.png", ['class' => 'img-responsive banner-side']),
    'https://www.instagram.com/evam_classic/',
    ['escape' => false, 'target' => '_blank']
) ?>
<?php if(\Core\Helper\Website::identifier() != \Core\Helper\Website::VWSP): ?>
    <?= $this->Html->link(
        $this->Html->image(
            \Core\Helper\Website::resource("klassic_fab_banner.jpg"),
            ['class' => 'img-responsive banner-side']
        ),
        'http://www.vwsplitparts.com/',
        ['escape' => false]
    ) ?>
<?php endif; ?>
<?php if(\Core\Helper\Website::identifier() == \Core\Helper\Website::VWSP): ?>
    <?= $this->Html->link(
        $this->Html->image("site/evam/logo.png", ['class' => 'img-responsive banner-side']),
        'https://www.evamclassic.com/',
        ['escape' => false]
    ) ?>
    <?= $this->Html->link(
        $this->Html->image("evamcars.png", ['class' => 'img-responsive banner-side']),
        'https://www.evamclassic.com/cars',
        ['escape' => false]
    ) ?>
<?php endif; ?>

