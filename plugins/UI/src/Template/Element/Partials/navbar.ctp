<?php
/** @var \Cake\View\View $this */
?>

<nav class="navbar navbar-default" id="navbar">
    <div class="container-fluid">
        <ul class="inline-links mobile-menu">
            <li>
                <a data-toggle="collapse" data-target="#navbar-cart-wrapper"
                   class="collapse-toggle nav-icon icon-basket collapsed"></a>
            </li>
            <li>
                <a data-toggle="collapse" data-target="#navbar-account"
                   class="collapse-toggle nav-icon icon-user collapsed"></a>
            </li>

            <button type="button" class="navbar-toggle nav-icon" data-toggle="collapse"
                    data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </ul>

        <div class="nav navbar-nav navbar-right shop-actions">
            <div data-target="navbar-account" id="navbar-account" class="navbar-account collapse">
                <ul class="inline-links small-links">
                    <?php
                    if (!empty($fullname)) {
                        echo '<li>' . $this->Html->link('Hello ' . $fullname, ['action' => 'account', 'controller' => 'Customers']) . '</li>';
                        echo '<li>' . $this->Html->link('Logout', ['action' => 'logout', 'controller' => 'Customers']) . '</li>';
                    } else {
                        echo '<li>' . $this->Html->link('Sign In', ['action' => 'login', 'controller' => 'Customers']) . '</li>';
                        echo '<li>' . $this->Html->link('Register', ['action' => 'register', 'controller' => 'Customers']) . '</li>';
                    }
                    ?>
                </ul>
            </div>

            <div class="shop-actions-secondary">
                <div data-target="navbar-search" id="navbar-search" class="navbar-search">
                    <div class="search-holder">
                        <?php
                        echo $this->Form->create(null, ['url' => ['controller' => 'Products', 'action' => 'search'], 'type' => 'get']);
                        echo $this->Form->button('<i class="icon-search"></i>',
                            ['class' => 'icon', 'type' => 'submit', 'escape' => false]);
                        echo '<input type="text" name="query" class="text-input" placeholder="Search" autocomplete="off" id="webshop-search" maxlength="128">';
                        echo $this->Form->end();
                        ?>
                    </div>
                </div>
                <div data-target="navbar-cart" id="navbar-cart" class="navbar-cart">
                    <?= $this->element('nav_cart', ['cart_items' => $cart_items]) ?>
                </div>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <?php if(\Core\Helper\Website::identifier() == \Core\Helper\Website::EVAM): ?>
                <ul class="nav navbar-nav">
                    <li <?= ((!empty($menu_active) && $menu_active == 1) ? 'class="active"' : '') ?>>
                        <?= $this->Html->link('Home', ['action' => 'index', 'controller' => 'Products'], ['class' => "notranslate"]) ?>
                    </li>
                    <li <?= ((!empty($menu_active) && $menu_active == 2) ? 'active' : '') ?>">
                    <?= $this->Html->link('<b>Parts</b>', ['action' => 'index', 'controller' => 'Products'], ['style' => 'color: #d6610b;', 'escape' => false]) ?>
                    </li>

                    <li <?= ((!empty($menu_active) && $menu_active == 3) ? 'class="active"' : '') ?>><?= $this->Html->link('<b>Cars for sale</b>', ['action' => 'index', 'controller' => 'Cars'], ['escape' => false]) ?></li>
                    <li><?= $this->Html->link('<b>Klassic FAB</b>', 'http://www.vwsplitparts.com/', ['style' => 'color: #01a91e;', 'escape' => false]) ?></li>
                    <!--<li <?= ((!empty($menu_active) && $menu_active == 6) ? 'class="active"' : '') ?>><?= $this->Html->link('Merchandise', ['action' => 'category', 'controller' => 'Products', 'Merchandise']) ?></li>-->
                    <li <?= ((!empty($menu_active) && $menu_active == 4) ? 'class="active"' : '') ?>><?= $this->Html->link('Company', ['action' => 'company', 'controller' => 'Main']) ?></li>
                    <li <?= ((!empty($menu_active) && $menu_active == 5) ? 'class="active"' : '') ?>><?= $this->Html->link('Contact', ['action' => 'contact', 'controller' => 'Main']) ?></li>
                    <li><a href="https://wa.me/31617351010"><b>Whatsapp: +31 6 1735 1010</b></a></ul>
                </ul>
            <?php else: ?>
                <ul class="nav navbar-nav">
                    <li <?= ((!empty($menu_active) && $menu_active == 1) ? 'class="active"' : '') ?>>
                        <?= $this->Html->link('Home', ['action' => 'index', 'controller' => 'Products'], ['class' => "notranslate"]) ?>
                    </li>
                    <li <?= ((!empty($menu_active) && $menu_active == 2) ? 'active' : '') ?>">
                    <?= $this->Html->link('Seats', ['action' => 'category', 'controller' => 'Products', 'new-seats'], ['class' => "notranslate"]) ?>
                    </li>
                    <li <?= ((!empty($menu_active) && $menu_active == 3) ? 'class="active"' : '') ?>><?= $this->Html->link('<b>Cars for sale</b>', ['action' => 'index', 'controller' => 'Cars'], ['style' => 'color: #ff751a;', 'escape' => false]) ?></li>
                    <li><?= $this->Html->link('<b>Evam Classic</b>', 'https://evamclassic.com/', ['style' => 'color: #ff751a;', 'escape' => false]) ?></li>
                    <!--<li <?= ((!empty($menu_active) && $menu_active == 6) ? 'class="active"' : '') ?>><?= $this->Html->link('Merchandise', ['action' => 'category', 'controller' => 'Products', 'Merchandise']) ?></li>-->
                    <li <?= ((!empty($menu_active) && $menu_active == 4) ? 'class="active"' : '') ?>><?= $this->Html->link('Company', ['action' => 'company', 'controller' => 'Main']) ?></li>
                    <li <?= ((!empty($menu_active) && $menu_active == 5) ? 'class="active"' : '') ?>><?= $this->Html->link('Contact', ['action' => 'contact', 'controller' => 'Main']) ?></li>
                    <li><a href="https://wa.me/31617351010">Whatsapp: +31 6 1735 1010</a></ul>
            <?php endif; ?>
        </div>
    </div>
</nav>
