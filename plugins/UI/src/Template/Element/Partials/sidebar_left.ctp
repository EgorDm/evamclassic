<?php
/** @var \Cake\View\View $this */
?>

<?= $this->fetch('sidebar_left') ?>
<?php if (!empty($wide) && empty($hide)) { ?>
    <?= $this->element('UI.Partials/sidebar_right') ?>
<?php } else { ?>
    <?php if (!empty($categories) && empty($hide)) {
        echo $this->element('category_sidebar', ['categories' => $categories]);
    } ?>
<?php } ?>
