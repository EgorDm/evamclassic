<?php
/** @var bool $show */
/** @var string $identifier */
/** @var string $title */
/** @var string $content */
?>

<?php $this->append('modal'); ?>
<div class="modal fade" id="<?= $identifier ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $identifier ?>-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="color: black">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="<?= $identifier ?>-label"><?= $title ?></h4>
            </div>
            <div class="modal-body">
                <?= $content ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Sluiten</button>
            </div>
        </div>
    </div>
</div>
<script>
    function open<?= $identifier ?>() {
        $('#<?= $identifier ?>').modal('show')
    }
</script>
<?php $this->end(); ?>
