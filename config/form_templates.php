<?php
return [
    'inputContainer' => '<div class="control-group"><div class="form-group floating-label-form-group controls">{{content}}' .
        '<p class="text-danger"></p></div></div>',
    'inputContainerError' => '<div class="control-group"><div class="form-group floating-label-form-group controls">{{content}}' .
        '<p class="text-danger">{{error}}</p></div></div>',
    'input' => '<input class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/>',
    'submitContainer' => '{{content}}',
    'checkbox' => '<div class="checkbox-input"><input type="checkbox" name="{{name}}" id="{{name}}" value="{{value}}" {{attrs}}>'
        .'<label for="{{name}}">{{label}}</label></div>',
    'select' => '<div class="control-group"><div class="form-group select-input-group"><label for="{{name}}">{{label}}</label>'.
        '<select name="{{name}}"{{attrs}}>{{content}}</select></div><p class="help-block text-danger"></p></div>',
    'error' => '{{content}}',
    'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
    'radioWrapper' => '<div class="radio">{{label}}</div>',
    'textarea' => '<div class="control-group"><div class="form-group floating-label-form-group controls">'.
        '<label for="{{name}}">{{label}}</label><textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea><p class="text-danger"></p></div></div>',
];