<?php
use Migrations\AbstractMigration;

class ImagesAndGroups extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('images')
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('path', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('file_name', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('thumbnail_mask', 'integer', ['default' => 0])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();
        $this->table('image_groups')
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('description', 'text', ['null' => true, 'default' => null])
            ->addColumn('group_type', 'integer', ['default' => 0])
            ->addColumn('thumbnail', 'integer', ['default' => 0])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();
        $this->table('image_group_associations')
            ->addColumn('image_group_id', 'integer', ['null' => false, 'default' => null])
            ->addColumn('image_id', 'integer', ['null' => false, 'default' => null])
            ->save();
    }
}
