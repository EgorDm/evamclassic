<?php
use Migrations\AbstractMigration;

class Shipping extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('shipping_methods')
            ->addColumn('title', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('description', 'text', ['null' => true])
            ->addColumn('countries', 'string', ['null' => true])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();

        //TODO: changed min_max to max weight
        $this->table('shipping_options')
            ->addColumn('shipping_method_id', 'integer', ['null' => false])
            ->addColumn('min_weight', 'float', ['null' => false])
            ->addColumn('max_weight', 'float', ['null' => true])
            ->addColumn('price', 'float', ['null' => false])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();
    }
}
