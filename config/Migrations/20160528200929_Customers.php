<?php
use Migrations\AbstractMigration;

class Customers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('customers')
            ->addColumn('exact_guid', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('email', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('password', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('type', 'integer', ['limit' => 50, 'default' => 0])
            ->addColumn('active', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('pass_reset', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();

        $this->table('addresses')
            ->addColumn('exact_guid', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('customer_id', 'integer')
            ->addColumn('type', 'integer', ['limit' => 50, 'default' => 0])
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('street', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('city', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('country', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('postcode', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('phone', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();
    }
}
