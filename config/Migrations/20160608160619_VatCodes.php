<?php
use Migrations\AbstractMigration;

class VatCodes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('vat_codes')
            ->addColumn('exact_guid', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('code', 'string', ['null' => false])
            ->addColumn('description', 'string', ['null' => true])
            ->addColumn('percentage', 'float', ['null' => false])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();
    }
}
