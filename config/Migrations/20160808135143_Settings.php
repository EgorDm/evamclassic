<?php
use Migrations\AbstractMigration;

class Settings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('preferences')
            ->addColumn('alias', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('title', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('text_val', 'string', ['null' => true])
            ->addColumn('int_val', 'integer', ['null' => true])
            ->addColumn('float_val', 'float', ['null' => true])
            ->save();
    }
}
