<?php
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class Users extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('users')
            ->addColumn('fullname', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('email', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('username', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('password', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('type', 'integer', ['limit' => 50, 'default' => 0])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();

        $UsersTable = TableRegistry::get('Users');
        $user = $UsersTable->newEntity();
        $user->fullname = 'Egor Dmitriev';
        $user->username = 'egordm';
        $user->email = 'egordmitriev2@gmail.com';
        $user->password = 'dmitriev';
        $user->type = 1;
        $UsersTable->save($user);
    }
}
