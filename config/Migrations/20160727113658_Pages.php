<?php
use Migrations\AbstractMigration;

class Pages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('pages')
            ->addColumn('title', 'string', ['limit' => 255])
            ->addColumn('slug', 'string', ['limit' => 255])
            ->addColumn('content', 'text', ['null' => true, 'default' => null])
            ->addColumn('editable', 'boolean')
            ->save();

        $this->table('texts')
            ->addColumn('title', 'string', ['limit' => 255])
            ->addColumn('slug', 'string', ['limit' => 255])
            ->addColumn('content', 'text', ['null' => true, 'default' => null])
            ->addColumn('edit_desc', 'text', ['null' => true, 'default' => null])
            ->addColumn('page_id', 'integer', ['limit' => 11])
            ->addColumn('image_group_id', 'integer', ['limit' => 11, 'default' => null])
            ->save();
    }
}
