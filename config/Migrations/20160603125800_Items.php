<?php
use Migrations\AbstractMigration;

class Items extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('item_groups')
            ->addColumn('exact_guid', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('code', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('title', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('description', 'text', ['null' => true])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();

        $this->table('item_prices')
            ->addColumn('exact_guid', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('item_id', 'integer', ['null' => true])
            ->addColumn('item_guid', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('default_unit', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('items_per_unit', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('quantity', 'integer', ['null' => false])
            ->addColumn('price', 'float', ['null' => false])
            ->addColumn('start_date', 'datetime', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('end_date', 'datetime', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();

        $this->table('items')
            ->addColumn('exact_guid', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('item_group_id', 'integer', ['null' => true])
            ->addColumn('exact_group_guid', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('warehouse_guid', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('category_id', 'integer', ['null' => true])
            ->addColumn('image_group_id', 'integer', ['null' => true])
            ->addColumn('code', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('alias', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('title', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('description', 'text', ['null' => true])
            ->addColumn('vat_code', 'string', ['null' => true])
            ->addColumn('stock', 'integer', ['null' => true])
            ->addColumn('weight', 'float', ['null' => true])
            ->addColumn('image', 'string', ['null' => true])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();

        $this->table('categories')
            ->addColumn('title', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('description', 'text', ['null' => true])
            ->addColumn('parent_id', 'integer', ['null' => true])
            ->addColumn('alias', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('lft', 'integer', ['default' => null, 'limit' => 10, 'null' => false])
            ->addColumn('rght', 'integer', ['default' => null, 'limit' => 10, 'null' => false])
            ->addColumn('default_cat', 'boolean', ['default' => false])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();

        $this->table('item_category_associations')
            ->addColumn('item_id', 'integer', ['null' => false, 'default' => null])
            ->addColumn('category_id', 'integer', ['null' => false, 'default' => null])
            ->save();
    }
}
