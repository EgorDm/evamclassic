<?php

use Migrations\AbstractMigration;

class CreateProductLocaleTagFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('items')
            ->addColumn('tags_english', 'text', ['null' => true])
            ->addColumn('tags_german', 'text', ['null' => true])
            ->update();
    }
}
