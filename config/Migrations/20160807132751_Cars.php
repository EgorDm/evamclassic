<?php
use Migrations\AbstractMigration;

class Cars extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('cars')
            ->addColumn('reference', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('alias', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('title', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('description', 'text', ['null' => true])
            ->addColumn('category', 'string', ['null' => true])
            ->addColumn('model', 'string', ['null' => true])
            ->addColumn('year', 'integer', ['null' => true])
            ->addColumn('color', 'string', ['null' => true])
            ->addColumn('condition', 'string', ['null' => true])
            ->addColumn('price', 'float', ['null' => true])
            ->addColumn('status', 'integer', ['null' => true])
            ->addColumn('image_group_id', 'integer', ['null' => true])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();
    }
}
