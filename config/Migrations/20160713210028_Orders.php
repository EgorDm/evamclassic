<?php
use Migrations\AbstractMigration;

class Orders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('orders')
            ->addColumn('exact_guid', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('customer_id', 'integer', ['null' => false])
            ->addColumn('billing_id', 'integer', ['null' => true])
            ->addColumn('shipping_id', 'integer', ['null' => true])
            ->addColumn('local_status', 'integer', ['default' => 0])
            ->addColumn('status', 'integer', ['default' => 0])
            ->addColumn('shipment_status', 'integer', ['default' => 0])
            ->addColumn('payment_method', 'integer', ['null' => true])
            ->addColumn('payment_id', 'string', ['null' => true])
            ->addColumn('shipping_method_id', 'integer', ['null' => true])
            ->addColumn('shipping_option_id', 'integer', ['null' => true])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->save();

        $this->table('order_item_associations')
            ->addColumn('order_id', 'integer', ['null' => true])
            ->addColumn('item_id', 'integer', ['null' => true])
            ->addColumn('quantity', 'integer', ['null' => true])
            ->save();
    }
}
