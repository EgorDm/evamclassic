<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 6/6/2016
 * Time: 3:31 PM
 */

return [
    'ExactCredentials' => [
        'CLIENT_ID' => env('EXACT_CLIENT_ID'),
        'CLIENT_SECRET' => env('EXACT_CLIENT_SECRET'),
        'REDIRECT_BASE' => env('EXACT_REDIRECT_BASE'),
        'DIVISION' => env('EXACT_DIVISION')
    ]
];
