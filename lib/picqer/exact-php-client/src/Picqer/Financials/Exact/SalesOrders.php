<?php
/**
 * Created by PhpStorm.
 * User: EgorDm
 * Date: 5/27/2016
 * Time: 8:11 PM
 */

namespace Picqer\Financials\Exact;


class SalesOrders extends Model
{
    use Query\Findable;
    use Persistance\Storable;

    protected $primaryKey = 'OrderID';

    protected $saleOrderLines = [ ];

    protected $fillable = [
        'OrderID',
        'Currency',
        'DeliverTo',
        'DeliverToContactPerson',
        'DeliveryAddress',
        'DeliveryDate',
        'DeliveryStatus',
        'Description',
        'Document',
        'InvoiceStatus',
        'InvoiceTo',
        'InvoiceToContactPerson',
        'OrderDate',
        'OrderedBy',
        'OrderedByContactPerson',
        'OrderNumber',
        'PaymentCondition',
        'PaymentReference',
        'Remarks',
        'SalesOrderLines',
        'Salesperson',
        'ShippingMethod',
        'Status',
        'TaxSchedule',
        'WarehouseID',
        'YourRef',
        'AmountDC'
    ];

    public function addItem(array $array)
    {
        if ( ! isset( $this->attributes['SalesOrderLines'] ) || $this->attributes['SalesOrderLines'] == null) {
            $this->attributes['SalesOrderLines'] = [ ];
        }
        if ( ! isset( $array['LineNumber'] )) {
            $array['LineNumber'] = count($this->attributes['SalesOrderLines']) + 1;
        }
        $this->attributes['SalesOrderLines'][] = $array;
    }


    protected $url = 'salesorder/SalesOrders';

}